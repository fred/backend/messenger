#!/usr/bin/env python3
"""Send message to messenger.

This script is intended for testing purposes only.

Usage: send-message.py [options] [--archive | --no-archive] email <recipient> <subject> <body> <context>
       send-message.py [options] [--archive | --no-archive] sms <recipient> <body> <context>
       send-message.py [options] [--archive | --no-archive] letter <file>
       send-message.py -h | --help

Arguments:
  <recipient>           An e-mail or SMS recipient.
  <subject>             A subject template name.
  <body>                A body template name.
  <file>                A file to be send.
  <context>             Message context as JSON.

Options:
  -h, --help            Show this help message and exit.
  --server=NETLOC       Set host and port of the messenger server [default: localhost:50052].
  --archive             Mark message to archive.
  --no-archive          Mark message not to archive.
  --type=TYPE           Set message type.
  --subject-uuid=UUID   Set subject template UUID.
  --body-uuid=UUID      Set body template UUID.
  --body-html=NAME      Set body HTML template name.
  --body-html-uuid=UUID Set body HTML template UUID.
  --sender=SENDER       Set message sender.
  --headers=HEADERS     Set message headers as JSON.
  --attachments=UUIDS   Set message attachments as JSON.
  --recipient=ADDR      Set letter recipient address as JSON.
"""
import json
from typing import Dict, Type, Union

import grpc
from docopt import docopt
from fred_api.messenger.service_email_grpc_pb2 import SendEmailRequest
from fred_api.messenger.service_email_grpc_pb2_grpc import EmailMessengerStub
from fred_api.messenger.service_letter_grpc_pb2 import SendLetterRequest
from fred_api.messenger.service_letter_grpc_pb2_grpc import LetterMessengerStub
from fred_api.messenger.service_sms_grpc_pb2 import SendSmsRequest
from fred_api.messenger.service_sms_grpc_pb2_grpc import SmsMessengerStub


def get_stub(netloc: str, stub_cls: Type):
    """Return a stub"""
    channel = grpc.insecure_channel(netloc)
    return stub_cls(channel)


def _set_type(request: Union[SendEmailRequest, SendSmsRequest, SendLetterRequest], options: Dict):
    """Handle type flag."""
    if options['--type']:
        request.message_data.type = options['--type']


def _set_archive(request: Union[SendEmailRequest, SendSmsRequest, SendLetterRequest], options: Dict):
    """Handle archive flag."""
    if options['--archive']:
        assert not options['--no-archive']
        request.archive = True
    elif options['--no-archive']:
        assert not options['--archive']
        request.archive = False


def send_email(options: Dict) -> None:
    """Send email."""
    context = json.loads(options['<context>'])
    if options['--headers']:
        headers = json.loads(options['--headers'])
    else:
        headers = None
    if options['--attachments']:
        attachments = json.loads(options['--attachments'])
    else:
        attachments = None

    stub = get_stub(options['--server'], EmailMessengerStub)
    request = SendEmailRequest()
    if options['--sender']:
        request.message_data.sender = options['--sender']
    request.message_data.recipient = options['<recipient>']
    if headers:
        request.message_data.extra_headers.update(headers)
    request.message_data.subject_template = options['<subject>']
    if options['--subject-uuid']:
        request.message_data.subject_template_uuid.value = options['--subject-uuid']
    request.message_data.body_template = options['<body>']
    if options['--body-uuid']:
        request.message_data.body_template_uuid.value = options['--body-uuid']
    if options['--body-html']:
        request.message_data.body_template_html = options['--body-html']
    if options['--body-html-uuid']:
        request.message_data.body_template_html_uuid.value = options['--body-html-uuid']
    request.message_data.context.update(context)
    for attachment in attachments or ():
        request.message_data.attachments.add(value=attachment)

    _set_type(request, options)
    _set_archive(request, options)
    reply = stub.send(request)
    print("New email: {}".format(reply.data.uid.value))


def send_sms(options: Dict) -> None:
    """Send SMS."""
    context = json.loads(options['<context>'])
    stub = get_stub(options['--server'], SmsMessengerStub)
    request = SendSmsRequest()
    request.message_data.recipient = options['<recipient>']
    request.message_data.body_template = options['<body>']
    if options['--body-uuid']:
        request.message_data.body_template_uuid.value = options['--body-uuid']
    request.message_data.context.update(context)
    _set_type(request, options)
    _set_archive(request, options)
    reply = stub.send(request)
    print("New SMS: {}".format(reply.data.uid.value))


def send_letter(options: Dict) -> None:
    """Send letter."""
    if options['--recipient']:
        address = json.loads(options['--recipient'])
    else:
        address = None

    stub = get_stub(options['--server'], LetterMessengerStub)
    request = SendLetterRequest()
    request.message_data.file.value = options['<file>']
    if address:
        for key, value in address.items():
            setattr(request.message_data.recipient, key, value)
    _set_type(request, options)
    _set_archive(request, options)
    reply = stub.send(request)
    print("New letter: {}".format(reply.data.uid.value))


def main() -> None:
    """Send message to messenger."""
    options = docopt(__doc__)

    if options['email']:
        send_email(options)
    elif options['sms']:
        send_sms(options)
    else:
        send_letter(options)


if __name__ == '__main__':
    main()
