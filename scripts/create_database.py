"""Simple script to create database for tests."""
from messenger.database import get_engine
from messenger.tests.utils import create_database, database_exists


def main() -> None:
    engine_url = get_engine().url
    if not database_exists(engine_url):
        create_database(engine_url)


if __name__ == '__main__':
    main()
