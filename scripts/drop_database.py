"""Simple script to drop database for tests."""
from messenger.database import get_engine
from messenger.tests.utils import database_exists, drop_database


def main() -> None:
    engine_url = get_engine().url
    if database_exists(engine_url):
        drop_database(engine_url)


if __name__ == '__main__':
    main()
