#!/usr/bin/env python3
"""Resend a message from messenger.

This script is intended for testing purposes only.

Usage: resend-message.py [options] email <UID>
       resend-message.py [options] sms <UID>
       resend-message.py [options] letter <UID>
       resend-message.py -h | --help

Options:
  -h, --help            Show this help message and exit.
  --server=NETLOC       Set host and port of the messenger server [default: localhost:50052].
"""
from typing import Dict, Type, Union, Any

import grpc
from docopt import docopt
from fred_api.messenger.service_email_grpc_pb2 import GetEmailRequest, SendEmailRequest
from fred_api.messenger.service_email_grpc_pb2_grpc import EmailMessengerStub
from fred_api.messenger.service_letter_grpc_pb2 import GetLetterRequest, SendLetterRequest
from fred_api.messenger.service_letter_grpc_pb2_grpc import LetterMessengerStub
from fred_api.messenger.service_sms_grpc_pb2 import GetSmsRequest, SendSmsRequest
from fred_api.messenger.service_sms_grpc_pb2_grpc import SmsMessengerStub


def get_stub(netloc: str, stub_cls: Type) -> Any:
    """Return a stub."""
    channel = grpc.insecure_channel(netloc)
    return stub_cls(channel)


def resend_message(uid: str,
                   stub: Union[EmailMessengerStub, LetterMessengerStub, SmsMessengerStub],
                   get_cls: Type[Union[GetEmailRequest, GetLetterRequest, GetSmsRequest]],
                   send_cls: Type[Union[SendEmailRequest, SendLetterRequest, SendSmsRequest]]) -> None:
    """Resend message."""
    get_msg = get_cls()
    get_msg.uid.value = uid

    get_reply = stub.get(get_msg)

    send_msg = send_cls()
    send_msg.message_data.CopyFrom(get_reply.data.envelope.message_data)
    send_msg.references.MergeFrom(get_reply.data.envelope.references)
    send_msg.archive = get_reply.data.envelope.archive

    send_reply = stub.send(send_msg)

    print("New message: {}".format(send_reply.data.uid.value))


def resend_email(options: Dict) -> None:
    """Resend email."""
    stub = get_stub(options['--server'], EmailMessengerStub)
    resend_message(options['<UID>'], stub, GetEmailRequest, SendEmailRequest)


def resend_sms(options: Dict) -> None:
    """Resend SMS."""
    stub = get_stub(options['--server'], SmsMessengerStub)
    resend_message(options['<UID>'], stub, GetSmsRequest, SendSmsRequest)


def resend_letter(options: Dict) -> None:
    """Resend letter."""
    stub = get_stub(options['--server'], LetterMessengerStub)
    resend_message(options['<UID>'], stub, GetLetterRequest, SendLetterRequest)


def main() -> None:
    """Send message to messenger."""
    options = docopt(__doc__)

    if options['email']:
        resend_email(options)
    elif options['sms']:
        resend_sms(options)
    else:
        resend_letter(options)


if __name__ == '__main__':
    main()
