#!/usr/bin/env python3
"""Migrate reference data.

This is a data migration after 'd7c84f4dbfb7'.
"""
from datetime import datetime, timedelta
from typing import Iterable, Tuple, Type

import click
from dateutil.rrule import MONTHLY, rrule
from pytz import UTC
from sqlalchemy import null, update

from messenger.commands.utils import messenger_command
from messenger.database import atomic
from messenger.models import (ArchiveEmailReference, ArchiveLetterReference, ArchiveSmsReference, EmailReference,
                              LetterReference, SmsReference)
from messenger.models.message import ReferenceMixin, ArchiveReferenceMixin

try:
    from itertools import pairwise
except ImportError:
    # Fallback to python 3.9 and lower. Copied recipe from itertools docs.
    from itertools import tee

    def pairwise(iterable):
        """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
        a, b = tee(iterable)
        next(b, None)
        return zip(a, b)


def get_month_start(value: datetime) -> datetime:
    """Return datetime at the month start."""
    return datetime(value.year, value.month, day=1, hour=0, tzinfo=value.tzinfo)


def get_intervals(start_date: datetime) -> Iterable[Tuple[datetime, datetime]]:
    """Generate pairs of intervals to migrate."""
    daterule = rrule(MONTHLY, dtstart=get_month_start(start_date),
                     until=get_month_start(datetime.now(UTC) + timedelta(days=30)))

    yield from pairwise(daterule)


def migrate_references(model_cls: Type[ReferenceMixin]) -> None:
    """Migrate non-archive references."""
    with atomic() as session:
        query = update(model_cls, model_cls._new_type == null()).values(_new_type=model_cls._old_type)
        session.execute(query)


def migrate_archive_references(model_cls: Type[ArchiveReferenceMixin]) -> None:
    """Migrate archive references."""
    with atomic() as session:
        oldest = session.query(model_cls).order_by(model_cls.create_datetime).first()
        if not oldest:
            return
        oldest_datetime = oldest.create_datetime

    for start, end in get_intervals(oldest_datetime):
        click.echo("Migrating {} from {} to {}".format(model_cls, start, end))
        query = update(model_cls).where(
            model_cls._new_type == null(),
            model_cls.create_datetime >= start,
            model_cls.create_datetime < end)
        query = query.values(_new_type=model_cls._old_type)
        with atomic() as session:
            session.execute(query)


@messenger_command()
def main() -> None:
    """Migrate reference data."""
    for model_cls in (EmailReference, SmsReference, LetterReference):
        click.echo("Migrating {}".format(model_cls.__name__))
        migrate_references(model_cls)
        click.echo("Migration of {} completed.".format(model_cls.__name__))

    for model_cls in (ArchiveEmailReference, ArchiveSmsReference, ArchiveLetterReference):
        click.echo("Migrating {}".format(model_cls.__name__))
        migrate_archive_references(model_cls)
        click.echo("Migration of {} completed.".format(model_cls.__name__))

    with atomic() as session:
        for model_cls in (EmailReference, SmsReference, LetterReference, ArchiveEmailReference, ArchiveSmsReference,
                          ArchiveLetterReference):
            count = session.query(model_cls).filter(model_cls._new_type == null(),
                                                    model_cls._old_type != null()).count()
            click.echo("{} unmigrated {}".format(count, model_cls.__name__))


if __name__ == '__main__':
    main()
