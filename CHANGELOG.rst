ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

0.5.5 (2023-11-16)
------------------

* Add support for python 3.11 and 3.12.
* Drop support for python 3.7.
* Use ``imap_tools`` 1.X (#89).
* Use ``fred-filed`` 0.3.
* Fix typing.
* Fix ``StrEnum`` and use backport.

0.5.4 (2023-02-08)
------------------

* Fix emails with multiple recipients (#87).
* Fix signatures on multipart alternative emails (#88).
* Fix bugs in ``send-message`` script.

0.5.3 (2023-01-26)
------------------

* Prevent sending one message multiple times (#84).
* Fix email ``To`` header (#85).
* Fix typing and flake8.

0.5.2 (2022-07-25)
------------------

* Always convert DSN values to string (#79).

0.5.1 (2022-06-28)
------------------

* Optimize email search in delivery (#76).

0.5.0 (2022-06-20)
------------------

* Replace EasySettings with pydantic (#33).
* Use API 1.3 (#70).
* Add a ``folder`` option to email delivery (#57).
* Fix delivery for rfc822 MIME type (#60).
* Skip autosubmitted emails in delivery processing (#62).
* Add an option to skip delivered emails (#63).
* Improve delivery processing (#42, #75).
* Switch reference types (#69, #72).
* Add ``Result`` utility (#58).
* Add an option to disable certificate verification (#59).
* Add secretary token (#64).
* Add sender specific processors (#66).
* Add email headers processor (#65).
* Add options for server limits (#48).
* Fix SQLAlchemy 2.0 deprecation warnings (#68).
* Mark ScalarListType cacheable (#71).
* Resolve frgal warnings (#74).
* Flush objects at the end of session in tests (#30).
* Update project setup.

0.4.0 (2022-04-06)
------------------

* Switch to API 1.2 (#50).
* Add ``multi_send`` to email servicer (#51).
* Add batch send to email servicer (#53).
* Add a resend script.

0.3.1 (2022-03-28)
------------------

* Set ``MIME-Version`` header on S/MIME emails (#49).

0.3.0 (2022-03-03)
------------------

* Use new UID format (#43).
* Refactor S/MIME signatures to use cryptography (#31).
* Shorten a length of an ``Email`` string representation (#14).
* Log summaries of operations (#10).
* Add script to send a message.
* Add test utils for commands (#24).
* Fix manual page for archive (#28).
* Increase coverage.
* Make mypy more strict and fix annotations.
* Update project setup.
* Add issue template.

0.2.2 (2022-02-23)
------------------

* Send messages using round-robin by type (#32).
* Expect non-multipart email in delivery (#36).
* Expect headers of undelivered message (#37).
* Handle exceptions in delivery processing (#38).

0.2.1 (2022-01-20)
------------------

* Delete references on clean (#29).

0.2.0 (2022-01-12)
------------------

* Add support for python 3.10.
* Drop support for python 3.6.
* Drop support for SQLAlchemy 1.3 (#7).
  * Replace deprecated ``has_table`` method.
* Fix comments on address country (#2).
* Drop ``SmsMessage.msisdn`` (#5).
* Fix section of man pages (#12).
* Add gRPC reflection (#1).
* Enable multiple ``--type`` options (#19).
* Support undelivered messages in backends (#21).
* Fail on defect in recipient's email address (#23).
* Add ``--from`` and ``--to`` to ``archive`` command (#22).
* Add ``--format`` to ``monitor`` command (#25).
* Rename ``load_instance`` args (#26).
* Rename type to method in code (#27).
* Update static checks.

0.1.0 (2021-12-13)
------------------

Initial version.

* Support emails, SMS and letters.
* Include message archive.
* Include migration of FRED message archives.
* Support blocked addresses.
* Provides email senders and delivery checks.
* SMS and letter senders are supported, but no particular backends are implemented.
