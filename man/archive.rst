fred-messenger-archive
======================

Archives sent e-mails intended for archivation. Only
e-mails in ``sent``, ``failed``, ``delivered``,
and ``undelivered`` and ``cancelled`` states are archived.

Synopsis
--------

``fred-messenger-archive [OPTIONS]``

Options
-------

.. include:: limits-options.rst

.. option:: --method [all|email|sms|letter]

   Run command only on messages with selected transport method.
   May be set multiple times.

   Default: all

.. option:: --type [all|email|sms|letter]

   Alias for --method. Deprecated.

.. include:: shared-options.rst

Example
-------

``fred-messenger-archive``

See also
--------

| :doc:`fred-messenger-block </block>`
| :doc:`fred-messenger-clean </clean>`
| :doc:`fred-messenger-delivery </delivery>`
| :doc:`fred-messenger-migrate-archive </migrate_archive>`
| :doc:`fred-messenger-monitor </monitor>`
| :doc:`fred-messenger-partition </partition>`
| :doc:`fred-messenger-sender </sender>`
| :doc:`fred-messenger-server </server>`
