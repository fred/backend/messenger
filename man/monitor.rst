fred-messenger-monitor
======================

Lists e-mail queue state.

Synopsis
--------

``fred-messenger-monitor [OPTIONS]``

Options
-------

.. option:: --period INTEGER

   Set length of a monitored period in seconds.

   Default: 3600

.. option:: --format [json|text|yaml]

   Set output format.

   Default: text

.. option:: --method [all|email|sms|letter]

   Run command only on messages with selected transport method.
   May be set multiple times.

   Default: all

.. option:: --type [all|email|sms|letter]

   Alias for --method. Deprecated.

.. include:: shared-options.rst

Example
-------

``fred-messenger-monitor``

Output
^^^^^^

.. parsed-literal::

   Statistics per last 3600 seconds:
   Created: 2
   Sent: 2
   Failed: 1
   Errors: 0
   Pending total: 14

See also
--------

| :doc:`fred-messenger-archive </archive>`
| :doc:`fred-messenger-block </block>`
| :doc:`fred-messenger-clean </clean>`
| :doc:`fred-messenger-delivery </delivery>`
| :doc:`fred-messenger-migrate-archive </migrate_archive>`
| :doc:`fred-messenger-partition </partition>`
| :doc:`fred-messenger-sender </sender>`
| :doc:`fred-messenger-server </server>`
