.. option:: --from [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S]

   Set minimal date and time limit for messages in UTC timezone.

.. option:: --to [%Y-%m-%d|%Y-%m-%dT%H:%M:%S|%Y-%m-%d %H:%M:%S]

   Set maximal date and time limit for messages in UTC timezone.
