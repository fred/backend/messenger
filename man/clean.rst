fred-messenger-clean
====================

Deletes e-mails that are not intended for archivation. All
e-mails without the archivation tag and all e-mails in
``error`` state older than defined number of days are deleted.

Synopsis
--------

``fred-messenger-clean [OPTIONS]``

Options
-------

.. option:: --days INTEGER

   Set custom number of days before message deletion.

.. option:: --method [all|email|sms|letter]

   Run command only on messages with selected transport method.
   May be set multiple times.

   Default: all

.. option:: --type [all|email|sms|letter]

   Alias for --method. Deprecated.

.. include:: shared-options.rst

Example
-------

``fred-messenger-clean``

See also
--------

| :doc:`fred-messenger-archive </archive>`
| :doc:`fred-messenger-block </block>`
| :doc:`fred-messenger-delivery </delivery>`
| :doc:`fred-messenger-migrate-archive </migrate_archive>`
| :doc:`fred-messenger-monitor </monitor>`
| :doc:`fred-messenger-partition </partition>`
| :doc:`fred-messenger-sender </sender>`
| :doc:`fred-messenger-server </server>`
