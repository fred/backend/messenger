.. fred-messenger documentation master file, created by
   sphinx-quickstart on Mon Jul 22 20:45:40 2019.

Messenger Manual
================

.. toctree::
   :maxdepth: 2
   :caption: Commands:

   archive
   block
   clean
   delivery
   migrate_archive
   monitor
   partition
   sender
   server

..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
