fred-messenger-sender
=====================

Sends e-mails in queue. E-mails in ``pending`` state
are sent. It is also possible to force e-mails in
``failed`` or ``error`` state to be sent again.

Synopsis
--------

``fred-messenger-sender [OPTIONS]``

Options
-------

.. option:: --retry-failed

   Retry to send the messages with failed status.

.. option:: --retry-errors

   Retry to send the messages with error status.

.. option:: --loop

   Run in infinite loop.

.. option:: --loop-delay INTEGER

   Set delay in seconds between batches in loop mode.

   Default: 300

.. option:: --sender TEXT

   Set custom sender to be used.

   Default: default

.. option:: --method [all|email|sms|letter]

   Run command only on messages with selected transport method.
   May be set multiple times.

   Default: all

.. option:: --type [all|email|sms|letter]

   Alias for --method. Deprecated.

.. option:: --fileman-netloc TEXT

   Set custom host and port of a fileman service.

.. include:: shared-options.rst


Example
-------

``fred-messenger-sender``

See also
--------

| :doc:`fred-messenger-archive </archive>`
| :doc:`fred-messenger-block </block>`
| :doc:`fred-messenger-clean </clean>`
| :doc:`fred-messenger-delivery </delivery>`
| :doc:`fred-messenger-migrate-archive </migrate_archive>`
| :doc:`fred-messenger-monitor </monitor>`
| :doc:`fred-messenger-partition </partition>`
| :doc:`fred-messenger-server </server>`
