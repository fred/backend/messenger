fred-messenger-delivery
=======================

Checks messages delivery.

Synopsis
--------

``fred-messenger-delivery [OPTIONS]``

Options
-------

.. option:: --limit INTEGER

   Set limit for delivery checker.

.. option:: --delivery-backend TEXT

   Set custom backend.

.. option:: --delivery-param TEXT

   Set custom parameters for the backend,
   can be used multiple times.

.. include:: shared-options.rst

Example
-------

``fred-messenger-delivery``

See also
--------

| :doc:`fred-messenger-archive </archive>`
| :doc:`fred-messenger-block </block>`
| :doc:`fred-messenger-clean </clean>`
| :doc:`fred-messenger-migrate-archive </migrate_archive>`
| :doc:`fred-messenger-monitor </monitor>`
| :doc:`fred-messenger-partition </partition>`
| :doc:`fred-messenger-sender </sender>`
| :doc:`fred-messenger-server </server>`
