fred-messenger-block
====================

Manages blocked addresses. There are subcommands
``add``, ``list`` and ``remove`` that work with
blocked items ``email``, ``phone`` and ``address``.

Synopsis
--------

``fred-messenger-block [OPTIONS]``

Options
-------

.. include:: shared-options.rst

Example
-------

``fred-admin --contact_reminder --date 2019-07-22``

See also
--------

| :doc:`fred-messenger-archive </archive>`
| :doc:`fred-messenger-clean </clean>`
| :doc:`fred-messenger-delivery </delivery>`
| :doc:`fred-messenger-migrate-archive </migrate_archive>`
| :doc:`fred-messenger-monitor </monitor>`
| :doc:`fred-messenger-partition </partition>`
| :doc:`fred-messenger-sender </sender>`
| :doc:`fred-messenger-server </server>`
