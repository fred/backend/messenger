fred-messenger-migrate-archive
==============================

Migrates e-mails, SMS or leters from ``mail_archive/message_archive`` in Fred DB.

Synopsis
--------

``fred-messenger-migrate-archive [OPTIONS]``

Options
-------

.. include:: limits-options.rst

.. option:: --archive-only

   Migrate only archived messages. This is the default behaviour.

.. option:: --all-messages

   Migrate also messages pending to be sent.
   Such messages should be processed only by messenger, as they will not be updated by subsequent migration runs.

.. option:: --method [all|email|sms|letter]

   Run command only on messages with selected transport method.
   May be set multiple times.

   Default: all

.. option:: --type [all|email|sms|letter]

   Alias for --method. Deprecated.

.. include:: shared-options.rst

Example
-------

``fred-messenger-migrate-archive --from='1970-01-01 00:00:00' --to '1971-01-01 00:00:00'``

See also
--------

| :doc:`fred-messenger-archive </archive>`
| :doc:`fred-messenger-block </block>`
| :doc:`fred-messenger-clean </clean>`
| :doc:`fred-messenger-delivery </delivery>`
| :doc:`fred-messenger-monitor </monitor>`
| :doc:`fred-messenger-partition </partition>`
| :doc:`fred-messenger-sender </sender>`
| :doc:`fred-messenger-server </server>`
