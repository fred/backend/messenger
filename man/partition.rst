fred-messenger-partition
========================

Creates or deletes a partition in archive.

Synopsis
--------

``fred-messenger-partition [OPTIONS] (create|drop) (DEFAULT|<year> <month>)``

Options
-------

.. option:: -n, --dry-run

   Just output SQL.

.. include:: shared-options.rst

Example
-------

``fred-messenger-partition create 2020 01``

See also
--------

| :doc:`fred-messenger-archive </archive>`
| :doc:`fred-messenger-block </block>`
| :doc:`fred-messenger-clean </clean>`
| :doc:`fred-messenger-delivery </delivery>`
| :doc:`fred-messenger-migrate-archive </migrate_archive>`
| :doc:`fred-messenger-monitor </monitor>`
| :doc:`fred-messenger-sender </sender>`
| :doc:`fred-messenger-server </server>`
