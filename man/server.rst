fred-messenger-server
=====================

Runs ``EmailMessenger``, ``SmsMessenger`` or ``LetterMessenger`` gRPC services.

Synopsis
--------

``fred-messenger-server [OPTIONS]``

Options
-------

.. option:: --method [all|email|sms|letter]

   Run command only on messages with selected transport method.
   May be set multiple times.

   Default: all

.. option:: --type [all|email|sms|letter]

   Alias for --method. Deprecated.

.. option:: -p, --port INTEGER

   Set custom port.

.. option:: --grpc-options TEXT

   Set custom options to the gRPC server,
   can be used multiple times.

.. include:: shared-options.rst

Example
-------

``fred-messenger-server``

See also
--------

| :doc:`fred-messenger-archive </archive>`
| :doc:`fred-messenger-block <block>`
| :doc:`fred-messenger-clean </clean>`
| :doc:`fred-messenger-delivery </delivery>`
| :doc:`fred-messenger-migrate-archive </migrate_archive>`
| :doc:`fred-messenger-monitor </monitor>`
| :doc:`fred-messenger-partition </partition>`
| :doc:`fred-messenger-sender </sender>`

