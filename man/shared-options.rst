.. option:: --connection TEXT

   Set custom database connection string.

.. option:: --config FILE

   Set custom config file.

.. option:: --help

   Show this message and exit.

.. option:: --verbosity LEVEL

   Set the verbosity level (0–3, default value is 1)

.. option:: --version

   Show the version and exit.
