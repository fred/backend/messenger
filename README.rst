==============
Fred-messenger
==============

Fred-messenger is a service for sending and archiving various types of messages.

Learn more about the project and our community on the `FRED's home page`__

.. _FRED: https://fred.nic.cz

__ FRED_

Configuration
=============

Messenger searches for configuration files in following order:

1. Configuration file set by ``MESSENGER_CONFIG`` environment variable.
2. Configuration file set by ``--config`` option of a command.
3. ``~/.fred/messenger.conf``
4. ``/etc/fred/messenger.conf``

The configuration file is in YAML format with following options:

``clean_days``
--------------
Lifetime of an message that is not to be archived in days.
Applies to messages with ``archive`` flag disabled or error messages.
Default is 30 days.

``db_connection``
-----------------
A connection string to database.
See https://docs.sqlalchemy.org/core/engines.html#database-urls for possible values.
Default is ``sqlite:///:memory:``.

``db_echo``
-----------
Whether to log SQL statements.
Default is ``False``.

``delivery_backend``
--------------------
Dotted path to a backend used for processing e-mail message delivery.
Default is ``messenger.delivery.email.ImapDelivery``.

``delivery_options``
--------------------
Dictionary with options for the delivery backend.
Default is empty.

``fileman_netloc``
------------------
Network location, i.e. host and port, of a fileman service.
Default is ``None``, i.e. fileman disabled.
Emails with attachments will not be send.

``fred_db_connection``
----------------------
A connection string to FRED database for mail archive migration.
See https://docs.sqlalchemy.org/core/engines.html#database-urls for possible values.
Default is ``None``, which causes error when running migration.

``logging``
-----------
Logging configuration, see https://docs.python.org/3.8/library/logging.config.html for possible values.
Default is ``None``, i.e. use ``logging`` defaults.

``grpc_max_workers``
--------------------
The maximum number of workers for gRPC server, see https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ThreadPoolExecutor.
Default is ``None``, i.e. use Python library default.

``grpc_maximum_concurrent_rpcs``
--------------------------------
The maximum number of concurrent RPCs this server will service, see http://grpc.github.io/grpc/python/grpc.html#grpc.server.
Default is ``None``, i.e. no limit.

``grpc_options``
----------------
Custom options for a gRPC server.
Valid value is a list of key-value pairs.
Default is ``None``, i.e. use gRPC defaults.

``grpc_port``
-------------
Port to which the messenger gRPC service will bind and listen.
Default is 50051.

``monitoring_email``
--------------------
An email address which is excluded from migrations of emails in ``migrate_archive`` command.
Default is ``None``, i.e. migrate all emails.

``msgid_domain``
----------------
Domain portion of generated Message-ID.
Default is ``None``, i.e. the local hostname.

``processors``
--------------
Default is empty.

Definition of message processors in a complex structure.
The setting maps transport method to a list of processor settings.
Each processor setting has to define ``class`` with dotted path to a processor class used and optionally keyword arguments for the processor.
Example::

    processors:
        email:
            - class: messenger.processors.VcardEmailProcessor
              data: "VCARD_DATA"

By default, no processors are defined.

``senders``
-----------
Definition of message senders in a complex structure.
The highest level maps transport method to senders.
Medium level maps sender alias to its settings.
Inner level contains a backend settings:

* ``backend`` contains dotted path to a backend. This setting is required.
* ``filters`` contain a filter settings, see below.
* ``options`` contains mapping of backend specific options.

Example::

    senders:
        smtp:
            backend: messenger.backends.email.SmtpBackend
            options:
                host: red-dwarf.example.org
                default_from: holly@example.org

By default, no senders are defined.

``filters``
^^^^^^^^^^^
A setting of filters as a list of filter settings.
Each filter setting has to define ``class`` with dotted path to a filter class used and optionally keyword arguments for the filter.
Example::

    filters:
        - class: red.dwarf.IgnoreFilter
          ignore: Rimmer

By default, no filters are defined.

``sentry``
----------
Sentry settings in a nested structure.
May contain following keys:

 * ``dsn`` contains data source name (DSN), see https://docs.sentry.io/product/sentry-basics/dsn-explainer/.
   If not provided, Sentry client is not set up.
 * ``environment`` may contain an environment identifier.
 * ``ca_certs`` may contain a path to CA certificates file.

``test_db_connection``
----------------------
A connection string to database to be used in tests.
See https://docs.sqlalchemy.org/core/engines.html#database-urls for possible values.
Default is ``sqlite:///:memory:``.
