"""Messenger gRPC services."""
import logging
from abc import ABC, abstractmethod
from dataclasses import dataclass
from datetime import date, datetime
from email.headerregistry import AddressHeader, BaseHeader, HeaderRegistry
from typing import Any, ClassVar, Dict, Generator, Generic, Iterable, Iterator, Optional, Type, TypeVar, Union, cast
from uuid import UUID

from dateutil.parser import parse as _parse_datetime
from fred_api.messenger.common_types_pb2 import MessageStatus as ProtoMessageStatus, Reference, ReferenceType, Uuid
from fred_api.messenger.email_pb2 import Email as ProtoEmail, EmailEnvelope
from fred_api.messenger.letter_pb2 import Address as ProtoAddress, Letter as ProtoLetter, LetterEnvelope
from fred_api.messenger.service_email_grpc_pb2 import (
    GetEmailReply,
    ListEmailReply,
    MultiSendEmailReply,
    SendEmailReply,
    SendEmailRequest,
)
from fred_api.messenger.service_email_grpc_pb2_grpc import EmailMessengerServicer
from fred_api.messenger.service_letter_grpc_pb2 import GetLetterReply, ListLetterReply, SendLetterReply
from fred_api.messenger.service_letter_grpc_pb2_grpc import LetterMessengerServicer
from fred_api.messenger.service_sms_grpc_pb2 import GetSmsReply, ListSmsReply, SendSmsReply
from fred_api.messenger.service_sms_grpc_pb2_grpc import SmsMessengerServicer
from fred_api.messenger.sms_pb2 import Sms as ProtoSms, SmsEnvelope
from frgal import GrpcDecoder
from frgal.exceptions import ServiceError
from frgal.service import wrap_stream, wrap_unary
from google.protobuf.message import DecodeError, Message
from grpc import StatusCode
from grpc._server import _Context
from pytz import UTC
from sqlalchemy import Column, Date, and_, cast as sql_cast, func, or_, select, union
from sqlalchemy.engine import Engine
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Query
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import expression, literal
from sqlalchemy_utils import get_type

from .constants import MessageStatus
from .database import atomic
from .models import AnyRecord, ArchiveEmail, ArchiveLetter, ArchiveRecord, ArchiveSms, Email, Letter, Record, Sms
from .models.message import TemplateArchiveMessageMixin, TemplateMessageMixin

_LOGGER = logging.getLogger(__name__)


@dataclass(frozen=True)
class Uid:
    """Represents a message UID.

    Attributes:
        uuid: A message UUID.
        timestamp: A date or a datetime of the message creation.

    Class attributes:
        SEPARATOR: A separator used in UID format.
        DEPRECATED_SEPARATOR: A separator used in deprecated UID format.
    """

    SEPARATOR: ClassVar[str] = '.'
    DEPRECATED_SEPARATOR: ClassVar[str] = '#'

    uuid: UUID
    timestamp: Optional[Union[date, datetime]] = None

    @property
    def create_date(self) -> Optional[date]:
        """Return an effective create date."""
        if self.timestamp is None:
            return None

        if isinstance(self.timestamp, datetime):
            if self.timestamp.tzinfo is None:
                # Expect naive datetimes to be in UTC.
                return self.timestamp.date()
            else:
                return self.timestamp.astimezone(UTC).date()
        else:
            return self.timestamp

    def __str__(self) -> str:
        create_date = self.create_date
        if create_date is None:
            return str(self.uuid)
        return str(self.uuid) + self.SEPARATOR + str(create_date)

    @classmethod
    def parse(cls, value: str) -> 'Uid':
        """Parse UID from string.

        Raises:
            ValueError: If the string has invalid format.
        """
        if cls.DEPRECATED_SEPARATOR in value:
            # It's a deprecated style UID.
            create_datetime, _, uuid = value.partition(cls.DEPRECATED_SEPARATOR)
            return cls(UUID(uuid), _parse_datetime(create_datetime))
        uuid, _, create_date = value.partition(cls.SEPARATOR)
        if create_date:
            return cls(UUID(uuid), date.fromisoformat(create_date))
        else:
            return cls(UUID(uuid))

    @classmethod
    def from_record(cls, record: AnyRecord) -> 'Uid':
        """Create UID from a record."""
        return cls(record.uuid, record.create_datetime)


class MessengerDecoder(GrpcDecoder):
    """Decoder for messenger services."""

    decode_unset_messages = False
    decode_empty_string_none = False

    def __init__(self) -> None:
        super().__init__()
        self.set_decoder(Uuid, self._decode_uuid)

    def _decode_uuid(self, value: Uuid) -> Optional[UUID]:
        """Decode fred_api.messenger.email_pb2.Uuid to UUID."""
        if not value.value:
            return None
        return UUID(value.value)


MessageType = TypeVar('MessageType', bound=Record)
ArchiveMessageType = TypeVar('ArchiveMessageType', bound=ArchiveRecord)
Q = TypeVar('Q', bound=Query)


class MessageServicerMixin(Generic[MessageType, ArchiveMessageType], ABC):
    """Mixin for gRPC message services.

    Attributes:
        message_cls: A model of a message.
        archive_message_cls: A model of an archived message.
        message_proto_cls: An interface message type.
        envelope_proto_cls: An interface envelope type.
        get_reply_proto_cls: An interface reply type for the get method.
        list_reply_proto_cls: An interface reply type for the list method.
    """

    message_cls = None  # type: Type[MessageType]
    archive_message_cls = None  # type: Type[ArchiveMessageType]
    message_proto_cls = None  # type: Type[Message]
    envelope_proto_cls = None  # type: Type[Message]
    get_reply_proto_cls = None  # type: Type[Message]
    list_reply_proto_cls = None  # type: Type[Message]
    send_reply_proto_cls: Type[Message]

    def __init__(self) -> None:
        self.decoder = MessengerDecoder()

    def _get_message(self, message: Union[MessageType, ArchiveMessageType]) -> Message:
        """Return interface message for message model.

        Converts only base fields. Recipient is not converted.
        """
        proto_message = self.message_proto_cls()
        if message.type is not None:
            proto_message.type = message.type  # type: ignore[attr-defined]
        return proto_message

    def _get_envelope(self, message: Union[MessageType, ArchiveMessageType]) -> Message:
        """Return message envelope for message model."""
        envelope = self.envelope_proto_cls()
        envelope.uid.value = str(Uid.from_record(message))  # type: ignore[attr-defined]
        envelope.create_datetime.FromDatetime(message.create_datetime)  # type: ignore[attr-defined]
        if message.send_datetime:
            envelope.send_datetime.FromDatetime(message.send_datetime)  # type: ignore[attr-defined]
        if message.delivery_datetime:
            envelope.delivery_datetime.FromDatetime(message.delivery_datetime)  # type: ignore[attr-defined]
        envelope.attempts = message.attempts  # type: ignore[attr-defined]
        envelope.status = ProtoMessageStatus.Value(message.status)  # type: ignore[attr-defined]
        envelope.message_data.CopyFrom(self._get_message(message))  # type: ignore[attr-defined]
        envelope.archive = message.archive  # type: ignore[attr-defined]
        for reference in message.references:
            envelope.references.add(  # type: ignore[attr-defined]
                old_type=reference.old_type or ReferenceType.other,
                type=reference.type,
                value=reference.value
            )
        return envelope

    def _get_reference_type(self, reference: Reference) -> str:
        """Extract reference type from a reference.

        Utility method for reference type transition.

        Raises:
            ServiceError: If the reference is invalid.
        """
        if reference.type:
            return cast(str, reference.type)
        if reference.old_type == ReferenceType.other:
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "Reference has no valid type defined.")
        return cast(str, ReferenceType.Name(reference.old_type))

    @wrap_unary
    def get(self, request: Message, context: _Context) -> Message:
        """Return message data."""
        try:
            uid = Uid.parse(request.uid.value)  # type: ignore[attr-defined]
        except ValueError as error:
            raise ServiceError(
                StatusCode.INVALID_ARGUMENT,
                "'{}' is not a valid UID".format(request.uid.value),  # type: ignore[attr-defined]
            ) from error

        with atomic() as session:
            query = session.query(self.message_cls).filter_by(uuid=uid.uuid)
            # SQLite can't properly compare datetimes.
            # But there is also no partitioning, so just skip the create_datetime filter in there.
            if cast(Engine, session.bind).dialect.name != 'sqlite':
                # Convert create_datetime to UTC and then use only the date itself.
                create_op = cast(Column, self.message_cls.create_datetime).op('AT TIME ZONE')(literal('UTC'))
                query = query.filter(sql_cast(create_op, Date) == uid.create_date)

            try:
                message: Union[MessageType, ArchiveMessageType] = query.one()
            except NoResultFound:
                a_query = session.query(self.archive_message_cls).filter_by(uuid=uid.uuid)
                # The same tricks as above.
                if cast(Engine, session.bind).dialect.name != 'sqlite':
                    create_op = cast(Column, self.archive_message_cls.create_datetime).op('AT TIME ZONE')(
                        literal('UTC'))
                    a_query = a_query.filter(sql_cast(create_op, Date) == uid.create_date)
                try:
                    message = a_query.one()
                except NoResultFound as error:
                    raise ServiceError(
                        StatusCode.NOT_FOUND,
                        "'{}' not found.".format(request.uid.value),  # type: ignore[attr-defined]
                    ) from error
            except SQLAlchemyError as error:
                _LOGGER.error("Error in SQL query: %s", error)
                raise ServiceError(StatusCode.FAILED_PRECONDITION, "Database error: {}".format(error)) from error

            reply = self.get_reply_proto_cls()
            reply.data.envelope.CopyFrom(self._get_envelope(message))  # type: ignore[attr-defined]
        return reply

    @abstractmethod
    def _filter_recipients(self, query: Q, model: Union[Type[MessageType], Type[ArchiveMessageType]],
                           recipients: Iterable) -> Q:
        """Filter query according to recipients."""

    def _filter_query(self, query: Q, model: Union[Type[MessageType], Type[ArchiveMessageType]],
                      request: Message) -> Q:
        """Filter query according to message parameters."""
        created_from = request.created_from  # type: ignore[attr-defined]
        if created_from.ToSeconds():
            query = query.filter(
                cast(Column, model.create_datetime) >= UTC.localize(created_from.ToDatetime()))
        created_to = request.created_to  # type: ignore[attr-defined]
        if created_to.ToSeconds():
            query = query.filter(
                cast(Column, model.create_datetime) < UTC.localize(created_to.ToDatetime()))
        recipients = request.recipients  # type: ignore[attr-defined]
        if recipients:
            query = self._filter_recipients(query, model, recipients)
        references = request.references  # type: ignore[attr-defined]
        if references:
            reference_cls = cast(Column, model.references).property.entity.class_
            ref_filter = or_(
                *[and_(*[reference_cls.type == self._get_reference_type(r), reference_cls.value == r.value])
                  for r in references])
            query = query.join(reference_cls).filter(ref_filter)
        types = request.types  # type: ignore[attr-defined]
        if types:
            query = query.filter(cast(Column, model.type).in_(types))
        return query

    @wrap_stream
    def list(self, request: Message, context: _Context) -> Generator[Message, None, None]:
        """Return messages filtered by parameters."""
        with atomic() as session:
            message_query = session.query(cast(Type[Record], self.message_cls).uuid,
                                          cast(Type[Record], self.message_cls).create_datetime,
                                          expression.false())
            archive_query = session.query(
                cast(Type[ArchiveRecord], self.archive_message_cls).uuid,
                cast(Type[ArchiveRecord], self.archive_message_cls).create_datetime,
                expression.true())
            message_query = self._filter_query(message_query, self.message_cls, request)
            archive_query = self._filter_query(archive_query, self.archive_message_cls, request)
            subquery = union(message_query, archive_query).subquery()  # type: ignore[arg-type]
            uuid_query = select(subquery).order_by(subquery.columns[1].desc())
            limit = request.limit  # type: ignore[attr-defined]
            if limit:
                uuid_query = uuid_query.limit(limit)
            try:
                for uuid, create_datetime, archive in session.execute(uuid_query):
                    model_cls = self.archive_message_cls if archive else self.message_cls
                    query = session.query(model_cls).filter_by(uuid=uuid)
                    # SQLite can't properly compare datetimes.
                    # But there is also no partitioning, so just skip the create_datetime filter in there.
                    if cast(Engine, session.bind).dialect.name != 'sqlite':
                        query = query.filter_by(create_datetime=create_datetime)

                    message = query.one()
                    reply = self.list_reply_proto_cls()
                    reply.data.envelope.CopyFrom(self._get_envelope(message))  # type: ignore[attr-defined]
                    yield reply
            except SQLAlchemyError as error:
                _LOGGER.error("Error in SQL query: %s", error)
                raise ServiceError(StatusCode.FAILED_PRECONDITION, "Database error: {}".format(error)) from error

    def _decode_send_request(self, request: Message) -> Dict[str, Any]:
        """Decode send request.

        Raises:
            ServiceError: An error in request.
        """
        if not request.message_data.recipient:  # type: ignore[attr-defined]
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "Recipient is missing.")
        try:
            return cast(Dict[str, Any], self.decoder.decode(request.message_data))  # type: ignore[attr-defined]
        except Exception as error:
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "Decoding error: {}".format(error)) from error

    @wrap_unary
    def send(self, request: Message, context: _Context) -> Message:
        """Store email in database and return its data."""
        model_kwargs = self._decode_send_request(request)
        with atomic() as session:
            if request.HasField('archive'):
                model_kwargs['archive'] = request.archive  # type: ignore[attr-defined]
            reference_cls = get_type(cast(Type[Record], self.message_cls).references)
            model_kwargs['references'] = [reference_cls(type=self._get_reference_type(r), value=r.value)
                                          for r in request.references]  # type: ignore[attr-defined]
            model = self.message_cls(status=MessageStatus.PENDING, **model_kwargs)
            session.add(model)
            try:
                session.commit()
            except SQLAlchemyError as error:
                _LOGGER.error("Error in SQL query: %s", error)
                raise ServiceError(StatusCode.FAILED_PRECONDITION, "Database error: {}".format(error)) from error

            reply = self.send_reply_proto_cls()
            reply.data.uid.value = str(Uid.from_record(model))  # type: ignore[attr-defined]
        return reply


TemplateMessageType = TypeVar('TemplateMessageType', bound=TemplateMessageMixin)
TemplateArchiveMessageType = TypeVar('TemplateArchiveMessageType', bound=TemplateArchiveMessageMixin)


class TemplateMessageServicerMixin(MessageServicerMixin, Generic[TemplateMessageType, TemplateArchiveMessageType]):
    """Mixin for gRPC template message services."""

    message_cls = None  # type: Type[TemplateMessageType]
    archive_message_cls = None  # type: Type[TemplateArchiveMessageType]

    def _get_message(self, message: Union[TemplateMessageType, TemplateArchiveMessageType]) -> Message:
        """Return interface message for template message model.

        Converts only base and template fields. Recipient is not converted.
        """
        proto_message = super()._get_message(message)
        proto_message.body_template = message.body_template  # type: ignore[attr-defined]
        proto_message.context.update(message.context or {})  # type: ignore[attr-defined]
        if message.body_template_uuid is not None:
            proto_message.body_template_uuid.value = str(message.body_template_uuid)  # type: ignore[attr-defined]
        return proto_message

    def _filter_query(self, query: Q, model: Union[Type[TemplateMessageType], Type[TemplateArchiveMessageType]],
                      request: Message) -> Q:
        """Filter query according to message parameters."""
        query = super()._filter_query(query, model, request)
        body_templates = request.body_templates  # type: ignore[attr-defined]
        if body_templates:
            query = query.filter(cast(Column, model.body_template).in_(body_templates))
        return query

    def _decode_send_request(self, request: Message) -> Dict[str, Any]:
        """Decode send request.

        Raises:
            ServiceError: An error in request.
        """
        if not request.message_data.body_template:  # type: ignore[attr-defined]
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "Body is missing.")
        return super()._decode_send_request(request)


class EmailServicer(TemplateMessageServicerMixin, EmailMessengerServicer):
    """Email gRPC service."""

    message_cls = Email
    archive_message_cls = ArchiveEmail
    message_proto_cls = ProtoEmail
    envelope_proto_cls = EmailEnvelope
    get_reply_proto_cls = GetEmailReply
    list_reply_proto_cls = ListEmailReply
    send_reply_proto_cls = SendEmailReply

    def __init__(self) -> None:
        super().__init__()
        self._headers = HeaderRegistry()

    def _get_message(self, message: Union[Email, ArchiveEmail]) -> ProtoEmail:
        """Return interface email for email model."""
        proto_message = cast(ProtoEmail, super()._get_message(message))
        if message.sender:
            proto_message.sender = message.sender
        proto_message.recipient = message.recipient
        proto_message.extra_headers.update(**(message.extra_headers or {}))
        proto_message.subject_template = message.subject_template
        if message.body_template_html:
            proto_message.body_template_html = message.body_template_html
        for uuid in (message.attachments or ()):
            proto_message.attachments.add(value=str(uuid))
        if message.subject_template_uuid is not None:
            proto_message.subject_template_uuid.value = str(message.subject_template_uuid)
        if message.body_template_html_uuid is not None:
            proto_message.body_template_html_uuid.value = str(message.body_template_html_uuid)
        return proto_message

    def _get_envelope(self, message: Union[Email, ArchiveEmail]) -> EmailEnvelope:
        """Return email envelope for email model."""
        envelope = cast(EmailEnvelope, super()._get_envelope(message))
        if message.message_id:
            envelope.message_id.value = message.message_id
        return envelope

    def _filter_recipients(self, query: Q, model: Union[Type[Email], Type[ArchiveEmail]],
                           recipients: Iterable) -> Q:
        """Filter query according to recipients."""
        query_filter = or_(*[cast(Column, model.recipient).ilike('%' + r + '%') for r in recipients])
        return query.filter(query_filter)

    def _decode_send_request(self, request: SendEmailRequest, *, validate_recipients: bool = True) -> Dict[str, Any]:
        """Decode send request.

        Raises:
            ServiceError: An error in request.
        """
        if not request.message_data.subject_template:
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "Subject is missing.")
        model_kwargs = super()._decode_send_request(request)
        parsed = cast(AddressHeader, self._headers('To', model_kwargs['recipient']))
        if validate_recipients and cast(BaseHeader, parsed).defects:
            raise ServiceError(
                StatusCode.INVALID_ARGUMENT,
                "Invalid recipient {!r}: {}.".format(model_kwargs['recipient'], cast(BaseHeader, parsed).defects))
        return model_kwargs

    @wrap_unary
    def multi_send(self, request: SendEmailRequest, context: _Context) -> MultiSendEmailReply:
        """Store single email for each valid recipient and return result."""
        model_kwargs = self._decode_send_request(request, validate_recipients=False)
        # Check recipients
        addresses = (str(a) for a in cast(AddressHeader, self._headers('To', model_kwargs.pop('recipient'))).addresses)
        recipients = tuple(a for a in addresses if not self._headers('To', a).defects)
        if not recipients:
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "No valid recipient address found.")

        if request.HasField('archive'):
            model_kwargs['archive'] = request.archive
        reference_cls = get_type(cast(Type[Record], self.message_cls).references)
        model_kwargs['references'] = [reference_cls(type=self._get_reference_type(r), value=r.value)
                                      for r in request.references]

        with atomic() as session:
            reply = MultiSendEmailReply()
            models = []
            for recipient in recipients:
                model = self.message_cls(status=MessageStatus.PENDING, recipient=recipient, **model_kwargs)
                session.add(model)
                models.append(model)

            try:
                session.commit()
            except SQLAlchemyError as error:
                _LOGGER.error("Error in SQL query: %s", error)
                raise ServiceError(StatusCode.FAILED_PRECONDITION, "Database error: {}".format(error)) from error

            # We need to commit the models to get UIDs.
            for model in models:
                info = MultiSendEmailReply.EmailInfo()
                info.uid.value = str(Uid.from_record(model))
                info.recipient = model.recipient
                reply.data.emails.append(info)
        return reply

    @wrap_stream
    def batch_send(self, requests: Iterator[SendEmailRequest], context: _Context) -> Iterator[SendEmailReply]:
        """Store emails in database and return their IDs."""
        models = []
        with atomic() as session:
            for request in requests:
                model_kwargs = self._decode_send_request(request)
                if request.HasField('archive'):
                    model_kwargs['archive'] = request.archive
                reference_cls = get_type(cast(Type[Record], self.message_cls).references)
                model_kwargs['references'] = [reference_cls(type=self._get_reference_type(r), value=r.value)
                                              for r in request.references]
                model = self.message_cls(status=MessageStatus.PENDING, **model_kwargs)
                session.add(model)
                models.append(model)

            try:
                session.commit()
            except SQLAlchemyError as error:
                _LOGGER.error("Error in SQL query: %s", error)
                raise ServiceError(StatusCode.FAILED_PRECONDITION, "Database error: {}".format(error)) from error

            # We need to commit the models to get UIDs.
            for model in models:
                reply = self.send_reply_proto_cls()
                reply.data.uid.value = str(Uid.from_record(model))
                yield reply


class SmsServicer(TemplateMessageServicerMixin, SmsMessengerServicer):
    """Sms gRPC service."""

    message_cls = Sms
    archive_message_cls = ArchiveSms
    message_proto_cls = ProtoSms
    envelope_proto_cls = SmsEnvelope
    get_reply_proto_cls = GetSmsReply
    list_reply_proto_cls = ListSmsReply
    send_reply_proto_cls = SendSmsReply

    def _get_message(self, message: Union[Sms, ArchiveSms]) -> ProtoSms:
        """Return interface email for email model."""
        proto_message = cast(ProtoSms, super()._get_message(message))
        proto_message.recipient = message.recipient
        return proto_message

    def _filter_recipients(self, query: Q, model: Union[Type[Sms], Type[ArchiveSms]],
                           recipients: Iterable) -> Q:
        """Filter query according to recipients."""
        # Strip '.' from both stored number and query. Strip '+' only from query, since we search by 'contains'.
        query_filter = or_(*[func.replace(model.recipient, '.', '').contains(r.replace('.', '').replace('+', ''))
                             for r in recipients])
        return query.filter(query_filter)


# Maps ProtoAddress attributes to Letter recipient attributes
ADDRESS_FIELD_MAP = {
    'name': 'recipient_name',
    'organization': 'recipient_organization',
    'street': 'recipient_street',
    'city': 'recipient_city',
    'state_or_province': 'recipient_state',
    'postal_code': 'recipient_postal_code',
    'country': 'recipient_country',
}


class LetterServicer(MessageServicerMixin, LetterMessengerServicer):
    """Letter gRPC service."""

    message_cls = Letter
    archive_message_cls = ArchiveLetter
    message_proto_cls = ProtoLetter
    envelope_proto_cls = LetterEnvelope
    get_reply_proto_cls = GetLetterReply
    list_reply_proto_cls = ListLetterReply
    send_reply_proto_cls = SendLetterReply

    def _get_message(self, message: Union[Letter, ArchiveLetter]) -> ProtoLetter:
        """Return interface email for email model."""
        proto_message = cast(ProtoLetter, super()._get_message(message))
        address = message.recipient
        if address.name:
            proto_message.recipient.name = address.name
        if address.organization:
            proto_message.recipient.organization = address.organization
        if address.street:
            proto_message.recipient.street = address.street
        if address.city:
            proto_message.recipient.city = address.city
        if address.state:
            proto_message.recipient.state_or_province = address.state
        if address.postal_code:
            proto_message.recipient.postal_code = address.postal_code
        if address.country:
            proto_message.recipient.country = address.country
        proto_message.file.value = str(message.file)
        return proto_message

    def _filter_recipients(self, query: Q, model: Union[Type[Letter], Type[ArchiveLetter]],
                           recipients: Iterable) -> Q:
        """Filter query according to recipients."""
        filters = []
        for recipient in recipients:
            try:
                address = ProtoAddress.FromString(recipient.encode('utf-8'))
            except DecodeError:
                address = None
            if address is not None and address.ListFields():
                # It's an Address object.
                filters.append(and_(
                    *[func.lower(getattr(model, ADDRESS_FIELD_MAP[f.name])) == func.lower(v)
                      for f, v in address.ListFields()]))
            else:
                # It's a string.
                subfilters = []
                for keyword in recipient.split():
                    subfilters.append(or_(
                        *[func.lower(getattr(model, f)).contains(func.lower(keyword))
                          for f in ADDRESS_FIELD_MAP.values()]))
                filters.append(and_(*subfilters))
        return query.filter(or_(*filters))

    def _decode_send_request(self, request: SendEmailRequest) -> Dict[str, Any]:
        """Decode send request.

        Raises:
            ServiceError: An error in request.
        """
        if request.message_data.body_template:
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "Field body_template not yet supported.")
        if request.message_data.HasField('body_template_uuid'):
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "Field body_template_uuid not yet supported.")
        if request.message_data.HasField('context'):
            raise ServiceError(StatusCode.INVALID_ARGUMENT, "Field context not yet supported.")
        model_kwargs = super()._decode_send_request(request)
        model_kwargs.pop('body_template', None)
        model_kwargs.pop('body_template_uuid', None)
        model_kwargs.pop('context', None)
        recipient = model_kwargs.pop('recipient') or {}
        model_kwargs.update({ADDRESS_FIELD_MAP[k]: v for k, v in recipient.items()})
        return model_kwargs
