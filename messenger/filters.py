"""Message filters."""
from abc import ABC, abstractmethod
from fnmatch import fnmatch
from typing import Any, Collection, Container, Optional, Union

from .models import Letter, Record
from .utils import load_instance


class Filter(ABC):
    """Abstract base class for filters."""

    @abstractmethod
    def filter(self, model: Record) -> bool:
        """Return whether message instance matches filter."""


class NegativeFilter(Filter):
    """Filter which negates other filters.

    Attributes:
        inner_filter: The negated filter.
    """

    def __init__(self, inner_filter: Union[str, Filter], **kwargs: Any):
        """Wrap the provided filter, load if required.

        Arguments:
            inner_filter: An instance or dotted-path to a filter.
            **kwargs: Other arguments to initialize inner filter from dotted-path.
        """
        if isinstance(inner_filter, str):
            self.inner_filter = load_instance(inner_filter, **kwargs)
        else:
            self.inner_filter = inner_filter

    def filter(self, model: Record) -> bool:
        """Return negated result from internal filter."""
        return not self.inner_filter.filter(model)


class TypeFilter(Filter):
    """Filter messages based on their type.

    Attributes:
        types: Collection of accepted types.
               Accepted types support Unix shell-style wildcards.
               See https://docs.python.org/3/library/fnmatch.html for details.
    """

    def __init__(self, types: Collection[Optional[str]]):
        self.types = types

    def filter(self, model: Record) -> bool:
        """Return True if message's type matches at least one of specified types."""
        # Handle None first
        if model.type is None:
            return None in self.types

        for type in self.types:
            if type is None:
                # None is already handled
                continue
            if fnmatch(model.type, type):
                return True
        return False


class AddressCountryFilter(Filter):
    """Filter letters based on recipient's country.

    Attributes:
        countries: Container with accepted countries.
    """

    def __init__(self, countries: Container[Optional[str]]):
        self.countries = countries

    def filter(self, model: Letter) -> bool:  # type: ignore[override]
        """Return True if letter recipient's country matches at least one of specified countries."""
        return model.recipient.country in self.countries
