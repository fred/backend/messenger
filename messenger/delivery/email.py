"""Email delivery checkers for messenger."""
import logging
from datetime import datetime, timedelta
from email import message_from_string
from email.message import Message
from typing import Any, Dict, List, Optional, Union, cast

from imap_tools import AND, BaseMailBox, MailBox, MailBoxTls, MailBoxUnencrypted, MailMessage, MailMessageFlags
from pytz import UTC
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session
from sqlalchemy.orm.exc import NoResultFound

from ..constants import ArchiveMessageStatus, MessageStatus
from ..database import atomic
from ..models import ArchiveEmail, Email
from ..utils import Result, create_ssl_context

_LOGGER = logging.getLogger(__name__)
MIMETYPE_REPORT = 'multipart/report'
MIMETYPE_MESSAGE = 'message/rfc822'
MIMETYPE_MESSAGE_HEADERS = 'text/rfc822-headers'
MIMETYPE_DSN = 'message/delivery-status'
REPORT_TYPE_DSN = 'delivery-status'
DSN_FIELD_NAMES = (
    # Message fields
    'Reporting-MTA',
    # Recipient fields
    'Action', 'Status', 'Remote-MTA', 'Diagnostic-Code', 'Final-Recipient',
)


class ImapDelivery:
    """Check email delivery from mailbox using IMAP."""

    def __init__(self, host: str = '', port: Optional[int] = None, ssl: bool = True, tls: bool = False,
                 keyfile: Optional[str] = None, certfile: Optional[str] = None, verify: bool = True,
                 username: Optional[str] = None, password: Optional[str] = None, folder: str = 'INBOX',
                 mark_delivered: bool = False):
        self.host = host
        self.port = port
        self.ssl = ssl
        self.tls = tls
        self.keyfile = keyfile
        self.certfile = certfile
        self.verify = verify
        self.username = username
        self.password = password
        self.folder = folder
        self.mark_delivered = mark_delivered

    def get_mailbox(self) -> BaseMailBox:
        """Get a mailbox instance."""
        if self.ssl:
            ssl_context = create_ssl_context(self.certfile, self.keyfile, verify=self.verify)
            mailbox: BaseMailBox = MailBox(host=self.host, port=self.port, ssl_context=ssl_context)
        elif self.tls:
            ssl_context = create_ssl_context(self.certfile, self.keyfile, verify=self.verify)
            mailbox = MailBoxTls(host=self.host, port=self.port, ssl_context=ssl_context)
        else:
            mailbox = MailBoxUnencrypted(host=self.host, port=self.port)

        if self.username and self.password:
            mailbox.login(username=self.username, password=self.password)
        mailbox.folder.set(self.folder)
        return mailbox

    def is_dsn(self, message: MailMessage) -> bool:
        """Return True if message is a delivery status."""
        return bool(message.obj.get_content_type() == MIMETYPE_REPORT
                    and message.obj.get_param('report-type') == REPORT_TYPE_DSN)

    def get_status(self, message: MailMessage) -> Optional[MessageStatus]:
        """Return an email status based on a message content."""
        if message.obj.get_content_type() != MIMETYPE_REPORT:
            # It's a reply, check whether it's autosubmitted.
            if message.obj.get('Auto-Submitted', 'no') != 'no':
                # It's auto-submitted, ignore it.
                return None
            return MessageStatus.DELIVERED
        if not self.is_dsn(message):
            # Not a valid delivery status
            return None
        # Collect all Action fields
        actions = [cast(Message, fields)['Action'] for part in message.obj.get_payload()
                   for fields in cast(Message, part).get_payload() if 'Action' in fields]
        if not actions:
            return None
        # XXX: Just use the first one, and ignore the rest for now.
        action = actions[0]
        if action == 'failed':
            return MessageStatus.UNDELIVERED
        elif action == 'delivered':
            return MessageStatus.DELIVERED
        # Ignore other `Action` values.
        return None

    def get_message_id(self, message: MailMessage) -> Optional[str]:
        """Return a Message-ID of the email object."""
        # If it's a generic reply.
        message_id = cast(Optional[str], message.obj.get('In-Reply-To', message.obj.get('References')))
        if message_id:
            return message_id
        if not message.obj.is_multipart():
            # Proper DSN messages are multipart. This is not the case.
            return None
        # Otherwise expect it to be a DSN. Check payload for the original message.
        for part in message.obj.get_payload():
            part = cast(Message, part)
            if part.get_content_type() == MIMETYPE_MESSAGE:
                # The original message is in the content of the message. Not an attachment.
                returned = cast(List[Message], part.get_payload())[0]
                if 'Message-ID' in returned:
                    return cast(str, returned['Message-ID'])
            elif part.get_content_type() == MIMETYPE_MESSAGE_HEADERS:
                # The original headers are in the content of the message. Not an attachment.
                returned = message_from_string(cast(str, part.get_payload()))
                if 'Message-ID' in returned:
                    return cast(str, returned['Message-ID'])
        return None

    def get_dsn_fields(self, message: MailMessage) -> Dict[str, Any]:
        """Return a selected fields from delivery status."""
        if not self.is_dsn(message):
            # Not a valid delivery status
            return {}

        result = {}
        dsn_parts = [part for part in message.obj.get_payload()
                     if cast(Message, part).get_content_type() == MIMETYPE_DSN]
        dsn_fields = [fields for part in dsn_parts for fields in cast(Message, part).get_payload()]
        # XXX: Collect the fields backwards, to keep the first value of each.
        for fields in reversed(dsn_fields):
            # Always convert value to string.
            # In some spurious cases, the value may be a Header object, which is not JSON serializable.
            # I have not found a way to create a test for that.
            result.update({key: str(value) for key, value in cast(Message, fields).items() if key in DSN_FIELD_NAMES})
        return result

    def _get_email(self, session: Session, message_id: str, delivery_date: Optional[datetime] = None,
                   ) -> Optional[Union[Email, ArchiveEmail]]:
        """Return an email by Message ID."""
        query = session.query(Email).with_for_update().filter_by(message_id=message_id, status=MessageStatus.SENT)
        try:
            return query.one()
        except NoResultFound:
            a_query = session.query(ArchiveEmail).with_for_update().filter_by(message_id=message_id,
                                                                              status=ArchiveMessageStatus.SENT)
            # Use delivery date as a hint for the partitioning.
            if delivery_date and cast(Engine, session.bind).dialect.name != 'sqlite':
                # Delivery notification usually come in a week since the original message is sent.
                lower_bound = delivery_date - timedelta(days=10)
                # Some responses come with Date before the original message. Probably a differences in system times.
                upper_bound = delivery_date + timedelta(days=1)
                fast_query = a_query.filter(ArchiveEmail.create_datetime >= lower_bound,
                                            ArchiveEmail.create_datetime < upper_bound)
                try:
                    return fast_query.one()
                except NoResultFound:
                    # Continue with full search.
                    pass
            # Search the whole archive. Some messages don't provide Date header.
            # XXX: Could be optimized in case of further performance problems.
            try:
                return a_query.one()
            except NoResultFound:
                return None

    def handle_message(self, message: MailMessage) -> bool:
        """Handle an incomming email message.

        Check if the message is reply or a delivery status notification to an existing message and store data.
        Return whether the message was handled (or skipped).
        """
        status = self.get_status(message)
        if not status:
            # The message is unspecific.
            return False

        if status == MessageStatus.DELIVERED and not self.mark_delivered:
            # Skip delivered messages if not required.
            return False

        message_id = self.get_message_id(message)
        if not message_id:
            # No Message-ID found.
            return False

        with atomic() as session:
            # XXX: `message.date` returns dummy date, if Date is empty or not defined :-/
            response_datetime = None
            if message.date_str:
                response_datetime = message.date

            email = self._get_email(session, message_id, response_datetime)
            if email is None:
                return False

            delivery_datetime = response_datetime or datetime.now(UTC)
            delivery_info = self.get_dsn_fields(message)
            if message.subject:
                delivery_info['Subject'] = message.subject
            delivery_message_id = message.obj.get('Message-ID')
            if delivery_message_id is not None:
                delivery_info['Message-ID'] = delivery_message_id
            email.status = status  # type: ignore[assignment]
            email.update_datetime = datetime.now(UTC)
            email.delivery_datetime = delivery_datetime
            email.delivery_info = delivery_info
        return True

    def check_delivery(self, limit: Optional[int] = None) -> Result:
        """Check email delivery and return number of processed messages.

        Delivery status messages are deleted from the mailbox.
        """
        result = Result()
        with self.get_mailbox() as mailbox:
            # Use bulk=True, because mailservers are not in favor of fetching mails one by one.
            # See https://github.com/ikvk/imap_tools/issues/99 for details.
            for message in mailbox.fetch(AND(seen=False, deleted=False), mark_seen=False, limit=limit, bulk=True):
                try:
                    # Mark all handled messages as seen.
                    flags = [MailMessageFlags.SEEN]
                    if self.handle_message(message):
                        result.mark_success()
                        # Mark processed DSN messages as deleted.
                        if self.is_dsn(message):
                            flags.append(MailMessageFlags.DELETED)
                    else:
                        result.mark_skip()
                    mailbox.flag(message.uid, flags, True)
                except Exception as error:
                    result.mark_error()
                    _LOGGER.exception("Processing a delivery message %s caused following error: %s", message, error)
            # Delete all deleted message.
            mailbox.expunge()
            return result
