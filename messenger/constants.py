"""Constants for messenger."""
from enum import unique
from functools import lru_cache
from typing import TYPE_CHECKING, Dict, Type

from backports.strenum import StrEnum

# Prevent circular imports
if TYPE_CHECKING:
    from .models import ArchiveRecord, Record


@unique
class TransportMethod(StrEnum):
    """Enum with message transport methods.

    Members:
        EMAIL: Email
        SMS: SMS
        LETTER: Letter
    """

    EMAIL = 'email'
    SMS = 'sms'
    LETTER = 'letter'

    @property
    @lru_cache  # noqa: B019
    def model_cls(self) -> Type['Record']:
        """Return model class which represents a transport method message."""
        from .models import Email, Letter, Sms

        map: Dict[TransportMethod, Type[Record]] = {
            TransportMethod.EMAIL: Email,
            TransportMethod.SMS: Sms,
            TransportMethod.LETTER: Letter,
        }
        return map[self]

    @property
    @lru_cache  # noqa: B019
    def archive_cls(self) -> Type['ArchiveRecord']:
        """Return model class which represents an archived transport method message."""
        from .models import ArchiveEmail, ArchiveLetter, ArchiveSms

        map: Dict[TransportMethod, Type[ArchiveRecord]] = {
            TransportMethod.EMAIL: ArchiveEmail,
            TransportMethod.SMS: ArchiveSms,
            TransportMethod.LETTER: ArchiveLetter,
        }
        return map[self]


@unique
class MessageStatus(StrEnum):
    """Enum of message statuses.

    Members:
        PENDING: Message pending for sending.
        SENT: Message was sucessfully send.
        FAILED: All attempts to send a message failed.
        ERROR: Message contains an error.
        DELIVERED: Message was successfully delivered.
        UNDELIVERED: Message was not delivered.
        CANCELED: Message was canceled.
    """

    PENDING = 'pending'
    SENT = 'sent'
    FAILED = 'failed'
    ERROR = 'error'
    DELIVERED = 'delivered'
    UNDELIVERED = 'undelivered'
    CANCELED = 'canceled'


@unique
class ArchiveMessageStatus(StrEnum):
    """Enum of archived message statuses.

    Members:
        SENT: Message was sucessfully send.
        FAILED: All attempts to send a message failed.
        DELIVERED: Message was successfully delivered.
        UNDELIVERED: Message was not delivered.
        CANCELED: Message was canceled.
    """

    SENT = 'sent'
    FAILED = 'failed'
    DELIVERED = 'delivered'
    UNDELIVERED = 'undelivered'
    CANCELED = 'canceled'


@unique
class ReferenceType(StrEnum):
    """Enum with reference types.

    Used as an enum of values for `migrate_archive` command.
    """

    CONTACT = 'contact'
    DOMAIN = 'domain'
    NSSET = 'nsset'
    KEYSET = 'keyset'
    REGISTRAR = 'registrar'
