"""Database utilities."""
from contextlib import contextmanager
from functools import lru_cache
from typing import Generator

from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.orm import Session, sessionmaker

from .settings import APPLICATION


@lru_cache
def get_engine() -> Engine:
    """Return engine as defined by settings."""
    return create_engine(APPLICATION.settings.db_connection, echo=APPLICATION.settings.db_echo, future=True)


@lru_cache
def get_session_cls() -> sessionmaker[Session]:
    """Return session class for defined engine."""
    return sessionmaker(bind=get_engine(), future=True)


@contextmanager
def atomic() -> Generator[Session, None, None]:
    """Context manager which wraps content into a transaction."""
    session = get_session_cls()()
    try:
        yield session
        session.commit()
    except BaseException:
        session.rollback()
        raise
    finally:
        session.close()
