"""Messenger settings management."""
import logging
from functools import cached_property
from typing import Optional, Sequence

from filed import FileClient, Storage
from setapp import BaseApplication, BaseSettings, SettingsConfigDict

_LOGGER = logging.getLogger(__name__)
ENVIRON_NAME = 'MESSENGER_CONFIG'
CONFIG_FILES = ('~/.fred/messenger.conf', '/etc/fred/messenger.conf')


class Settings(BaseSettings):
    """Actual settings."""

    model_config = SettingsConfigDict(env_prefix='MESSENGER_',
                                      config_files=CONFIG_FILES,
                                      config_environ=ENVIRON_NAME)

    clean_days: int = 30
    db_connection: str = 'sqlite:///:memory:'
    db_echo: bool = False
    delivery_backend: str = 'messenger.delivery.email.ImapDelivery'
    delivery_options: dict = {}
    processors: dict = {}
    fileman_netloc: Optional[str] = None
    fred_db_connection: Optional[str] = None
    grpc_max_workers: Optional[int] = None
    grpc_maximum_concurrent_rpcs: Optional[int] = None
    grpc_options: Optional[Sequence] = None
    grpc_port: int = 50051
    monitoring_email: Optional[str] = None
    msgid_domain: Optional[str] = None
    senders: dict = {}
    test_db_connection: str = 'sqlite:///:memory:'


class Application(BaseApplication[Settings]):
    """Represent application settings and basic setup."""

    @cached_property
    def storage(self) -> Optional[Storage]:
        """Return a file storage."""
        if self.settings.fileman_netloc is None:
            return None
        return Storage(FileClient(self.settings.fileman_netloc))

    def clear_cache(self) -> None:
        """Clear all caches."""
        self._del('storage')


APPLICATION = Application(Settings)
