"""Messenge processors."""
import os
from abc import ABC, abstractmethod
from email.message import EmailMessage
from email.mime.application import MIMEApplication
from typing import Any, Dict, Iterable, Mapping, Set, Tuple, Union, cast

from cryptography.hazmat.primitives.asymmetric.ec import EllipticCurvePrivateKey
from cryptography.hazmat.primitives.asymmetric.rsa import RSAPrivateKey
from cryptography.hazmat.primitives.hashes import SHA256
from cryptography.hazmat.primitives.serialization import Encoding, load_pem_private_key
from cryptography.hazmat.primitives.serialization.pkcs7 import PKCS7Options, PKCS7SignatureBuilder
from cryptography.x509 import ExtensionNotFound, ExtensionOID, RFC822Name, load_pem_x509_certificate
from sqlalchemy import or_
from sqlalchemy.orm import class_mapper
from sqlalchemy.sql import func

from .backends.letter import LetterMessage
from .backends.sms import SmsMessage
from .database import atomic
from .exceptions import MessageCanceled
from .models import BlockedAddress, BlockedEmail, BlockedPhone
from .utils import normalize_email_address


class Processor(ABC):
    """Abstract base class for processors."""

    @abstractmethod
    def process_message(self, message: Any) -> Any:
        """Process and return the message.

        Raises:
            ValueError: Message can't be created.
            RuntimeError: Transient error in message creation.
            MessageCanceled: The message is canceled.
        """


class EmailHeadersProcessor(Processor):
    """Processor which appends custom headers to email messages."""

    def __init__(self, headers: Mapping[str, Any]):
        self.headers = headers

    def process_message(self, message: EmailMessage) -> EmailMessage:
        """Append headers to the email message."""
        for key, value in self.headers.items():
            message[key] = value
        return message


class SmimeEmailProcessor(Processor):
    """Processor which signs email messages using S/MIME signatures.

    Class attributes:
        signed_headers: List of header names which are part of the signature.
    """

    signed_headers = ("Content-Type", "MIME-Version", "Content-Transfer-Encoding")

    def __init__(self, smime_certificates: Iterable[Tuple[os.PathLike, os.PathLike]]):
        # Mapping of email to loaded key and certificate.
        self.signers = self._parse_certificates(smime_certificates)

    def _parse_certificates(self, certificates: Iterable[Tuple[os.PathLike, os.PathLike]],
                            ) -> Dict[str, PKCS7SignatureBuilder]:
        """Parse keys and certificates and return mapping of normalized email address to a signer."""
        result: Dict[str, PKCS7SignatureBuilder] = {}
        for keyfile, certfile in certificates:
            with open(keyfile, 'rb') as file:
                key = load_pem_private_key(file.read(), None)
            with open(certfile, 'rb') as file:
                cert = load_pem_x509_certificate(file.read())
            # According to RFC 8550 https://tools.ietf.org/html/rfc8550#section-3,
            # only a subjectAltName should be used as source for email addresses.
            try:
                ext = cert.extensions.get_extension_for_oid(ExtensionOID.SUBJECT_ALTERNATIVE_NAME)
            except ExtensionNotFound:
                continue
            signer = PKCS7SignatureBuilder().add_signer(cert,
                                                        cast(Union[RSAPrivateKey, EllipticCurvePrivateKey], key),
                                                        SHA256())
            for name in ext.value.get_values_for_type(RFC822Name):  # type: ignore[attr-defined]
                normalized = normalize_email_address(name)
                if normalized is not None:
                    result[normalized] = signer
        return result

    def _get_body_message(self, message: EmailMessage) -> EmailMessage:
        """Return the message which will be actually signed."""
        body = EmailMessage()
        body.set_payload(message.get_payload())
        for header in self.signed_headers:
            if header in message:
                body[header] = message[header]
        return body

    def _get_signature_message(self, signature: bytes) -> MIMEApplication:
        """Return the message with the signature."""
        message = MIMEApplication(signature, 'pkcs7-signature', name="smime.p7s")
        message.add_header('Content-Disposition', 'attachment', filename="smime.p7s")
        message.add_header('Content-Description', 'S/MIME Cryptographic Signature')
        del message["MIME-Version"]
        return message

    def sign_message(self, message: EmailMessage, signer: PKCS7SignatureBuilder) -> EmailMessage:
        """Sign message and return the envelope with signature."""
        # Sign the message.
        body_msg = self._get_body_message(message)

        # Signed messages are serialized without a line length limit. Do the same thing here to prevent unwanted
        # signature failures caused by differences in header wrappings.
        # See email.generator.Generator._handle_multipart_signed for details.
        policy = message.policy.clone(max_line_length=0)
        body_msg_bytes = body_msg.as_bytes(policy=policy)
        # The signature is actually in the DER and not SMIME format.
        signature = signer.set_data(body_msg_bytes).sign(Encoding.DER, [PKCS7Options.DetachedSignature])
        signature_msg = self._get_signature_message(signature)

        # Create the envelope to return.
        envelope = EmailMessage()
        envelope.add_header("Content-Type", "multipart/signed", protocol="application/pkcs7-signature",
                            micalg='sha-256')
        # EmailMessage doesn't set MIME-Version header, but it's required by Section 4 of RFC 2045,
        # see https://datatracker.ietf.org/doc/html/rfc2045#section-4.
        envelope.add_header('MIME-Version', '1.0')
        for header, value in message.items():
            if header not in self.signed_headers:
                envelope[header] = value
        envelope.preamble = "This is an S/MIME signed message"
        envelope.attach(body_msg)
        envelope.attach(signature_msg)  # type: ignore[arg-type]

        return envelope

    def process_message(self, message: EmailMessage) -> EmailMessage:
        """Sign email message if possible."""
        signed_email = normalize_email_address(message['From'])
        if signed_email:
            signer = self.signers.get(signed_email)
            if signer is not None:
                message = self.sign_message(message, signer)
        return message


class VcardEmailProcessor(Processor):
    """Processor which appends a vCard to the email messages."""

    def __init__(self, data: str):
        self.data = data

    def process_message(self, message: EmailMessage) -> EmailMessage:
        """Append vCard to the email message."""
        # Someday we may use 'text/vcard' mime type, but currently it changes behaviour, at least in Thunderbird.
        message.add_attachment(self.data, subtype='x-vcard')
        return message


class BlockedEmailProcessor(Processor):
    """Processor prevent emails from being sent to blocked email addresses.

    Removes blocked email addresses from Destination Address Fields (To, Cc, Bcc).
    If no addresses remain, the message is canceled.
    """

    address_headers = ('To', 'Cc', 'Bcc')

    def process_message(self, message: EmailMessage) -> EmailMessage:
        """Check recipients and disable blocked ones.

        Raises:
            MessageCanceled: All recipients are blocked.
        """
        # Collect all recipient addresses.
        addresses: Set[str] = set()
        for header in self.address_headers:
            if message[header]:
                addresses.update(a.addr_spec for a in message[header].addresses)

        # Find blocked recipients
        query_filter = or_(*[BlockedEmail.email.ilike(a) for a in addresses])
        with atomic() as session:
            blocked = tuple(b.email.lower() for b in session.query(BlockedEmail).filter(query_filter))

        # Filter out blocked recipients.
        for header in self.address_headers:
            if message[header]:
                filtered = tuple(a for a in message[header].addresses if a.addr_spec.lower() not in blocked)
                if not filtered:
                    del message[header]
                else:
                    message.replace_header(header, filtered)

        # Check if any recipients remained.
        if all(message[h] is None for h in self.address_headers):
            raise MessageCanceled("All recipients blocked.")

        return message


class BlockedSmsProcessor(Processor):
    """Processor prevent SMSes from being sent to blocked phone numbers.

    If phone is blocked, the message is canceled.
    """

    def process_message(self, message: SmsMessage) -> SmsMessage:
        """Check recipients and cancel send if phone is blocked.

        Raises:
            MessageCanceled: Phone number is blocked.
        """
        with atomic() as session:
            if session.query(BlockedPhone).filter_by(phone=message.recipient).first():
                raise MessageCanceled("Phone number blocked.")
        return message


class BlockedLetterProcessor(Processor):
    """Processor that prevents letters from being sent to blocked addresses.

    If address is blocked, the message is canceled.
    """

    attributes = tuple(k for k in class_mapper(BlockedAddress).columns.keys() if k != 'id')

    def process_message(self, message: LetterMessage) -> LetterMessage:
        """Check recipient and cancel send if address is blocked.

        Raises:
            MessageCanceled: Address is blocked.
        """
        with atomic() as session:
            query = session.query(BlockedAddress)
            for attr in self.attributes:
                value = getattr(message.recipient, attr)
                if value is not None:
                    value = value.lower()
                query = query.filter(func.lower(getattr(BlockedAddress, attr)) == value)
            if query.first():
                raise MessageCanceled("Address blocked.")
        return message
