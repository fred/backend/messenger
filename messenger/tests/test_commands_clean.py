from datetime import datetime
from typing import Iterable, Optional
from unittest import TestCase
from uuid import UUID

from freezegun import freeze_time
from pytz import UTC

from messenger.commands.clean import main
from messenger.constants import MessageStatus
from messenger.models import Address, Email, EmailReference, Letter, LetterReference, Sms, SmsReference

from .utils import CommandMixin, make_email, make_letter, make_sms, override_database, test_session_cls


@freeze_time('2020-02-02 14:00:00')
@override_database()
class CleanMainTest(CommandMixin, TestCase):
    command = main

    old_datetime = datetime(2020, 1, 3, tzinfo=UTC)
    new_datetime = datetime(2020, 1, 4, tzinfo=UTC)

    def setUp(self):
        self.session = test_session_cls()()

    def tearDown(self):
        self.session.close()

    def make_email(self, status: MessageStatus = MessageStatus.PENDING, create_datetime: datetime = old_datetime,
                   archive: bool = True, references: Optional[Iterable[EmailReference]] = None) -> None:
        make_email(self.session, status=status, create_datetime=create_datetime, archive=archive,
                   recipient='kryten@example.com', subject_template='subject.txt', body_template='body.txt',
                   references=(references or []))

    def make_sms(self, status: MessageStatus = MessageStatus.PENDING, create_datetime: datetime = old_datetime,
                 archive: bool = True, references: Optional[Iterable[SmsReference]] = None) -> None:
        make_sms(self.session, status=status, create_datetime=create_datetime, archive=archive, recipient='+1234',
                 body_template='body.txt', references=(references or []))

    def make_letter(self, status: MessageStatus = MessageStatus.PENDING, create_datetime: datetime = old_datetime,
                    archive: bool = True, references: Optional[Iterable[LetterReference]] = None) -> None:
        make_letter(self.session, status=status, create_datetime=create_datetime, archive=archive,
                    recipient=Address(name='Kryten'), file=UUID(int=42), references=(references or []))

    def test_main_no_messages(self):
        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 0)

    def test_main_new(self):
        # Test new messages are left intact
        self.make_email(status=MessageStatus.ERROR, create_datetime=self.new_datetime)
        self.make_sms(status=MessageStatus.ERROR, create_datetime=self.new_datetime)
        self.make_letter(status=MessageStatus.ERROR, create_datetime=self.new_datetime)

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(Letter).count(), 1)

    def test_main_pending(self):
        # Test pending messages are left intact
        self.make_email(archive=False)
        self.make_sms(archive=False)
        self.make_letter(archive=False)

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(Letter).count(), 1)

    def test_main_archived(self):
        # Test message to be archived are left intact
        self.make_email(status=MessageStatus.SENT)
        self.make_sms(status=MessageStatus.SENT)
        self.make_letter(status=MessageStatus.SENT)

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(Letter).count(), 1)

    def test_main_delete_error(self):
        # Test errored messages are deleted
        self.make_email(status=MessageStatus.ERROR)
        self.make_sms(status=MessageStatus.ERROR)
        self.make_letter(status=MessageStatus.ERROR)

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 0)

    def test_main_delete_not_archived(self):
        # Test messages, which are not to be archived, are deleted
        self.make_email(status=MessageStatus.SENT, archive=False)
        self.make_sms(status=MessageStatus.SENT, archive=False)
        self.make_letter(status=MessageStatus.SENT, archive=False)

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 0)

    def test_main_delete_references(self):
        # Test message references are also deleted.
        self.make_email(status=MessageStatus.SENT, archive=False,
                        references=[EmailReference(type='person', value='kryten')])
        self.make_sms(status=MessageStatus.SENT, archive=False,
                      references=[SmsReference(type='person', value='kryten')])
        self.make_letter(status=MessageStatus.SENT, archive=False,
                         references=[LetterReference(type='person', value='kryten')])

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(EmailReference).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 0)
        self.assertEqual(self.session.query(SmsReference).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 0)
        self.assertEqual(self.session.query(LetterReference).count(), 0)

    def test_main_days(self):
        # Test --days argument work correctly.
        older_datetime = datetime(2020, 1, 1, tzinfo=UTC)
        self.make_email(status=MessageStatus.ERROR, create_datetime=older_datetime)
        self.make_email(status=MessageStatus.ERROR)
        self.make_sms(status=MessageStatus.ERROR, create_datetime=older_datetime)
        self.make_sms(status=MessageStatus.ERROR)
        self.make_letter(status=MessageStatus.ERROR, create_datetime=older_datetime)
        self.make_letter(status=MessageStatus.ERROR)

        self.assertCommandSuccess(['--days', '32'])

        # Check one of each message is deleted
        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(Letter).count(), 1)

    def test_main_type(self):
        # Test messages of selected type are deleted.
        self.make_email(status=MessageStatus.ERROR)
        self.make_sms(status=MessageStatus.ERROR)
        self.make_letter(status=MessageStatus.ERROR)

        self.assertCommandSuccess(['--method', 'letter'])

        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(Letter).count(), 0)

    def _test_output(self, verbosity: int, stdout: str) -> None:
        self.make_email(status=MessageStatus.ERROR)

        self.assertCommandSuccess(['--verbosity', str(verbosity)], stdout=stdout)

    def test_silent(self):
        self._test_output(0, '')

    def test_info(self):
        self._test_output(1, 'Deleted 1 emails.\nDeleted 0 smss.\nDeleted 0 letters.\n')
