from datetime import datetime
from typing import Optional
from unittest import TestCase
from unittest.mock import call, patch, sentinel
from uuid import UUID

from typist import Template

from messenger.backends.sms import BaseSmsBackend, SmsMessage
from messenger.models import Sms


class SmsMessageTest(TestCase):
    def test_normalize_recipient(self):
        data = (
            # raw, normalized
            (' +1.234 ', '+1234'),
            # Invalid number is kept as is.
            ('invalid', 'invalid'),
        )
        for raw, output in data:
            with self.subTest(raw=raw):
                message = SmsMessage(uuid=UUID(int=42), recipient=raw, body=sentinel.body)
                self.assertEqual(message.recipient, output)

    def test_id_default(self):
        message = SmsMessage(uuid=UUID(int=42), recipient='+1234', body=sentinel.body)
        self.assertEqual(message.id, '42')

    def test_id_init(self):
        message = SmsMessage(uuid=UUID(int=42), recipient='+1234', body=sentinel.body, id=sentinel.id)
        self.assertEqual(message.id, sentinel.id)

    def test_id_setter(self):
        message = SmsMessage(uuid=UUID(int=42), recipient='+1234', body=sentinel.body)
        message.id = sentinel.id
        self.assertEqual(message.id, sentinel.id)


class TestSmsBackend(BaseSmsBackend):
    """Simple backend for tests of BaseSmsBackend."""

    def _send(self, message: SmsMessage) -> None:
        pass  # pragma: no cover


class BaseSmsBackendTest(TestCase):
    uuid = UUID(int=42)
    recipient = '+1234'
    body_uuid = UUID(int=255)

    def setUp(self):
        patcher = patch('messenger.backends.base.SecretaryClient', autospec=True)
        self.addCleanup(patcher.stop)
        client_mock = patcher.start()
        self.secretary_mock = client_mock.return_value

        self.backend = TestSmsBackend(secretary_url='http://secretary.example/api/')

    def get_sms(self, body_template: Optional[str] = None, body_template_uuid: Optional[UUID] = None) -> Sms:
        return Sms(uuid=self.uuid, recipient=self.recipient, body_template=body_template,
                   body_template_uuid=body_template_uuid, context=sentinel.context)

    def _test_get_message(self, sms: Sms, sender: Optional[str] = None) -> SmsMessage:
        message = self.backend.get_message(sms)

        self.assertEqual(message, SmsMessage(self.uuid, self.recipient, sentinel.body, sender))
        return message

    def test_get_message(self):
        self.secretary_mock.render.return_value = sentinel.body
        sms = self.get_sms(body_template_uuid=self.body_uuid)

        self._test_get_message(sms)

        self.assertEqual(self.secretary_mock.mock_calls, [call.render(self.body_uuid, sentinel.context)])

    def test_get_message_sender(self):
        self.backend = TestSmsBackend(secretary_url='http://secretary.example/api/', sender=sentinel.sender)
        self.secretary_mock.render.return_value = sentinel.body
        sms = self.get_sms(body_template_uuid=self.body_uuid)

        self._test_get_message(sms, sender=sentinel.sender)

        self.assertEqual(self.secretary_mock.mock_calls, [call.render(self.body_uuid, sentinel.context)])

    def test_get_message_get_body_uuid(self):
        self.secretary_mock.get_template_info.return_value = Template(
            name='example.txt', is_active=True, uuid=str(self.body_uuid), create_datetime=datetime.now())
        self.secretary_mock.render.return_value = sentinel.body
        sms = self.get_sms(body_template=sentinel.template)

        self._test_get_message(sms)

        self.assertEqual(self.secretary_mock.mock_calls,
                         [call.get_template_info(sentinel.template), call.render(self.body_uuid, sentinel.context)])
        # Check body UUID was updated
        self.assertEqual(sms.body_template_uuid, self.body_uuid)

    def test_get_message_secretary_partial_error(self):
        # Test retrieved data from secretary are stored even though the whole message fails.
        self.secretary_mock.get_template_info.return_value = Template(
            name='example.txt', is_active=True, uuid=str(self.body_uuid), create_datetime=datetime.now())
        self.secretary_mock.render.side_effect = RuntimeError('Smeghead!')
        sms = self.get_sms(body_template=sentinel.template)

        with self.assertRaisesRegex(RuntimeError, 'Smeghead!'):
            self.backend.get_message(sms)

        # Check body UUID was updated
        self.assertEqual(sms.body_template_uuid, self.body_uuid)
