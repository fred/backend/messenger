import tempfile
from datetime import datetime
from typing import Iterable, List, Optional, Sequence, Tuple, cast
from unittest import TestCase
from unittest.mock import Mock, call

import click
import yaml
from click.testing import CliRunner
from setapp.constants import Verbosity
from setapp.utils import override_environ
from testfixtures import OutputCapture

import messenger
from messenger.commands.utils import echo, limits_options, messenger_command, method_option
from messenger.constants import TransportMethod
from messenger.settings import APPLICATION, ENVIRON_NAME


class EchoTest(TestCase):
    def test_output(self):
        message = 'Smeghead!'
        output = 'Smeghead!\n'
        data = (
            # settings, verbosity, result
            (Verbosity.SILENT, Verbosity.SILENT, output),
            (Verbosity.SILENT, Verbosity.INFO, ''),
            (Verbosity.SILENT, Verbosity.DETAIL, ''),
            (Verbosity.SILENT, Verbosity.DEBUG, ''),
            (Verbosity.INFO, Verbosity.SILENT, output),
            (Verbosity.INFO, Verbosity.INFO, output),
            (Verbosity.INFO, Verbosity.DETAIL, ''),
            (Verbosity.INFO, Verbosity.DEBUG, ''),
            (Verbosity.DETAIL, Verbosity.SILENT, output),
            (Verbosity.DETAIL, Verbosity.INFO, output),
            (Verbosity.DETAIL, Verbosity.DETAIL, output),
            (Verbosity.DETAIL, Verbosity.DEBUG, ''),
            (Verbosity.DEBUG, Verbosity.SILENT, output),
            (Verbosity.DEBUG, Verbosity.INFO, output),
            (Verbosity.DEBUG, Verbosity.DETAIL, output),
            (Verbosity.DEBUG, Verbosity.DEBUG, output),
            (1, Verbosity.SILENT, output),
            (1, Verbosity.INFO, output),
            (1, Verbosity.DETAIL, ''),
            (1, Verbosity.DEBUG, ''),
        )

        for settings, verbosity, result in data:
            with self.subTest(settings=settings, verbosity=verbosity):
                APPLICATION.verbosity = cast(Verbosity, settings)

                with OutputCapture(separate=True) as capture:
                    echo(message, verbosity)

                self.assertEqual(capture.stdout.getvalue(), result)
                self.assertEqual(capture.stderr.getvalue(), '')

    def test_error(self):
        APPLICATION.verbosity = Verbosity.DEBUG

        with OutputCapture(separate=True) as capture:
            echo('Smeghead!', Verbosity.DEBUG, err=True)

        self.assertEqual(capture.stdout.getvalue(), '')
        self.assertEqual(capture.stderr.getvalue(), 'Smeghead!\n')

    def test_no_newline(self):
        APPLICATION.verbosity = Verbosity.DEBUG

        with OutputCapture(separate=True) as capture:
            echo('Smeghead!', Verbosity.DEBUG, nl=False)

        self.assertEqual(capture.stdout.getvalue(), 'Smeghead!')
        self.assertEqual(capture.stderr.getvalue(), '')

    def test_log(self):
        # Test message is logged, regardless of the verbosity setting.
        message = 'Smeghead!'

        for settings in (Verbosity.DEBUG, Verbosity.SILENT):
            log_mock = Mock()
            with OutputCapture(separate=True):
                APPLICATION.verbosity = settings
                echo(message, Verbosity.DEBUG, log=log_mock)

            self.assertEqual(log_mock.mock_calls, [call(message)])


@messenger_command()
def test_command():
    pass


class MessengerCommandTest(TestCase):
    def setUp(self):
        self.runner = CliRunner(env={ENVIRON_NAME: None})

        # Delete settings for tests to check loading.
        self.old_settings = APPLICATION.settings
        APPLICATION.settings = None  # type: ignore[assignment]

    def tearDown(self):
        APPLICATION.settings = self.old_settings

    def test_help(self):
        result = self.runner.invoke(test_command, ['--help'])

        self.assertEqual(result.exit_code, 0)
        self.assertIn('--help', result.stdout)

    def test_version(self):
        result = self.runner.invoke(test_command, ['--version'])

        self.assertEqual(result.exit_code, 0)
        self.assertIn('fred-messenger ' + messenger.__version__, result.stdout)

    def test_config(self):
        db_connection = 'file://example/connection'
        config_file = tempfile.NamedTemporaryFile(mode='w')
        yaml.dump({'db_connection': db_connection}, config_file)

        with override_environ():
            result = self.runner.invoke(test_command, ['--config', config_file.name])

        self.assertEqual(APPLICATION.settings.db_connection, db_connection)
        self.assertEqual(result.exit_code, 0)

    def test_connection(self):
        db_connection = 'args://example/connection'

        result = self.runner.invoke(test_command, ['--connection', db_connection])

        self.assertEqual(APPLICATION.settings.db_connection, db_connection)
        self.assertEqual(result.exit_code, 0)

    def test_verbosity_long(self):
        result = self.runner.invoke(test_command, ['--verbosity', '3'])

        self.assertEqual(APPLICATION.verbosity, Verbosity.DEBUG)
        self.assertEqual(result.exit_code, 0)

    def test_verbosity_short(self):
        result = self.runner.invoke(test_command, ['-v', '3'])

        self.assertEqual(APPLICATION.verbosity, Verbosity.DEBUG)
        self.assertEqual(result.exit_code, 0)

    def test_group(self):
        # Test command can be set as a member of a click group.
        @click.group()
        def group():
            pass

        @messenger_command(command=group.command)
        def test_command():
            click.echo('Subgroup!')

        result = self.runner.invoke(group, ['test-command'])

        self.assertEqual(result.exit_code, 0)
        # In py36-postgres the output contains a spurious ResourceWarning.
        # Thus, we use assertIn instead of assertEqual to avoid that.
        self.assertIn('Subgroup!\n', result.stdout)


@click.command()
@method_option
def test_method_command(methods: List[TransportMethod]) -> None:
    """Simple command to test method_option."""
    click.echo("METHOD: {}".format([m.value for m in methods]))


class MethodOptionTest(TestCase):
    def setUp(self):
        self.runner = CliRunner(mix_stderr=False)

    def test_type_value(self):
        data: Iterable[Tuple[List[str], List[str], bool]] = (
            # args, methods, warning
            ([], ['email', 'sms', 'letter'], False),
            (['--method', 'all'], ['email', 'sms', 'letter'], False),
            (['--method', 'email'], ['email'], False),
            (['--method', 'sms'], ['sms'], False),
            (['--method', 'letter'], ['letter'], False),
            (['--method', 'email', '--method', 'letter'], ['email', 'letter'], False),
            (['--method', 'email', '--method', 'all'], ['email', 'sms', 'letter'], False),
            (['--method', 'all', '--method', 'email'], ['email', 'sms', 'letter'], False),
            (['--type', 'all'], ['email', 'sms', 'letter'], True),
            (['--type', 'email'], ['email'], True),
            (['--type', 'sms'], ['sms'], True),
            (['--type', 'letter'], ['letter'], True),
            (['--type', 'email', '--type', 'letter'], ['email', 'letter'], True),
            (['--type', 'email', '--type', 'all'], ['email', 'sms', 'letter'], True),
            (['--type', 'all', '--type', 'email'], ['email', 'sms', 'letter'], True),
        )
        for args, output, warning in data:
            with self.subTest(args=args):
                result = self.runner.invoke(test_method_command, args)

                self.assertEqual(result.exit_code, 0)
                self.assertIn('METHOD: {}\n'.format(output), result.stdout)
                if warning:
                    self.assertEqual(result.stderr, "Option --type is deprecated, use --method instead.\n")
                else:
                    self.assertEqual(result.stderr, "")

    def test_invalid(self):
        result = self.runner.invoke(test_method_command, ['--method', 'invalid'])

        self.assertNotEqual(result.exit_code, 0)
        self.assertIn("Invalid value for '--method'", result.stderr)

    def test_invalid_type(self):
        result = self.runner.invoke(test_method_command, ['--type', 'invalid'])

        self.assertNotEqual(result.exit_code, 0)
        self.assertIn("Invalid value for '--type'", result.stderr)

    def test_mismatch(self):
        result = self.runner.invoke(test_method_command, ['--method', 'email', '--type', 'sms'])

        self.assertNotEqual(result.exit_code, 0)
        self.assertIn("Options --type and --method can't be both set.", result.stderr)


@click.command()
@limits_options
def test_limits_command(from_datetime: Optional[datetime], to_datetime: Optional[datetime]) -> None:
    """Simple command to test limits_options."""
    click.echo("FROM: {}".format(from_datetime))
    click.echo("TO: {}".format(to_datetime))


class LimitsOptionsTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()

    def test_limits_values(self):
        data: Iterable[Tuple[Sequence[str], str]] = (
            # args, output
            ([], 'FROM: None\nTO: None\n'),
            (['--from', '2020-03-01'], 'FROM: 2020-03-01 00:00:00+00:00\nTO: None\n'),
            (['--to', '2020-03-02'], 'FROM: None\nTO: 2020-03-02 00:00:00+00:00\n'),
            (['--from', '2020-03-01', '--to', '2020-03-02'],
             'FROM: 2020-03-01 00:00:00+00:00\nTO: 2020-03-02 00:00:00+00:00\n'),
        )
        for args, output in data:
            with self.subTest(type=type):
                result = self.runner.invoke(test_limits_command, args)

                self.assertEqual(result.exit_code, 0)
                self.assertEqual(result.stdout, output)

    def test_invalid(self):
        result = self.runner.invoke(test_limits_command, ['--from', 'invalid'])

        self.assertNotEqual(result.exit_code, 0)
        self.assertIn("Invalid value for '--from'", result.stdout)
