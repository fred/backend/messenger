import unittest
from datetime import date, datetime
from typing import Any, Dict, Iterable, List, Optional, Sequence, Tuple, Type, Union, cast
from unittest import TestCase
from unittest.mock import Mock, _Call, call, sentinel
from uuid import UUID

from fred_api.messenger.common_types_pb2 import (
    MessageStatus as ProtoMessageStatus,
    ReferenceType as ProtoReferenceType,
    Uuid,
)
from fred_api.messenger.email_pb2 import Email as ProtoEmail, EmailEnvelope
from fred_api.messenger.letter_pb2 import Address as ProtoAddress, Letter as ProtoLetter, LetterEnvelope
from fred_api.messenger.service_email_grpc_pb2 import (
    GetEmailReply,
    GetEmailRequest,
    ListEmailReply,
    ListEmailRequest,
    MultiSendEmailReply,
    SendEmailReply,
    SendEmailRequest,
)
from fred_api.messenger.service_letter_grpc_pb2 import (
    GetLetterReply,
    GetLetterRequest,
    ListLetterReply,
    ListLetterRequest,
    SendLetterReply,
    SendLetterRequest,
)
from fred_api.messenger.service_sms_grpc_pb2 import (
    GetSmsReply,
    GetSmsRequest,
    ListSmsReply,
    ListSmsRequest,
    SendSmsReply,
    SendSmsRequest,
)
from fred_api.messenger.sms_pb2 import Sms as ProtoSms, SmsEnvelope
from google.protobuf.message import Message
from grpc import StatusCode
from pytz import UTC, timezone
from sqlalchemy import null, text
from testfixtures import LogCapture, StringComparison

from messenger.constants import ArchiveMessageStatus, MessageStatus, ReferenceType
from messenger.database import get_engine
from messenger.models import (
    Address,
    AnyRecord,
    ArchiveEmail,
    ArchiveLetter,
    ArchiveSms,
    Email,
    Letter,
    Sms,
    SmsReference,
)
from messenger.service import (
    EmailServicer,
    LetterServicer,
    MessageServicerMixin,
    MessengerDecoder,
    Q,
    SmsServicer,
    TemplateMessageServicerMixin,
    Uid,
)
from messenger.settings import APPLICATION

from .utils import make_archive_email, make_email, make_letter, make_sms, override_database, test_session_cls


class UidTest(TestCase):
    def test_create_date(self):
        data = (
            # uid, create_date
            (Uid(UUID(int=42)), None),
            (Uid(UUID(int=42), date(2019, 12, 11)), date(2019, 12, 11)),
            (Uid(UUID(int=42), datetime(2019, 12, 11)), date(2019, 12, 11)),
            (Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC)), date(2019, 12, 11)),
            # Check the datetime in another timezone with a difference in a date.
            (Uid(UUID(int=42), timezone('Europe/Prague').localize(datetime(2019, 12, 11, 0, 15))),
             date(2019, 12, 10)),
        )
        for uid, create_date in data:
            with self.subTest(uid=uid):
                self.assertEqual(uid.create_date, create_date)

    def test_str(self):
        data = (
            # uid, value
            (Uid(UUID(int=42)), '00000000-0000-0000-0000-00000000002a'),
            (Uid(UUID(int=42), date(2019, 12, 11)), '00000000-0000-0000-0000-00000000002a.2019-12-11'),
            (Uid(UUID(int=42), datetime(2019, 12, 11)), '00000000-0000-0000-0000-00000000002a.2019-12-11'),
            (Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC)), '00000000-0000-0000-0000-00000000002a.2019-12-11'),
            # Check the datetime in another timezone with a difference in a date.
            (Uid(UUID(int=42), timezone('Europe/Prague').localize(datetime(2019, 12, 11, 0, 15))),
             '00000000-0000-0000-0000-00000000002a.2019-12-10'),
        )
        for uid, value in data:
            with self.subTest(uid=uid):
                self.assertEqual(str(uid), value)

    def test_parse(self):
        data = (
            # value, uid
            ('0000000000000000000000000000002a', Uid(UUID(int=42))),
            ('00000000-0000-0000-0000-00000000002a', Uid(UUID(int=42))),
            ('00000000-0000-0000-0000-00000000002a.2019-12-11', Uid(UUID(int=42), date(2019, 12, 11))),
            # deprecated format
            ('2019-12-11T00:00:00+00:00#00000000-0000-0000-0000-00000000002a',
             Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC))),
            ('2019-12-11 00:00:00+00:00#00000000-0000-0000-0000-00000000002a',
             Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC))),
            ('2019-12-11 00:00:00Z#00000000-0000-0000-0000-00000000002a',
             Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC))),
        )
        for value, uid in data:
            with self.subTest(value=value):
                self.assertEqual(Uid.parse(value), uid)

    def test_parse_invalid(self):
        data = (
            '',
            'INVALID',
            '0000-0000-0000-00000000002a',
            '00000000-0000-0000-0000-00000000002a.2019-13-11',
            '00000000-0000-0000-0000-00000000002a.2019-12-11T00:00:00',
            '2019-13-11T00:00:00+00:00#00000000-0000-0000-0000-00000000002a',
        )
        for value in data:
            with self.subTest(value=value):
                self.assertRaises(ValueError, Uid.parse, value)

    def test_from_record(self):
        data: Iterable[Tuple[AnyRecord, Uid]] = (
            # record, uid
            (Email(uuid=UUID(int=42), create_datetime=datetime(2019, 12, 11, tzinfo=UTC)),
             Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC))),
            (ArchiveEmail(uuid=UUID(int=42), create_datetime=datetime(2019, 12, 11, tzinfo=UTC)),
             Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC))),
            (Sms(uuid=UUID(int=42), create_datetime=datetime(2019, 12, 11, tzinfo=UTC)),
             Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC))),
            (ArchiveSms(uuid=UUID(int=42), create_datetime=datetime(2019, 12, 11, tzinfo=UTC)),
             Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC))),
            (Letter(uuid=UUID(int=42), create_datetime=datetime(2019, 12, 11, tzinfo=UTC)),
             Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC))),
            (ArchiveLetter(uuid=UUID(int=42), create_datetime=datetime(2019, 12, 11, tzinfo=UTC)),
             Uid(UUID(int=42), datetime(2019, 12, 11, tzinfo=UTC))),
        )
        for record, uid in data:
            with self.subTest(record=record):
                self.assertEqual(Uid.from_record(record), uid)


class MessengerDecoderTest(unittest.TestCase):
    def test_decode_uuid_empty(self):
        message = Uuid()

        decoder = MessengerDecoder()
        self.assertIsNone(decoder.decode(message))

    def test_decode_uuid(self):
        uuid = UUID(int=42)
        message = Uuid()
        message.value = str(uuid)

        decoder = MessengerDecoder()
        self.assertEqual(decoder.decode(message), uuid)

    def test_decode_uuid_invalid(self):
        message = Uuid()
        message.value = 'invalid-uuid'

        decoder = MessengerDecoder()
        with self.assertRaises(ValueError):
            decoder.decode(message)


class TestMessageServicer(MessageServicerMixin):
    """Simple message service for tests.

    Use SMS models as the simplest there are.
    """

    message_cls = Sms
    archive_message_cls = ArchiveSms
    message_proto_cls = ProtoSms
    envelope_proto_cls = SmsEnvelope
    get_reply_proto_cls = GetSmsReply
    list_reply_proto_cls = ListSmsReply
    send_reply_proto_cls = SendSmsReply

    def _filter_recipients(self, query: Q, model: Union[Type[Sms], Type[ArchiveSms]],
                           recipients: Iterable) -> Q:
        return query  # pragma: no cover

    def _decode_send_request(self, request: Message) -> Dict[str, Any]:
        model_kwargs = super()._decode_send_request(request)
        model_kwargs['body_template'] = 'example.txt'
        return model_kwargs


@override_database()
class MessageServicerMixinTest(unittest.TestCase):
    recipient = '+123456'

    def setUp(self):
        self.session = test_session_cls()()
        self.service = TestMessageServicer()
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()
        self.session.close()

    def _make_message(self, *, recipient: str, status: MessageStatus = MessageStatus.PENDING,
                      references: Optional[Iterable[SmsReference]] = None, **kwargs: Any) -> Sms:
        message = Sms(recipient=recipient, status=status, body_template='placeholder.txt', **kwargs)
        for reference in references or ():
            message.references.append(reference)
        self.session.add(message)
        self.session.commit()
        return message

    def _get_message(self, recipient: str = '', type: str = '') -> ProtoSms:
        message = ProtoSms()
        message.recipient = recipient
        message.type = type
        return message

    def _get_envelope(
            self, uid: str, create_datetime: datetime, message: ProtoSms, send_datetime: Optional[datetime] = None,
            delivery_datetime: Optional[datetime] = None, attempts: int = 0,
            status: ProtoMessageStatus = ProtoMessageStatus.pending, archive: bool = False,
            references: Optional[Dict[Tuple[str, int], List[str]]] = None) -> SmsEnvelope:
        envelope = SmsEnvelope()
        envelope.uid.value = str(uid)
        envelope.create_datetime.FromDatetime(create_datetime)
        if send_datetime:
            envelope.send_datetime.FromDatetime(send_datetime)
        if delivery_datetime:
            envelope.delivery_datetime.FromDatetime(delivery_datetime)
        envelope.attempts = attempts
        envelope.status = status
        envelope.message_data.CopyFrom(message)
        envelope.archive = archive
        for (ref_type, old_ref_type), values in (references or {}).items():
            for value in values:
                envelope.references.add(old_type=old_ref_type, type=ref_type, value=value)
        return envelope

    def test_get_empty(self):
        request = GetSmsRequest()
        request.uid.value = ''
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.get(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.INVALID_ARGUMENT, "'' is not a valid UID")])

    def test_get_invalid(self):
        request = GetSmsRequest()
        request.uid.value = 'INVALID_UID'
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.get(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls,
                         [call.abort(StatusCode.INVALID_ARGUMENT, "'INVALID_UID' is not a valid UID")])

    def test_get_db_error(self):
        # Test DB error occurs on database level - dropped table
        sql = "DROP TABLE {}".format(Sms.__tablename__)
        if not APPLICATION.settings.db_connection.startswith('sqlite'):
            sql += ' CASCADE'
        with get_engine().begin() as conn:
            conn.execute(text(sql))
        request = GetSmsRequest()
        request.uid.value = str(UUID(int=42)) + '.2019-12-11'
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.get(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls,
                         [call.abort(StatusCode.FAILED_PRECONDITION, StringComparison("Database error: .*"))])

    def test_get_unknown(self):
        uuid = UUID(int=42)
        uid = str(uuid) + '.2019-12-11'
        request = GetSmsRequest()
        request.uid.value = uid
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.get(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls,
                         [call.abort(StatusCode.NOT_FOUND, "'{}' not found.".format(uid))])

    def _test_get(self, model_kwargs: Optional[Dict] = None, envelope_kwargs: Optional[Dict] = None,
                  message_kwargs: Optional[Dict] = None) -> None:
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        recipient = 'kryten@example.com'
        model_kwargs = dict({
                'uuid': uuid, 'create_datetime': create_datetime, 'recipient': recipient,
            }, **(model_kwargs or {}))
        self._make_message(**model_kwargs)

        uid = str(uuid) + '.2019-12-11'
        request = GetSmsRequest()
        request.uid.value = uid
        result = self.service.get(request, sentinel.context)

        reply = GetSmsReply()
        message = self._get_message(**(message_kwargs or {}))
        envelope_kwargs = dict({
                'uid': uid, 'create_datetime': create_datetime, 'message': message, 'archive': True,
            }, **(envelope_kwargs or {}))
        reply.data.envelope.CopyFrom(self._get_envelope(**envelope_kwargs))
        self.assertEqual(result, reply)

    def test_get(self):
        self._test_get()

    def test_get_send_datetime(self):
        send_datetime = datetime(2019, 12, 24, 23, 22, 21, tzinfo=UTC)
        self._test_get(model_kwargs={'send_datetime': send_datetime}, envelope_kwargs={'send_datetime': send_datetime})

    def test_get_delivery_datetime(self):
        delivery_datetime = datetime(2019, 12, 25, 7, 8, 9, tzinfo=UTC)
        self._test_get(model_kwargs={'delivery_datetime': delivery_datetime},
                       envelope_kwargs={'delivery_datetime': delivery_datetime})

    def test_get_attempts(self):
        attempts = 255
        self._test_get(model_kwargs={'attempts': attempts}, envelope_kwargs={'attempts': attempts})

    def test_get_status(self):
        self._test_get(model_kwargs={'status': MessageStatus.DELIVERED},
                       envelope_kwargs={'status': ProtoMessageStatus.delivered})

    def test_get_type(self):
        self._test_get(model_kwargs={'type': 'gossip'}, message_kwargs={'type': 'gossip'})

    def test_get_archive(self):
        archive = True
        self._test_get(model_kwargs={'archive': archive}, envelope_kwargs={'archive': archive})

    def test_get_references(self):
        references = [SmsReference(type=ReferenceType.CONTACT, value='rimmer')]
        proto_references = {('contact', ProtoReferenceType.contact): ['rimmer']}
        self._test_get(model_kwargs={'references': references},
                       envelope_kwargs={'references': proto_references})

    def test_get_references_other(self):
        references = [SmsReference(type='person', value='rimmer')]
        proto_references = {('person', ProtoReferenceType.other): ['rimmer']}
        self._test_get(model_kwargs={'references': references},
                       envelope_kwargs={'references': proto_references})

    def test_get_default_datetime_sqlite(self):
        # Regression for default value of create_datetime isn't found by query in SQLite database.
        uuid = UUID(int=42)
        recipient = 'kryten@example.com'
        message = self._make_message(uuid=uuid, recipient=recipient)
        create_datetime = message.create_datetime
        uid = str(uuid) + '.' + str(create_datetime.date())
        request = GetSmsRequest()
        request.uid.value = uid
        result = self.service.get(request, sentinel.context)

        reply = GetSmsReply()
        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True)
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(result, reply)

    def test_get_archived(self):
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        recipient = 'kryten@example.com'
        message = ArchiveSms(id=42, uuid=uuid, create_datetime=create_datetime, status=ArchiveMessageStatus.SENT,
                             recipient=recipient, body_template='placeholder.txt', body_template_uuid=UUID(int=31))
        self.session.add(message)
        self.session.commit()

        uid = str(uuid) + '.2019-12-11'
        request = GetSmsRequest()
        request.uid.value = uid
        result = self.service.get(request, sentinel.context)

        reply = GetSmsReply()
        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True,
                                      status=ProtoMessageStatus.sent)
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(result, reply)

    def test_list_empty(self):
        request = ListSmsRequest()

        result = self.service.list(request, sentinel.context)

        self.assertEqual(tuple(result), ())

    def test_list_db_error(self):
        # Test DB error occurs on database level - dropped table
        sql = "DROP TABLE {}".format(Sms.__tablename__)
        if not APPLICATION.settings.db_connection.startswith('sqlite'):
            sql += ' CASCADE'
        with get_engine().begin() as conn:
            conn.execute(text(sql))
        request = ListSmsRequest()
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            tuple(self.service.list(request, context))

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls,
                         [call.abort(StatusCode.FAILED_PRECONDITION, StringComparison("Database error: .*"))])

    def test_list(self):
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        uid = str(uuid) + '.2019-12-11'
        recipient = 'kryten@example.com'
        self._make_message(uuid=uuid, create_datetime=create_datetime, recipient=recipient)
        request = ListSmsRequest()

        result = self.service.list(request, sentinel.context)

        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_archive(self):
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        uid = str(uuid) + '.2019-12-11'
        recipient = 'kryten@example.com'
        message = ArchiveSms(id=1, uuid=uuid, create_datetime=create_datetime, status=ArchiveMessageStatus.SENT,
                             recipient=recipient, body_template='placeholder.txt', body_template_uuid=UUID(int=31))
        self.session.add(message)
        self.session.commit()
        request = ListSmsRequest()

        result = self.service.list(request, sentinel.context)

        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message,
                                      status=ProtoMessageStatus.sent, archive=True)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_created_from(self):
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        uid = str(uuid) + '.2019-12-11'
        recipient = 'kryten@example.com'
        self._make_message(uuid=uuid, create_datetime=create_datetime, recipient=recipient)
        uuid_2 = UUID(int=43)
        create_datetime_2 = datetime(2019, 12, 11, 10, 9, 7, tzinfo=UTC)
        self._make_message(uuid=uuid_2, create_datetime=create_datetime_2, recipient=recipient)
        request = ListSmsRequest()
        request.created_from.FromDatetime(create_datetime)

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_created_to(self):
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        recipient = 'kryten@example.com'
        self._make_message(uuid=uuid, create_datetime=create_datetime, recipient=recipient)
        uuid_2 = UUID(int=43)
        create_datetime_2 = datetime(2019, 12, 11, 10, 9, 7, tzinfo=UTC)
        uid = str(uuid_2) + '.2019-12-11'
        self._make_message(uuid=uuid_2, create_datetime=create_datetime_2, recipient=recipient)
        request = ListSmsRequest()
        request.created_to.FromDatetime(create_datetime)

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime_2, message=message, archive=True)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_references(self):
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        uid = str(uuid) + '.2019-12-11'
        recipient = 'kryten@example.com'
        ref = 'KRYTEN'
        self._make_message(uuid=uuid, create_datetime=create_datetime, recipient=recipient,
                           references=[SmsReference(type=ReferenceType.CONTACT, value=ref)])
        uuid_2 = UUID(int=43)
        ref_2 = 'ARNOLD'
        self._make_message(uuid=uuid_2, create_datetime=create_datetime, recipient=recipient,
                           references=[SmsReference(type=ReferenceType.CONTACT, value=ref_2)])
        request = ListSmsRequest()
        # Old type is ignored.
        request.references.add(old_type=ProtoReferenceType.domain, type='contact', value=ref)

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True,
                                      references={('contact', ProtoReferenceType.contact): [ref]})
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_references_old(self):
        # Test list references with only an old_type present.
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        uid = str(uuid) + '.2019-12-11'
        recipient = 'kryten@example.com'
        ref = 'KRYTEN'
        self._make_message(uuid=uuid, create_datetime=create_datetime, recipient=recipient,
                           references=[SmsReference(type=ReferenceType.CONTACT, value=ref)])
        uuid_2 = UUID(int=43)
        ref_2 = 'ARNOLD'
        self._make_message(uuid=uuid_2, create_datetime=create_datetime, recipient=recipient,
                           references=[SmsReference(type=ReferenceType.CONTACT, value=ref_2)])
        request = ListSmsRequest()
        request.references.add(old_type=ProtoReferenceType.contact, value=ref)

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True,
                                      references={('contact', ProtoReferenceType.contact): [ref]})
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_references_other(self):
        # Test list references with only an old_type present and set to other.
        request = ListSmsRequest()
        request.references.add(old_type=ProtoReferenceType.other, value='KRYTEN')
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            tuple(self.service.list(request, context))

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(
            context.mock_calls,
            [call.abort(StatusCode.INVALID_ARGUMENT, "Reference has no valid type defined.")])

    def test_list_references_any(self):
        # Test all messages with at least one of references are returned.
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        uid = str(uuid) + '.2019-12-11'
        recipient = 'kryten@example.com'
        ref = 'KRYTEN'
        self._make_message(uuid=uuid, create_datetime=create_datetime, recipient=recipient,
                           references=[SmsReference(type=ReferenceType.CONTACT, value=ref)])
        uuid_2 = UUID(int=43)
        uid_2 = str(uuid_2) + '.2019-12-11'
        ref_2 = 'ARNOLD'
        self._make_message(uuid=uuid_2, create_datetime=create_datetime, recipient=recipient,
                           references=[SmsReference(type=ReferenceType.CONTACT, value=ref_2)])
        request = ListSmsRequest()
        request.references.add(type='contact', value=ref)
        request.references.add(type='contact', value=ref_2)

        result = self.service.list(request, sentinel.context)

        # Check both messages are returned.
        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True,
                                      references={('contact', ProtoReferenceType.contact): [ref]})
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        message_2 = self._get_message()
        envelope_2 = self._get_envelope(uid=uid_2, create_datetime=create_datetime, message=message_2, archive=True,
                                        references={('contact', ProtoReferenceType.contact): [ref_2]})
        reply_2 = ListSmsReply()
        reply_2.data.envelope.CopyFrom(envelope_2)
        self.assertEqual(tuple(result), (reply, reply_2))

    def test_list_limit(self):
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        uid = str(uuid) + '.2019-12-11'
        recipient = 'kryten@example.com'
        self._make_message(uuid=uuid, create_datetime=create_datetime, recipient=recipient)
        uuid_2 = UUID(int=43)
        create_datetime_2 = datetime(2019, 12, 11, 10, 9, 7, tzinfo=UTC)
        self._make_message(uuid=uuid_2, create_datetime=create_datetime_2, recipient=recipient)
        request = ListSmsRequest()
        request.limit = 1

        result = self.service.list(request, sentinel.context)

        # Check only newer message is returned.
        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_types(self):
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        uid = str(uuid) + '.2019-12-11'
        recipient = 'kryten@example.com'
        type = 'cleaning'
        self._make_message(uuid=uuid, create_datetime=create_datetime, recipient=recipient, type=type)
        uuid_2 = UUID(int=43)
        type_2 = 'cooking'
        self._make_message(uuid=uuid_2, create_datetime=create_datetime, recipient=recipient, type=type_2)
        request = ListSmsRequest()
        request.types.append(type)

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_message(type=type)
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_default_datetime_sqlite(self):
        # Regression for default value of create_datetime isn't found by query in SQLite database.
        uuid = UUID(int=42)
        recipient = 'kryten@example.com'
        message = self._make_message(uuid=uuid, recipient=recipient)
        create_datetime = message.create_datetime
        uid = str(uuid) + '.' + str(create_datetime.date())
        request = ListSmsRequest()
        result = self.service.list(request, sentinel.context)

        message = self._get_message()
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def _test_send_error(self, request: SendSmsRequest, calls: Sequence[_Call]) -> None:
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.send(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls, calls)

    def test_send_empty(self):
        request = SendSmsRequest()
        self._test_send_error(request, [call.abort(StatusCode.INVALID_ARGUMENT, "Recipient is missing.")])

    def test_send_db_error(self):
        # Test DB error occurs on database level - dropped table
        sql = "DROP TABLE {}".format(Sms.__tablename__)
        if not APPLICATION.settings.db_connection.startswith('sqlite'):
            sql += ' CASCADE'
        with get_engine().begin() as conn:
            conn.execute(text(sql))
        request = SendSmsRequest()
        request.message_data.recipient = '+1234'
        self._test_send_error(request,
                              [call.abort(StatusCode.FAILED_PRECONDITION, StringComparison("Database error: .*"))])

    def _test_send_internal(self, request: SendEmailRequest) -> Sms:
        # Utility to test send method.
        result = self.service.send(request, sentinel.context)

        # Check model is stored
        model = cast(Sms, self.session.query(Sms).one())
        self.assertEqual(model.recipient, self.recipient)

        # Check reply
        reply = SendSmsReply()
        reply.data.uid.value = str(model.uuid) + '.' + str(model.create_datetime.date())
        self.assertEqual(result, reply)

        return model

    def _test_send(self, send_kwargs: Optional[Dict] = None, model_kwargs: Optional[Dict] = None) -> None:
        request = SendSmsRequest()
        request.message_data.CopyFrom(self._get_message(recipient=self.recipient, **(send_kwargs or {})))

        model = self._test_send_internal(request)

        # Check stored attributes.
        self.assertTrue(model.archive)
        for key, value in (model_kwargs or {}).items():
            self.assertEqual(getattr(model, key), value)

    def test_send(self):
        self._test_send()

    def test_send_type(self):
        type = 'gossip'
        self._test_send(send_kwargs={'type': type}, model_kwargs={'type': type})

    def _test_send_archive(self, archive: bool) -> None:
        request = SendSmsRequest()
        request.message_data.CopyFrom(self._get_message(recipient=self.recipient))
        request.archive = archive

        model = self._test_send_internal(request)

        self.assertEqual(model.archive, archive)

    def test_send_archive(self):
        self._test_send_archive(True)

    def test_send_not_archive(self):
        self._test_send_archive(False)

    def test_send_references(self):
        reference = 'rimmer'

        request = SendSmsRequest()
        request.message_data.CopyFrom(self._get_message(recipient=self.recipient))
        # Old type is ignored.
        request.references.add(old_type=ProtoReferenceType.domain, type='contact', value=reference)

        model = self._test_send_internal(request)

        # Check stored references.
        self.assertEqual([(r.type, r.value) for r in model.references], [(ReferenceType.CONTACT, reference)])

    def test_send_references_old(self):
        # Test send references with only an old_type present.
        reference = 'rimmer'

        request = SendSmsRequest()
        request.message_data.CopyFrom(self._get_message(recipient=self.recipient))
        request.references.add(old_type=ProtoReferenceType.contact, value=reference)

        model = self._test_send_internal(request)

        # Check stored references.
        self.assertEqual([(r.type, r.value) for r in model.references], [(ReferenceType.CONTACT, reference)])

    def test_send_references_other(self):
        # Test send references with only an old_type present and set to other.
        reference = 'rimmer'

        request = SendSmsRequest()
        request.message_data.CopyFrom(self._get_message(recipient=self.recipient))
        request.references.add(old_type=ProtoReferenceType.other, value=reference)

        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.send(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(
            context.mock_calls,
            [call.abort(StatusCode.INVALID_ARGUMENT, "Reference has no valid type defined.")])


class TestTemplateMessageServicer(TemplateMessageServicerMixin):
    """Simple template message service for tests.

    Use SMS models as the simplest there are.
    """

    message_cls = Sms
    archive_message_cls = ArchiveSms
    message_proto_cls = ProtoSms
    envelope_proto_cls = SmsEnvelope
    get_reply_proto_cls = GetSmsReply
    list_reply_proto_cls = ListSmsReply
    send_reply_proto_cls = SendSmsReply

    def _filter_recipients(self, query: Q, model: Union[Type[Sms], Type[ArchiveSms]],
                           recipients: Iterable) -> Q:
        return query  # pragma: no cover


@override_database()
class TemplateMessageServicerMixinTest(unittest.TestCase):
    recipient = '+123456'
    body_template = 'body.txt'

    def setUp(self):
        self.session = test_session_cls()()
        self.service = TestTemplateMessageServicer()
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()
        self.session.close()

    def _make_message(self, *, recipient: str, body_template: str, status: MessageStatus = MessageStatus.PENDING,
                      **kwargs: Any) -> Sms:
        message = Sms(recipient=recipient, status=status, body_template=body_template, **kwargs)
        self.session.add(message)
        self.session.commit()
        return message

    def _get_message(self, recipient: str = '', body_template: str = '', context: Optional[Dict] = None,
                     body_template_uuid: Optional[UUID] = None) -> ProtoSms:
        message = ProtoSms()
        message.recipient = recipient
        message.body_template = body_template
        message.context.update(context or {})
        if body_template_uuid is not None:
            message.body_template_uuid.value = str(body_template_uuid)
        return message

    def _get_envelope(
            self, uid: str, create_datetime: datetime, message: ProtoSms, attempts: int = 0,
            status: ProtoMessageStatus = ProtoMessageStatus.pending, archive: bool = False) -> SmsEnvelope:
        envelope = SmsEnvelope()
        envelope.uid.value = str(uid)
        envelope.create_datetime.FromDatetime(create_datetime)
        envelope.attempts = attempts
        envelope.status = status
        envelope.message_data.CopyFrom(message)
        envelope.archive = archive
        return envelope

    def _test_get(self, model_kwargs: Optional[Dict] = None, envelope_kwargs: Optional[Dict] = None,
                  message_kwargs: Optional[Dict] = None) -> None:
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        recipient = 'kryten@example.com'
        body_template = 'body.txt'
        model_kwargs = dict({
                'uuid': uuid, 'create_datetime': create_datetime, 'recipient': recipient,
                'body_template': body_template,
            }, **(model_kwargs or {}))
        self._make_message(**model_kwargs)

        uid = str(uuid) + '.2019-12-11'
        request = GetSmsRequest()
        request.uid.value = uid
        result = self.service.get(request, sentinel.context)

        reply = GetSmsReply()
        message_kwargs = dict({'body_template': body_template}, **(message_kwargs or {}))
        message = self._get_message(**message_kwargs)
        envelope_kwargs = dict({
                'uid': uid, 'create_datetime': create_datetime, 'message': message, 'archive': True,
            }, **(envelope_kwargs or {}))
        reply.data.envelope.CopyFrom(self._get_envelope(**envelope_kwargs))
        self.assertEqual(result, reply)

    def test_get_context(self):
        context = {'secret': 'Gazpacho!'}
        self._test_get(model_kwargs={'context': context}, message_kwargs={'context': context})

    def test_get_body_template_uuid(self):
        body_template_uuid = UUID(int=197)
        self._test_get(model_kwargs={'body_template_uuid': body_template_uuid},
                       message_kwargs={'body_template_uuid': body_template_uuid})

    def test_list_body_templates(self):
        uuid = UUID(int=42)
        create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
        uid = str(uuid) + '.2019-12-11'
        recipient = 'kryten@example.com'
        body_template = 'body.txt'
        self._make_message(uuid=uuid, create_datetime=create_datetime, recipient=recipient, body_template=body_template)
        uuid_2 = UUID(int=43)
        body_template_2 = 'another.txt'
        self._make_message(uuid=uuid_2, create_datetime=create_datetime, recipient=recipient,
                           body_template=body_template_2)
        request = ListSmsRequest()
        request.body_templates.append(body_template)

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_message(body_template=body_template)
        envelope = self._get_envelope(uid=uid, create_datetime=create_datetime, message=message, archive=True)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def _test_send_invalid(self, request, message):
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.send(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.INVALID_ARGUMENT, message)])

    def test_send_missing_body(self):
        request = SendSmsRequest()
        request.message_data.recipient = '+1234'
        message = "Body is missing."
        self._test_send_invalid(request, message)

    def test_send_invalid_body_uuid(self):
        request = SendSmsRequest()
        request.message_data.recipient = '+1234'
        request.message_data.body_template = 'subject.txt'
        request.message_data.body_template_uuid.value = 'invalid-uuid'
        message = StringComparison("Decoding error: .*")
        self._test_send_invalid(request, message)

    def _test_send(self, send_kwargs: Optional[Dict] = None, model_kwargs: Optional[Dict] = None) -> None:
        request = SendSmsRequest()
        request.message_data.CopyFrom(
            self._get_message(recipient=self.recipient, body_template=self.body_template, **(send_kwargs or {})))
        result = self.service.send(request, sentinel.context)

        # Check model is stored
        model = self.session.query(Sms).one()
        self.assertEqual(model.recipient, self.recipient)
        self.assertEqual(model.body_template, self.body_template)
        for key, value in (model_kwargs or {}).items():
            self.assertEqual(getattr(model, key), value)

        # Check reply
        reply = SendSmsReply()
        reply.data.uid.value = str(model.uuid) + '.' + str(model.create_datetime.date())
        self.assertEqual(result, reply)

    def test_send_body_template(self):
        self._test_send()

    def test_send_body_template_uuid(self):
        body_template_uuid = UUID(int=197)
        self._test_send(send_kwargs={'body_template_uuid': body_template_uuid},
                        model_kwargs={'body_template_uuid': body_template_uuid})


@override_database()
class EmailServiceTest(unittest.TestCase):
    uuid = UUID(int=42)
    create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
    uid = str(uuid) + '.2019-12-11'
    recipient = 'kryten@example.com'
    subject_template = 'subject.txt'
    body_template = 'body.txt'
    message_id = '<test-message@example.com>'

    def setUp(self):
        self.session = test_session_cls()()
        self.service = EmailServicer()
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()
        self.session.close()

    def _make_email(self, **kwargs: Any) -> Email:
        kwargs = dict({
                'uuid': self.uuid, 'create_datetime': self.create_datetime, 'message_id': self.message_id,
                'recipient': self.recipient, 'subject_template': self.subject_template,
                'body_template': self.body_template,
            }, **(kwargs or {}))
        return make_email(self.session, **kwargs)

    def _get_email_message(
            self, sender: str = '', recipient: str = recipient, extra_headers: Optional[Dict] = None, type: str = '',
            subject_template: str = subject_template, body_template: str = body_template, body_template_html: str = '',
            context: Optional[Dict] = None, attachments: Optional[Iterable[UUID]] = None,
            subject_template_uuid: Optional[UUID] = None, body_template_html_uuid: Optional[UUID] = None) -> ProtoEmail:
        message = ProtoEmail()
        message.sender = sender
        message.recipient = recipient
        message.extra_headers.update(**(extra_headers or {}))
        message.type = type
        message.subject_template = subject_template
        message.body_template = body_template
        message.body_template_html = body_template_html
        message.context.update(context or {})
        for uuid in (attachments or ()):
            message.attachments.add(value=str(uuid))
        if subject_template_uuid is not None:
            message.subject_template_uuid.value = str(subject_template_uuid)
        if body_template_html_uuid is not None:
            message.body_template_html_uuid.value = str(body_template_html_uuid)
        return message

    def _get_envelope(
            self, message: ProtoEmail, uid: str = uid, create_datetime: datetime = create_datetime, attempts: int = 0,
            status: ProtoMessageStatus = ProtoMessageStatus.pending, archive: bool = True,
            message_id: Optional[str] = message_id) -> EmailEnvelope:
        envelope = EmailEnvelope()
        envelope.uid.value = str(uid)
        envelope.create_datetime.FromDatetime(create_datetime)
        envelope.attempts = attempts
        envelope.status = status
        envelope.message_data.CopyFrom(message)
        envelope.archive = archive
        if message_id:
            envelope.message_id.value = message_id
        return envelope

    def _test_get(self, email_kwargs: Optional[Dict] = None, envelope_kwargs: Optional[Dict] = None,
                  message_kwargs: Optional[Dict] = None) -> None:
        uid = str(self.uuid) + '.' + str(self.create_datetime.date())
        self._make_email(**(email_kwargs or {}))

        request = GetEmailRequest()
        request.uid.value = uid
        result = self.service.get(request, sentinel.context)

        reply = GetEmailReply()
        message = self._get_email_message(**(message_kwargs or {}))
        reply.data.envelope.CopyFrom(self._get_envelope(message=message, **(envelope_kwargs or {})))
        self.assertEqual(result, reply)

    def test_get_message_id(self):
        message_id = '<Gazpacho@example.com>'
        self._test_get(email_kwargs={'message_id': message_id}, envelope_kwargs={'message_id': message_id})

    def test_get_message_id_null(self):
        # Test email where Message-ID is NULL.
        make_archive_email(self.session, id=42, uuid=self.uuid, create_datetime=self.create_datetime, message_id=null(),
                           recipient=self.recipient, subject_template=self.subject_template,
                           body_template=self.body_template)
        uid = str(self.uuid) + '.' + str(self.create_datetime.date())

        request = GetEmailRequest()
        request.uid.value = uid
        result = self.service.get(request, sentinel.context)

        reply = GetEmailReply()
        message = self._get_email_message()
        envelope = self._get_envelope(message=message, status=ProtoMessageStatus.sent, message_id=None)
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(result, reply)

    def test_get_sender(self):
        sender = 'holly@example.com'
        self._test_get(email_kwargs={'sender': sender}, message_kwargs={'sender': sender})

    def test_get_extra_headers(self):
        extra_headers = {'cc': 'abel@example.com'}
        self._test_get(email_kwargs={'extra_headers': extra_headers}, message_kwargs={'extra_headers': extra_headers})

    def test_get_body_template_html(self):
        body_template_html = 'body.html'
        self._test_get(email_kwargs={'body_template_html': body_template_html},
                       message_kwargs={'body_template_html': body_template_html})

    def test_get_attachments(self):
        attachments = [UUID(int=255)]
        self._test_get(email_kwargs={'attachments': attachments}, message_kwargs={'attachments': attachments})

    def test_get_subject_template_uuid(self):
        subject_template_uuid = UUID(int=196)
        self._test_get(email_kwargs={'subject_template_uuid': subject_template_uuid},
                       message_kwargs={'subject_template_uuid': subject_template_uuid})

    def test_get_body_template_html_uuid(self):
        body_template_html = 'body.html'
        body_template_html_uuid = UUID(int=198)
        email_kwargs = {'body_template_html': body_template_html, 'body_template_html_uuid': body_template_html_uuid}
        message_kwargs = {'body_template_html': body_template_html, 'body_template_html_uuid': body_template_html_uuid}
        self._test_get(email_kwargs=email_kwargs, message_kwargs=message_kwargs)

    def test_list_recipients(self):
        self._make_email()
        uuid_2 = UUID(int=43)
        recipient_2 = 'rimmer@example.com'
        self._make_email(uuid=uuid_2, recipient=recipient_2, message_id='<other@example.org>')
        request = ListEmailRequest()
        request.recipients.append(self.recipient)

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_email_message()
        envelope = self._get_envelope(message=message)
        reply = ListEmailReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_recipients_multiple(self):
        self._make_email(recipient='rimmer@example.org, arnold@example.org')
        request = ListEmailRequest()
        request.recipients.append('rimmer@example.org')

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_email_message(recipient='rimmer@example.org, arnold@example.org')
        envelope = self._get_envelope(message=message)
        reply = ListEmailReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def _test_send_invalid(self, request, message):
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.send(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.INVALID_ARGUMENT, message)])

    def test_send_missing_subject(self):
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.com'
        message = "Subject is missing."
        self._test_send_invalid(request, message)

    def test_send_invalid_subject_uuid(self):
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.com'
        request.message_data.subject_template = 'subject.txt'
        request.message_data.body_template = 'body.txt'
        request.message_data.subject_template_uuid.value = 'invalid-uuid'
        message = StringComparison("Decoding error: .*")
        self._test_send_invalid(request, message)

    def _test_send(self, send_kwargs: Optional[Dict] = None, email_kwargs: Optional[Dict] = None,
                   envelope_kwargs: Optional[Dict] = None) -> None:
        request = SendEmailRequest()
        request.message_data.CopyFrom(self._get_email_message(**(send_kwargs or {})))
        result = self.service.send(request, sentinel.context)

        # Check email is stored
        email = self.session.query(Email).one()
        self.assertEqual(email.recipient, (email_kwargs or {}).get('recipient', self.recipient))
        self.assertEqual(email.subject_template, self.subject_template)
        self.assertEqual(email.body_template, self.body_template)
        self.assertTrue(email.archive)
        for key, value in (email_kwargs or {}).items():
            self.assertEqual(getattr(email, key), value)

        # Check reply
        reply = SendEmailReply()
        reply.data.uid.value = str(email.uuid) + '.' + str(email.create_datetime.date())
        self.assertEqual(result, reply)

    def test_send_recipient_unicode(self):
        self._test_send(send_kwargs={'recipient': 'kryten@🤖.example.org'},
                        email_kwargs={'recipient': 'kryten@🤖.example.org'})

    def test_send_more_recipients(self):
        self._test_send(send_kwargs={'recipient': 'kryten@example.org, holly@example.org'},
                        email_kwargs={'recipient': 'kryten@example.org, holly@example.org'})

    def test_send_more_recipients_invalid(self):
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.org holly@example.org'
        request.message_data.subject_template = 'subject.txt'
        request.message_data.body_template = 'body.txt'
        message = StringComparison("Invalid recipient 'kryten@example.org holly@example.org': .*")
        self._test_send_invalid(request, message)

    def test_send_sender(self):
        sender = 'holly@example.com'
        self._test_send(send_kwargs={'sender': sender}, email_kwargs={'sender': sender})

    def test_send_extra_headers(self):
        extra_headers = {'cc': 'abel@example.com'}
        self._test_send(send_kwargs={'extra_headers': extra_headers}, email_kwargs={'extra_headers': extra_headers})

    def test_send_body_template_html(self):
        body_template_html = 'body.html'
        self._test_send(send_kwargs={'body_template_html': body_template_html},
                        email_kwargs={'body_template_html': body_template_html})

    def test_send_context(self):
        context = {'secret': 'Gazpacho!'}
        self._test_send(send_kwargs={'context': context}, email_kwargs={'context': context})

    def test_send_attachments(self):
        attachments = [UUID(int=255)]
        self._test_send(send_kwargs={'attachments': attachments}, email_kwargs={'attachments': attachments})

    def test_send_subject_template_uuid(self):
        subject_template_uuid = UUID(int=196)
        self._test_send(send_kwargs={'subject_template_uuid': subject_template_uuid},
                        email_kwargs={'subject_template_uuid': subject_template_uuid})

    def test_send_body_template_html_uuid(self):
        body_template_html = 'body.html'
        body_template_html_uuid = UUID(int=198)
        send_kwargs = {'body_template_html': body_template_html, 'body_template_html_uuid': body_template_html_uuid}
        email_kwargs = {'body_template_html': body_template_html, 'body_template_html_uuid': body_template_html_uuid}
        self._test_send(send_kwargs=send_kwargs, email_kwargs=email_kwargs)

    def _test_batch_send_invalid(self, requests: Iterable[SendEmailRequest], message: Union[str, StringComparison],
                                 ) -> None:
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            tuple(self.service.batch_send(iter(requests), context))

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.INVALID_ARGUMENT, message)])

        # Check no email is stored
        self.assertEqual(self.session.query(Email).all(), [])

    def test_batch_send_missing_subject(self):
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.com'
        message = "Subject is missing."
        self._test_batch_send_invalid((request, ), message)

    def test_batch_send_invalid_subject_uuid(self):
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.com'
        request.message_data.subject_template = 'subject.txt'
        request.message_data.body_template = 'body.txt'
        request.message_data.subject_template_uuid.value = 'invalid-uuid'
        message = StringComparison("Decoding error: .*")
        self._test_batch_send_invalid((request, ), message)

    def test_batch_send_invalid_multiple(self):
        # Test two messages, second without a subject.
        request_1 = SendEmailRequest()
        request_1.message_data.CopyFrom(self._get_email_message())
        request_2 = SendEmailRequest()
        request_2.message_data.recipient = 'kryten@example.com'
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            tuple(self.service.batch_send(iter((request_1, request_2)), context))

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.INVALID_ARGUMENT, "Subject is missing.")])

        # Check no email is stored
        self.assertEqual(self.session.query(Email).all(), [])

    def test_batch_send_db_error(self):
        # Test DB error occurs on database level - dropped table
        sql = "DROP TABLE {}".format(Email.__tablename__)
        if not APPLICATION.settings.db_connection.startswith('sqlite'):
            sql += ' CASCADE'
        with get_engine().begin() as conn:
            conn.execute(text(sql))
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.com'
        request.message_data.subject_template = 'subject.txt'
        request.message_data.body_template = 'body.txt'

        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            tuple(self.service.batch_send(iter((request, )), context))

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls,
                         [call.abort(StatusCode.FAILED_PRECONDITION, StringComparison("Database error: .*"))])

    def _test_batch_send(self, send_kwargs: Optional[Dict] = None, email_kwargs: Optional[Dict] = None,
                         envelope_kwargs: Optional[Dict] = None) -> None:
        request = SendEmailRequest()
        request.message_data.CopyFrom(self._get_email_message(**(send_kwargs or {})))

        result = tuple(self.service.batch_send(iter((request, )), sentinel.context))

        # Check email is stored
        email = self.session.query(Email).one()
        self.assertEqual(email.recipient, (email_kwargs or {}).get('recipient', self.recipient))
        self.assertEqual(email.subject_template, self.subject_template)
        self.assertEqual(email.body_template, self.body_template)
        self.assertTrue(email.archive)
        for key, value in (email_kwargs or {}).items():
            self.assertEqual(getattr(email, key), value)

        # Check reply
        reply = SendEmailReply()
        reply.data.uid.value = str(email.uuid) + '.' + str(email.create_datetime.date())
        self.assertEqual(result, (reply, ))

    def _test_batch_send_archive(self, archive: bool) -> None:
        request = SendEmailRequest()
        request.message_data.CopyFrom(self._get_email_message())
        request.archive = archive

        result = tuple(self.service.batch_send(iter((request, )), sentinel.context))

        # Check email is stored
        email = self.session.query(Email).one()
        self.assertEqual(email.recipient, self.recipient)
        self.assertEqual(email.archive, archive)

        # Check reply
        reply = SendEmailReply()
        reply.data.uid.value = str(email.uuid) + '.' + str(email.create_datetime.date())
        self.assertEqual(result, (reply, ))

    def test_batch_send_archive(self):
        self._test_batch_send_archive(True)

    def test_batch_send_not_archive(self):
        self._test_batch_send_archive(False)

    def test_batch_send_recipient_unicode(self):
        self._test_batch_send(send_kwargs={'recipient': 'kryten@🤖.example.org'},
                              email_kwargs={'recipient': 'kryten@🤖.example.org'})

    def test_batch_send_more_recipients(self):
        # Test single message with multiple recipients.
        self._test_batch_send(send_kwargs={'recipient': 'kryten@example.org, holly@example.org'},
                              email_kwargs={'recipient': 'kryten@example.org, holly@example.org'})

    def test_batch_send_multiple(self):
        # Test actual batch message send.
        request_1 = SendEmailRequest()
        request_1.message_data.CopyFrom(self._get_email_message(recipient='kryten@example.org'))
        request_2 = SendEmailRequest()
        request_2.message_data.CopyFrom(self._get_email_message(recipient='holly@example.org'))

        result = tuple(self.service.batch_send(iter((request_1, request_2)), sentinel.context))

        # Check emails are stored
        emails = self.session.query(Email).all()
        self.assertEqual(len(emails), 2)
        email_kryten = [e for e in emails if e.recipient == 'kryten@example.org'][0]
        email_holly = [e for e in emails if e.recipient == 'holly@example.org'][0]
        self.assertEqual(email_kryten.subject_template, self.subject_template)
        self.assertEqual(email_kryten.body_template, self.body_template)
        self.assertTrue(email_kryten.archive)
        self.assertEqual(email_holly.subject_template, self.subject_template)
        self.assertEqual(email_holly.body_template, self.body_template)
        self.assertTrue(email_holly.archive)

        # Check reply
        reply_1 = SendEmailReply()
        reply_1.data.uid.value = str(email_kryten.uuid) + '.' + str(email_kryten.create_datetime.date())
        reply_2 = SendEmailReply()
        reply_2.data.uid.value = str(email_holly.uuid) + '.' + str(email_holly.create_datetime.date())
        self.assertEqual(result, (reply_1, reply_2))

    def test_batch_send_more_recipients_invalid(self):
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.org holly@example.org'
        request.message_data.subject_template = 'subject.txt'
        request.message_data.body_template = 'body.txt'
        message = StringComparison("Invalid recipient 'kryten@example.org holly@example.org': .*")
        self._test_batch_send_invalid((request, ), message)

    def test_batch_send_sender(self):
        sender = 'holly@example.com'
        self._test_batch_send(send_kwargs={'sender': sender}, email_kwargs={'sender': sender})

    def test_batch_send_extra_headers(self):
        extra_headers = {'cc': 'abel@example.com'}
        self._test_batch_send(send_kwargs={'extra_headers': extra_headers},
                              email_kwargs={'extra_headers': extra_headers})

    def test_batch_send_body_template_html(self):
        body_template_html = 'body.html'
        self._test_batch_send(send_kwargs={'body_template_html': body_template_html},
                              email_kwargs={'body_template_html': body_template_html})

    def test_batch_send_context(self):
        context = {'secret': 'Gazpacho!'}
        self._test_batch_send(send_kwargs={'context': context}, email_kwargs={'context': context})

    def test_batch_send_attachments(self):
        attachments = [UUID(int=255)]
        self._test_batch_send(send_kwargs={'attachments': attachments}, email_kwargs={'attachments': attachments})

    def test_batch_send_subject_template_uuid(self):
        subject_template_uuid = UUID(int=196)
        self._test_batch_send(send_kwargs={'subject_template_uuid': subject_template_uuid},
                              email_kwargs={'subject_template_uuid': subject_template_uuid})

    def test_batch_send_body_template_html_uuid(self):
        body_template_html = 'body.html'
        body_template_html_uuid = UUID(int=198)
        send_kwargs = {'body_template_html': body_template_html, 'body_template_html_uuid': body_template_html_uuid}
        email_kwargs = {'body_template_html': body_template_html, 'body_template_html_uuid': body_template_html_uuid}
        self._test_batch_send(send_kwargs=send_kwargs, email_kwargs=email_kwargs)

    def _test_multi_send_invalid(self, request, message):
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.multi_send(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.INVALID_ARGUMENT, message)])

    def test_multi_send_missing_subject(self):
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.com'
        message = "Subject is missing."
        self._test_multi_send_invalid(request, message)

    def test_multi_send_invalid_subject_uuid(self):
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.com'
        request.message_data.subject_template = 'subject.txt'
        request.message_data.body_template = 'body.txt'
        request.message_data.subject_template_uuid.value = 'invalid-uuid'
        message = StringComparison("Decoding error: .*")
        self._test_multi_send_invalid(request, message)

    def test_send_db_error(self):
        # Test DB error occurs on database level - dropped table
        sql = "DROP TABLE {}".format(Email.__tablename__)
        if not APPLICATION.settings.db_connection.startswith('sqlite'):
            sql += ' CASCADE'
        with get_engine().begin() as conn:
            conn.execute(text(sql))
        request = SendEmailRequest()
        request.message_data.recipient = 'kryten@example.com'
        request.message_data.subject_template = 'subject.txt'
        request.message_data.body_template = 'body.txt'

        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.multi_send(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls,
                         [call.abort(StatusCode.FAILED_PRECONDITION, StringComparison("Database error: .*"))])

    def _test_multi_send(self, send_kwargs: Optional[Dict] = None, email_kwargs: Optional[Dict] = None,
                         envelope_kwargs: Optional[Dict] = None) -> None:
        request = SendEmailRequest()
        request.message_data.CopyFrom(self._get_email_message(**(send_kwargs or {})))
        result = self.service.multi_send(request, sentinel.context)

        # Check email is stored
        email = self.session.query(Email).one()
        self.assertEqual(email.recipient, (email_kwargs or {}).get('recipient', self.recipient))
        self.assertEqual(email.subject_template, self.subject_template)
        self.assertEqual(email.body_template, self.body_template)
        self.assertTrue(email.archive)
        for key, value in (email_kwargs or {}).items():
            self.assertEqual(getattr(email, key), value)

        # Check reply
        reply = MultiSendEmailReply()
        email_info = MultiSendEmailReply.EmailInfo()
        email_info.uid.value = str(email.uuid) + '.' + str(email.create_datetime.date())
        email_info.recipient = (email_kwargs or {}).get('recipient', self.recipient)
        reply.data.emails.append(email_info)
        self.assertEqual(result, reply)

    def _test_multi_send_archive(self, archive: bool) -> None:
        request = SendEmailRequest()
        request.message_data.CopyFrom(self._get_email_message())
        request.archive = archive

        result = self.service.multi_send(request, sentinel.context)

        # Check email is stored
        email = self.session.query(Email).one()
        self.assertEqual(email.recipient, self.recipient)
        self.assertEqual(email.archive, archive)

        # Check reply
        reply = MultiSendEmailReply()
        email_info = MultiSendEmailReply.EmailInfo()
        email_info.uid.value = str(email.uuid) + '.' + str(email.create_datetime.date())
        email_info.recipient = self.recipient
        reply.data.emails.append(email_info)
        self.assertEqual(result, reply)

    def test_multi_send_archive(self):
        self._test_multi_send_archive(True)

    def test_multi_send_not_archive(self):
        self._test_multi_send_archive(False)

    def test_multi_send_recipient_unicode(self):
        self._test_multi_send(send_kwargs={'recipient': 'kryten@🤖.example.org'},
                              email_kwargs={'recipient': 'kryten@🤖.example.org'})

    def test_multi_send_more_recipients(self):
        # Test each of the recipients will get it's own email.
        request = SendEmailRequest()
        request.message_data.CopyFrom(self._get_email_message(recipient='kryten@example.org, holly@example.org'))

        result = self.service.multi_send(request, sentinel.context)

        # Check emails are stored
        emails = self.session.query(Email).all()
        self.assertEqual(len(emails), 2)
        email_kryten = [e for e in emails if e.recipient == 'kryten@example.org'][0]
        email_holly = [e for e in emails if e.recipient == 'holly@example.org'][0]
        self.assertEqual(email_kryten.subject_template, self.subject_template)
        self.assertEqual(email_kryten.body_template, self.body_template)
        self.assertTrue(email_kryten.archive)
        self.assertEqual(email_holly.subject_template, self.subject_template)
        self.assertEqual(email_holly.body_template, self.body_template)
        self.assertTrue(email_holly.archive)

        # Check reply
        reply = MultiSendEmailReply()
        email_info_1 = MultiSendEmailReply.EmailInfo()
        email_info_1.uid.value = str(email_kryten.uuid) + '.' + str(email_kryten.create_datetime.date())
        email_info_1.recipient = 'kryten@example.org'
        reply.data.emails.append(email_info_1)
        email_info_2 = MultiSendEmailReply.EmailInfo()
        email_info_2.uid.value = str(email_holly.uuid) + '.' + str(email_holly.create_datetime.date())
        email_info_2.recipient = 'holly@example.org'
        reply.data.emails.append(email_info_2)
        self.assertEqual(result, reply)

    def test_multi_send_more_recipients_invalid(self):
        # Test invalid recipients addresses are skipped.
        self._test_multi_send(send_kwargs={'recipient': 'kryten@example.org holly@example.org'},
                              email_kwargs={'recipient': 'kryten@example.org'})

    def test_multi_send_more_recipients_all_invalid(self):
        # Test request with all invalid recipients returns error.
        request = SendEmailRequest()
        request.message_data.recipient = 'křýten@example.com, římmer@example.com'
        request.message_data.subject_template = 'example.txt'
        request.message_data.body_template = 'example.txt'
        message = "No valid recipient address found."
        self._test_multi_send_invalid(request, message)

    def test_multi_send_multi_sender(self):
        sender = 'holly@example.com'
        self._test_multi_send(send_kwargs={'sender': sender},
                              email_kwargs={'sender': sender})

    def test_multi_send_extra_headers(self):
        extra_headers = {'cc': 'abel@example.com'}
        self._test_multi_send(send_kwargs={'extra_headers': extra_headers},
                              email_kwargs={'extra_headers': extra_headers})

    def test_multi_send_body_template_html(self):
        body_template_html = 'body.html'
        self._test_multi_send(send_kwargs={'body_template_html': body_template_html},
                              email_kwargs={'body_template_html': body_template_html})

    def test_multi_send_context(self):
        context = {'secret': 'Gazpacho!'}
        self._test_multi_send(send_kwargs={'context': context}, email_kwargs={'context': context})

    def test_multi_send_attachments(self):
        attachments = [UUID(int=255)]
        self._test_multi_send(send_kwargs={'attachments': attachments}, email_kwargs={'attachments': attachments})

    def test_multi_send_subject_template_uuid(self):
        subject_template_uuid = UUID(int=196)
        self._test_multi_send(send_kwargs={'subject_template_uuid': subject_template_uuid},
                              email_kwargs={'subject_template_uuid': subject_template_uuid})

    def test_multi_send_body_template_html_uuid(self):
        body_template_html = 'body.html'
        body_template_html_uuid = UUID(int=198)
        send_kwargs = {'body_template_html': body_template_html, 'body_template_html_uuid': body_template_html_uuid}
        email_kwargs = {'body_template_html': body_template_html, 'body_template_html_uuid': body_template_html_uuid}
        self._test_multi_send(send_kwargs=send_kwargs, email_kwargs=email_kwargs)


@override_database()
class SmsServiceTest(unittest.TestCase):
    uuid = UUID(int=42)
    create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
    uid = str(uuid) + '.2019-12-11'
    recipient = '+42.123456'
    body_template = 'subject.txt'

    def setUp(self):
        self.session = test_session_cls()()
        self.service = SmsServicer()
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()
        self.session.close()

    def _make_sms(self, **kwargs: Any) -> Sms:
        kwargs = dict({
                'uuid': self.uuid, 'create_datetime': self.create_datetime, 'recipient': self.recipient,
                'body_template': self.body_template,
            }, **(kwargs or {}))
        return make_sms(self.session, **kwargs)

    def _get_sms_message(self, recipient: str = recipient, body_template: str = body_template,
                         context: Optional[Dict] = None) -> ProtoSms:
        message = ProtoSms()
        message.recipient = recipient
        message.body_template = body_template
        message.context.update(context or {})
        return message

    def _get_envelope(
            self, message: ProtoSms, uid: str = uid, create_datetime: datetime = create_datetime, attempts: int = 0,
            status: ProtoMessageStatus = ProtoMessageStatus.pending, archive: bool = True) -> SmsEnvelope:
        envelope = SmsEnvelope()
        envelope.uid.value = str(uid)
        envelope.create_datetime.FromDatetime(create_datetime)
        envelope.attempts = attempts
        envelope.status = status
        envelope.message_data.CopyFrom(message)
        envelope.archive = archive
        return envelope

    def test_get_recipient(self):
        self._make_sms()

        request = GetSmsRequest()
        request.uid.value = self.uid
        result = self.service.get(request, sentinel.context)

        reply = GetSmsReply()
        message = self._get_sms_message()
        reply.data.envelope.CopyFrom(self._get_envelope(message=message))
        self.assertEqual(result, reply)

    def test_list_recipients(self):
        self._make_sms()
        uuid_2 = UUID(int=43)
        recipient_2 = '+42.123789'
        self._make_sms(uuid=uuid_2, recipient=recipient_2)
        request = ListSmsRequest()
        request.recipients.append(self.recipient)

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_sms_message()
        envelope = self._get_envelope(message=message)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_recipients_ignore_dot(self):
        self._make_sms()
        request = ListSmsRequest()
        request.recipients.append('+42123456')  # Phone number without a dot.

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_sms_message()
        envelope = self._get_envelope(message=message)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_recipients_ignore_plus(self):
        self._make_sms(recipient='42.123456')  # Phone number without a plus.
        request = ListSmsRequest()
        request.recipients.append('+42.123456')

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_sms_message(recipient='42.123456')
        envelope = self._get_envelope(message=message)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_recipients_no_prefix(self):
        self._make_sms()
        request = ListSmsRequest()
        request.recipients.append('123456')  # Phone number without a prefix

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_sms_message()
        envelope = self._get_envelope(message=message)
        reply = ListSmsReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))


@override_database()
class LetterServiceTest(unittest.TestCase):
    uuid = UUID(int=42)
    create_datetime = datetime(2019, 12, 11, 10, 9, 8, tzinfo=UTC)
    uid = str(uuid) + '.2019-12-11'
    file_uuid = UUID(int=255)

    def setUp(self):
        self.session = test_session_cls()()
        self.service = LetterServicer()
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()
        self.session.close()

    def _make_letter(self, **kwargs: Any) -> Letter:
        kwargs = dict({'uuid': self.uuid, 'create_datetime': self.create_datetime, 'file': self.file_uuid},
                      **(kwargs or {}))
        return make_letter(self.session, **kwargs)

    def _get_letter_message(self, recipient: Optional[ProtoAddress] = None, file: UUID = file_uuid) -> ProtoLetter:
        message = ProtoLetter()
        if recipient:
            message.recipient.CopyFrom(recipient)
        message.file.value = str(file)
        return message

    def _get_envelope(
            self, message: ProtoLetter, uid: str = uid, create_datetime: datetime = create_datetime, attempts: int = 0,
            status: ProtoMessageStatus = ProtoMessageStatus.pending, archive: bool = True) -> LetterEnvelope:
        envelope = LetterEnvelope()
        envelope.uid.value = str(uid)
        envelope.create_datetime.FromDatetime(create_datetime)
        envelope.attempts = attempts
        envelope.status = status
        envelope.message_data.CopyFrom(message)
        envelope.archive = archive
        return envelope

    def _test_get_recipient(self, model_kwargs: Dict, recipient: ProtoAddress) -> None:
        self._make_letter(**model_kwargs)

        request = GetLetterRequest()
        request.uid.value = self.uid
        result = self.service.get(request, sentinel.context)

        reply = GetLetterReply()
        message = self._get_letter_message(recipient=recipient)
        reply.data.envelope.CopyFrom(self._get_envelope(message=message))
        self.assertEqual(result, reply)

    def test_get_recipient_name(self):
        name = 'Kryten'
        address = ProtoAddress(name=name)
        self._test_get_recipient(model_kwargs={'recipient_name': name}, recipient=address)

    def test_get_recipient_organization(self):
        organization = 'JMC'
        address = ProtoAddress(organization=organization)
        self._test_get_recipient(model_kwargs={'recipient_organization': organization}, recipient=address)

    def test_get_recipient_street(self):
        street = 'Deck 16'
        address = ProtoAddress(street=street)
        self._test_get_recipient(model_kwargs={'recipient_street': street}, recipient=address)

    def test_get_recipient_city(self):
        city = 'Red Dwarf'
        address = ProtoAddress(city=city)
        self._test_get_recipient(model_kwargs={'recipient_city': city}, recipient=address)

    def test_get_recipient_state(self):
        state = 'Deep universe'
        address = ProtoAddress(state_or_province=state)
        self._test_get_recipient(model_kwargs={'recipient_state': state}, recipient=address)

    def test_get_recipient_postal_code(self):
        postal_code = 'JMC'
        address = ProtoAddress(postal_code=postal_code)
        self._test_get_recipient(model_kwargs={'recipient_postal_code': postal_code}, recipient=address)

    def test_get_recipient_country(self):
        country = 'Jupiter'
        address = ProtoAddress(country=country)
        self._test_get_recipient(model_kwargs={'recipient_country': country}, recipient=address)

    def _test_list_recipients(self, address: Address, address_2: Address, query: str, recipient: str) -> None:
        self._make_letter(recipient=address)
        uuid_2 = UUID(int=43)
        self._make_letter(uuid=uuid_2, recipient=address_2)
        request = ListLetterRequest()
        request.recipients.append(query)

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_letter_message(recipient=recipient)
        envelope = self._get_envelope(message=message)
        reply = ListLetterReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_recipients(self):
        name = 'Kryten'
        address = Address(name=name)
        address_2 = Address(name='Holly')
        recipient = ProtoAddress(name=name)
        self._test_list_recipients(address, address_2, name, recipient)

    def test_list_recipients_case_insensitive(self):
        name = 'Kryten'
        address = Address(name=name)
        address_2 = Address(name='Holly')
        recipient = ProtoAddress(name=name)
        self._test_list_recipients(address, address_2, 'kryTEN', recipient)

    def test_list_recipients_substring(self):
        organization = 'Jupiter Mining Company'
        address = Address(organization=organization)
        address_2 = Address(organization='Diva-Droid International')
        recipient = ProtoAddress(organization=organization)
        self._test_list_recipients(address, address_2, 'Mining', recipient)

    def test_list_recipients_multiple_keys(self):
        name = 'Kryten'
        organization = 'Jupiter Mining Company'
        address = Address(name=name, organization=organization)
        address_2 = Address(organization=organization)
        recipient = ProtoAddress(name=name, organization=organization)
        self._test_list_recipients(address, address_2, 'Kryten Mining', recipient)

    def _test_list_recipients_old(self, address: Address, address_2: Address, query: ProtoAddress,
                                  recipient: ProtoAddress) -> None:
        self._make_letter(recipient=address)
        uuid_2 = UUID(int=43)
        self._make_letter(uuid=uuid_2, recipient=address_2)
        request = ListLetterRequest()
        request.recipients.append(query.SerializeToString())

        result = self.service.list(request, sentinel.context)

        # Check only correct message is returned.
        message = self._get_letter_message(recipient=recipient)
        envelope = self._get_envelope(message=message)
        reply = ListLetterReply()
        reply.data.envelope.CopyFrom(envelope)
        self.assertEqual(tuple(result), (reply, ))

    def test_list_recipients_old_name(self):
        name = 'Kryten'
        address = Address(name=name)
        address_2 = Address(name='Holly')
        query = ProtoAddress(name=name)
        self._test_list_recipients_old(address, address_2, query, query)

    def test_list_recipients_old_organization(self):
        organization = 'Jupiter Mining Company'
        address = Address(organization=organization)
        address_2 = Address(organization='Diva-Droid International')
        query = ProtoAddress(organization=organization)
        self._test_list_recipients_old(address, address_2, query, query)

    def test_list_recipients_old_street(self):
        street = 'Deck 16'
        address = Address(street=street)
        address_2 = Address(street='Deck B')
        query = ProtoAddress(street=street)
        self._test_list_recipients_old(address, address_2, query, query)

    def test_list_recipients_old_city(self):
        city = 'Red Dwarf'
        address = Address(city=city)
        address_2 = Address(city='Nova 5')
        query = ProtoAddress(city=city)
        self._test_list_recipients_old(address, address_2, query, query)

    def test_list_recipients_old_state(self):
        state = 'Deep universe'
        address = Address(state=state)
        address_2 = Address(state='Parallel universe')
        query = ProtoAddress(state_or_province=state)
        self._test_list_recipients_old(address, address_2, query, query)

    def test_list_recipients_old_postal_code(self):
        postal_code = 'JMC'
        address = Address(postal_code=postal_code)
        address_2 = Address(postal_code='DDI')
        query = ProtoAddress(postal_code=postal_code)
        self._test_list_recipients_old(address, address_2, query, query)

    def test_list_recipients_old_country(self):
        country = 'Jupiter'
        address = Address(country=country)
        address_2 = Address(country='TI')
        query = ProtoAddress(country=country)
        self._test_list_recipients_old(address, address_2, query, query)

    def test_list_recipients_old_complex(self):
        # Test search using two address parts.
        name = 'Kryten'
        city = 'Red Dwarf'
        address = Address(name=name, city=city)
        address_2 = Address(name=name)
        query = ProtoAddress(name=name, city=city)
        self._test_list_recipients_old(address, address_2, query, query)

    def _test_send(self, send_kwargs: Optional[Dict] = None, model_kwargs: Optional[Dict] = None) -> None:
        request = SendLetterRequest()
        request.message_data.CopyFrom(self._get_letter_message(file=self.file_uuid, **(send_kwargs or {})))
        result = self.service.send(request, sentinel.context)

        # Check model is stored
        model = self.session.query(Letter).one()
        self.assertEqual(model.file, self.file_uuid)
        for key, value in (model_kwargs or {}).items():
            self.assertEqual(getattr(model, key), value)

        # Check reply
        reply = SendLetterReply()
        reply.data.uid.value = str(model.uuid) + '.' + str(model.create_datetime.date())
        self.assertEqual(result, reply)

    def test_send(self):
        self._test_send()

    def test_send_recipient(self):
        send_kwargs = {'recipient': ProtoAddress(name='Kryten', organization='JMC', street='Deck 16', city='Red Dwarf',
                                                 state_or_province='Deep universe', postal_code='JMC',
                                                 country='Jupiter')}
        model_kwargs = {'recipient': Address(name='Kryten', organization='JMC', street='Deck 16', city='Red Dwarf',
                                             state='Deep universe', postal_code='JMC', country='Jupiter')}
        self._test_send(send_kwargs, model_kwargs)

    def _test_send_not_implemented(self, request: SendLetterRequest, message: str) -> None:
        # Test templates are not yet supported.
        context = Mock(name='context')
        context.abort.side_effect = Exception(sentinel.abort)

        with self.assertRaises(Exception) as catcher:
            self.service.send(request, context)

        self.assertEqual(catcher.exception.args, (sentinel.abort, ))
        self.assertEqual(context.mock_calls, [call.abort(StatusCode.INVALID_ARGUMENT, message)])

    def test_send_body_template_not_implemented(self):
        request = SendLetterRequest()
        request.message_data.body_template = 'example.txt'
        self._test_send_not_implemented(request, 'Field body_template not yet supported.')

    def test_send_body_template_uuid_not_implemented(self):
        request = SendLetterRequest()
        request.message_data.body_template_uuid.value = str(UUID(int=42))
        self._test_send_not_implemented(request, 'Field body_template_uuid not yet supported.')

    def test_send_context_not_implemented(self):
        request = SendLetterRequest()
        request.message_data.context.update({'example': 42})
        self._test_send_not_implemented(request, 'Field context not yet supported.')
