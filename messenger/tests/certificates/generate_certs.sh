#!/bin/sh
# Generate test CA
openssl genrsa -out test_ca.key 4096
chmod a+r test_ca.key
openssl req -x509 -new -nodes -key test_ca.key -sha256 -days 3650 -out test_ca.crt -subj "/CN=Fred-Messenger Test CA"

# Generate holly's certificate request
openssl genrsa -out holly.key 4096
chmod a+r holly.key
openssl req -new -key holly.key -out holly.csr -subj '/C=JU/ST=Jupiter Mining Company/L=Red Dwarf/CN=Holly' -addext 'subjectAltName = email:holly@example.org'
# Sign holly's request and create certificate
openssl x509 -req -in holly.csr -CA test_ca.crt -CAkey test_ca.key -out holly.crt -CAcreateserial -days 3650 -sha256 -extfile holly_smime.conf -extensions smime

# Sign holly's request and create certificate with invalid email address
openssl x509 -req -in holly.csr -CA test_ca.crt -CAkey test_ca.key -out holly-invalid.crt -CAserial test_ca.srl -days 3650 -sha256 -extfile holly-invalid_smime.conf -extensions smime

# Sign holly's request and create certificate with mixed case email address
openssl x509 -req -in holly.csr -CA test_ca.crt -CAkey test_ca.key -out holly-case.crt -CAserial test_ca.srl -days 3650 -sha256 -extfile holly-case_smime.conf -extensions smime

# Sign holly's request and create certificate with expanded email address
openssl x509 -req -in holly.csr -CA test_ca.crt -CAkey test_ca.key -out holly-expanded.crt -CAserial test_ca.srl -days 3650 -sha256 -extfile holly-expanded_smime.conf -extensions smime
