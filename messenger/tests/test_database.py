from tempfile import NamedTemporaryFile
from unittest import TestCase

from messenger.constants import MessageStatus
from messenger.database import atomic, get_engine, get_session_cls
from messenger.models import Email

from .utils import override_database


class GetEngineTest(TestCase):
    def setUp(self):
        self.tmp_db = NamedTemporaryFile()

    def test_result(self):
        with override_database(db_connection='sqlite:///{}'.format(self.tmp_db.name)):
            engine = get_engine()

        self.assertEqual(str(engine.url), 'sqlite:///{}'.format(self.tmp_db.name))

    def test_cached(self):
        with override_database(db_connection='sqlite:///{}'.format(self.tmp_db.name)):
            engine1 = get_engine()
            engine2 = get_engine()

        self.assertIs(engine1, engine2)


class GetSessionClsTest(TestCase):
    def setUp(self):
        self.tmp_db = NamedTemporaryFile()

    def test_result(self):
        with override_database(db_connection='sqlite:///{}'.format(self.tmp_db.name)):
            session_cls = get_session_cls()

        self.assertEqual(str(session_cls.kw['bind'].url), 'sqlite:///{}'.format(self.tmp_db.name))

    def test_cached(self):
        with override_database(db_connection='sqlite:///{}'.format(self.tmp_db.name)):
            cls1 = get_session_cls()
            cls2 = get_session_cls()

        self.assertIs(cls1, cls2)


class AtomicTest(TestCase):
    def test_commit(self):
        with override_database(db_connection='sqlite:///:memory:'):
            with atomic() as session:
                email = Email(recipient='kryten@example.org', status=MessageStatus.PENDING,
                              subject_template='subject.txt', body_template='body.txt')
                session.add(email)

            session = get_session_cls()()
            self.assertEqual(session.query(Email).one().recipient, 'kryten@example.org')

    def test_rollback(self):
        with override_database(db_connection='sqlite:///:memory:'):
            with self.assertRaisesRegex(Exception, 'Gazpacho!'):
                with atomic() as session:
                    email = Email(recipient='kryten@example.org', status=MessageStatus.PENDING,
                                  subject_template='subject.txt', body_template='body.txt')
                    session.add(email)
                    raise Exception('Gazpacho!')

            session = get_session_cls()()
            self.assertEqual(session.query(Email).count(), 0)
