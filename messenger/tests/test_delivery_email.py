from datetime import datetime
from email.message import EmailMessage, MIMEPart
from typing import Any, Dict, Optional, Type
from unittest import TestCase
from unittest.mock import call, patch, sentinel
from uuid import UUID

from freezegun import freeze_time
from imap_tools import AND, MailMessage, MailMessageFlags
from pytz import UTC
from testfixtures import LogCapture, StringComparison

from messenger.constants import ArchiveMessageStatus, MessageStatus
from messenger.delivery.email import (
    MIMETYPE_DSN,
    MIMETYPE_MESSAGE,
    MIMETYPE_MESSAGE_HEADERS,
    MIMETYPE_REPORT,
    REPORT_TYPE_DSN,
    ImapDelivery,
)
from messenger.models import ArchiveEmail, Email
from messenger.utils import Result

from .utils import make_archive_email, make_email, override_database, test_session_cls


@freeze_time('2020-02-01 14:00:00')
@override_database()
class ImapDeliveryTest(TestCase):
    recipient = 'kryten@example.org'
    subject_template = 'subject.txt'
    body_template = 'body.txt'
    message_id = '<original@example.org>'

    reply_id = '<reply@example.org>'
    reply_subject = 'Quagaars!'
    reply_date = datetime(2001, 2, 3, 4, tzinfo=UTC)

    def setUp(self):
        self.log_handler = LogCapture('messenger', propagate=False)
        self.session = test_session_cls()()

    def tearDown(self):
        self.session.close()
        self.log_handler.uninstall()

    def get_message(self, headers: Optional[Dict[str, Any]] = None) -> EmailMessage:
        message = EmailMessage()
        message['Subject'] = self.reply_subject
        message['Message-ID'] = self.reply_id
        for key, value in (headers or {}).items():
            message[key] = value
        return message

    def get_delivery_status(self, message_dsn_fields: Optional[Dict[str, Any]] = None,
                            recipient_dsn_fields: Optional[Dict[str, Any]] = None) -> MIMEPart:
        delivery_status = MIMEPart()
        delivery_status['Content-Type'] = MIMETYPE_DSN
        message_dsn = MIMEPart()
        for key, value in (message_dsn_fields or {}).items():
            message_dsn[key] = value
        delivery_status.attach(message_dsn)
        recipient_dsn = MIMEPart()
        for key, value in (recipient_dsn_fields or {}).items():
            recipient_dsn[key] = value
        delivery_status.attach(recipient_dsn)
        return delivery_status

    def get_returned_message(self, mimetype: str = MIMETYPE_MESSAGE, message_id: Optional[str] = message_id,
                             ) -> MIMEPart:
        # Create a returned message with message/rfc822 or text/rfc822-headers MIME type.
        original_message = MIMEPart()
        if message_id:
            original_message['Message-ID'] = message_id

        returned_message = MIMEPart()
        returned_message['Content-Type'] = mimetype
        returned_message.set_payload(str(original_message))
        return returned_message

    def get_dsn_message(self, headers: Optional[Dict[str, Any]] = None, delivery_status: Optional[MIMEPart] = None,
                        returned_message: Optional[MIMEPart] = None) -> EmailMessage:
        headers = headers or {}
        headers['Content-Type'] = '{}; report-type={}'.format(MIMETYPE_REPORT, REPORT_TYPE_DSN)
        message = self.get_message(headers)
        if delivery_status:
            message.attach(delivery_status)  # type: ignore[arg-type]
        if returned_message:
            message.attach(returned_message)  # type: ignore[arg-type]
        return message

    def test_get_mailbox_default(self):
        # Test mailbox created with default arguments.
        delivery = ImapDelivery()

        with patch('ssl.create_default_context', autospec=True, return_value=sentinel.context):
            with patch('messenger.delivery.email.MailBox', autospec=True) as imap_mock:
                imap_mock.return_value.mock_add_spec(('folder', ))
                self.assertEqual(delivery.get_mailbox(), imap_mock.return_value)

        self.assertEqual(imap_mock.mock_calls,
                         [call(host='', port=None, ssl_context=sentinel.context), call().folder.set('INBOX')])

    def test_get_mailbox_insecure(self):
        # Test mailbox created with default arguments.
        delivery = ImapDelivery(ssl=False)

        with patch('messenger.delivery.email.MailBoxUnencrypted', autospec=True) as imap_mock:
            imap_mock.return_value.mock_add_spec(('folder', ))
            self.assertEqual(delivery.get_mailbox(), imap_mock.return_value)

        self.assertEqual(imap_mock.mock_calls, [call(host='', port=None), call().folder.set('INBOX')])

    def test_get_mailbox_custom(self):
        # Test mailbox created with custom arguments.
        delivery = ImapDelivery(host=sentinel.host, port=sentinel.port, ssl=False, folder=sentinel.folder)

        with patch('messenger.delivery.email.MailBoxUnencrypted', autospec=True) as imap_mock:
            imap_mock.return_value.mock_add_spec(('folder', ))
            self.assertEqual(delivery.get_mailbox(), imap_mock.return_value)

        self.assertEqual(imap_mock.mock_calls,
                         [call(host=sentinel.host, port=sentinel.port), call().folder.set(sentinel.folder)])

    def test_get_mailbox_ssl(self):
        data = (
            # certfile, keyfile, verify, ssl_call
            (None, None, True, call(None, None, verify=True)),
            (None, None, False, call(None, None, verify=False)),
            (sentinel.certfile, None, True, call(sentinel.certfile, None, verify=True)),
            (sentinel.certfile, sentinel.keyfile, True, call(sentinel.certfile, sentinel.keyfile, verify=True)),
            (sentinel.certfile, sentinel.keyfile, False, call(sentinel.certfile, sentinel.keyfile, verify=False)),
        )
        for certfile, keyfile, verify, ssl_call in data:
            with self.subTest(certfile=certfile, keyfile=keyfile, verify=verify):
                delivery = ImapDelivery(host=sentinel.host, port=sentinel.port, keyfile=keyfile, certfile=certfile,
                                        verify=verify)

                with patch('messenger.delivery.email.create_ssl_context', autospec=True) as ssl_mock:
                    with patch('messenger.delivery.email.MailBox', autospec=True) as imap_mock:
                        imap_mock.return_value.mock_add_spec(('folder', ))
                        self.assertEqual(delivery.get_mailbox(), imap_mock.return_value)

                calls = [call(host=sentinel.host, port=sentinel.port, ssl_context=ssl_mock.return_value),
                         call().folder.set('INBOX')]
                self.assertEqual(imap_mock.mock_calls, calls)
                self.assertEqual(ssl_mock.mock_calls, [ssl_call])

    def test_get_mailbox_tls(self):
        data = (
            # certfile, keyfile, verify, ssl_call
            (None, None, True, call(None, None, verify=True)),
            (None, None, False, call(None, None, verify=False)),
            (sentinel.certfile, None, True, call(sentinel.certfile, None, verify=True)),
            (sentinel.certfile, sentinel.keyfile, True, call(sentinel.certfile, sentinel.keyfile, verify=True)),
            (sentinel.certfile, sentinel.keyfile, False, call(sentinel.certfile, sentinel.keyfile, verify=False)),
        )
        for certfile, keyfile, verify, ssl_call in data:
            with self.subTest(certfile=certfile, keyfile=keyfile, verify=verify):
                delivery = ImapDelivery(ssl=False, tls=True, certfile=certfile, keyfile=keyfile, verify=verify)

                with patch('messenger.delivery.email.create_ssl_context', autospec=True) as ssl_mock:
                    # Skip autospec=True because it doesn't match box property.
                    with patch('messenger.delivery.email.MailBoxTls') as imap_mock:
                        mailbox = delivery.get_mailbox()

                calls = [call(host='', port=None, ssl_context=ssl_mock.return_value),
                         call().folder.set('INBOX')]
                self.assertEqual(imap_mock.mock_calls, calls)
                self.assertEqual(ssl_mock.mock_calls, [ssl_call])
                # Check calls first, then mailbox. To avoid __eq__ call on mailbox.
                self.assertEqual(mailbox, imap_mock.return_value)

    def test_get_mailbox_login(self):
        # Test login. Ensure tls is started before login.
        delivery = ImapDelivery(ssl=False, tls=True, username=sentinel.username, password=sentinel.password)

        with patch('ssl.create_default_context', autospec=True, return_value=sentinel.context):
            # Skip autospec=True because it doesn't match box property.
            with patch('messenger.delivery.email.MailBoxTls') as imap_mock:
                delivery.get_mailbox()

        calls = [call(host='', port=None, ssl_context=sentinel.context),
                 call().login(username=sentinel.username, password=sentinel.password),
                 call().folder.set('INBOX')]
        self.assertEqual(imap_mock.mock_calls, calls)

    def test_is_dsn(self):
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_message()))

        self.assertFalse(delivery.is_dsn(message))

    def test_is_dsn_report(self):
        # Test message is a report, but not a delivery status
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_message({'Content-Type': MIMETYPE_REPORT})))

        self.assertFalse(delivery.is_dsn(message))

    def test_is_dsn_delivery(self):
        # Test message is a report, but not a delivery status
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_dsn_message()))

        self.assertTrue(delivery.is_dsn(message))

    def test_get_status(self):
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_message()))

        self.assertEqual(delivery.get_status(message), MessageStatus.DELIVERED)

    def test_get_status_auto_submitted_no(self):
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_message({'Auto-Submitted': 'no'})))

        self.assertEqual(delivery.get_status(message), MessageStatus.DELIVERED)

    def test_get_status_auto_submitted(self):
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_message({'Auto-Submitted': 'auto-generated'})))

        self.assertIsNone(delivery.get_status(message))

    def test_get_status_report(self):
        # Test message is a report, but not a delivery status
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_message({'Content-Type': MIMETYPE_REPORT})))

        self.assertIsNone(delivery.get_status(message))

    def test_get_status_delivery(self):
        # Test message is a delivery status, but does not contain `Action` field
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_dsn_message()))

        self.assertIsNone(delivery.get_status(message))

    def test_get_status_delivery_failed(self):
        # Test message is a delivery status with `Action` failed
        delivery = ImapDelivery()
        delivery_status = self.get_delivery_status(recipient_dsn_fields={'Action': 'failed'})
        message = MailMessage.from_bytes(bytes(self.get_dsn_message(delivery_status=delivery_status)))

        self.assertEqual(delivery.get_status(message), MessageStatus.UNDELIVERED)

    def test_get_status_delivery_delivered(self):
        # Test message is a delivery status with `Action` delivered
        delivery = ImapDelivery()
        delivery_status = self.get_delivery_status(recipient_dsn_fields={'Action': 'delivered'})
        message = MailMessage.from_bytes(bytes(self.get_dsn_message(delivery_status=delivery_status)))

        self.assertEqual(delivery.get_status(message), MessageStatus.DELIVERED)

    def test_get_status_delivery_other(self):
        # Test message is a delivery status with `Action` failed
        delivery = ImapDelivery()
        delivery_status = self.get_delivery_status(recipient_dsn_fields={'Action': 'delayed'})
        message = MailMessage.from_bytes(bytes(self.get_dsn_message(delivery_status=delivery_status)))

        self.assertIsNone(delivery.get_status(message))

    def test_get_message_id_none(self):
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_message()))

        self.assertIsNone(delivery.get_message_id(message))

    def test_get_message_id_reply(self):
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_message({'In-Reply-To': self.message_id})))

        self.assertEqual(delivery.get_message_id(message), self.message_id)

    def test_get_message_id_references(self):
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_message({'References': self.message_id})))

        self.assertEqual(delivery.get_message_id(message), self.message_id)

    def test_get_message_id_original(self):
        # Test Message-ID extracted from original message in DSN.
        delivery = ImapDelivery()
        returned_message = self.get_returned_message()
        message = MailMessage.from_bytes(bytes(self.get_dsn_message(returned_message=returned_message)))

        self.assertEqual(delivery.get_message_id(message), self.message_id)

    def test_get_message_id_original_id_not_found(self):
        # Test DSN with original message with no Message-ID.
        delivery = ImapDelivery()
        returned_message = self.get_returned_message(message_id=None)
        message = MailMessage.from_bytes(bytes(self.get_dsn_message(returned_message=returned_message)))

        self.assertIsNone(delivery.get_message_id(message))

    def test_get_message_id_original_headers(self):
        # Test Message-ID extracted from original message headers in DSN.
        delivery = ImapDelivery()
        returned_message = self.get_returned_message(mimetype=MIMETYPE_MESSAGE_HEADERS)
        message = MailMessage.from_bytes(bytes(self.get_dsn_message(returned_message=returned_message)))

        self.assertEqual(delivery.get_message_id(message), self.message_id)

    def test_get_message_id_original_headers_id_not_found(self):
        # Test DSN with original message headers with no Message-ID.
        delivery = ImapDelivery()
        returned_message = self.get_returned_message(mimetype=MIMETYPE_MESSAGE_HEADERS, message_id=None)
        message = MailMessage.from_bytes(bytes(self.get_dsn_message(returned_message=returned_message)))

        self.assertIsNone(delivery.get_message_id(message))

    def test_get_message_id_other_content(self):
        # Test multipart mail with other content.
        delivery = ImapDelivery()
        other_message = MIMEPart()
        message = MailMessage.from_bytes(bytes(self.get_dsn_message(returned_message=other_message)))

        self.assertIsNone(delivery.get_message_id(message))

    def test_get_dsn_fields_empty(self):
        delivery = ImapDelivery()
        message = MailMessage.from_bytes(bytes(self.get_dsn_message()))

        self.assertEqual(delivery.get_dsn_fields(message), {})

    def test_get_dsn_fields_all(self):
        delivery = ImapDelivery()
        message_dsn_fields = {'Reporting-MTA': 'dns; mail.example.org'}
        recipient_dsn_fields = {'Action': 'failed', 'Status': '5.1.1', 'Remote-MTA': 'dns; mx1.example.org',
                                'Diagnostic-Code': 'smtp; 550 5.1.1 Sorry, no mailbox here by that name.',
                                'Final-Recipient': 'rfc822; {}'.format(self.recipient)}
        delivery_status = self.get_delivery_status(message_dsn_fields=message_dsn_fields,
                                                   recipient_dsn_fields=recipient_dsn_fields)
        message = MailMessage.from_bytes(bytes(self.get_dsn_message(delivery_status=delivery_status)))

        self.assertEqual(delivery.get_dsn_fields(message), dict(**message_dsn_fields, **recipient_dsn_fields))

    def test_handle_message_unknown(self):
        # Test message with no identifiers does nothing.
        delivery = ImapDelivery()
        dsn_message = self.get_dsn_message(
            delivery_status=self.get_delivery_status(recipient_dsn_fields={'Action': 'failed'}))
        message = MailMessage.from_bytes(bytes(dsn_message))

        self.assertFalse(delivery.handle_message(message))

        self.assertEqual(self.session.query(Email).count(), 0)

    def test_handle_message_unspecific(self):
        # Test message, which doesn't provide information about delivery.
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        delivery = ImapDelivery()
        dsn_message = self.get_message({'In-Reply-To': self.message_id, 'Content-Type': MIMETYPE_REPORT})
        message = MailMessage.from_bytes(bytes(dsn_message))

        self.assertFalse(delivery.handle_message(message))

        email = self.session.query(Email).one()
        # Check email wasn't updated
        self.assertEqual(email.status, MessageStatus.SENT)
        self.assertIsNone(email.update_datetime)
        self.assertIsNone(email.delivery_datetime)
        self.assertIsNone(email.delivery_info)

    def test_handle_message_processed(self):
        # Test email is not updated if in other status
        make_email(self.session, recipient=self.recipient, status=MessageStatus.UNDELIVERED,
                   message_id=self.message_id, subject_template=self.subject_template, body_template=self.body_template)
        delivery = ImapDelivery()
        dsn_message = self.get_dsn_message(
            {'In-Reply-To': self.message_id},
            delivery_status=self.get_delivery_status(recipient_dsn_fields={'Action': 'failed'}))
        message = MailMessage.from_bytes(bytes(dsn_message))

        self.assertFalse(delivery.handle_message(message))

        email = self.session.query(Email).one()
        # Check email wasn't updated
        self.assertEqual(email.status, MessageStatus.UNDELIVERED)
        self.assertIsNone(email.update_datetime)
        self.assertIsNone(email.delivery_datetime)
        self.assertIsNone(email.delivery_info)

    def test_handle_message_processed_archive(self):
        # Test archive email is not updated if in other status
        make_archive_email(
            self.session, id=42, create_datetime=datetime(1999, 8, 7, 6, tzinfo=UTC), recipient=self.recipient,
            status=ArchiveMessageStatus.UNDELIVERED, message_id=self.message_id,
            subject_template=self.subject_template, subject_template_uuid=UUID(int=255),
            body_template=self.body_template, body_template_uuid=UUID(int=256))
        delivery = ImapDelivery()
        dsn_message = self.get_dsn_message(
            {'In-Reply-To': self.message_id},
            delivery_status=self.get_delivery_status(recipient_dsn_fields={'Action': 'failed'}))
        message = MailMessage.from_bytes(bytes(dsn_message))

        self.assertFalse(delivery.handle_message(message))

        email = self.session.query(ArchiveEmail).one()
        # Check email wasn't updated
        self.assertEqual(email.status, MessageStatus.UNDELIVERED)
        self.assertIsNone(email.update_datetime)
        self.assertIsNone(email.delivery_datetime)
        self.assertIsNone(email.delivery_info)

    def test_handle_message_headless(self):
        # Message with no useful headers.
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        delivery = ImapDelivery()
        dsn_message = EmailMessage()
        dsn_message['In-Reply-To'] = self.message_id
        message = MailMessage.from_bytes(bytes(dsn_message))

        self.assertFalse(delivery.handle_message(message))

        email = self.session.query(Email).one()
        # Check email was updated
        self.assertEqual(email.status, MessageStatus.SENT)
        self.assertIsNone(email.update_datetime)
        self.assertIsNone(email.delivery_datetime)
        self.assertIsNone(email.delivery_info)

    def test_handle_message_headless_delivered(self):
        # Message with no useful headers with mark delivered.
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        delivery = ImapDelivery(mark_delivered=True)
        dsn_message = EmailMessage()
        dsn_message['In-Reply-To'] = self.message_id
        message = MailMessage.from_bytes(bytes(dsn_message))

        self.assertTrue(delivery.handle_message(message))

        email = self.session.query(Email).one()
        # Check email was updated
        self.assertEqual(email.status, MessageStatus.DELIVERED)
        self.assertEqual(email.update_datetime, datetime(2020, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.delivery_datetime, datetime(2020, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.delivery_info, {})

    def _test_handle_message_delivered(self, dsn_message: EmailMessage, email_cls: Type,
                                       delivery_datetime: datetime = datetime(2020, 2, 1, 14, tzinfo=UTC)) -> None:
        # If the message references known Message-ID mark as delivered.
        delivery = ImapDelivery(mark_delivered=True)
        message = MailMessage.from_bytes(bytes(dsn_message))

        self.assertTrue(delivery.handle_message(message))

        email = self.session.query(email_cls).one()
        # Check email was updated
        self.assertEqual(email.status, MessageStatus.DELIVERED)
        self.assertEqual(email.update_datetime, datetime(2020, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.delivery_datetime, delivery_datetime)
        self.assertEqual(email.delivery_info, {'Message-ID': self.reply_id, 'Subject': self.reply_subject})

    def test_handle_message_delivered(self):
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        message = self.get_message({'In-Reply-To': self.message_id})
        self._test_handle_message_delivered(message, Email)

    def test_handle_message_delivered_archive(self):
        make_archive_email(
            self.session, id=42, create_datetime=datetime(1999, 8, 7, 6, tzinfo=UTC), recipient=self.recipient,
            message_id=self.message_id, subject_template=self.subject_template, subject_template_uuid=UUID(int=255),
            body_template=self.body_template, body_template_uuid=UUID(int=256))
        message = self.get_message({'In-Reply-To': self.message_id})
        self._test_handle_message_delivered(message, ArchiveEmail)

    def test_handle_message_delivered_skip(self):
        # If the message references known Message-ID mark as delivered, but mark_delivered is disabled.
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        delivery = ImapDelivery()
        dsn_message = self.get_message({'In-Reply-To': self.message_id})
        message = MailMessage.from_bytes(bytes(dsn_message))

        self.assertFalse(delivery.handle_message(message))

        email = self.session.query(Email).one()
        # Check email was updated
        self.assertEqual(email.status, MessageStatus.SENT)
        self.assertIsNone(email.update_datetime)
        self.assertIsNone(email.delivery_datetime)
        self.assertIsNone(email.delivery_info)

    def test_handle_message_delivered_date(self):
        # Test Date header is used as a delivery time
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        message = self.get_message({'In-Reply-To': self.message_id, 'Date': self.reply_date})
        self._test_handle_message_delivered(message, Email, delivery_datetime=self.reply_date)

    def test_handle_message_delivered_date_archive_fast(self):
        # Test search in archive on email with Date header (the fast query on postgres backend).
        make_archive_email(
            self.session, id=42, create_datetime=datetime(2001, 2, 1, 6, tzinfo=UTC), recipient=self.recipient,
            message_id=self.message_id, subject_template=self.subject_template, subject_template_uuid=UUID(int=255),
            body_template=self.body_template, body_template_uuid=UUID(int=256))
        message = self.get_message({'In-Reply-To': self.message_id, 'Date': self.reply_date})
        self._test_handle_message_delivered(message, ArchiveEmail, delivery_datetime=self.reply_date)

    def test_handle_message_delivered_date_archive_slow(self):
        # Test search in archive on old email with Date header (the slow query on postgres backend).
        make_archive_email(
            self.session, id=42, create_datetime=datetime(1999, 8, 7, 6, tzinfo=UTC), recipient=self.recipient,
            message_id=self.message_id, subject_template=self.subject_template, subject_template_uuid=UUID(int=255),
            body_template=self.body_template, body_template_uuid=UUID(int=256))
        message = self.get_message({'In-Reply-To': self.message_id, 'Date': self.reply_date})
        self._test_handle_message_delivered(message, ArchiveEmail, delivery_datetime=self.reply_date)

    def _test_handle_message_undelivered(
            self, email_cls: Type, headers: Optional[Dict[str, Any]] = None,
            delivery_datetime: datetime = datetime(2020, 2, 1, 14, tzinfo=UTC)) -> None:
        # If the message is undelivered delivery status.
        message_dsn_fields = {'Reporting-MTA': 'dns; mail.example.org'}
        recipient_dsn_fields = {'Action': 'failed', 'Status': '5.1.1'}
        delivery = ImapDelivery()
        delivery_status = self.get_delivery_status(message_dsn_fields=message_dsn_fields,
                                                   recipient_dsn_fields=recipient_dsn_fields)
        dsn_message = self.get_dsn_message(headers, delivery_status=delivery_status)
        message = MailMessage.from_bytes(bytes(dsn_message))

        self.assertTrue(delivery.handle_message(message))

        email = self.session.query(email_cls).one()
        # Check email was updated
        self.assertEqual(email.status, MessageStatus.UNDELIVERED)
        self.assertEqual(email.update_datetime, datetime(2020, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.delivery_datetime, delivery_datetime)
        delivery_info = dict({'Message-ID': self.reply_id, 'Subject': self.reply_subject},
                             **message_dsn_fields, **recipient_dsn_fields)
        self.assertEqual(email.delivery_info, delivery_info)

    def test_handle_message_undelivered(self):
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        self._test_handle_message_undelivered(Email, {'In-Reply-To': self.message_id})

    def test_handle_message_undelivered_archive(self):
        make_archive_email(
            self.session, id=42, create_datetime=datetime(1999, 8, 7, 6, tzinfo=UTC), recipient=self.recipient,
            message_id=self.message_id, subject_template=self.subject_template, subject_template_uuid=UUID(int=255),
            body_template=self.body_template, body_template_uuid=UUID(int=256))
        self._test_handle_message_undelivered(ArchiveEmail, {'In-Reply-To': self.message_id})

    def test_handle_message_undelivered_date(self):
        # Test Date header is used as a delivery time
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        self._test_handle_message_undelivered(Email, {'In-Reply-To': self.message_id, 'Date': self.reply_date},
                                              delivery_datetime=self.reply_date)

    def test_check_delivery_empty(self):
        delivery = ImapDelivery(ssl=False)

        with patch('messenger.delivery.email.MailBoxUnencrypted', autospec=True) as imap_mock:
            imap_mock.return_value.mock_add_spec(('folder', '__enter__', '__exit__'))
            imap_mock.return_value.__enter__.return_value.fetch.return_value = []

            self.assertEqual(delivery.check_delivery(), Result())

        calls = [call(host='', port=None),
                 call().folder.set('INBOX'),
                 call().__enter__(),
                 call().__enter__().fetch(AND(seen=False, deleted=False), mark_seen=False, limit=None, bulk=True),
                 call().__enter__().expunge(),
                 call().__exit__(None, None, None)]
        self.assertEqual(imap_mock.mock_calls, calls)

    def test_check_delivery(self):
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        delivery = ImapDelivery(ssl=False, mark_delivered=True)
        dsn_message = self.get_message({'In-Reply-To': self.message_id})
        message = MailMessage([(b'UID 42', bytes(dsn_message))])

        with patch('messenger.delivery.email.MailBoxUnencrypted', autospec=True) as imap_mock:
            imap_mock.return_value.mock_add_spec(('folder', '__enter__', '__exit__'))
            imap_mock.return_value.__enter__.return_value.fetch.return_value = [message]

            self.assertEqual(delivery.check_delivery(), Result(success=1))

        email = self.session.query(Email).one()
        # Check email was updated
        self.assertEqual(email.status, MessageStatus.DELIVERED)
        self.assertEqual(email.update_datetime, datetime(2020, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.delivery_datetime, datetime(2020, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.delivery_info, {'Message-ID': self.reply_id, 'Subject': self.reply_subject})

        calls = [call(host='', port=None),
                 call().folder.set('INBOX'),
                 call().__enter__(),
                 call().__enter__().fetch(AND(seen=False, deleted=False), mark_seen=False, limit=None, bulk=True),
                 call().__enter__().flag('42', [MailMessageFlags.SEEN], True),
                 call().__enter__().expunge(),
                 call().__exit__(None, None, None)]
        self.assertEqual(imap_mock.mock_calls, calls)

    def test_check_delivery_skip(self):
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        delivery = ImapDelivery(ssl=False)
        dsn_message = self.get_message({'In-Reply-To': self.message_id})
        message = MailMessage([(b'UID 42', bytes(dsn_message))])

        with patch('messenger.delivery.email.MailBoxUnencrypted', autospec=True) as imap_mock:
            imap_mock.return_value.mock_add_spec(('folder', '__enter__', '__exit__'))
            imap_mock.return_value.__enter__.return_value.fetch.return_value = [message]

            self.assertEqual(delivery.check_delivery(), Result(skip=1))

        email = self.session.query(Email).one()
        # Check email was updated
        self.assertEqual(email.status, MessageStatus.SENT)
        self.assertIsNone(email.update_datetime)
        self.assertIsNone(email.delivery_datetime)
        self.assertIsNone(email.delivery_info)

        calls = [call(host='', port=None),
                 call().folder.set('INBOX'),
                 call().__enter__(),
                 call().__enter__().fetch(AND(seen=False, deleted=False), mark_seen=False, limit=None, bulk=True),
                 call().__enter__().flag('42', [MailMessageFlags.SEEN], True),
                 call().__enter__().expunge(),
                 call().__exit__(None, None, None)]
        self.assertEqual(imap_mock.mock_calls, calls)

    def test_check_delivery_undelivered(self):
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT, message_id=self.message_id,
                   subject_template=self.subject_template, body_template=self.body_template)
        delivery = ImapDelivery(ssl=False)
        delivery_status = self.get_delivery_status(recipient_dsn_fields={'Action': 'failed'})
        dsn_message = self.get_dsn_message({'In-Reply-To': self.message_id}, delivery_status=delivery_status)
        message = MailMessage([(b'UID 42', bytes(dsn_message))])

        with patch('messenger.delivery.email.MailBoxUnencrypted', autospec=True) as imap_mock:
            imap_mock.return_value.mock_add_spec(('folder', '__enter__', '__exit__'))
            imap_mock.return_value.__enter__.return_value.fetch.return_value = [message]

            self.assertEqual(delivery.check_delivery(), Result(success=1))

        email = self.session.query(Email).one()
        # Check email was updated
        self.assertEqual(email.status, MessageStatus.UNDELIVERED)
        self.assertEqual(email.update_datetime, datetime(2020, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.delivery_datetime, datetime(2020, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.delivery_info,
                         {'Message-ID': self.reply_id, 'Subject': self.reply_subject, 'Action': 'failed'})

        calls = [call(host='', port=None),
                 call().folder.set('INBOX'),
                 call().__enter__(),
                 call().__enter__().fetch(AND(seen=False, deleted=False), mark_seen=False, limit=None, bulk=True),
                 call().__enter__().flag('42', [MailMessageFlags.SEEN, MailMessageFlags.DELETED], True),
                 call().__enter__().expunge(),
                 call().__exit__(None, None, None)]
        self.assertEqual(imap_mock.mock_calls, calls)

    def test_check_delivery_undelivered_skip(self):
        # Test DNS message, but skipped (missing email).
        delivery = ImapDelivery(ssl=False)
        delivery_status = self.get_delivery_status(recipient_dsn_fields={'Action': 'failed'})
        dsn_message = self.get_dsn_message({'In-Reply-To': self.message_id}, delivery_status=delivery_status)
        message = MailMessage([(b'UID 42', bytes(dsn_message))])

        with patch('messenger.delivery.email.MailBoxUnencrypted', autospec=True) as imap_mock:
            imap_mock.return_value.mock_add_spec(('folder', '__enter__', '__exit__'))
            imap_mock.return_value.__enter__.return_value.fetch.return_value = [message]

            self.assertEqual(delivery.check_delivery(), Result(skip=1))

        calls = [call(host='', port=None),
                 call().folder.set('INBOX'),
                 call().__enter__(),
                 call().__enter__().fetch(AND(seen=False, deleted=False), mark_seen=False, limit=None, bulk=True),
                 call().__enter__().flag('42', [MailMessageFlags.SEEN], True),
                 call().__enter__().expunge(),
                 call().__exit__(None, None, None)]
        self.assertEqual(imap_mock.mock_calls, calls)

    def test_check_delivery_limit(self):
        delivery = ImapDelivery(ssl=False)

        with patch('messenger.delivery.email.MailBoxUnencrypted', autospec=True) as imap_mock:
            imap_mock.return_value.mock_add_spec(('folder', '__enter__', '__exit__'))
            imap_mock.return_value.__enter__.return_value.fetch.return_value = []

            self.assertEqual(delivery.check_delivery(limit=sentinel.limit), Result())

        calls = [
            call(host='', port=None),
            call().folder.set('INBOX'),
            call().__enter__(),
            call().__enter__().fetch(AND(seen=False, deleted=False), mark_seen=False, limit=sentinel.limit, bulk=True),
            call().__enter__().expunge(),
            call().__exit__(None, None, None)]
        self.assertEqual(imap_mock.mock_calls, calls)

    def test_check_delivery_error(self):
        # Test an error in delivery processing is captured.
        delivery = ImapDelivery(ssl=False)

        with LogCapture('messenger', propagate=False) as logs:
            with patch('messenger.delivery.email.MailBoxUnencrypted', autospec=True) as imap_mock:
                imap_mock.return_value.mock_add_spec(('folder', '__enter__', '__exit__'))
                imap_mock.return_value.__enter__.return_value.fetch.return_value = [sentinel.junk]

                self.assertEqual(delivery.check_delivery(), Result(error=1))

        calls = [call(host='', port=None),
                 call().folder.set('INBOX'),
                 call().__enter__(),
                 call().__enter__().fetch(AND(seen=False, deleted=False), mark_seen=False, limit=None, bulk=True),
                 call().__enter__().expunge(),
                 call().__exit__(None, None, None)]
        self.assertEqual(imap_mock.mock_calls, calls)
        log = StringComparison("Processing a delivery message .* caused following error:.*")
        logs.check(('messenger.delivery.email', 'ERROR', log))
