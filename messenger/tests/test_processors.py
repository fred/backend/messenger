import base64
import os
from email.message import EmailMessage, Message
from email.mime.base import MIMEBase
from pathlib import Path
from typing import Any, Dict, Optional, Tuple, cast
from unittest import TestCase
from unittest.mock import Mock, sentinel
from uuid import UUID

from cryptography.hazmat.primitives.asymmetric.rsa import RSAPrivateKey
from cryptography.hazmat.primitives.hashes import SHA256
from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.primitives.serialization.pkcs7 import PKCS7SignatureBuilder, load_der_pkcs7_certificates
from cryptography.x509 import load_pem_x509_certificate

from messenger.backends.letter import LetterMessage
from messenger.backends.sms import SmsMessage
from messenger.exceptions import MessageCanceled
from messenger.models import Address, BlockedAddress, BlockedEmail, BlockedPhone
from messenger.processors import (
    BlockedEmailProcessor,
    BlockedLetterProcessor,
    BlockedSmsProcessor,
    EmailHeadersProcessor,
    SmimeEmailProcessor,
    VcardEmailProcessor,
)

from .utils import override_database, test_session_cls

CERTIFICATE_PATH = Path(os.path.dirname(__file__)) / 'certificates'


class EmailHeadersProcessorTest(TestCase):
    def test_process_message(self):
        message = EmailMessage()
        processor = EmailHeadersProcessor({'X-Custom-Header': 'Gazpacho!'})

        result = processor.process_message(message)

        self.assertEqual(result.get_all('X-Custom-Header'), ['Gazpacho!'])

    def test_process_message_multiple(self):
        message = EmailMessage()
        processor = EmailHeadersProcessor({'X-Custom-Header': 'Gazpacho!', 'X-Other': 'McGruder'})

        result = processor.process_message(message)

        self.assertEqual(result.get_all('X-Custom-Header'), ['Gazpacho!'])
        self.assertEqual(result.get_all('X-Other'), ['McGruder'])


class VcardEmailProcessorTest(TestCase):
    def test_process_message(self):
        message = EmailMessage()
        vcard = 'VCARD DATA\n'
        processor = VcardEmailProcessor(vcard)

        result = processor.process_message(message)

        attachments = cast(Tuple[EmailMessage], tuple(result.iter_attachments()))
        self.assertEqual(len(attachments), 1)
        self.assertEqual(attachments[0].get_content_type(), 'text/x-vcard')
        self.assertEqual(attachments[0].get_content(), vcard)


class SmimeEmailProcessorTest(TestCase):
    sender = 'holly@example.org'
    recipient = 'kryten@example.org'
    subject = 'Secret message'
    content = 'Gazpacho!'

    def _make_message(self, headers: Optional[Dict[str, Any]] = None, content: Optional[str] = None) -> EmailMessage:
        message = EmailMessage()
        for key, value in (headers or {}).items():
            message[key] = value
        if content:
            message.set_content(content)
        return message

    def test_parse_certificates_no_certs(self):
        # Test _parse_certificates with no certificates defined.
        processor = SmimeEmailProcessor([])
        self.assertEqual(processor.signers, {})

    def test_get_signers_no_mails(self):
        # Test _parse_certificates with certificate with no emails
        keyfile = CERTIFICATE_PATH / 'test_ca.key'
        certfile = CERTIFICATE_PATH / 'test_ca.crt'
        processor = SmimeEmailProcessor([(keyfile, certfile)])
        self.assertEqual(processor.signers, {})

    def test_parse_certificates_invalid_email(self):
        # Test _parse_certificates with certificates with invalid email.
        keyfile = CERTIFICATE_PATH / 'holly.key'
        certfile = CERTIFICATE_PATH / 'holly-invalid.crt'
        processor = SmimeEmailProcessor([(keyfile, certfile)])
        self.assertEqual(processor.signers, {})

    def test_parse_certificates(self):
        # Test _parse_certificates with certificate with valid email.
        keyfile = CERTIFICATE_PATH / 'holly.key'
        certfile = CERTIFICATE_PATH / 'holly.crt'
        processor = SmimeEmailProcessor([(keyfile, certfile)])

        self.assertEqual(tuple(processor.signers), ('holly@example.org', ))
        self.assertIsInstance(processor.signers['holly@example.org'], PKCS7SignatureBuilder)
        with open(certfile, 'rb') as file:
            cert = load_pem_x509_certificate(file.read())
        self.assertEqual(processor.signers['holly@example.org']._signers[0][0], cert)
        self.assertIsInstance(processor.signers['holly@example.org']._signers[0][1], RSAPrivateKey)
        self.assertIsInstance(processor.signers['holly@example.org']._signers[0][2], SHA256)

    def test_parse_certificates_bare(self):
        # Test _parse_certificates with certificate with valid email.
        keyfile = CERTIFICATE_PATH / 'holly.key'
        certfile = CERTIFICATE_PATH / 'holly-expanded.crt'
        processor = SmimeEmailProcessor([(keyfile, certfile)])

        self.assertEqual(tuple(processor.signers), ('holly@example.org', ))
        self.assertIsInstance(processor.signers['holly@example.org'], PKCS7SignatureBuilder)
        with open(certfile, 'rb') as file:
            cert = load_pem_x509_certificate(file.read())
        self.assertEqual(processor.signers['holly@example.org']._signers[0][0], cert)
        self.assertIsInstance(processor.signers['holly@example.org']._signers[0][1], RSAPrivateKey)
        self.assertIsInstance(processor.signers['holly@example.org']._signers[0][2], SHA256)

    def test_get_body_message(self):
        processor = SmimeEmailProcessor([])

        data = [
            # orig message, body message
            (EmailMessage(), EmailMessage()),
            (self._make_message(content=self.content), self._make_message(content=self.content)),
            (self._make_message({'Content-Type': 'text/plain'}), self._make_message({'Content-Type': 'text/plain'})),
            (self._make_message({'MIME-Version': '1.0'}), self._make_message({'MIME-Version': '1.0'})),
            (self._make_message({'Content-Transfer-Encoding': '8bit'}),
             self._make_message({'Content-Transfer-Encoding': '8bit'})),
            (self._make_message({'From': 'rimmer@example.org'}), EmailMessage()),
            (self._make_message({'Content-Type': 'text/plain', 'From': 'rimmer@example.org'}, self.content),
             self._make_message({'Content-Type': 'text/plain'}, self.content)),
        ]
        for original, body in data:
            with self.subTest(original=original):
                result = processor._get_body_message(original)

                # Check it's not the same object.
                self.assertIsNot(result, body)
                # Compare the messages.
                self.assertEqual(dict(result), dict(body))
                self.assertEqual(result.get_payload(), body.get_payload())

    def test_get_signature_message(self):
        processor = SmimeEmailProcessor([])
        signature = b'Gazpacho!'

        result = processor._get_signature_message(signature)

        self.assertEqual(result['Content-Type'], 'application/pkcs7-signature; name="smime.p7s"')
        self.assertEqual(result['Content-Transfer-Encoding'], 'base64')
        self.assertEqual(result['Content-Disposition'], 'attachment; filename="smime.p7s"')
        self.assertEqual(result['Content-Description'], 'S/MIME Cryptographic Signature')
        self.assertEqual(result.get_payload(), 'R2F6cGFjaG8h\n')

    def test_sign_message(self):
        keyfile = CERTIFICATE_PATH / 'holly.key'
        certfile = CERTIFICATE_PATH / 'holly.crt'
        processor = SmimeEmailProcessor([])
        with open(keyfile, 'rb') as file:
            key = load_pem_private_key(file.read(), None)
        with open(certfile, 'rb') as file:
            cert = load_pem_x509_certificate(file.read())
        signer = PKCS7SignatureBuilder().add_signer(cert, key, SHA256())  # type: ignore[arg-type] # bad annotation

        message = EmailMessage()
        message['From'] = 'holly@example.org'
        message['To'] = self.recipient
        message['Subject'] = self.subject
        message.set_content(self.content)

        envelope = processor.sign_message(message, signer)

        self.assertTrue(envelope.is_multipart())
        self.assertEqual(envelope['MIME-Version'], '1.0')
        self.assertEqual(envelope['Content-Type'],
                         'multipart/signed; protocol="application/pkcs7-signature"; micalg="sha-256"')
        self.assertEqual(envelope.preamble, "This is an S/MIME signed message")
        self.assertEqual(envelope['From'], 'holly@example.org')
        self.assertEqual(envelope['To'], self.recipient)
        self.assertEqual(envelope['Subject'], self.subject)
        self.assertEqual(cast(EmailMessage, envelope.get_payload(0)).get_content().strip(), self.content)
        self.assertEqual(cast(Message, envelope.get_payload(1)).get_content_type(), 'application/pkcs7-signature')
        # Check certificate from the signature.
        sign_certs = load_der_pkcs7_certificates(
            base64.b64decode(cast(str, cast(Message, envelope.get_payload(1)).get_payload())))
        self.assertEqual(sign_certs[0].subject.rfc4514_string(), 'CN=Holly,L=Red Dwarf,ST=Jupiter Mining Company,C=JU')

    def test_sign_message_multipart(self):
        # Regression for #88 - Ensure the signed message is serialized the same way as when it is signed.
        processor = SmimeEmailProcessor([])
        # Mock the signer to retrieve the signed data.
        signer = Mock(name='signer')
        signer.set_data.return_value = signer
        signer.sign.return_value = b'SIGNATURE'

        message = EmailMessage()
        message['From'] = 'holly@example.org'
        message['To'] = self.recipient
        message['Subject'] = self.subject
        message.set_content(self.content)
        # Add a HTML alternative
        message.add_alternative(self.content, subtype='html')

        envelope = processor.sign_message(message, signer)

        self.assertTrue(envelope.is_multipart())
        self.assertEqual(envelope['Content-Type'],
                         'multipart/signed; protocol="application/pkcs7-signature"; micalg="sha-256"')
        self.assertEqual(cast(Message, envelope.get_payload(1)).get_payload(decode=True), b'SIGNATURE')

        # Check the bytes signed are really a part of the result message.
        self.assertIn(signer.mock_calls[0][1][0], bytes(envelope))

    def _test_process_message_signed(self, sender: str) -> None:
        keyfile = CERTIFICATE_PATH / 'holly.key'
        certfile = CERTIFICATE_PATH / 'holly.crt'
        processor = SmimeEmailProcessor([(keyfile, certfile)])
        message = EmailMessage()
        message['From'] = sender
        message.set_content(self.content)

        signed_message = processor.process_message(message)

        self.assertEqual(signed_message.get_content_type(), 'multipart/signed')
        self.assertEqual(signed_message['MIME-Version'], '1.0')
        self.assertEqual(signed_message['From'], sender)
        body, signature = cast(Tuple[EmailMessage, MIMEBase], signed_message.get_payload())
        self.assertEqual(body.get_content().strip(), self.content)
        self.assertEqual(signature.get_content_type(), 'application/pkcs7-signature')

    def test_get_message_signed(self):
        self._test_process_message_signed('holly@example.org')

    def test_get_message_signed_bare(self):
        # Test message is signed with matching email address - bare email.
        self._test_process_message_signed('Holly <holly@example.org>')

    def test_get_message_not_signed(self):
        # Test message is not signed, if sender doesn't match certificate.
        keyfile = CERTIFICATE_PATH / 'holly.key'
        certfile = CERTIFICATE_PATH / 'holly.crt'
        processor = SmimeEmailProcessor([(keyfile, certfile)])
        sender = 'queeg-500@example.org'
        message = EmailMessage()
        message['From'] = sender
        message.set_content(self.content)

        unsigned_message = processor.process_message(message)

        self.assertEqual(unsigned_message.get_content_type(), 'text/plain')
        self.assertEqual(unsigned_message['MIME-Version'], '1.0')
        self.assertEqual(unsigned_message['From'], sender)
        self.assertEqual(unsigned_message.get_content().strip(), self.content)

    def test_process_message_no_from(self):
        # Test message is not signed, if it has no From header.
        keyfile = CERTIFICATE_PATH / 'holly.key'
        certfile = CERTIFICATE_PATH / 'holly.crt'
        processor = SmimeEmailProcessor([(keyfile, certfile)])
        message = EmailMessage()
        message['To'] = self.recipient
        message['Subject'] = self.subject
        message.set_content(self.content)

        unsigned_message = processor.process_message(message)

        self.assertEqual(unsigned_message.get_content_type(), 'text/plain')
        self.assertEqual(unsigned_message['MIME-Version'], '1.0')
        self.assertEqual(unsigned_message['To'], self.recipient)
        self.assertEqual(unsigned_message['Subject'], self.subject)
        self.assertEqual(unsigned_message.get_content().strip(), self.content)


@override_database()
class BlockedEmailProcessorTest(TestCase):
    def setUp(self):
        self.session = test_session_cls()()
        self.processor = BlockedEmailProcessor()

    def tearDown(self):
        self.session.close()

    def get_message(self, recipient: str) -> EmailMessage:
        message = EmailMessage()
        message['To'] = recipient
        return message

    def block_email(self, email: str) -> None:
        self.session.add(BlockedEmail(email=email))
        self.session.commit()

    def test_no_blocked(self):
        recipient = 'kryten@example.org'
        message = self.get_message(recipient)

        result = self.processor.process_message(message)

        self.assertEqual(result['To'], recipient)

    def test_blocked(self):
        recipient = 'kryten@example.org, rimmer@example.org'
        self.block_email('rimmer@example.org')
        message = self.get_message(recipient)

        result = self.processor.process_message(message)

        self.assertEqual(result['To'], 'kryten@example.org')

    def _test_canceled(self, recipient: str, blocked: str) -> None:
        self.block_email(blocked)
        message = self.get_message(recipient)

        with self.assertRaises(MessageCanceled):
            self.processor.process_message(message)

    def test_canceled(self):
        self._test_canceled('kryten@example.org', 'kryten@example.org')

    def test_canceled_case_insensitive(self):
        self._test_canceled('KRYTEN@example.org', 'kryten@EXAMPLE.org')

    def test_canceled_name(self):
        self._test_canceled('Kryten <kryten@example.org>', 'kryten@example.org')

    def test_canceled_unicode(self):
        self._test_canceled('Kryten 🤖 <kryten-🤖@example.org>', 'kryten-🤖@example.org')

    def _test_blocked_copy_headers(self, header: str) -> None:
        recipient = 'kryten@example.org'
        blocked = 'rimmer@example.org'
        self.block_email(blocked)
        message = self.get_message(recipient)
        message[header] = blocked

        result = self.processor.process_message(message)

        self.assertEqual(result['To'], 'kryten@example.org')
        self.assertIsNone(result[header])

    def test_blocked_cc(self):
        self._test_blocked_copy_headers('Cc')

    def test_blocked_bcc(self):
        self._test_blocked_copy_headers('Bcc')


@override_database()
class BlockedSmsProcessorTest(TestCase):
    def setUp(self):
        self.session = test_session_cls()()
        self.processor = BlockedSmsProcessor()

    def tearDown(self):
        self.session.close()

    def test_no_blocked(self):
        recipient = '+1234'
        message = SmsMessage(UUID(int=42), recipient, '')

        result = self.processor.process_message(message)

        self.assertEqual(result.recipient, recipient)

    def test_canceled(self):
        recipient = '+1234'
        self.session.add(BlockedPhone(phone=recipient))
        self.session.commit()
        message = SmsMessage(UUID(int=42), recipient, '')

        with self.assertRaises(MessageCanceled):
            self.processor.process_message(message)


@override_database()
class BlockedLetterProcessorTest(TestCase):
    def setUp(self):
        self.session = test_session_cls()()
        self.processor = BlockedLetterProcessor()

    def tearDown(self):
        self.session.close()

    def test_no_blocked(self):
        recipient = Address(name='Arnold Rimmer', organization='Jupiter Mining Company', street='Deck 16',
                            city='Red Dwarf', state='Deep Space', postal_code='JMC', country='Jupiter')
        message = LetterMessage(sentinel.uuid, recipient, sentinel.file)

        result = self.processor.process_message(message)

        self.assertEqual(result.recipient, recipient)

    def _test_not_blocked(self, recipient: Address, blocked: Address) -> None:
        # Test when message is not blocked.
        blocked_address = BlockedAddress(
            name=blocked.name, organization=blocked.organization, street=blocked.street, city=blocked.city,
            state=blocked.state, postal_code=blocked.postal_code, country=blocked.country)
        self.session.add(blocked_address)
        self.session.commit()
        message = LetterMessage(sentinel.uuid, recipient, sentinel.file)

        result = self.processor.process_message(message)

        self.assertEqual(result.recipient, recipient)

    def test_partial_block_not_blocked(self):
        # Test partial address is different from full address - blocked is partial.
        recipient = Address(name='Arnold Rimmer', organization='Jupiter Mining Company', street='Deck 16',
                            city='Red Dwarf', state='Deep Space', postal_code='JMC', country='Jupiter')
        blocked = Address(name='Arnold Rimmer', city='Red Dwarf')
        self._test_not_blocked(recipient, blocked)

    def test_partial_recipient_not_blocked(self):
        # Test partial address is different from full address - recipient is partial.
        recipient = Address(name='Arnold Rimmer', city='Red Dwarf')
        blocked = Address(name='Arnold Rimmer', organization='Jupiter Mining Company', street='Deck 16',
                          city='Red Dwarf', state='Deep Space', postal_code='JMC', country='Jupiter')
        self._test_not_blocked(recipient, blocked)

    def test_canceled(self):
        recipient = Address(name='Arnold Rimmer', organization='Jupiter Mining Company', street='Deck 16',
                            city='Red Dwarf', state='Deep Space', postal_code='JMC', country='Jupiter')
        blocked_address = BlockedAddress(
            name=recipient.name, organization=recipient.organization, street=recipient.street, city=recipient.city,
            state=recipient.state, postal_code=recipient.postal_code, country=recipient.country)
        self.session.add(blocked_address)
        self.session.commit()
        message = LetterMessage(sentinel.uuid, recipient, sentinel.file)

        with self.assertRaises(MessageCanceled):
            self.processor.process_message(message)

    def test_canceled_case_insensitive(self):
        recipient = Address(name='Arnold Rimmer', organization='Jupiter Mining Company', street='Deck 16',
                            city='Red Dwarf', state='Deep Space', postal_code='JMC', country='Jupiter')
        blocked = Address(name='arnold rimmer', organization='jupiter mining company', street='deck 16',
                          city='red dwarf', state='deep space', postal_code='jmc', country='jupiter')
        blocked_address = BlockedAddress(
            name=blocked.name, organization=blocked.organization, street=blocked.street, city=blocked.city,
            state=blocked.state, postal_code=blocked.postal_code, country=blocked.country)
        self.session.add(blocked_address)
        self.session.commit()
        message = LetterMessage(sentinel.uuid, recipient, sentinel.file)

        with self.assertRaises(MessageCanceled):
            self.processor.process_message(message)
