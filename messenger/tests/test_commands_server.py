from concurrent.futures import ThreadPoolExecutor
from typing import Dict, List, Optional
from unittest import TestCase
from unittest.mock import call, patch, sentinel

from grpc._utilities import DictionaryGenericHandler
from testfixtures import Comparison

from messenger.commands.server import main

from .utils import CommandMixin, override_database


@override_database()
class ServiceMainTest(CommandMixin, TestCase):
    command = main

    def setUp(self):
        grpc_patcher = patch('messenger.commands.server.grpc')
        self.addCleanup(grpc_patcher.stop)
        self.grpc_mock = grpc_patcher.start()
        # Disable reflection
        ref_patcher = patch('messenger.commands.server.reflection', new=None)
        self.addCleanup(ref_patcher.stop)
        ref_patcher.start()

    def _test_main(self, argv: List[str], services: Optional[List[str]] = None, *, port: int = 50051,
                   options: Optional[List] = None, max_concurrency: Optional[int] = None,
                   settings: Optional[Dict] = None) -> None:
        services = services or ['Fred.Messenger.Api.Email.EmailMessenger', 'Fred.Messenger.Api.Sms.SmsMessenger',
                                'Fred.Messenger.Api.Letter.LetterMessenger']
        with patch('messenger.commands.server.sleep', side_effect=KeyboardInterrupt):
            self.assertCommandSuccess(argv, settings=settings)

        calls = [call.server(Comparison(ThreadPoolExecutor), options=options, maximum_concurrent_rpcs=max_concurrency)]
        calls += len(services) * [call.server().add_generic_rpc_handlers((Comparison(DictionaryGenericHandler), ))]
        calls += [call.server().add_insecure_port('[::]:{}'.format(port)),
                  call.server().start(),
                  call.server().stop(0)]
        self.assertEqual(self.grpc_mock.mock_calls, calls)
        self.assertEqual([c[1][0][0]._name for c in self.grpc_mock.mock_calls[1:1 + len(services)]], services)

    def test_main(self):
        self._test_main([])

    def test_main_email(self):
        services = ['Fred.Messenger.Api.Email.EmailMessenger']
        self._test_main(['--method', 'email'], services)

    def test_main_sms(self):
        services = ['Fred.Messenger.Api.Sms.SmsMessenger']
        self._test_main(['--method', 'sms'], services)

    def test_main_letter(self):
        services = ['Fred.Messenger.Api.Letter.LetterMessenger']
        self._test_main(['--method', 'letter'], services)

    def test_port(self):
        self._test_main(['--port', '12345'], port=12345)

    def test_grpc_options(self):
        self._test_main(['--grpc-options', 'name=Kryten'], options=[['name', 'Kryten']])

    def test_max_workers_config(self):
        self._test_main([], settings={'grpc_max_workers': 42})

        # Check max workers
        pool = self.grpc_mock.mock_calls[0][1][0]
        self.assertEqual(pool._max_workers, 42)

    def test_max_workers_option(self):
        self._test_main(['--max-workers', '42'])

        # Check max workers
        pool = self.grpc_mock.mock_calls[0][1][0]
        self.assertEqual(pool._max_workers, 42)

    def test_max_concurrency_config(self):
        self._test_main([], settings={'grpc_maximum_concurrent_rpcs': 42}, max_concurrency=42)

    def test_max_concurrency_option(self):
        self._test_main(['--max-concurrency', '42'], max_concurrency=42)

    def _test_reflection(self, args: List[str], services: List[str]) -> None:
        with patch('messenger.commands.server.reflection') as reflection_mock:
            reflection_mock.SERVICE_NAME = sentinel.reflection
            self._test_main(args, services=services)

        ref_calls = [
            call.enable_server_reflection(services + [sentinel.reflection], self.grpc_mock.server.return_value)]
        self.assertEqual(reflection_mock.mock_calls, ref_calls)

    def test_reflection(self):
        services = ['Fred.Messenger.Api.Email.EmailMessenger', 'Fred.Messenger.Api.Sms.SmsMessenger',
                    'Fred.Messenger.Api.Letter.LetterMessenger']
        self._test_reflection([], services)

    def test_reflection_email(self):
        self._test_reflection(['--method', 'email'], ['Fred.Messenger.Api.Email.EmailMessenger'])

    def test_reflection_sms(self):
        self._test_reflection(['--method', 'sms'], ['Fred.Messenger.Api.Sms.SmsMessenger'])

    def test_reflection_letter(self):
        self._test_reflection(['--method', 'letter'], ['Fred.Messenger.Api.Letter.LetterMessenger'])
