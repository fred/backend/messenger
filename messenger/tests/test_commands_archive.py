from datetime import datetime
from typing import Iterable, Optional, Type, cast
from unittest import TestCase, skipIf
from unittest.mock import patch
from uuid import UUID

from pytz import UTC
from sqlalchemy.orm.session import make_transient

from messenger.commands.archive import _archive_model, main
from messenger.constants import ArchiveMessageStatus, MessageStatus
from messenger.models import (
    Address,
    ArchiveEmail,
    ArchiveLetter,
    ArchiveRecord,
    ArchiveSms,
    Email,
    EmailReference,
    Letter,
    LetterReference,
    Record,
    Sms,
    SmsReference,
)
from messenger.settings import APPLICATION

from .utils import CommandMixin, make_email, make_letter, make_sms, override_database, test_session_cls


@override_database()
class ArchiveMainTest(CommandMixin, TestCase):
    command = main

    # Email fields
    email_recipient = 'kryten@example.com'
    subject_template = 'subject.txt'
    body_template = 'subject.txt'
    subject_uuid = UUID(int=255)
    body_uuid = UUID(int=1024)
    reference = 'toilet'

    # SMS fields
    sms_recipient = '+1.234'
    sms_body = 'body.txt'
    sms_body_uuid = UUID(int=256)

    # Letter fields
    letter_recipient = Address(name='Kryten')
    letter_uuid = UUID(int=44)
    file_uuid = UUID(int=257)

    def setUp(self):
        self.session = test_session_cls()()

    def tearDown(self):
        self.session.close()

    def make_email(self, status: MessageStatus = MessageStatus.SENT, archive: bool = True,
                   subject_template_uuid: Optional[UUID] = None, body_template_uuid: Optional[UUID] = None,
                   references: Optional[Iterable[EmailReference]] = None, create_datetime: Optional[datetime] = None,
                   ) -> Email:
        return make_email(self.session, recipient=self.email_recipient, archive=archive, status=status,
                          subject_template=self.subject_template, subject_template_uuid=subject_template_uuid,
                          body_template=self.body_template, body_template_uuid=body_template_uuid,
                          references=(references or []), create_datetime=create_datetime)

    def make_sms(self, status: MessageStatus = MessageStatus.SENT, archive: bool = True,
                 body_template_uuid: Optional[UUID] = None, references: Optional[Iterable[SmsReference]] = None,
                 create_datetime: Optional[datetime] = None) -> Sms:
        return make_sms(self.session, recipient=self.sms_recipient, archive=archive, status=status,
                        body_template=self.sms_body, body_template_uuid=body_template_uuid,
                        references=(references or []), create_datetime=create_datetime)

    def make_letter(self, status: MessageStatus = MessageStatus.SENT, archive: bool = True,
                    references: Optional[Iterable[LetterReference]] = None, create_datetime: Optional[datetime] = None,
                    ) -> Letter:
        return make_letter(self.session, recipient=self.letter_recipient, archive=archive, status=status,
                           file=self.file_uuid, references=(references or []), create_datetime=create_datetime)

    def test_main_no_messages(self):
        self.assertCommandSuccess()

        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)

    def test_main_no_archive(self):
        # Test messages without archive are not archived.
        self.make_email(archive=False)
        self.make_sms(archive=False)
        self.make_letter(archive=False)

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 1)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def test_main_pending(self):
        # Test pending messages are not archived.
        self.make_email(status=MessageStatus.PENDING)
        self.make_sms(status=MessageStatus.PENDING)
        self.make_letter(status=MessageStatus.PENDING)

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 1)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def test_main_error(self):
        # Test errored messages are not archived.
        self.make_email(status=MessageStatus.ERROR)
        self.make_sms(status=MessageStatus.ERROR)
        self.make_letter(status=MessageStatus.ERROR)

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 1)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def _test_main(self, model: Record, archive_cls: Type[ArchiveRecord],
                   status: ArchiveMessageStatus) -> ArchiveRecord:
        # Detach model from session.
        self.session.refresh(model)
        make_transient(model)

        self.assertCommandSuccess()

        self.assertEqual(self.session.query(model.__class__).count(), 0)
        archived = cast(ArchiveRecord, self.session.query(archive_cls).one())
        self.assertEqual(archived.uuid, model.uuid)
        self.assertEqual(archived.create_datetime, model.create_datetime)
        self.assertEqual(archived.status, status)
        return archived

    def test_main_sent_email(self):
        # Test sent messages are archived.
        email = self.make_email(subject_template_uuid=self.subject_uuid, body_template_uuid=self.body_uuid)
        self._test_main(email, ArchiveEmail, ArchiveMessageStatus.SENT)

    def test_main_sent_sms(self):
        # Test sent messages are archived.
        sms = self.make_sms(body_template_uuid=self.sms_body_uuid)
        self._test_main(sms, ArchiveSms, ArchiveMessageStatus.SENT)

    def test_main_sent_letter(self):
        # Test sent messages are archived.
        letter = self.make_letter()
        self._test_main(letter, ArchiveLetter, ArchiveMessageStatus.SENT)

    def _test_main_references(self, model: Record, archive_cls: Type[ArchiveRecord]) -> None:
        # Test references are archived.
        archived = self._test_main(model, archive_cls, ArchiveMessageStatus.SENT)
        self.assertEqual([(r.type, r.value) for r in archived.references], [('job', self.reference)])

    def test_main_references_email(self):
        email = self.make_email(subject_template_uuid=self.subject_uuid, body_template_uuid=self.body_uuid,
                                references=[EmailReference(type='job', value=self.reference)])
        self._test_main_references(email, ArchiveEmail)

    def test_main_references_sms(self):
        sms = self.make_sms(body_template_uuid=self.sms_body_uuid,
                            references=[SmsReference(type='job', value=self.reference)])
        self._test_main_references(sms, ArchiveSms)

    def test_main_references_letter(self):
        letter = self.make_letter(references=[LetterReference(type='job', value=self.reference)])
        self._test_main_references(letter, ArchiveLetter)

    def test_mail_failed_email(self):
        # Test failed messages (without template UUIDs) are archived.
        email = self.make_email(status=MessageStatus.FAILED)
        self._test_main(email, ArchiveEmail, ArchiveMessageStatus.FAILED)

    def test_mail_failed_sms(self):
        # Test failed messages (without template UUIDs) are archived.
        sms = self.make_sms(status=MessageStatus.FAILED)
        self._test_main(sms, ArchiveSms, ArchiveMessageStatus.FAILED)

    def test_mail_failed_letter(self):
        # Test failed messages (without template UUIDs) are archived.
        letter = self.make_letter(status=MessageStatus.FAILED)
        self._test_main(letter, ArchiveLetter, ArchiveMessageStatus.FAILED)

    def test_main_type(self):
        # Test messages of selected type are archived.
        self.make_email(subject_template_uuid=self.subject_uuid, body_template_uuid=self.body_uuid)
        self.make_sms(body_template_uuid=self.sms_body_uuid)
        self.make_letter()

        self.assertCommandSuccess(['--method', 'letter'])

        # Check only letter was archived.
        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 0)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 1)

    def test_main_limits(self):
        # Test --from and --to limits.
        self.make_email(create_datetime=datetime(2010, 2, 1, 13, tzinfo=UTC))
        self.make_email(create_datetime=datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.make_email(create_datetime=datetime(2010, 2, 1, 15, tzinfo=UTC))

        self.make_sms(create_datetime=datetime(2010, 2, 1, 13, tzinfo=UTC))
        self.make_sms(create_datetime=datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.make_sms(create_datetime=datetime(2010, 2, 1, 15, tzinfo=UTC))

        self.make_letter(create_datetime=datetime(2010, 2, 1, 13, tzinfo=UTC))
        self.make_letter(create_datetime=datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.make_letter(create_datetime=datetime(2010, 2, 1, 15, tzinfo=UTC))

        self.assertCommandSuccess(['--from', '2010-02-01 13:10:00', '--to', '2010-02-01 14:10:00'])

        # Check only matching messages were archived.
        self.assertEqual(self.session.query(Email).count(), 2)
        archived_mail = self.session.query(ArchiveEmail).one()
        self.assertEqual(archived_mail.create_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(self.session.query(Sms).count(), 2)
        archived_sms = self.session.query(ArchiveSms).one()
        self.assertEqual(archived_sms.create_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(self.session.query(Letter).count(), 2)
        archived_letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(archived_letter.create_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))

    @skipIf(APPLICATION.settings.test_db_connection.startswith('sqlite'), 'Does not work on SQLite database.')
    def test_main_skip_locked(self):
        # Test locked rows are skipped.
        self.make_email(status=MessageStatus.SENT, subject_template_uuid=self.subject_uuid,
                        body_template_uuid=self.body_uuid)

        # Lock the email in test transaction.
        self.session.query(Email).with_for_update().all()

        self.assertCommandSuccess()

        # Release the lock.
        self.session.rollback()

        # Check email wasn't archived.
        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)

    @skipIf(APPLICATION.settings.test_db_connection.startswith('sqlite'), 'Does not work on SQLite database.')
    def test_main_lock(self):
        # Test emails are locked during process.
        self.make_email(status=MessageStatus.SENT, subject_template_uuid=self.subject_uuid,
                        body_template_uuid=self.body_uuid)

        archive_model_orig = _archive_model

        def archive_events(*args, **kwargs):
            # Check FOR UPDATE SKIP LOCKED from parallel transaction doesn't lock any rows.
            session = test_session_cls()()
            try:
                unlocked = session.query(Email).with_for_update(skip_locked=True).all()
                self.assertEqual(len(unlocked), 0)
            finally:
                session.close()
            archive_model_orig(*args, **kwargs)

        with patch('messenger.commands.archive._archive_model', new=archive_events):
            self.assertCommandSuccess()

        # Check email was archived.
        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 1)

    def _test_output(self, verbosity: int, stdout: str) -> None:
        self.make_email(status=MessageStatus.SENT, subject_template_uuid=self.subject_uuid,
                        body_template_uuid=self.body_uuid)
        self.assertCommandSuccess(['--verbosity', str(verbosity), '--method', 'email'], stdout=stdout)

    def test_silent(self):
        self._test_output(0, '')

    def test_info(self):
        self._test_output(1, 'Archived 1 emails.\n')

    def test_detail(self):
        self._test_output(2, '.\nArchived 1 emails.\n')
