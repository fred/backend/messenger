from typing import cast
from unittest import TestCase
from unittest.mock import call, patch, sentinel

from filed import Storage

from messenger.settings import Application, Settings


class ApplicationTest(TestCase):
    def setUp(self):
        self.application = Application(Settings)

    def test_storage_unset(self):
        self.application.settings = Settings(fileman_netloc=None)
        self.assertIsNone(self.application.storage)

    def test_storage(self):
        self.application.settings = Settings()
        self.application.settings.fileman_netloc = sentinel.netloc
        with patch('messenger.settings.FileClient', return_value=sentinel.client) as client_mock:
            storage = cast(Storage, self.application.storage)

        self.assertEqual(storage.client, sentinel.client)
        self.assertEqual(client_mock.mock_calls, [call(sentinel.netloc)])

    def test_clear_cache(self):
        with patch('messenger.settings.FileClient') as client_mock:
            self.application.settings = Settings()
            self.application.settings.fileman_netloc = sentinel.netloc

            storage = self.application.storage

            self.application.clear_cache()

            self.assertIsNot(self.application.storage, storage)

        self.assertEqual(client_mock.mock_calls, [call(sentinel.netloc)] * 2)
