from typing import Type
from unittest import TestCase, skipIf, skipUnless

from sqlalchemy.engine import CursorResult
from sqlalchemy.sql import text

from messenger.commands.partition import MODELS, main
from messenger.database import get_engine
from messenger.models import (
    ArchiveEmail,
    ArchiveEmailReference,
    ArchiveLetter,
    ArchiveLetterReference,
    ArchiveSms,
    ArchiveSmsReference,
)
from messenger.models.base import BaseModel
from messenger.settings import APPLICATION

from .utils import CommandMixin, override_database

LIST_PARTITIONS_SQL = text("SELECT relid FROM pg_partition_tree(:tablename) WHERE isleaf")


@override_database()
class PartitionMainTest(CommandMixin, TestCase):
    command = main

    def setUp(self):
        self.engine = get_engine()

        # Drop default partitions from database
        if self.engine.dialect.name == 'postgresql':
            sql = "DROP TABLE {}_default CASCADE"
            for model in MODELS:
                self._execute_sql(sql.format(model.__tablename__))

    def _execute_sql(self, sql: str) -> CursorResult:
        with self.engine.connect() as connection:
            result = connection.execute(text(sql))
            connection.commit()
        return result

    def assertPartitionExist(self, model: Type[BaseModel], partition_name: str) -> None:
        """Check if partition for a table exists."""
        with self.engine.connect() as connection:
            result = connection.execute(LIST_PARTITIONS_SQL, {'tablename': model.__tablename__})
        self.assertIn(partition_name, [r[0] for r in result])

    def assertPartitionNotExist(self, model: Type[BaseModel], partition_name: str) -> None:
        """Check if partition for a table exists."""
        with self.engine.connect() as connection:
            result = connection.execute(LIST_PARTITIONS_SQL, {'tablename': model.__tablename__})
        self.assertNotIn(partition_name, [r[0] for r in result])

    @skipIf(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for non-postgres databases.')
    def test_main_not_postgres(self):
        self.assertCommandFail(['create', 'DEFAULT'], stderr="Partition only supported for PostgreSQL databases")

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_invalid_month(self):
        self.assertCommandFail(['create', '2020', '42'], stderr="Provided arguments are not valid year and month.")

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_invalid_args(self):
        self.assertCommandFail(['create', 'INVALID'], stderr="Invalid number of arguments.")

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_create_default(self):
        self.assertCommandSuccess(['create', 'DEFAULT'])

        suffix = '_default'
        self.assertPartitionExist(ArchiveEmail, ArchiveEmail.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveEmailReference, ArchiveEmailReference.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveSms, ArchiveSms.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveSmsReference, ArchiveSmsReference.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveLetter, ArchiveLetter.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveLetterReference, ArchiveLetterReference.__tablename__ + suffix)

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_create_month(self):
        self.assertCommandSuccess(['create', '2019', '8'])

        suffix = '_2019_08'
        self.assertPartitionExist(ArchiveEmail, ArchiveEmail.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveEmailReference, ArchiveEmailReference.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveSms, ArchiveSms.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveSmsReference, ArchiveSmsReference.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveLetter, ArchiveLetter.__tablename__ + suffix)
        self.assertPartitionExist(ArchiveLetterReference, ArchiveLetterReference.__tablename__ + suffix)

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_create_dry_run(self):
        output = [
            "CREATE TABLE messenger_archiveemail_default PARTITION OF messenger_archiveemail DEFAULT",
            ("CREATE TABLE messenger_archiveemailreference_default PARTITION OF messenger_archiveemailreference "
             "DEFAULT"),
            "CREATE TABLE messenger_archiveletter_default PARTITION OF messenger_archiveletter DEFAULT",
            ("CREATE TABLE messenger_archiveletterreference_default PARTITION OF messenger_archiveletterreference "
             "DEFAULT"),
            "CREATE TABLE messenger_archivesms_default PARTITION OF messenger_archivesms DEFAULT",
            "CREATE TABLE messenger_archivesmsreference_default PARTITION OF messenger_archivesmsreference DEFAULT",
        ]
        self.assertCommandSuccess(['create', 'DEFAULT', '--dry-run'], stdout='\n'.join(output) + '\n')

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_create_skip_existing(self):
        sql = "CREATE TABLE {table}_default PARTITION OF {table} DEFAULT"
        self._execute_sql(sql.format(table='messenger_archiveemail'))
        self._execute_sql(sql.format(table='messenger_archiveemailreference'))

        output = [
            "CREATE TABLE messenger_archiveletter_default PARTITION OF messenger_archiveletter DEFAULT",
            ("CREATE TABLE messenger_archiveletterreference_default PARTITION OF messenger_archiveletterreference "
             "DEFAULT"),
            "CREATE TABLE messenger_archivesms_default PARTITION OF messenger_archivesms DEFAULT",
            "CREATE TABLE messenger_archivesmsreference_default PARTITION OF messenger_archivesmsreference DEFAULT",
        ]
        self.assertCommandSuccess(['create', 'DEFAULT', '--dry-run'], stdout='\n'.join(output) + '\n')

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_create_skip_existing_verbose(self):
        sql = "CREATE TABLE {table}_default PARTITION OF {table} DEFAULT"
        self._execute_sql(sql.format(table='messenger_archiveemail'))
        self._execute_sql(sql.format(table='messenger_archiveemailreference'))

        output = [
            "-- Partition messenger_archiveemail_default already exists",
            "-- Partition messenger_archiveemailreference_default already exists",
            "CREATE TABLE messenger_archiveletter_default PARTITION OF messenger_archiveletter DEFAULT",
            ("CREATE TABLE messenger_archiveletterreference_default PARTITION OF messenger_archiveletterreference "
             "DEFAULT"),
            "CREATE TABLE messenger_archivesms_default PARTITION OF messenger_archivesms DEFAULT",
            "CREATE TABLE messenger_archivesmsreference_default PARTITION OF messenger_archivesmsreference DEFAULT",
        ]
        self.assertCommandSuccess(['create', 'DEFAULT', '--dry-run', '-v2'], stdout='\n'.join(output) + '\n')

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_drop_default(self):
        sql = "CREATE TABLE {table}_default PARTITION OF {table} DEFAULT"
        for model in MODELS:
            self._execute_sql(sql.format(table=model.__tablename__))

        self.assertCommandSuccess(['drop', 'DEFAULT'])

        suffix = '_default'
        self.assertPartitionNotExist(ArchiveEmail, ArchiveEmail.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveEmailReference, ArchiveEmailReference.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveSms, ArchiveSms.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveSmsReference, ArchiveSmsReference.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveLetter, ArchiveLetter.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveLetterReference, ArchiveLetterReference.__tablename__ + suffix)

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_drop_month(self):
        sql = "CREATE TABLE {table}_2019_11 PARTITION OF {table} DEFAULT"
        for model in MODELS:
            self._execute_sql(sql.format(table=model.__tablename__))

        self.assertCommandSuccess(['drop', '2019', '8'])

        suffix = '_2019_08'
        self.assertPartitionNotExist(ArchiveEmail, ArchiveEmail.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveEmailReference, ArchiveEmailReference.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveSms, ArchiveSms.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveSmsReference, ArchiveSmsReference.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveLetter, ArchiveLetter.__tablename__ + suffix)
        self.assertPartitionNotExist(ArchiveLetterReference, ArchiveLetterReference.__tablename__ + suffix)

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_drop_dry_run(self):
        sql = "CREATE TABLE {table}_default PARTITION OF {table} DEFAULT"
        for model in MODELS:
            self._execute_sql(sql.format(table=model.__tablename__))

        output = [
            "ALTER TABLE messenger_archivesmsreference DETACH PARTITION messenger_archivesmsreference_default",
            "DROP TABLE messenger_archivesmsreference_default",
            "ALTER TABLE messenger_archivesms DETACH PARTITION messenger_archivesms_default",
            "DROP TABLE messenger_archivesms_default",
            "ALTER TABLE messenger_archiveletterreference DETACH PARTITION messenger_archiveletterreference_default",
            "DROP TABLE messenger_archiveletterreference_default",
            "ALTER TABLE messenger_archiveletter DETACH PARTITION messenger_archiveletter_default",
            "DROP TABLE messenger_archiveletter_default",
            "ALTER TABLE messenger_archiveemailreference DETACH PARTITION messenger_archiveemailreference_default",
            "DROP TABLE messenger_archiveemailreference_default",
            "ALTER TABLE messenger_archiveemail DETACH PARTITION messenger_archiveemail_default",
            "DROP TABLE messenger_archiveemail_default",
        ]
        self.assertCommandSuccess(['drop', 'DEFAULT', '--dry-run'], stdout='\n'.join(output) + '\n')

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_drop_skip_missing(self):
        sql = "CREATE TABLE {table}_default PARTITION OF {table} DEFAULT"
        self._execute_sql(sql.format(table='messenger_archiveemail'))
        self._execute_sql(sql.format(table='messenger_archiveemailreference'))

        output = [
            "ALTER TABLE messenger_archiveemailreference DETACH PARTITION messenger_archiveemailreference_default",
            "DROP TABLE messenger_archiveemailreference_default",
            "ALTER TABLE messenger_archiveemail DETACH PARTITION messenger_archiveemail_default",
            "DROP TABLE messenger_archiveemail_default",
        ]
        self.assertCommandSuccess(['drop', 'DEFAULT', '--dry-run'], stdout='\n'.join(output) + '\n')

    @skipUnless(APPLICATION.settings.test_db_connection.startswith('postgresql'), 'Test for postgres database only.')
    def test_main_drop_skip_detached(self):
        # Skip detach if partitions exist, but are not attached.
        sql = "CREATE TABLE {table}_default (LIKE {table})"
        self._execute_sql(sql.format(table='messenger_archiveemail'))
        self._execute_sql(sql.format(table='messenger_archiveemailreference'))

        output = [
            "DROP TABLE messenger_archiveemailreference_default",
            "DROP TABLE messenger_archiveemail_default",
        ]
        self.assertCommandSuccess(['drop', 'DEFAULT', '--dry-run'], stdout='\n'.join(output) + '\n')
