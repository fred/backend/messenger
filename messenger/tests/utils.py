"""Utilities for tests."""
import os
import warnings
from copy import copy
from functools import partial
from typing import Any, Dict, Optional, Sequence, Type, Union, cast
from unittest import TestCase
from unittest.mock import ANY, patch
from uuid import UUID

from click import BaseCommand
from click.testing import CliRunner, Result
from setapp.utils import override_settings as _override_settings
from sqlalchemy import create_engine
from sqlalchemy.engine import URL, make_url
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy.sql import text
from sqlalchemy_utils.functions import (
    create_database as _create_database,
    database_exists as _database_exists,
    drop_database as _drop_database,
)
from testfixtures import StringComparison

from messenger.commands.migrate_archive import get_fred_engine
from messenger.constants import ArchiveMessageStatus, MessageStatus
from messenger.database import get_engine, get_session_cls
from messenger.filters import Filter
from messenger.models import (
    METADATA,
    ArchiveEmail,
    ArchiveEmailReference,
    ArchiveLetter,
    ArchiveLetterReference,
    ArchiveSms,
    ArchiveSmsReference,
    Email,
    Letter,
    Sms,
)
from messenger.settings import APPLICATION, Application, Settings

try:
    from psycopg2.extensions import connection as psycopg2_connection
except ImportError:
    psycopg2_connection = type(None)

override_settings = partial(_override_settings, APPLICATION)


########################################################################################################################
# DATABASE UTILITIES
class TestSession(Session):
    def close(self):
        if self._transaction is not None and self._transaction.is_active:
            # Flush all the prepared objects.
            self.flush()
        super().close()

    def __del__(self):
        if self._transaction is not None and self._transaction.is_active:  # pragma: no cover
            warnings.warn("The test session hasn't been closed.", ResourceWarning, stacklevel=4)


def test_session_cls():
    return sessionmaker(bind=get_engine(), class_=TestSession)


def database_exists(url: URL) -> bool:
    """Check if database exists.

    Tweaked version to use template1 database to investigate postgresql databases and use environment variables.
    """
    url = copy(make_url(url))
    engine = None
    try:
        if url.drivername == 'postgresql':  # pragma: no cover
            opts = url.translate_connect_args(username='user')
            if opts:
                database = url.database
            else:
                # No connection parameters, use environment instead.
                database = os.environ.get('PGDATABASE')
            url = url.set(database='template1')
            sql = text("SELECT 1 FROM pg_database WHERE datname= :database").bindparams(database=database)

            engine = create_engine(url)
            with engine.connect() as conn:
                return bool(conn.scalar(sql))
        return cast(bool, _database_exists(url))
    finally:
        if engine:
            engine.dispose()


def create_database(url: URL, encoding: str = 'utf-8', template: Optional[str] = None) -> None:
    """Create database.

    Tweaked version to use postgresql environment variables, when appropriate.
    """
    if url.drivername == 'postgresql':  # pragma: no cover
        opts = url.translate_connect_args(username='user')
        if not opts:
            # No connection parameters, use environment instead.
            url = url.set(database=os.environ.get('PGDATABASE'))
    _create_database(url, encoding=encoding, template=template)


def drop_database(url: URL) -> None:
    """Drop database.

    Tweaked version to use postgresql environment variables, when appropriate.
    """
    if url.drivername == 'postgresql':  # pragma: no cover
        opts = url.translate_connect_args(username='user')
        if not opts:
            # No connection parameters, use environment instead.
            url = url.set(database=os.environ.get('PGDATABASE'))
    _drop_database(url)


class override_database:
    """Decorator and context manager to set up test database."""

    def __init__(self, *, db_connection: Optional[str] = None, db_echo: Optional[bool] = None,
                 fred_db_connection: Optional[str] = None):
        db_connection = db_connection or APPLICATION.settings.test_db_connection
        kwargs: Dict[str, Any] = {}
        if db_echo is not None:  # pragma: no cover
            kwargs['db_echo'] = db_echo
        if fred_db_connection is not None:
            kwargs['fred_db_connection'] = fred_db_connection
        self._override_settings = override_settings(db_connection=db_connection, **kwargs)

    def reset_database(self) -> None:
        """Reset database caches."""
        get_engine.cache_clear()
        get_session_cls.cache_clear()
        get_fred_engine.cache_clear()

    def enable(self) -> None:
        """Set up test database."""
        self._override_settings.enable()
        self.reset_database()

        # Create and migrate test database.
        if not database_exists(get_engine().url):
            create_database(get_engine().url)
        with get_engine().connect() as connection:
            METADATA.create_all(connection)
            if connection.engine.name == 'postgresql':
                sql = "CREATE TABLE IF NOT EXISTS {table}_default PARTITION OF {table} DEFAULT"
                # Create default partitions for partitioned tables.
                connection.execute(text(sql.format(table=ArchiveEmail.__tablename__)))
                connection.execute(text(sql.format(table=ArchiveEmailReference.__tablename__)))
                connection.execute(text(sql.format(table=ArchiveSms.__tablename__)))
                connection.execute(text(sql.format(table=ArchiveSmsReference.__tablename__)))
                connection.execute(text(sql.format(table=ArchiveLetter.__tablename__)))
                connection.execute(text(sql.format(table=ArchiveLetterReference.__tablename__)))
            connection.commit()

    def disable(self) -> None:
        """Tear down test database."""
        if not get_engine().name == 'sqlite':  # pragma: no cover
            drop_database(get_engine().url)
        self._override_settings.disable()
        self.reset_database()

    def __enter__(self):
        self.enable()

    def __exit__(self, exc_type, exc_value, traceback):
        self.disable()

    def __call__(self, decorated: Type[TestCase]) -> Type[TestCase]:
        """Decorate TestCase."""
        original_setup = decorated.setUp
        original_teardown = decorated.tearDown

        def setUp(inner_self):
            self.enable()
            try:
                original_setup(inner_self)
            except Exception:  # pragma: no cover
                self.disable()
                raise

        def tearDown(inner_self):
            original_teardown(inner_self)
            self.disable()

        decorated.setUp = setUp  # type: ignore
        decorated.tearDown = tearDown  # type: ignore
        return decorated


def make_email(
        session: Session, *, recipient: str, subject_template: str, body_template: str,
        status: MessageStatus = MessageStatus.PENDING, **kwargs: Any) -> Email:
    """Make and save an email."""
    email = Email(recipient=recipient, status=status, subject_template=subject_template,
                  body_template=body_template, **kwargs)
    session.add(email)
    session.commit()
    return email


def make_archive_email(
        session: Session, *, id: int, recipient: Sequence[str], subject_template: str, body_template: str,
        status: ArchiveMessageStatus = ArchiveMessageStatus.SENT, **kwargs: Any) -> ArchiveEmail:
    """Make and save an archive email."""
    email = ArchiveEmail(id=id, recipient=recipient, status=status, subject_template=subject_template,
                         body_template=body_template, **kwargs)
    session.add(email)
    session.commit()
    return email


def make_sms(session: Session, *, recipient: str, body_template: str, status: MessageStatus = MessageStatus.PENDING,
             **kwargs: Any) -> Sms:
    """Make and save a sms."""
    sms = Sms(recipient=recipient, status=status, body_template=body_template, **kwargs)
    session.add(sms)
    session.commit()
    return sms


def make_archive_sms(session: Session, *, id: int, recipient: str, body_template: str,
                     status: ArchiveMessageStatus = ArchiveMessageStatus.SENT, **kwargs: Any) -> ArchiveSms:
    """Make and save an archive SMS."""
    sms = ArchiveSms(id=id, recipient=recipient, status=status, body_template=body_template, **kwargs)
    session.add(sms)
    session.commit()
    return sms


def make_letter(session: Session, *, file: UUID, status: MessageStatus = MessageStatus.PENDING,
                **kwargs: Any) -> Letter:
    """Make and save a letter."""
    letter = Letter(status=status, file=file, **kwargs)
    session.add(letter)
    session.commit()
    return letter


def make_archive_letter(
        session: Session, *, id: int, file: UUID, status: ArchiveMessageStatus = ArchiveMessageStatus.SENT,
        **kwargs: Any) -> ArchiveLetter:
    """Make and save an archive letter."""
    letter = ArchiveLetter(id=id, status=status, file=file, **kwargs)
    session.add(letter)
    session.commit()
    return letter


class StaticFilter(Filter):
    """Filter which always return static value.

    By default, it accept all messages, as if no filter was defined.
    """

    def __init__(self, result: bool = True):
        self.result = result

    def filter(self, model: Any) -> bool:
        # Return whatever is inserted.
        return self.result


########################################################################################################################
# COMMAND UTILITIES
class TestApplication(Application):
    def load(self, config_file: Optional[str] = None) -> None:
        """Do nothing. Overrides are used instead."""


class CommandMixin:
    """Mixin for command tests."""

    runner = CliRunner(mix_stderr=False)
    command: BaseCommand

    def invoke(self, args: Optional[Sequence[str]] = None, *, settings: Optional[Dict] = None) -> Result:
        """Invoke a command and return a result."""
        # Replace the settings with test settings for the command run:
        # 1. Replace the application for `messenger_command` wrapper to bypass settings loading,
        #    but keep parsing of command line arguments.
        # 2. Replace the settings object within the APPLICATION, so it's used everywhere.
        test_app = TestApplication(Settings)
        new_settings = dict(APPLICATION.settings.model_dump(), **(settings or {}))
        test_app.settings = Settings(**new_settings)
        with patch('messenger.commands.utils.APPLICATION', new=test_app):
            old_settings = APPLICATION.settings
            try:
                APPLICATION.settings = test_app.settings
                return self.runner.invoke(self.command, args)
            finally:
                APPLICATION.settings = old_settings
                APPLICATION.clear_cache()

    def assertCommandSuccess(self, args: Optional[Sequence[str]] = None, *, stdout: Union[str, StringComparison] = ANY,
                             settings: Optional[Dict] = None) -> None:
        """Invoke command and check it succeeded."""
        result = self.invoke(args, settings=settings)

        cast(TestCase, self).assertIsNone(result.exception)
        err_msg = "command failed with code {}, stderr: {!r}".format(result.exit_code, result.stderr)
        cast(TestCase, self).assertEqual(result.exit_code, 0, msg=err_msg)
        cast(TestCase, self).assertEqual(result.stdout, stdout)
        cast(TestCase, self).assertEqual(result.stderr, '')

    def assertCommandFail(self, args: Optional[Sequence[str]] = None, *, stdout: str = '', stderr: str = '',
                          settings: Optional[Dict] = None) -> None:
        """Invoke command and check it failed."""
        result = self.invoke(args, settings=settings)

        cast(TestCase, self).assertNotEqual(result.exit_code, 0)
        cast(TestCase, self).assertEqual(result.stdout, stdout)
        cast(TestCase, self).assertIn(stderr, result.stderr)
