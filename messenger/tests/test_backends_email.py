import os
from datetime import datetime
from email.message import EmailMessage
from pathlib import Path
from smtplib import SMTP, SMTPConnectError, SMTPNotSupportedError
from tempfile import TemporaryDirectory
from typing import Dict, Iterable, List, Optional, Tuple, cast
from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel
from uuid import UUID

from filed import FileClient, Storage
from freezegun import freeze_time
from pytz import UTC
from testfixtures import Comparison, LogCapture
from typist import Template
from typist.client import TemplateNotFound

from messenger.backends.email import BaseEmailBackend, FileBackend, SmtpBackend
from messenger.constants import MessageStatus
from messenger.models import Email
from messenger.settings import APPLICATION

from .utils import override_settings

CERTIFICATE_PATH = Path(os.path.dirname(__file__)) / 'certificates'


class TestBackend(BaseEmailBackend):
    """Simple backend for tests of BaseEmailBackend."""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.emails: List[EmailMessage] = []

    def _send(self, message: EmailMessage) -> None:
        self.emails.append(message)


SENDER = 'holly@example.org'
RECIPIENT = 'kryten@example.org'
MESSAGE_ID = '<MESSAGE@ID>'
SUBJECT_TEMPLATE = 'subject.txt'
SUBJECT_UUID = UUID(int=255)
BODY_TEMPLATE = 'body.txt'
BODY_UUID = UUID(int=1024)
BODY_HTML_TEMPLATE = 'body.html'
BODY_HTML_UUID = UUID(int=2048)


def get_email(
        sender: Optional[str] = None, recipient: Optional[str] = None, message_id: str = MESSAGE_ID,
        extra_headers: Optional[Dict] = None, subject_template_uuid: Optional[UUID] = None,
        body_template_uuid: Optional[UUID] = None, body_template_html: Optional[str] = None,
        body_template_html_uuid: Optional[UUID] = None, attachments: Optional[Iterable[UUID]] = None,
        attempts: int = 0) -> Email:
    recipient = recipient or RECIPIENT
    return Email(
        sender=sender, recipient=recipient, message_id=message_id, status=MessageStatus.PENDING, attempts=attempts,
        extra_headers=extra_headers, subject_template=SUBJECT_TEMPLATE, subject_template_uuid=subject_template_uuid,
        body_template=BODY_TEMPLATE, body_template_uuid=body_template_uuid,
        body_template_html=body_template_html, body_template_html_uuid=body_template_html_uuid,
        context=sentinel.context, attachments=attachments)


@freeze_time('2020-02-02 14:00:00')
@override_settings()
class BaseEmailBackendTest(TestCase):
    subject = 'Very important document'
    body = 'Gazpacho!'
    body_html = '<h1>Gazpacho!</h1>'

    def setUp(self):
        patcher = patch('messenger.backends.base.SecretaryClient', autospec=True)
        self.addCleanup(patcher.stop)
        client_mock = patcher.start()
        self.secretary_mock = client_mock.return_value

        self.file_client = Mock(FileClient)
        APPLICATION.storage = Storage(self.file_client)
        self.backend = TestBackend(secretary_url='http://secretary.example/api/')
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()

    def _test_get_message(self, email: Email, sender: str = 'webmaster@localhost', recipient: str = RECIPIENT
                          ) -> EmailMessage:
        message = self.backend.get_message(email)

        self.assertEqual(message['Auto-Submitted'], 'auto-generated')
        self.assertEqual(message['From'], sender)
        self.assertEqual(message['To'], recipient)
        self.assertEqual(message['Subject'], self.subject)
        self.assertEqual(cast(EmailMessage, message.get_body('plain')).get_content().strip(), self.body)
        return message

    def test_get_message(self):
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID)

        message = self._test_get_message(email)

        self.assertFalse(message.is_multipart())
        self.assertEqual(tuple(message.iter_attachments()), ())
        self.assertEqual(self.secretary_mock.mock_calls,
                         [call.render(SUBJECT_UUID, sentinel.context), call.render(BODY_UUID, sentinel.context)])

    def test_get_message_multi_recipient(self):
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID,
                          recipient='kryten@example.org, rimmer@example.org')

        message = self._test_get_message(email, recipient='kryten@example.org, rimmer@example.org')

        self.assertFalse(message.is_multipart())
        self.assertEqual(tuple(message.iter_attachments()), ())
        self.assertEqual(self.secretary_mock.mock_calls,
                         [call.render(SUBJECT_UUID, sentinel.context), call.render(BODY_UUID, sentinel.context)])

    def test_get_message_html(self):
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        self.secretary_mock.render_html.return_value = self.body_html
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID,
                          body_template_html_uuid=BODY_HTML_UUID)

        message = self._test_get_message(email)

        self.assertTrue(message.is_multipart())
        self.assertEqual(tuple(message.iter_attachments()), ())
        self.assertEqual(cast(EmailMessage, message.get_body('html')).get_content().strip(), self.body_html)
        calls = [call.render(SUBJECT_UUID, sentinel.context), call.render(BODY_UUID, sentinel.context),
                 call.render_html(BODY_HTML_UUID, sentinel.context)]
        self.assertEqual(self.secretary_mock.mock_calls, calls)

    def test_get_message_sender(self):
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        email = get_email(sender=SENDER, subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID)

        message = self._test_get_message(email, sender=SENDER)

        self.assertFalse(message.is_multipart())
        self.assertEqual(tuple(message.iter_attachments()), ())
        self.assertEqual(self.secretary_mock.mock_calls,
                         [call.render(SUBJECT_UUID, sentinel.context), call.render(BODY_UUID, sentinel.context)])

    def test_get_message_sender_header(self):
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        sender = 'queeg-500@example.org'
        backend = TestBackend(secretary_url='http://secretary.example/api/', sender=sender)
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID)

        message = backend.get_message(email)

        self.assertEqual(message['From'], 'webmaster@localhost')
        self.assertEqual(message['Sender'], sender)
        self.assertEqual(self.secretary_mock.mock_calls,
                         [call.render(SUBJECT_UUID, sentinel.context), call.render(BODY_UUID, sentinel.context)])

    def test_get_message_attachments(self):
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        uuid = UUID(int=196)
        content = b'An attachment content'
        name = 'attachment.dat'
        mimetype = 'application/octet-stream'
        self.file_client.stat.return_value = {'name': name, 'mimetype': mimetype}
        self.file_client.read.return_value = [content]

        email = get_email(sender=SENDER, subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID,
                          attachments=[uuid])

        message = self._test_get_message(email, sender=SENDER)

        self.assertTrue(message.is_multipart())
        attachments = cast(Tuple[EmailMessage], tuple(message.iter_attachments()))
        self.assertEqual(len(attachments), 1)
        self.assertEqual(attachments[0].get_content_type(), mimetype)
        self.assertEqual(attachments[0].get_filename(), name)
        self.assertEqual(attachments[0].get_content(), content)
        self.assertEqual(self.secretary_mock.mock_calls,
                         [call.render(SUBJECT_UUID, sentinel.context), call.render(BODY_UUID, sentinel.context)])

    def test_get_message_strip_subject(self):
        # Regression for #28059 - test subject is stripped of whitespaces, which may break the message.
        self.secretary_mock.render.side_effect = ['\n  ' + self.subject + '\t\r\n', self.body]
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID)

        self._test_get_message(email)

        self.assertEqual(self.secretary_mock.mock_calls,
                         [call.render(SUBJECT_UUID, sentinel.context), call.render(BODY_UUID, sentinel.context)])

    def test_get_message_get_subject_uuid(self):
        self.secretary_mock.get_template_info.return_value = Template(
            name=SUBJECT_TEMPLATE, is_active=True, uuid=str(SUBJECT_UUID), create_datetime=datetime.now())
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        email = get_email(body_template_uuid=BODY_UUID)

        self._test_get_message(email)

        # Check subject UUID was updated
        self.assertEqual(email.subject_template_uuid, SUBJECT_UUID)

    def test_get_message_get_body_uuid(self):
        self.secretary_mock.get_template_info.return_value = Template(
            name=BODY_TEMPLATE, is_active=True, uuid=str(BODY_UUID), create_datetime=datetime.now())
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        email = get_email(subject_template_uuid=SUBJECT_UUID)

        self._test_get_message(email)

        # Check body UUID was updated
        self.assertEqual(email.body_template_uuid, BODY_UUID)

    def test_get_message_get_body_html_uuid(self):
        self.secretary_mock.get_template_info.return_value = Template(
            name=BODY_HTML_TEMPLATE, is_active=True, uuid=str(BODY_HTML_UUID), create_datetime=datetime.now())
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        self.secretary_mock.render_html.return_value = self.body_html
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID,
                          body_template_html=BODY_HTML_TEMPLATE)

        self._test_get_message(email)

        # Check body HTML UUID was updated
        self.assertEqual(email.body_template_html_uuid, BODY_HTML_UUID)

    def test_get_message_secretary_partial_error(self):
        # Test retrieved data from secretary are stored even though the whole message fails.
        template = Template(name=SUBJECT_TEMPLATE, is_active=True, uuid=str(SUBJECT_UUID),
                            create_datetime=datetime.now())
        self.secretary_mock.get_template_info.side_effect = [template, RuntimeError('Smeghead!')]
        email = get_email()

        with self.assertRaisesRegex(RuntimeError, 'Smeghead!'):
            self.backend.get_message(email)

        # Check email data
        self.assertEqual(email.subject_template_uuid, SUBJECT_UUID)
        self.assertIsNone(email.body_template_uuid)

    def _test_send(self, email: Email, body_html: Optional[str] = None) -> Email:
        email = self.backend.send(email)

        self.assertEqual(len(self.backend.emails), 1)
        self.assertEqual(self.backend.emails[0]['To'], RECIPIENT)
        self.assertEqual(self.backend.emails[0]['Message-ID'], MESSAGE_ID)
        # Check email was updated
        self.assertEqual(email.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertEqual(email.send_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertEqual(email.attempts, 1)
        self.assertEqual(email.status, MessageStatus.SENT)
        return email

    def test_send(self):
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID)

        self._test_send(email)

    def test_send_extra_headers(self):
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        reply_to = 'queeg-500@example.org'
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID,
                          extra_headers={'Reply-To': reply_to})

        self._test_send(email)

        self.assertEqual(self.backend.emails[0]['Reply-To'], reply_to)

    def test_send_extra_headers_from(self):
        # Test conflicting headers from `extra_headers` are not allowed.
        self.secretary_mock.render.side_effect = [self.subject, self.body]
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID,
                          extra_headers={'From': 'lister@example.cz'})

        email = self.backend.send(email)

        # Nothing sent
        self.assertEqual(self.backend.emails, [])
        # Check email data
        self.assertEqual(email.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(email.send_datetime)
        self.assertEqual(email.attempts, 1)
        self.assertEqual(email.status, MessageStatus.ERROR)

    def test_send_secretary_not_found(self):
        self.secretary_mock.get_template_info.side_effect = TemplateNotFound
        email = get_email()

        email = self.backend.send(email)

        # Nothing sent
        self.assertEqual(self.backend.emails, [])
        # Check email data
        self.assertEqual(email.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(email.send_datetime)
        self.assertEqual(email.attempts, 1)
        self.assertEqual(email.status, MessageStatus.ERROR)
        self.assertIsNone(email.subject_template_uuid)
        self.assertIsNone(email.body_template_uuid)

    def test_send_secretary_unknown_error(self):
        self.secretary_mock.get_template_info.side_effect = Exception
        email = get_email()

        email = self.backend.send(email)

        # Nothing sent
        self.assertEqual(self.backend.emails, [])
        # Check email data
        self.assertEqual(email.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(email.send_datetime)
        self.assertEqual(email.attempts, 1)
        self.assertEqual(email.status, MessageStatus.ERROR)
        self.assertIsNone(email.subject_template_uuid)
        self.assertIsNone(email.body_template_uuid)

    def test_send_secretary_transient_error(self):
        self.secretary_mock.get_template_info.side_effect = RuntimeError
        email = get_email()

        email = self.backend.send(email)

        # Nothing sent
        self.assertEqual(self.backend.emails, [])
        # Check email data
        self.assertEqual(email.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(email.send_datetime)
        self.assertEqual(email.attempts, 1)
        self.assertEqual(email.status, MessageStatus.PENDING)
        self.assertIsNone(email.subject_template_uuid)
        self.assertIsNone(email.body_template_uuid)


@freeze_time('2020-02-02 14:00:00')
class FileBackendTest(TestCase):
    def setUp(self):
        patcher = patch('messenger.backends.base.SecretaryClient', autospec=True)
        self.addCleanup(patcher.stop)
        client_mock = patcher.start()
        self.secretary_mock = client_mock.return_value

        self.tmp_dir = TemporaryDirectory()

    def tearDown(self):
        self.tmp_dir.cleanup()

    def _test_send(self, path: str) -> None:
        self.secretary_mock.render.return_value = ''
        uuid = UUID(int=42)
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID)
        email.uuid = uuid

        backend = FileBackend(secretary_url='http://secretary.example/api/', path=path)
        backend.send(email)

        # Check email was written
        filename = '20200202-140000-{}.eml'.format(MESSAGE_ID)
        self.assertEqual(os.listdir(path), [filename])
        with open(os.path.join(path, filename)) as result:
            self.assertIn('To: {}'.format(RECIPIENT), result.read())

    def test_send(self):
        path = self.tmp_dir.name
        self._test_send(path)

    def test_send_mkdir(self):
        # Test send creates directory to write emails, if it doesn't exist.
        path = os.path.join(self.tmp_dir.name, 'some', 'where')
        self._test_send(path)


class SmtpBackendTest(TestCase):
    def setUp(self):
        patcher = patch('messenger.backends.base.SecretaryClient', autospec=True)
        self.addCleanup(patcher.stop)
        self.client_mock = patcher.start()
        self.secretary_mock = self.client_mock.return_value

        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()

    def test_init(self):
        backend = SmtpBackend(max_attempts=sentinel.max_attempts, host=sentinel.host, port=sentinel.port,
                              secretary_url=sentinel.secretary_url)
        self.assertEqual(backend.max_attempts, sentinel.max_attempts)
        self.assertEqual(backend.host, sentinel.host)
        self.assertEqual(backend.port, sentinel.port)

    def test_init_ssl_tls(self):
        with self.assertRaisesRegex(ValueError, 'Arguments use_ssl and use_tls are mutually exclusive.'):
            SmtpBackend(use_ssl=True, use_tls=True, secretary_url=sentinel.secretary_url)

    def test_open(self):
        backend = SmtpBackend(host=sentinel.host, port=sentinel.port, secretary_url=sentinel.secretary_url)
        with patch('smtplib.SMTP', autospec=True, return_value=sentinel.connection) as smtp_mock:
            backend.open()

        self.assertEqual(backend.connection, sentinel.connection)
        self.assertEqual(smtp_mock.mock_calls, [call(sentinel.host, sentinel.port)])

    def test_open_timeout(self):
        backend = SmtpBackend(host=sentinel.host, port=sentinel.port, timeout=sentinel.timeout,
                              secretary_url=sentinel.secretary_url)
        with patch('smtplib.SMTP', autospec=True, return_value=sentinel.connection) as smtp_mock:
            backend.open()

        self.assertEqual(backend.connection, sentinel.connection)
        self.assertEqual(smtp_mock.mock_calls, [call(sentinel.host, sentinel.port, timeout=sentinel.timeout)])

    def test_open_ssl(self):
        data = (
            # certfile, keyfile, verify, ssl_call
            (None, None, True, call(None, None, verify=True)),
            (None, None, False, call(None, None, verify=False)),
            (sentinel.certfile, None, True, call(sentinel.certfile, None, verify=True)),
            (sentinel.certfile, sentinel.keyfile, True, call(sentinel.certfile, sentinel.keyfile, verify=True)),
            (sentinel.certfile, sentinel.keyfile, False, call(sentinel.certfile, sentinel.keyfile, verify=False)),
        )
        for certfile, keyfile, verify, ssl_call in data:
            with self.subTest(certfile=certfile, keyfile=keyfile, verify=verify):
                backend = SmtpBackend(host=sentinel.host, port=sentinel.port, use_ssl=True, certfile=certfile,
                                      keyfile=keyfile, verify=verify, secretary_url=sentinel.secretary_url)
                with patch('messenger.backends.email.create_ssl_context', autospec=True) as ssl_mock:
                    with patch('smtplib.SMTP_SSL', autospec=True, return_value=sentinel.connection) as smtp_mock:
                        backend.open()

                self.assertEqual(backend.connection, sentinel.connection)
                self.assertEqual(smtp_mock.mock_calls,
                                 [call(sentinel.host, sentinel.port, context=ssl_mock.return_value)])
                self.assertEqual(ssl_mock.mock_calls, [ssl_call])

    def test_open_credentials(self):
        backend = SmtpBackend(host=sentinel.host, port=sentinel.port, username=sentinel.username,
                              password=sentinel.password, secretary_url=sentinel.secretary_url)
        with patch('smtplib.SMTP', autospec=True) as smtp_mock:
            backend.open()

        calls = [call(sentinel.host, sentinel.port), call().login(sentinel.username, sentinel.password)]
        self.assertEqual(smtp_mock.mock_calls, calls)

    def test_open_tls(self):
        data = (
            # certfile, keyfile, verify, ssl_call
            (None, None, True, call(None, None, verify=True)),
            (None, None, False, call(None, None, verify=False)),
            (sentinel.certfile, None, True, call(sentinel.certfile, None, verify=True)),
            (sentinel.certfile, sentinel.keyfile, True, call(sentinel.certfile, sentinel.keyfile, verify=True)),
            (sentinel.certfile, sentinel.keyfile, False, call(sentinel.certfile, sentinel.keyfile, verify=False)),
        )
        for certfile, keyfile, verify, ssl_call in data:
            with self.subTest(certfile=certfile, keyfile=keyfile, verify=verify):
                backend = SmtpBackend(host=sentinel.host, port=sentinel.port, use_tls=True, certfile=certfile,
                                      keyfile=keyfile, verify=verify, secretary_url=sentinel.secretary_url)
                with patch('messenger.backends.email.create_ssl_context', autospec=True) as ssl_mock:
                    with patch('smtplib.SMTP', autospec=True) as smtp_mock:
                        backend.open()

                calls = [call(sentinel.host, sentinel.port), call().starttls(context=ssl_mock.return_value)]
                self.assertEqual(smtp_mock.mock_calls, calls)
                self.assertEqual(ssl_mock.mock_calls, [ssl_call])

    def test_open_twice(self):
        # Test second call of open uses previous connection
        backend = SmtpBackend(host=sentinel.host, port=sentinel.port, secretary_url=sentinel.secretary_url)
        with patch('smtplib.SMTP', autospec=True, return_value=sentinel.connection) as smtp_mock:
            backend.open()
            backend.open()

        self.assertEqual(backend.connection, sentinel.connection)
        self.assertEqual(smtp_mock.mock_calls, [call(sentinel.host, sentinel.port)])

    def test_open_error(self):
        backend = SmtpBackend(host=sentinel.host, port=sentinel.port, secretary_url=sentinel.secretary_url)
        with patch('smtplib.SMTP', autospec=True,
                   side_effect=SMTPConnectError(sentinel.code, 'Quagaars!')) as smtp_mock:
            with self.assertRaisesRegex(SMTPConnectError, 'Quagaars!'):
                backend.open()

        self.assertEqual(smtp_mock.mock_calls, [call(sentinel.host, sentinel.port)])

    def test_open_credentials_error(self):
        backend = SmtpBackend(host=sentinel.host, port=sentinel.port, username=sentinel.username,
                              password=sentinel.password, secretary_url=sentinel.secretary_url)
        with patch('smtplib.SMTP', autospec=True) as smtp_mock:
            smtp_mock.return_value.login.side_effect = SMTPNotSupportedError('Quagaars!')
            with self.assertRaisesRegex(SMTPNotSupportedError, 'Quagaars!'):
                backend.open()

        calls = [call(sentinel.host, sentinel.port), call().login(sentinel.username, sentinel.password)]
        self.assertEqual(smtp_mock.mock_calls, calls)

    def test_open_tls_error(self):
        backend = SmtpBackend(host=sentinel.host, port=sentinel.port, use_tls=True,
                              secretary_url=sentinel.secretary_url)
        with patch('smtplib.SMTP', autospec=True) as smtp_mock:
            smtp_mock.return_value.starttls.side_effect = SMTPNotSupportedError('Quagaars!')
            with self.assertRaisesRegex(SMTPNotSupportedError, 'Quagaars!'):
                backend.open()

    def test_close(self):
        backend = SmtpBackend(secretary_url=sentinel.secretary_url, host=sentinel.host, port=sentinel.port)
        # Mock the connection.
        connection_mock = Mock()
        backend.connection = connection_mock

        backend.close()

        self.assertIsNone(backend.connection)
        self.assertEqual(connection_mock.mock_calls, [call.quit()])

    def test_close_unopened(self):
        backend = SmtpBackend(secretary_url=sentinel.secretary_url, host=sentinel.host, port=sentinel.port)
        # No connection opened.
        backend.close()

        self.assertIsNone(backend.connection)

    def test_close_disconnected(self):
        # Test closing a disconnected SMTP.
        backend = SmtpBackend(secretary_url=sentinel.secretary_url, host=sentinel.host, port=sentinel.port)
        # Set up a disconnected SMTP.
        backend.connection = SMTP()

        backend.close()

        self.assertIsNone(backend.connection)

    def test_send(self):
        self.secretary_mock.render.return_value = ''
        backend = SmtpBackend(secretary_url='http://secretary.example/api/')
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID)
        smtp_mock = Mock(SMTP)
        backend.connection = smtp_mock

        backend.send(email)

        self.assertEqual(smtp_mock.mock_calls, [call.send_message(Comparison(EmailMessage))])
        self.assertEqual(smtp_mock.mock_calls[0][1][0]['To'], RECIPIENT)

    @freeze_time('2020-02-02 14:00:00')
    def test_send_smtp_error(self):
        self.secretary_mock.render.return_value = ''
        backend = SmtpBackend(secretary_url='http://secretary.example/api/')
        email = get_email(subject_template_uuid=SUBJECT_UUID, body_template_uuid=BODY_UUID)
        smtp_mock = Mock(SMTP)
        smtp_mock.send_message.side_effect = SMTPNotSupportedError
        backend.connection = smtp_mock

        email = backend.send(email)

        self.assertEqual(smtp_mock.mock_calls, [call.send_message(Comparison(EmailMessage))])
        # Check email data
        self.assertEqual(email.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(email.send_datetime)
        self.assertEqual(email.attempts, 1)
        self.assertEqual(email.status, MessageStatus.PENDING)
