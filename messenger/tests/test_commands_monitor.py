import json
from datetime import datetime
from typing import Any, Dict
from unittest import TestCase

import yaml
from click.testing import Result
from freezegun import freeze_time
from pytz import UTC

from messenger.commands.monitor import main
from messenger.constants import MessageStatus, TransportMethod

from .utils import CommandMixin, make_email, override_database, test_session_cls


@freeze_time('2020-02-01 14:00:00')
@override_database()
class MonitorMainTest(CommandMixin, TestCase):
    command = main

    recipient = 'kryten@example.com'
    subject_template = 'subject.txt'
    body_template = 'body.txt'
    recent_time = datetime(2020, 2, 1, 13, 30, tzinfo=UTC)
    non_recent_time = datetime(2020, 2, 1, 12, 30, tzinfo=UTC)

    def setUp(self):
        self.session = test_session_cls()()

    def tearDown(self):
        self.session.close()

    def assertYamlOutput(self, result: Result, stats: Dict[str, Any]) -> None:
        """Check if result contains correct data in YAML format."""
        self.assertEqual(yaml.safe_load(result.stdout), stats)

    def _test_monitor(self, stats: Dict[str, Any]) -> None:
        result = self.invoke(['--format', 'yaml'])

        self.assertYamlOutput(result, stats)

    def test_main_no_messages(self):
        stats = {'period': 3600,
                 TransportMethod.EMAIL: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.SMS: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.LETTER: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0}}
        self._test_monitor(stats)

    def test_main_new_email(self):
        make_email(self.session, recipient=self.recipient, create_datetime=self.recent_time,
                   subject_template=self.subject_template, body_template=self.body_template)
        stats = {'period': 3600,
                 TransportMethod.EMAIL: {'created': 1, 'pending': 1, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.SMS: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.LETTER: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0}}
        self._test_monitor(stats)

    def test_main_sent(self):
        make_email(self.session, recipient=self.recipient, create_datetime=self.non_recent_time,
                   status=MessageStatus.SENT, send_datetime=self.recent_time,
                   subject_template=self.subject_template, body_template=self.body_template)
        stats = {'period': 3600,
                 TransportMethod.EMAIL: {'created': 0, 'pending': 0, 'sent': 1, 'failed': 0, 'errors': 0},
                 TransportMethod.SMS: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.LETTER: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0}}
        self._test_monitor(stats)

    def test_main_failed(self):
        make_email(self.session, recipient=self.recipient, create_datetime=self.non_recent_time,
                   update_datetime=self.recent_time, status=MessageStatus.FAILED,
                   subject_template=self.subject_template, body_template=self.body_template)
        stats = {'period': 3600,
                 TransportMethod.EMAIL: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 1, 'errors': 0},
                 TransportMethod.SMS: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.LETTER: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0}}
        self._test_monitor(stats)

    def test_main_errors(self):
        make_email(self.session, recipient=self.recipient, create_datetime=self.non_recent_time,
                   update_datetime=self.recent_time, status=MessageStatus.ERROR,
                   subject_template=self.subject_template, body_template=self.body_template)
        stats = {'period': 3600,
                 TransportMethod.EMAIL: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 1},
                 TransportMethod.SMS: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.LETTER: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0}}
        self._test_monitor(stats)

    def test_main_pending(self):
        # Test pending emails are counted regardless of their age.
        make_email(self.session, recipient=self.recipient, create_datetime=self.non_recent_time,
                   subject_template=self.subject_template, body_template=self.body_template)
        stats = {'period': 3600,
                 TransportMethod.EMAIL: {'created': 0, 'pending': 1, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.SMS: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.LETTER: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0}}
        self._test_monitor(stats)

    def test_main_custom_period(self):
        make_email(self.session, recipient=self.recipient, create_datetime=self.recent_time,
                   subject_template=self.subject_template, body_template=self.body_template)
        make_email(self.session, recipient=self.recipient, create_datetime=self.non_recent_time,
                   subject_template=self.subject_template, body_template=self.body_template)

        result = self.invoke(['--period', '7200', '--format', 'yaml'])

        stats = {'period': 7200,
                 TransportMethod.EMAIL: {'created': 2, 'pending': 2, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.SMS: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.LETTER: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0}}
        self.assertYamlOutput(result, stats)

    def test_type(self):
        make_email(self.session, recipient=self.recipient, create_datetime=self.recent_time,
                   subject_template=self.subject_template, body_template=self.body_template)

        result = self.invoke(['--method', 'email', '--format', 'yaml'])

        stats = {'period': 3600,
                 TransportMethod.EMAIL: {'created': 1, 'pending': 1, 'sent': 0, 'failed': 0, 'errors': 0}}
        self.assertYamlOutput(result, stats)

    def test_format_text(self):
        make_email(self.session, recipient=self.recipient, create_datetime=self.recent_time,
                   subject_template=self.subject_template, body_template=self.body_template)

        output = ('emails per last 3600 seconds:\n'
                  'Created: 1\n'
                  'Sent: 0\n'
                  'Failed: 0\n'
                  'Errors: 0\n'
                  'Pending total: 1\n'
                  'smss per last 3600 seconds:\n'
                  'Created: 0\n'
                  'Sent: 0\n'
                  'Failed: 0\n'
                  'Errors: 0\n'
                  'Pending total: 0\n'
                  'letters per last 3600 seconds:\n'
                  'Created: 0\n'
                  'Sent: 0\n'
                  'Failed: 0\n'
                  'Errors: 0\n'
                  'Pending total: 0\n')
        self.assertCommandSuccess(stdout=output)

    def test_format_text_method(self):
        # Test text output for a single method
        make_email(self.session, recipient=self.recipient, create_datetime=self.recent_time,
                   subject_template=self.subject_template, body_template=self.body_template)

        output = ('emails per last 3600 seconds:\n'
                  'Created: 1\n'
                  'Sent: 0\n'
                  'Failed: 0\n'
                  'Errors: 0\n'
                  'Pending total: 1\n')
        self.assertCommandSuccess(['--method', 'email'], stdout=output)

    def test_format_json(self):
        make_email(self.session, recipient=self.recipient, create_datetime=self.recent_time,
                   subject_template=self.subject_template, body_template=self.body_template)

        result = self.invoke(['--format', 'json'])

        stats = {'period': 3600,
                 TransportMethod.EMAIL: {'created': 1, 'pending': 1, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.SMS: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0},
                 TransportMethod.LETTER: {'created': 0, 'pending': 0, 'sent': 0, 'failed': 0, 'errors': 0}}
        self.assertEqual(json.loads(result.stdout), stats)
