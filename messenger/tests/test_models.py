import unittest
from typing import Type, Union, cast
from unittest import TestCase
from unittest.mock import sentinel
from uuid import UUID

from sqlalchemy import select
from sqlalchemy.orm import Mapped
from sqlalchemy.sql.expression import BinaryExpression

from messenger.constants import ReferenceType
from messenger.models import BlockedAddress, BlockedEmail, BlockedPhone
from messenger.models.base import BaseModel
from messenger.models.email import ArchiveEmail, Email, make_message_id
from messenger.models.letter import Address, ArchiveLetter, Letter
from messenger.models.message import ArchiveMessageMixin, BaseMessageMixin, ReferenceMixin
from messenger.models.utils import ScalarListType

from .utils import override_settings


class BaseTest(unittest.TestCase):
    def test_tablename(self):
        class TestModel(BaseModel):
            pass

        self.assertEqual(TestModel.__tablename__, 'messenger_testmodel')


class MakeMessageIdTest(unittest.TestCase):
    def test_msgid(self):
        with override_settings(msgid_domain='example.org'):
            msgid = make_message_id()

        self.assertTrue(msgid.endswith('@example.org>'))


class TestBaseMessage(BaseMessageMixin, BaseModel):
    """Simple model to test BaseMessageMixin."""
    uuid: Mapped[UUID]
    recipient: Mapped[str]


class BaseMessageMixinTest(unittest.TestCase):
    def test_repr_empty(self):
        self.assertEqual(repr(TestBaseMessage()), 'TestBaseMessage(uuid=None, recipient=None)')

    def test_repr(self):
        data = (
            ('arnold@example.org', "TestBaseMessage(uuid=None, recipient='arnold@example.org')"),
            ('arnold' * 200 + '@example.org',
             "TestBaseMessage(uuid=None, recipient='" + "arnold" * 164 + "ar...)"),
        )
        for recipient, result in data:
            with self.subTest(recipient=recipient):
                message = TestBaseMessage(recipient=recipient)
                self.assertEqual(repr(message), result)

    def test_repr_property(self):
        # Test repr with recipient as a property.

        class TestMessage(BaseMessageMixin, BaseModel):
            uuid: Mapped[UUID]

            @property
            def recipient(self):
                return 'arnold@example.org'

        message = TestMessage()
        self.assertEqual(repr(message), "TestMessage(uuid=None, recipient='arnold@example.org')")


class ArchiveMessageMixinTest(unittest.TestCase):
    def test_archive(self):
        class TestArchiveMessage(ArchiveMessageMixin, BaseModel):
            pass

        message = TestArchiveMessage()
        self.assertTrue(message.archive)


class TestReference(ReferenceMixin, BaseModel):
    """Simple model to test ReferenceMixin."""

    message_id: Mapped[int]


class ReferenceMixinTest(TestCase):
    def test_type_getter(self):
        data = (
            # reference, type
            (TestReference(), None),
            (TestReference(_old_type=ReferenceType.DOMAIN), 'domain'),
            (TestReference(_new_type='Gazpacho!'), 'Gazpacho!'),
            (TestReference(_old_type=ReferenceType.DOMAIN, _new_type='Gazpacho!'), 'Gazpacho!'),
        )
        for reference, type in data:
            with self.subTest(reference=reference):
                self.assertEqual(reference.type, type)

    def test_type_setter(self):
        data = (
            # reference, value, old_type, new_type
            (TestReference(), None, None, None),
            (TestReference(), 'Gazpacho!', None, 'Gazpacho!'),
            (TestReference(_old_type=ReferenceType.DOMAIN), 'Gazpacho!', ReferenceType.DOMAIN, 'Gazpacho!'),
            (TestReference(_new_type='Quagaars!'), 'Gazpacho!', None, 'Gazpacho!'),
        )
        for reference, value, old_type, new_type in data:
            with self.subTest(reference=reference):
                reference.type = value  # type: ignore[method-assign]  # hybrid_property

                self.assertEqual(reference._old_type, old_type)
                self.assertEqual(reference._new_type, new_type)

    def test_type_expression(self):
        # Test expression works properly.
        self.assertEqual(
            tuple(select(TestReference.type).selected_columns),  # type: ignore[call-overload]
            tuple(select(TestReference._new_type).selected_columns),
        )

    def test_old_type_getter(self):
        data = (
            # reference, old_type
            (TestReference(), None),
            (TestReference(_old_type=ReferenceType.DOMAIN), ReferenceType.DOMAIN),
            (TestReference(_new_type='domain'), ReferenceType.DOMAIN),
            (TestReference(_new_type='Gazpacho!'), None),
        )
        for reference, type in data:
            with self.subTest(reference=reference):
                self.assertEqual(reference.old_type, type)


class EmailTest(TestCase):
    model: Union[Type[Email], Type[ArchiveEmail]] = Email

    def test_recipient_getter(self):
        email = self.model(_recipient='kryten@example.org, rimmer@example.org')
        self.assertEqual(email.recipient, 'kryten@example.org, rimmer@example.org')

    def test_recipient_getter_old(self):
        email = self.model(_old_recipient=['kryten@example.org', 'rimmer@example.org'])
        self.assertEqual(email.recipient, 'kryten@example.org, rimmer@example.org')

    def test_recipient_setter(self):
        email = self.model()
        email.recipient = 'kryten@example.org, rimmer@example.org'

        self.assertEqual(email._recipient, 'kryten@example.org, rimmer@example.org')
        self.assertIsNone(email._old_recipient)

    def test_recipient_setter_old(self):
        # Test setting the recipient on model with _old_recipient already set.
        email = self.model(_old_recipient='holly@example.org')
        email.recipient = 'kryten@example.org, rimmer@example.org'

        self.assertEqual(email._recipient, 'kryten@example.org, rimmer@example.org')
        self.assertIsNone(email._old_recipient)

    def test_recipient_expression(self):
        # Test expression works properly.
        expr = cast(BinaryExpression, self.model.recipient == 'kryten@example.org')

        self.assertEqual(expr.left.clauses.clauses, [self.model._old_recipient, self.model._recipient])
        self.assertRegex(
            str(expr),
            r'CASE WHEN \(.*\.old_recipient IS NULL\) THEN .*\.recipient ELSE .*\.old_recipient END = :param_1')


class ArchiveEmailTest(EmailTest):
    model = ArchiveEmail


class AddressTest(TestCase):
    def test_str(self):
        data = (
            # address, str
            (Address(), ''),
            (Address(name='Kryten'), 'Kryten'),
            (Address(organization='Jupiter Mining Company'), 'Jupiter Mining Company'),
            (Address(street='Deck 16'), 'Deck 16'),
            (Address(city='Red Dwarf'), 'Red Dwarf'),
            (Address(state='Deep Space'), 'Deep Space'),
            (Address(postal_code='JMC'), 'JMC'),
            (Address(country='Jupiter'), 'Jupiter'),
            # Full address
            (Address(name='Kryten', organization='Jupiter Mining Company', street='Deck 16', city='Red Dwarf',
                     state='Deep Space', postal_code='JMC', country='Jupiter'),
             'Kryten, Jupiter Mining Company, Deck 16, Red Dwarf, Deep Space, JMC, Jupiter'),
        )
        for address, out in data:
            with self.subTest(address=address):
                self.assertEqual(str(address), out)


class LetterTest(unittest.TestCase):
    model_cls: Union[Type[Letter], Type[ArchiveLetter]] = Letter

    def test_recipient_getter(self):
        letter = self.model_cls(
            recipient_name=sentinel.name, recipient_organization=sentinel.organization,
            recipient_street=sentinel.street, recipient_city=sentinel.city, recipient_state=sentinel.state,
            recipient_postal_code=sentinel.postal_code, recipient_country=sentinel.country)

        recipient = letter.recipient

        address = Address(name=sentinel.name, organization=sentinel.organization, street=sentinel.street,
                          city=sentinel.city, state=sentinel.state, postal_code=sentinel.postal_code,
                          country=sentinel.country)
        self.assertEqual(recipient, address)

    def test_recipient_setter(self):
        letter = self.model_cls()
        address = Address(name=sentinel.name, organization=sentinel.organization, street=sentinel.street,
                          city=sentinel.city, state=sentinel.state, postal_code=sentinel.postal_code,
                          country=sentinel.country)

        letter.recipient = address

        self.assertEqual(letter.recipient_name, sentinel.name)
        self.assertEqual(letter.recipient_organization, sentinel.organization)
        self.assertEqual(letter.recipient_street, sentinel.street)
        self.assertEqual(letter.recipient_city, sentinel.city)
        self.assertEqual(letter.recipient_state, sentinel.state)
        self.assertEqual(letter.recipient_postal_code, sentinel.postal_code)
        self.assertEqual(letter.recipient_country, sentinel.country)


class ArchiveLetterTest(LetterTest):
    model_cls = ArchiveLetter


class ScalarListTypeTest(unittest.TestCase):
    def test_init_func(self):
        def _test():
            pass  # pragma: no cover

        with self.assertRaisesRegex(ValueError, 'Inner type .* is not a type.'):
            ScalarListType(_test)  # type: ignore[arg-type] # mock

    def test_process_bind_param(self):
        field = ScalarListType(inner_type=int)
        self.assertEqual(field.process_bind_param([1, 2, 3, 4], sentinel.dialect), '1,2,3,4')

    def test_process_bind_param_invalid(self):
        field = ScalarListType(inner_type=int)
        with self.assertRaisesRegex(ValueError, 'Not all items in value .* match the type .*'):
            field.process_bind_param([1, 2, 'invalid', 4], sentinel.dialect)

    def test_process_bind_param_empty(self):
        field = ScalarListType(inner_type=int)
        self.assertEqual(field.process_bind_param([], sentinel.dialect), '')

    def test_process_bind_param_none(self):
        field = ScalarListType(inner_type=int)
        self.assertIsNone(field.process_bind_param(None, sentinel.dialect))

    def test_process_result_value(self):
        field = ScalarListType(inner_type=int)
        self.assertEqual(field.process_result_value('1,2,3,4', sentinel.dialect), [1, 2, 3, 4])

    def test_process_result_value_invalid(self):
        field = ScalarListType(inner_type=int)
        with self.assertRaisesRegex(ValueError, 'invalid literal for int()'):
            field.process_result_value('1,2,invalid,4', sentinel.dialect)

    def test_process_result_value_none(self):
        field = ScalarListType(inner_type=int)
        self.assertIsNone(field.process_result_value(None, sentinel.dialect))

    def test_process_result_value_empty(self):
        field = ScalarListType(inner_type=int)
        self.assertEqual(field.process_result_value('', sentinel.dialect), [])


class BlockedEmailTest(TestCase):
    def test_str(self):
        self.assertEqual(str(BlockedEmail()), '')
        self.assertEqual(str(BlockedEmail(email='kryten@example.org')), 'kryten@example.org')


class BlockedPhoneTest(TestCase):
    def test_str(self):
        self.assertEqual(str(BlockedPhone()), '')
        self.assertEqual(str(BlockedPhone(phone='1234')), '1234')


class BlockedAddressTest(TestCase):
    def test_str(self):
        self.assertEqual(str(BlockedAddress()), '')
        self.assertEqual(str(BlockedAddress(name='Arnold Rimmer')), 'Arnold Rimmer')
        self.assertEqual(str(BlockedAddress(name='Arnold Rimmer', state='Deep Space')), 'Arnold Rimmer, Deep Space')
        address = BlockedAddress(name='Arnold Rimmer', organization='Jupiter Mining Company', street='Deck 16',
                                 state='Deep Space', postal_code='JMC', country='Jupiter')
        self.assertEqual(str(address), 'Arnold Rimmer, Jupiter Mining Company, Deck 16, Deep Space, JMC, Jupiter')
