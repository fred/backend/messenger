import json
from typing import Union
from unittest import TestCase

from testfixtures import StringComparison

from messenger.commands.block import main
from messenger.models import BlockedAddress, BlockedEmail, BlockedPhone

from .utils import CommandMixin, override_database, test_session_cls


@override_database()
class AddTest(CommandMixin, TestCase):
    command = main

    def setUp(self):
        self.session = test_session_cls()()

    def tearDown(self):
        self.session.close()

    def _test_email(self, email: str, verbosity: int, *, stdout: str) -> None:
        self.assertCommandSuccess(['add', 'email', email, '--verbosity', str(verbosity)], stdout=stdout)

        blocked = self.session.query(BlockedEmail).one()
        self.assertEqual(blocked.email, 'rimmer@example.org')

    def test_email_silent(self):
        email = 'rimmer@example.org'
        self._test_email(email, 0, stdout='')

    def test_email_info(self):
        email = 'rimmer@example.org'
        self._test_email(email, 1, stdout='Email rimmer@example.org was blocked.\n')

    def test_email_retry_silent(self):
        email = 'rimmer@example.org'
        self.session.add(BlockedEmail(email=email))
        self.session.commit()
        self._test_email(email, 0, stdout='')

    def test_email_retry_info(self):
        email = 'rimmer@example.org'
        self.session.add(BlockedEmail(email=email))
        self.session.commit()
        self._test_email(email, 1, stdout='Email rimmer@example.org is already blocked.\n')

    def test_email_lowercase(self):
        email = 'RimMer@eXamPle.oRg'
        self._test_email(email, 0, stdout='')

    def test_email_normalize(self):
        email = 'Arnold Rimmer <rimmer@example.org>'
        self._test_email(email, 0, stdout='')

    def test_email_error(self):
        self.assertCommandFail(['add', 'email', 'invalid'], stderr="'invalid' is not a valid email address.")
        self.assertEqual(self.session.query(BlockedEmail).count(), 0)

    def _test_phone(self, phone: str, verbosity: int, *, stdout: str) -> None:
        self.assertCommandSuccess(['add', 'phone', phone, '--verbosity', str(verbosity)], stdout=stdout)

        blocked = self.session.query(BlockedPhone).one()
        self.assertEqual(blocked.phone, '+1234')

    def test_phone_silent(self):
        phone = '+1234'
        self._test_phone(phone, 0, stdout='')

    def test_phone_info(self):
        phone = '+1234'
        self._test_phone(phone, 1, stdout='Phone +1234 was blocked.\n')

    def test_phone_retry_silent(self):
        phone = '+1234'
        self.session.add(BlockedPhone(phone=phone))
        self.session.commit()
        self._test_phone(phone, 0, stdout='')

    def test_phone_retry_info(self):
        phone = '+1234'
        self.session.add(BlockedPhone(phone=phone))
        self.session.commit()
        self._test_phone(phone, 1, stdout='Phone +1234 is already blocked.\n')

    def test_phone_normalize(self):
        phone = '+1.234'
        self._test_phone(phone, 0, stdout='')

    def test_phone_error(self):
        self.assertCommandFail(['add', 'phone', 'invalid'], stderr="'invalid' is not a valid phone number.")
        self.assertEqual(self.session.query(BlockedPhone).count(), 0)

    def _test_address(self, address: str, verbosity: int, *, stdout: Union[str, StringComparison]) -> None:
        self.assertCommandSuccess(['add', 'address', address, '--verbosity', str(verbosity)], stdout=stdout)

        blocked = self.session.query(BlockedAddress).one()
        self.assertEqual(blocked.name, 'Arnold Rimmer')
        self.assertEqual(blocked.organization, 'Jupiter Mining Company')
        self.assertEqual(blocked.street, 'Deck 16')
        self.assertEqual(blocked.city, 'Red Dwarf')
        self.assertEqual(blocked.state, 'Deep Space')
        self.assertEqual(blocked.postal_code, 'JMC')
        self.assertEqual(blocked.country, 'Jupiter')

    def test_address_silent(self):
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'city': 'Red Dwarf', 'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self._test_address(json.dumps(address), 0, stdout='')

    def test_address_info(self):
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'city': 'Red Dwarf', 'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self._test_address(
            json.dumps(address), 1,
            stdout=StringComparison('Address .*Arnold Rimmer.*Jupiter Mining Company.* was blocked.\n'))

    def test_address_retry_silent(self):
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'city': 'Red Dwarf', 'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self.session.add(BlockedAddress(**address))
        self.session.commit()
        self._test_address(json.dumps(address), 0, stdout='')

    def test_address_retry_info(self):
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'city': 'Red Dwarf', 'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self.session.add(BlockedAddress(**address))
        self.session.commit()
        self._test_address(
            json.dumps(address), 1,
            stdout=StringComparison('Address .*Arnold Rimmer.*Jupiter Mining Company.* is already blocked.\n'))

    def test_address_partial(self):
        address = {'name': 'Arnold Rimmer', 'city': 'Red Dwarf'}

        self.assertCommandSuccess(['add', 'address', json.dumps(address)])

        blocked = self.session.query(BlockedAddress).one()
        self.assertEqual(blocked.name, 'Arnold Rimmer')
        self.assertIsNone(blocked.organization)
        self.assertIsNone(blocked.street)
        self.assertEqual(blocked.city, 'Red Dwarf')
        self.assertIsNone(blocked.state)
        self.assertIsNone(blocked.postal_code)
        self.assertIsNone(blocked.country)

    def test_address_conflict(self):
        # Test partial address is different from full address.
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'city': 'Red Dwarf', 'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self.session.add(BlockedAddress(**address))
        self.session.commit()

        address = {'name': 'Arnold Rimmer', 'city': 'Red Dwarf'}
        self.assertCommandSuccess(['add', 'address', json.dumps(address)])

        self.assertEqual(self.session.query(BlockedAddress).count(), 2)
        blocked = self.session.query(BlockedAddress).filter_by(organization=None).one()
        self.assertEqual(blocked.name, 'Arnold Rimmer')
        self.assertIsNone(blocked.organization)
        self.assertIsNone(blocked.street)
        self.assertEqual(blocked.city, 'Red Dwarf')
        self.assertIsNone(blocked.state)
        self.assertIsNone(blocked.postal_code)
        self.assertIsNone(blocked.country)

    def test_address_error(self):
        data = ('invalid', '{}', "{'invalid': 'value'}")
        for value in data:
            with self.subTest(value=value):
                self.assertCommandFail(['add', 'address', value], stderr='is not a valid address')

                self.assertEqual(self.session.query(BlockedAddress).count(), 0)


@override_database()
class RemoveTest(CommandMixin, TestCase):
    command = main

    def setUp(self):
        self.session = test_session_cls()()

    def tearDown(self):
        self.session.close()

    def _test_email(self, email: str, verbosity: int, *, stdout: str) -> None:
        self.assertCommandSuccess(['remove', 'email', email, '--verbosity', str(verbosity)], stdout=stdout)

        self.assertEqual(self.session.query(BlockedEmail).count(), 0)

    def test_email_silent(self):
        email = 'rimmer@example.org'
        self.session.add(BlockedEmail(email=email))
        self.session.commit()
        self._test_email(email, 0, stdout='')

    def test_email_info(self):
        email = 'rimmer@example.org'
        self.session.add(BlockedEmail(email=email))
        self.session.commit()
        self._test_email(email, 1, stdout='Email rimmer@example.org block was removed.\n')

    def test_email_retry_silent(self):
        email = 'rimmer@example.org'
        self._test_email(email, 0, stdout='')

    def test_email_retry_info(self):
        email = 'rimmer@example.org'
        self._test_email(email, 1, stdout='Email rimmer@example.org is not blocked.\n')

    def test_email_lowercase(self):
        email = 'RimMer@eXamPle.oRg'
        self.session.add(BlockedEmail(email='rimmer@example.org'))
        self.session.commit()
        self._test_email(email, 0, stdout='')

    def test_email_normalize(self):
        email = 'Arnold Rimmer <rimmer@example.org>'
        self.session.add(BlockedEmail(email='rimmer@example.org'))
        self.session.commit()
        self._test_email(email, 0, stdout='')

    def test_email_error(self):
        self.assertCommandFail(['remove', 'email', 'invalid'], stderr="'invalid' is not a valid email address.")

        self.assertEqual(self.session.query(BlockedEmail).count(), 0)

    def _test_phone(self, phone: str, verbosity: int, *, stdout: str) -> None:
        self.assertCommandSuccess(['remove', 'phone', phone, '--verbosity', str(verbosity)], stdout=stdout)

        self.assertEqual(self.session.query(BlockedPhone).count(), 0)

    def test_phone_silent(self):
        phone = '+1234'
        self.session.add(BlockedPhone(phone=phone))
        self.session.commit()
        self._test_phone(phone, 0, stdout='')

    def test_phone_info(self):
        phone = '+1234'
        self.session.add(BlockedPhone(phone=phone))
        self.session.commit()
        self._test_phone(phone, 1, stdout='Phone +1234 block was removed.\n')

    def test_phone_retry_silent(self):
        phone = '+1234'
        self._test_phone(phone, 0, stdout='')

    def test_phone_retry_info(self):
        phone = '+1234'
        self._test_phone(phone, 1, stdout='Phone +1234 is not blocked.\n')

    def test_phone_normalize(self):
        phone = '+1.234'
        self._test_phone(phone, 0, stdout='')

    def test_phone_error(self):
        self.assertCommandFail(['remove', 'phone', 'invalid'], stderr="'invalid' is not a valid phone number.")

        self.assertEqual(self.session.query(BlockedPhone).count(), 0)

    def _test_address(self, address: str, verbosity: int, *, stdout: Union[str, StringComparison]) -> None:
        self.assertCommandSuccess(['remove', 'address', address, '--verbosity', str(verbosity)], stdout=stdout)

        self.assertEqual(self.session.query(BlockedAddress).count(), 0)

    def test_address_silent(self):
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self.session.add(BlockedAddress(**address))
        self.session.commit()
        self._test_address(json.dumps(address), 0, stdout='')

    def test_address_info(self):
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self.session.add(BlockedAddress(**address))
        self.session.commit()
        msg = 'Address Arnold Rimmer, Jupiter Mining Company, Deck 16, Deep Space, JMC, Jupiter block was removed.\n'
        self._test_address(json.dumps(address), 1, stdout=msg)

    def test_address_retry_silent(self):
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self._test_address(json.dumps(address), 0, stdout='')

    def test_address_retry_info(self):
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self._test_address(
            json.dumps(address), 1,
            stdout=StringComparison('Address .*Arnold Rimmer.*Jupiter Mining Company.* is not blocked.\n'))

    def test_address_partial(self):
        address = {'name': 'Arnold Rimmer', 'city': 'Red Dwarf'}
        self.session.add(BlockedAddress(**address))
        self.session.commit()

        self.assertCommandSuccess(['remove', 'address', json.dumps(address)])

        self.assertEqual(self.session.query(BlockedAddress).count(), 0)

    def test_address_conflict(self):
        # Test partial address does not match full address.
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'city': 'Red Dwarf', 'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self.session.add(BlockedAddress(**address))
        self.session.commit()

        address = {'name': 'Arnold Rimmer', 'city': 'Red Dwarf'}
        self.assertCommandSuccess(['remove', 'address', json.dumps(address)])

        # Check original block is left intact.
        blocked = self.session.query(BlockedAddress).one()
        self.assertEqual(blocked.organization, 'Jupiter Mining Company')

    def test_address_error(self):
        data = ('invalid', '{}', "{'invalid': 'value'}")
        for value in data:
            with self.subTest(value=value):
                self.assertCommandFail(['remove', 'address', value], stderr='is not a valid address')

                self.assertEqual(self.session.query(BlockedAddress).count(), 0)


@override_database()
class ListTest(CommandMixin, TestCase):
    command = main

    def setUp(self):
        self.session = test_session_cls()()

    def tearDown(self):
        self.session.close()

    def _test_list(self, model: str, *, stdout: str) -> None:
        self.assertCommandSuccess(['list', model], stdout=stdout)

    def test_email_empty(self):
        self._test_list('email', stdout='')

    def test_email(self):
        email = 'rimmer@example.org'
        self.session.add(BlockedEmail(email=email))
        self.session.commit()
        self._test_list('email', stdout='rimmer@example.org\n')

    def test_phone_empty(self):
        self._test_list('phone', stdout='')

    def test_phone_silent(self):
        phone = '+1234'
        self.session.add(BlockedPhone(phone=phone))
        self.session.commit()
        self._test_list('phone', stdout='+1234\n')

    def test_address_empty(self):
        self._test_list('address', stdout='')

    def test_address_silent(self):
        address = {'name': 'Arnold Rimmer', 'organization': 'Jupiter Mining Company', 'street': 'Deck 16',
                   'state': 'Deep Space', 'postal_code': 'JMC', 'country': 'Jupiter'}
        self.session.add(BlockedAddress(**address))
        self.session.commit()
        self._test_list('address', stdout='Arnold Rimmer, Jupiter Mining Company, Deck 16, Deep Space, JMC, Jupiter\n')
