from unittest import TestCase

from messenger.constants import TransportMethod
from messenger.models import ArchiveEmail, ArchiveLetter, ArchiveSms, Email, Letter, Sms


class TransportMethodTest(TestCase):
    def test_model_cls(self):
        data = (
            # method, model_cls
            (TransportMethod.EMAIL, Email),
            (TransportMethod.SMS, Sms),
            (TransportMethod.LETTER, Letter),
        )
        for method, model_cls in data:
            with self.subTest(method=method):
                self.assertEqual(method.model_cls, model_cls)

    def test_archive_cls(self):
        data = (
            # method, archive_cls
            (TransportMethod.EMAIL, ArchiveEmail),
            (TransportMethod.SMS, ArchiveSms),
            (TransportMethod.LETTER, ArchiveLetter),
        )
        for method, model_cls in data:
            with self.subTest(method=method):
                self.assertEqual(method.archive_cls, model_cls)
