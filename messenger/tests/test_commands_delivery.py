from datetime import datetime
from email.message import EmailMessage, MIMEPart
from typing import List
from unittest import TestCase
from unittest.mock import patch, sentinel

from freezegun import freeze_time
from imap_tools import MailMessage
from pytz import UTC

from messenger.commands.delivery import main
from messenger.constants import MessageStatus
from messenger.delivery.email import MIMETYPE_DSN, MIMETYPE_REPORT, REPORT_TYPE_DSN
from messenger.models import Email

from .utils import CommandMixin, make_email, override_database, test_session_cls


@freeze_time('2020-02-01 14:00:00')
@override_database()
class DeliveryMainTest(CommandMixin, TestCase):
    command = main

    recipient = 'kryten@example.org'
    subject_template = 'subject.txt'
    body_template = 'body.txt'
    message_id = '<original@example.org>'

    reply_id = '<reply@example.org>'
    reply_subject = 'Quagaars!'

    def setUp(self):
        patcher = patch('messenger.delivery.email.MailBox', autospec=True)
        self.addCleanup(patcher.stop)
        self.imap_mock = patcher.start()
        self.imap_mock.return_value.mock_add_spec(('folder', '__enter__', '__exit__'))

        self.session = test_session_cls()()

    def tearDown(self):
        self.session.close()

    def _test_main(self, args: List[str]) -> None:
        make_email(self.session, recipient=self.recipient, status=MessageStatus.SENT,
                   subject_template=self.subject_template, body_template=self.body_template, message_id=self.message_id)

        dsn_message = EmailMessage()
        dsn_message['Subject'] = self.reply_subject
        dsn_message['Message-ID'] = self.reply_id
        dsn_message['Content-Type'] = '{}; report-type={}'.format(MIMETYPE_REPORT, REPORT_TYPE_DSN)
        dsn_message['In-Reply-To'] = self.message_id
        delivery_status = MIMEPart()
        delivery_status['Content-Type'] = MIMETYPE_DSN
        recipient_dsn = MIMEPart()
        recipient_dsn['Action'] = 'failed'
        delivery_status.attach(recipient_dsn)
        dsn_message.attach(delivery_status)  # type: ignore[arg-type]
        message = MailMessage([(b'UID 42', bytes(dsn_message))])
        self.imap_mock.return_value.__enter__.return_value.fetch.return_value = [message]

        self.assertCommandSuccess(args, settings={'delivery_backend': 'messenger.delivery.email.ImapDelivery'})

        email = self.session.query(Email).one()
        # Check email was updated
        self.assertEqual(email.status, MessageStatus.UNDELIVERED)
        self.assertEqual(email.delivery_datetime, datetime(2020, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.delivery_info,
                         {'Message-ID': self.reply_id, 'Subject': self.reply_subject, 'Action': 'failed'})

    def test_main(self):
        self._test_main([])

    def test_main_param(self):
        with patch('ssl.create_default_context', autospec=True, return_value=sentinel.context):
            self._test_main(['--delivery-param', 'host=red-dwarf.test'])
        self.imap_mock.assert_called_once_with(host='red-dwarf.test', port=None, ssl_context=sentinel.context)

    def test_main_invalid_backend_value(self):
        self.assertCommandFail(['--delivery-backend=not-dotted'], stderr="Error in loading delivery backend:")

    def _test_output(self, verbosity: int, stdout: str) -> None:
        self.imap_mock.return_value.__enter__.return_value.fetch.return_value = []
        self.assertCommandSuccess(['--verbosity', str(verbosity)], stdout=stdout,
                                  settings={'delivery_backend': 'messenger.delivery.email.ImapDelivery'})

    def test_silent(self):
        self._test_output(0, '')

    def test_info(self):
        self._test_output(
            1, 'Checking email delivery (may take long time)...\nProcessed 0 messages: 0 OK, 0 errors, 0 skipped.\n')
