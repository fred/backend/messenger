from datetime import datetime
from typing import Any, Iterable, Tuple, cast
from unittest import TestCase
from unittest.mock import Mock, call, patch, sentinel
from uuid import UUID

from filed import FileClient, Storage
from freezegun import freeze_time
from pytz import UTC
from testfixtures import Comparison, LogCapture
from typist import HTTPTokenAuth, Template
from typist.client import TemplateNotFound

from messenger.backends.base import BaseBackend, FileBackendMixin, MemoryBackendMixin, TemplateBackendMixin
from messenger.constants import MessageStatus, TransportMethod
from messenger.exceptions import MessageCanceled, MessageUndelivered
from messenger.models import Sms
from messenger.processors import Processor
from messenger.settings import APPLICATION

from .utils import StaticFilter, override_settings


class TestBackend(BaseBackend):
    """Simple backend for tests."""
    # Use SMS as the simplest model.
    method = TransportMethod.SMS

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.mock = Mock(spec=['get_message', 'send'])

    def get_message(self, model: Sms) -> Any:
        return self.mock.get_message(model)

    def _send(self, message):
        self.mock.send(message)


class TestProcessor(Processor):
    """Simple processor for tests."""
    def __init__(self, value: Any = None):
        self.value = value

    def process_message(self, message):
        return sentinel.processed_message


@freeze_time('2020-02-02 14:00:00')
class BaseBackendTest(TestCase):
    def setUp(self):
        self.backend = TestBackend(name=sentinel.service)
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()

    def get_message(self, **kwargs: Any) -> Sms:
        return Sms(**dict({'attempts': 0, 'status': MessageStatus.PENDING}, **kwargs))

    def test_processors_empty(self):
        with override_settings():
            self.assertEqual(self.backend.processors, ())

    def test_processors_empty_list(self):
        with override_settings(processors={'sms': []}):
            self.assertEqual(self.backend.processors, ())

    def test_processors_no_class(self):
        with self.assertRaisesRegex(RuntimeError, "Processor has no class defined."):
            with override_settings(processors={'sms': [{}]}):
                self.assertEqual(self.backend.processors, ())

    def test_processors(self):
        processor_settings = [{'class': 'messenger.tests.test_backends_base.TestProcessor', 'value': sentinel.value}]
        with override_settings(processors={'sms': processor_settings}):
            processors = self.backend.processors

        self.assertEqual(len(processors), 1)
        self.assertIsInstance(processors[0], TestProcessor)
        self.assertEqual(cast(TestProcessor, processors[0]).value, sentinel.value)

    def test_processors_custom(self):
        processor_settings = [{'class': 'messenger.tests.test_backends_base.TestProcessor', 'value': sentinel.value}]
        backend = TestBackend(processors=processor_settings)
        with override_settings(processors={'sms': []}):
            processors = backend.processors

        self.assertEqual(len(processors), 1)
        self.assertIsInstance(processors[0], TestProcessor)
        self.assertEqual(cast(TestProcessor, processors[0]).value, sentinel.value)

    def test_processors_merged(self):
        processor_settings = [{'class': 'messenger.tests.test_backends_base.TestProcessor', 'value': sentinel.value}]
        custom_processor_settings = [
            {'class': 'messenger.tests.test_backends_base.TestProcessor', 'value': sentinel.custom}]
        backend = TestBackend(processors=custom_processor_settings)
        with override_settings(processors={'sms': processor_settings}):
            processors = backend.processors

        result = (Comparison(TestProcessor, {'value': sentinel.custom}),
                  Comparison(TestProcessor, {'value': sentinel.value}))
        self.assertEqual(processors, result)

    def test_processors_cached(self):
        processor_settings = [{'class': 'messenger.tests.test_backends_base.TestProcessor', 'value': sentinel.value}]
        with override_settings(processors={'sms': processor_settings}):
            processors1 = self.backend.processors
            processors2 = self.backend.processors

        self.assertIs(processors1[0], processors2[0])

    def test_processors_settings(self):
        # Regression for #29831 - loading processors changes settings.
        processor_settings = [{'class': 'messenger.tests.test_backends_base.TestProcessor', 'value': sentinel.value}]
        with override_settings(processors={'sms': processor_settings}):
            self.assertEqual(len(self.backend.processors), 1)
            # Check settings are unchanged.
            self.assertEqual(
                APPLICATION.settings.processors,
                {'sms': [{'class': 'messenger.tests.test_backends_base.TestProcessor', 'value': sentinel.value}]})

    def test_processors_invalid_backend_value(self):
        with self.assertRaisesRegex(RuntimeError, "Error in loading processor:"):
            with override_settings(processors={'sms': [{'class': 'not-dotted'}]}):
                self.backend.processors

    def test_filter_calls(self):
        # Test calls to individual filters.
        filter_true = Mock(spec=['filter'], name='true')
        filter_true.filter.return_value = True
        filter_false = Mock(spec=['filter'], name='false')
        filter_false.filter.return_value = False
        backend = TestBackend(name=sentinel.service, filters=[filter_true, filter_false])

        self.assertFalse(backend.filter(sentinel.message))
        self.assertEqual(filter_true.mock_calls, [call.filter(sentinel.message)])
        self.assertEqual(filter_false.mock_calls, [call.filter(sentinel.message)])

    def test_filter(self):
        # Test filter combinations.
        filter_true = StaticFilter(True)
        filter_false = StaticFilter(False)
        data: Iterable[Tuple[Iterable, bool]] = (
            # filters, result
            ([], True),
            ([filter_true], True),
            ([filter_false], False),
            ([filter_true, filter_true], True),
            ([filter_true, filter_false], False),
            ([filter_false, filter_true], False),
            ([filter_false, filter_false], False),
        )
        for filters, result in data:
            with self.subTest(filters=filters):
                backend = TestBackend(name=sentinel.service, filters=filters)
                self.assertEqual(backend.filter(sentinel.message), result)

    def test_open(self):
        # Open is noop, just check it exists.
        self.backend.open()

    def test_close(self):
        # Close is noop, just check it exists.
        self.backend.close()

    def test_send(self):
        self.backend.mock.get_message.return_value = sentinel.message
        model = self.get_message()

        model = self.backend.send(model)

        self.assertEqual(self.backend.mock.mock_calls, [call.get_message(model), call.send(sentinel.message)])
        # Check model was updated
        self.assertEqual(model.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertEqual(model.send_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertEqual(model.attempts, 1)
        self.assertEqual(model.status, MessageStatus.SENT)
        self.assertEqual(model.service, sentinel.service)

    def test_send_processors(self):
        # Test processors are applied.
        self.backend.mock.get_message.return_value = sentinel.message
        model = self.get_message()

        processor_settings = [{'class': 'messenger.tests.test_backends_base.TestProcessor', 'value': sentinel.value}]
        with override_settings(processors={'sms': processor_settings}):
            model = self.backend.send(model)

        self.assertEqual(self.backend.mock.mock_calls, [call.get_message(model), call.send(sentinel.processed_message)])
        # Check model was updated
        self.assertEqual(model.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertEqual(model.send_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertEqual(model.attempts, 1)
        self.assertEqual(model.status, MessageStatus.SENT)
        self.assertEqual(model.service, sentinel.service)

    def test_send_cancelled(self):
        # Test message is canceled.
        self.backend.mock.get_message.side_effect = MessageCanceled
        model = self.get_message()

        model = self.backend.send(model)

        self.assertEqual(self.backend.mock.mock_calls, [call.get_message(model)])
        # Check model was updated
        self.assertEqual(model.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(model.send_datetime)
        self.assertEqual(model.attempts, 1)
        self.assertEqual(model.status, MessageStatus.CANCELED)
        self.assertIsNone(model.service)

    def test_send_undelivered(self):
        # Test message is undelivered.
        self.backend.mock.get_message.side_effect = MessageUndelivered
        model = self.get_message()

        model = self.backend.send(model)

        self.assertEqual(self.backend.mock.mock_calls, [call.get_message(model)])
        # Check model was updated
        self.assertEqual(model.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertEqual(model.send_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertEqual(model.delivery_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertEqual(model.attempts, 1)
        self.assertEqual(model.status, MessageStatus.UNDELIVERED)
        self.assertEqual(model.service, sentinel.service)

    def test_send_get_message_fail(self):
        self.backend.mock.get_message.side_effect = RuntimeError('Smeghead!')
        model = self.get_message()

        model = self.backend.send(model)

        self.assertEqual(self.backend.mock.mock_calls, [call.get_message(model)])
        # Check model was updated
        self.assertEqual(model.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(model.send_datetime)
        self.assertEqual(model.attempts, 1)
        self.assertEqual(model.status, MessageStatus.PENDING)
        self.assertIsNone(model.service)

    def test_send_get_message_error(self):
        self.backend.mock.get_message.side_effect = ValueError('Smeghead!')
        model = self.get_message()

        model = self.backend.send(model)

        self.assertEqual(self.backend.mock.mock_calls, [call.get_message(model)])
        # Check model was updated
        self.assertEqual(model.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(model.send_datetime)
        self.assertEqual(model.attempts, 1)
        self.assertEqual(model.status, MessageStatus.ERROR)
        self.assertIsNone(model.service)

    def test_send_fail(self):
        self.backend.mock.get_message.return_value = sentinel.message
        self.backend.mock.send.side_effect = RuntimeError('Smeghead!')
        model = self.get_message()

        model = self.backend.send(model)

        self.assertEqual(self.backend.mock.mock_calls, [call.get_message(model), call.send(sentinel.message)])
        # Check model was updated
        self.assertEqual(model.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(model.send_datetime)
        self.assertEqual(model.attempts, 1)
        self.assertEqual(model.status, MessageStatus.PENDING)
        self.assertIsNone(model.service)

    def test_send_error(self):
        self.backend.mock.get_message.return_value = sentinel.message
        self.backend.mock.send.side_effect = ValueError('Smeghead!')
        model = self.get_message()

        model = self.backend.send(model)

        self.assertEqual(self.backend.mock.mock_calls, [call.get_message(model), call.send(sentinel.message)])
        # Check model was updated
        self.assertEqual(model.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(model.send_datetime)
        self.assertEqual(model.attempts, 1)
        self.assertEqual(model.status, MessageStatus.ERROR)
        self.assertIsNone(model.service)

    def test_send_max_attempts(self):
        self.backend.mock.get_message.return_value = sentinel.message
        self.backend.mock.send.side_effect = RuntimeError('Smeghead!')
        model = self.get_message(attempts=9)

        model = self.backend.send(model)

        self.assertEqual(self.backend.mock.mock_calls, [call.get_message(model), call.send(sentinel.message)])
        # Check model data
        self.assertEqual(model.update_datetime, datetime(2020, 2, 2, 14, tzinfo=UTC))
        self.assertIsNone(model.send_datetime)
        self.assertEqual(model.attempts, 10)
        self.assertEqual(model.status, MessageStatus.FAILED)
        self.assertIsNone(model.service)


# We don't actually need any backend methods, so don't inherit from any backend.
class TestFileBackend(FileBackendMixin):
    """Simple file backend for tests."""


@override_settings(fileman_netloc='example.org')
class FileBackendMixinTest(TestCase):
    def setUp(self):
        self.file_client = Mock(FileClient)
        APPLICATION.storage = Storage(self.file_client)
        self.backend = TestFileBackend()

    def test_get_file_no_storage(self):
        APPLICATION.storage = None
        backend = TestFileBackend()

        with self.assertRaisesRegex(RuntimeError, "Storage not available."):
            backend.get_file(UUID(int=196))

    def test_get_file(self):
        uuid = UUID(int=196)
        self.file_client.stat.return_value = {'name': sentinel.name}

        file = self.backend.get_file(uuid)

        self.assertEqual(file.name, sentinel.name)
        self.assertEqual(self.file_client.mock_calls, [call.stat(str(uuid))])

    def test_get_file_not_found(self):
        uuid = UUID(int=196)
        self.file_client.stat.side_effect = FileNotFoundError

        with self.assertRaises(ValueError):
            self.backend.get_file(uuid)

        self.assertEqual(self.file_client.mock_calls, [call.stat(str(uuid))])

    def test_get_file_error(self):
        uuid = UUID(int=196)
        self.file_client.stat.side_effect = ConnectionAbortedError

        with self.assertRaises(RuntimeError):
            self.backend.get_file(uuid)

        self.assertEqual(self.file_client.mock_calls, [call.stat(str(uuid))])


# We don't actually need any backend methods, so don't inherit from any backend.
class TestTemplateBackend(TemplateBackendMixin):
    """Simple template backend for tests."""


class TemplateBackendMixinTest(TestCase):
    def setUp(self):
        patcher = patch('messenger.backends.base.SecretaryClient', autospec=True)
        self.addCleanup(patcher.stop)
        self.client_mock = patcher.start()
        self.secretary_mock = self.client_mock.return_value
        self.backend = TestTemplateBackend(secretary_url='http://secretary.example/api/')
        # Ignore calls from the backend creation.
        self.client_mock.reset_mock()

    def test_init(self):
        TestTemplateBackend(secretary_url=sentinel.url)

        self.assertEqual(self.client_mock.mock_calls, [call(sentinel.url, 3.05, auth=None)])

    def test_init_timeout(self):
        TestTemplateBackend(secretary_url=sentinel.url, secretary_timeout=sentinel.timeout)

        self.assertEqual(self.client_mock.mock_calls, [call(sentinel.url, sentinel.timeout, auth=None)])

    def test_init_token(self):
        TestTemplateBackend(secretary_url=sentinel.url, secretary_token=sentinel.token)

        self.assertEqual(self.client_mock.mock_calls,
                         [call(sentinel.url, 3.05, auth=Comparison(HTTPTokenAuth, {'token': sentinel.token}))])

    def test_get_template_uuid(self):
        uuid = UUID(int=42)
        self.secretary_mock.get_template_info.return_value = Template(
            name='example.html', is_active=True, uuid=str(uuid), create_datetime=datetime.now())

        self.assertEqual(self.backend.get_template_uuid('example.html'), uuid)

        self.assertEqual(self.secretary_mock.mock_calls, [call.get_template_info('example.html')])

    def test_get_template_uuid_not_found(self):
        self.secretary_mock.get_template_info.side_effect = TemplateNotFound

        with self.assertRaisesRegex(ValueError, "Template 'example.html' not found."):
            self.backend.get_template_uuid('example.html')

        self.assertEqual(self.secretary_mock.mock_calls, [call.get_template_info('example.html')])

    def test_get_template_uuid_runtime_error(self):
        self.secretary_mock.get_template_info.side_effect = RuntimeError('Gazpacho!')

        with self.assertRaisesRegex(RuntimeError,
                                    r"Attempt to obtain template UUID failed with: RuntimeError.*Gazpacho!.*"):
            self.backend.get_template_uuid('example.html')

        self.assertEqual(self.secretary_mock.mock_calls, [call.get_template_info('example.html')])

    def test_get_template_uuid_value_error(self):
        self.secretary_mock.get_template_info.side_effect = ValueError('Gazpacho!')

        with self.assertRaisesRegex(RuntimeError,
                                    r"Attempt to obtain template UUID failed with: ValueError.*Gazpacho!.*"):
            self.backend.get_template_uuid('example.html')

        self.assertEqual(self.secretary_mock.mock_calls, [call.get_template_info('example.html')])

    def test_get_template_uuid_invalid_uuid(self):
        self.secretary_mock.get_template_info.return_value = Template(
            name='example.html', is_active=True, uuid='JUNK', create_datetime=datetime.now())

        with self.assertRaisesRegex(RuntimeError, "Response contain invalid UUID."):
            self.backend.get_template_uuid('example.html')

        self.assertEqual(self.secretary_mock.mock_calls, [call.get_template_info('example.html')])


class TestMemoryBackend(MemoryBackendMixin, BaseBackend):
    """Simple memory backend for tests."""
    # Use SMS as the simplest model.
    method = TransportMethod.SMS

    def get_message(self, model: Sms) -> Any:
        return sentinel.message


class MemoryBackendMixinTest(TestCase):
    def setUp(self):
        TestMemoryBackend.outbox.clear()
        self.backend = TestMemoryBackend()

    def get_message(self, **kwargs: Any) -> Sms:
        return Sms(**dict({'attempts': 0, 'status': MessageStatus.PENDING}, **kwargs))

    def test_send(self):
        message = self.get_message()

        self.backend.send(message)

        # Check message was send
        self.assertEqual(self.backend.outbox, [sentinel.message])
        # Check model was updated
        self.assertEqual(message.status, MessageStatus.SENT)
