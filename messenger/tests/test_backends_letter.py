from typing import Optional
from unittest import TestCase
from unittest.mock import Mock
from uuid import UUID

from filed import File, FileClient, Storage

from messenger.backends.letter import BaseLetterBackend, LetterMessage
from messenger.models import Address, Letter
from messenger.settings import APPLICATION


class TestLetterBackend(BaseLetterBackend):
    """Simple backend for tests of BaseLetterBackend."""

    def _send(self, message: LetterMessage) -> None:
        pass  # pragma: no cover


class BaseLetterBackendTest(TestCase):
    uuid = UUID(int=42)
    recipient = Address(name='Kryten 2X4B-523P')
    file_uuid = UUID(int=255)

    def setUp(self):
        self.file_client = Mock(FileClient)
        APPLICATION.storage = Storage(self.file_client)
        self.backend = TestLetterBackend()
        self.file = File(uuid=self.file_uuid, client=self.file_client)

    def get_letter(self, file_uuid: UUID = file_uuid) -> Letter:
        return Letter(uuid=self.uuid, recipient=self.recipient, file=file_uuid)

    def _test_get_message(self, letter: Letter, sender: Optional[Address] = None) -> LetterMessage:
        message = self.backend.get_message(letter)

        self.assertIsInstance(message, LetterMessage)
        self.assertEqual(message.uuid, self.uuid)
        self.assertEqual(message.recipient, self.recipient)
        self.assertEqual(message.file.uuid, self.file_uuid)
        self.assertEqual(message.sender, sender)
        return message

    def test_init_sender(self):
        backend = TestLetterBackend(sender={'name': 'Holly'})
        self.assertEqual(backend.sender, Address(name='Holly'))

    def test_init_invalid_address(self):
        with self.assertRaisesRegex(ValueError, "Invalid sender: .*"):
            TestLetterBackend(sender={'invalid': 'Holly'})

    def test_get_message(self):
        letter = self.get_letter()
        self._test_get_message(letter)

    def test_get_message_sender(self):
        sender = {'name': 'Holly'}
        sender_address = Address(name='Holly')
        self.backend = TestLetterBackend(sender=sender)
        letter = self.get_letter()

        self._test_get_message(letter, sender=sender_address)
