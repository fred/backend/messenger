import threading
from email.message import EmailMessage
from functools import partial
from typing import Callable, Dict, List, Optional, Sequence, Union, cast
from unittest import TestCase, skipIf
from unittest.mock import ANY, Mock, call, patch, sentinel
from uuid import UUID

from filed import FileClient, Storage
from testfixtures import Comparison, LogCapture

from messenger.backends.base import BaseBackend
from messenger.backends.email import BaseEmailBackend, MemoryBackend
from messenger.backends.letter import LetterMessage
from messenger.backends.sms import SmsMessage
from messenger.commands.sender import SenderManager, _send, main
from messenger.constants import MessageStatus, TransportMethod
from messenger.models import Email, Letter, Sms
from messenger.settings import APPLICATION, Settings

from .utils import (
    CommandMixin,
    StaticFilter,
    TestApplication,
    make_email,
    make_letter,
    make_sms,
    override_database,
    override_settings,
    test_session_cls,
)

RECIPIENT = 'kryten@example.com'
SUBJECT_TEMPLATE = 'subject.txt'
BODY_TEMPLATE = 'body.txt'
SUBJECT_UUID = UUID(int=255)
BODY_UUID = UUID(int=1024)


class SenderManagerTest(TestCase):
    def test_init_empty(self):
        with override_settings(senders={}):
            manager = SenderManager()

        self.assertEqual(manager.senders, {})

    def test_init_email(self):
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url}}
        with override_settings(senders={'memory': sender_settings}):
            manager = SenderManager()

        self.assertEqual(manager.senders, {TransportMethod.EMAIL: {'memory': Comparison(MemoryBackend)}})
        sender = cast(BaseEmailBackend, manager.senders[TransportMethod.EMAIL]['memory'])
        self.assertEqual(sender.name, 'memory')
        self.assertEqual(sender.secretary.server_url, sentinel.secretary_url)

    def test_init_no_backend(self):
        with override_settings(senders={'memory': {}}):
            with self.assertRaisesRegex(ValueError, "'memory' has no backend defined."):
                SenderManager()

    def test_init_error(self):
        with override_settings(senders={'memory': {'backend': 'invalid'}}):
            with self.assertRaisesRegex(ValueError, "Error in loading sender memory:"):
                SenderManager()

    def test_init_filters(self):
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url},
                           'filters': [{'class': 'messenger.tests.utils.StaticFilter'}]}
        with override_settings(senders={'memory': sender_settings}):
            manager = SenderManager()

        self.assertEqual(manager.senders, {TransportMethod.EMAIL: {'memory': Comparison(MemoryBackend)}})
        sender = cast(BaseEmailBackend, manager.senders[TransportMethod.EMAIL]['memory'])
        self.assertEqual(len(sender.filters), 1)
        self.assertIsInstance(sender.filters[0], StaticFilter)

    def test_init_filters_args(self):
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url},
                           'filters': [{'class': 'messenger.tests.test_commands_sender.StaticFilter',
                                        'result': sentinel.result}]}
        with override_settings(senders={'memory': sender_settings}):
            manager = SenderManager()

        self.assertEqual(manager.senders, {TransportMethod.EMAIL: {'memory': Comparison(MemoryBackend)}})
        sender = cast(BaseEmailBackend, manager.senders[TransportMethod.EMAIL]['memory'])
        self.assertEqual(len(sender.filters), 1)
        self.assertIsInstance(sender.filters[0], StaticFilter)
        self.assertEqual(cast(StaticFilter, sender.filters[0]).result, sentinel.result)

    def test_init_filter_no_class(self):
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url},
                           'filters': [{}]}
        with override_settings(senders={'memory': sender_settings}):
            with self.assertRaisesRegex(ValueError, "Filter has no class defined."):
                SenderManager()

    def test_init_filter_error(self):
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url},
                           'filters': [{'class': 'invalid'}]}
        with override_settings(senders={'memory': sender_settings}):
            with self.assertRaisesRegex(ValueError, "Error in loading filter:"):
                SenderManager()

    def test_get(self):
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url}}
        with override_settings(senders={'memory': sender_settings}):
            manager = SenderManager()

        sender = cast(BaseEmailBackend, manager.get(TransportMethod.EMAIL, 'memory'))

        self.assertIsInstance(sender, MemoryBackend)
        self.assertEqual(sender.name, 'memory')
        self.assertEqual(sender.secretary.server_url, sentinel.secretary_url)

    def test_get_sender_default(self):
        # Test first sender of the type is returned as default.
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url}}
        sender_settings_2 = {'backend': 'messenger.backends.email.MemoryBackend',
                             'options': {'secretary_url': sentinel.secretary_url_2}}
        with override_settings(senders={'memory': sender_settings, 'other': sender_settings_2}):
            manager = SenderManager()

        sender = cast(BaseEmailBackend, manager.get(TransportMethod.EMAIL))

        self.assertEqual(sender.name, 'memory')
        self.assertEqual(sender.secretary.server_url, sentinel.secretary_url)

    def test_get_unknown(self):
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url}}
        with override_settings(senders={'memory': sender_settings}):
            manager = SenderManager()

        with self.assertRaisesRegex(KeyError, "'unknown' sender not found."):
            manager.get(TransportMethod.EMAIL, 'unknown')

    def test_get_unknown_default(self):
        with override_settings(senders={}):
            manager = SenderManager()

        with self.assertRaisesRegex(KeyError, "No senders found for emails."):
            manager.get(TransportMethod.EMAIL)

    def test_getitem(self):
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url}}
        with override_settings(senders={'memory': sender_settings}):
            manager = SenderManager()

        senders = manager[TransportMethod.EMAIL]

        self.assertEqual(len(senders), 1)
        sender = cast(BaseEmailBackend, tuple(senders)[0])
        self.assertIsInstance(sender, MemoryBackend)
        self.assertEqual(sender.name, 'memory')
        self.assertEqual(sender.secretary.server_url, sentinel.secretary_url)

    def test_getitem_multiple(self):
        sender_settings = {'backend': 'messenger.backends.email.MemoryBackend',
                           'options': {'secretary_url': sentinel.secretary_url}}
        sender_settings_2 = {'backend': 'messenger.backends.email.MemoryBackend',
                             'options': {'secretary_url': sentinel.secretary_url_2}}
        with override_settings(senders={'memory': sender_settings, 'other': sender_settings_2}):
            manager = SenderManager()

        senders = manager[TransportMethod.EMAIL]

        self.assertEqual(len(senders), 2)
        sender = cast(BaseEmailBackend, tuple(senders)[0])
        self.assertIsInstance(sender, MemoryBackend)
        self.assertEqual(sender.name, 'memory')
        self.assertEqual(sender.secretary.server_url, sentinel.secretary_url)
        sender = cast(BaseEmailBackend, tuple(senders)[1])
        self.assertIsInstance(sender, MemoryBackend)
        self.assertEqual(sender.name, 'other')
        self.assertEqual(sender.secretary.server_url, sentinel.secretary_url_2)

    def test_getitem_empty(self):
        with override_settings(senders={}):
            manager = SenderManager()

        self.assertEqual(tuple(manager[TransportMethod.EMAIL]), ())


class TestBackend(BaseBackend):
    """Simple backend for testing."""

    method = TransportMethod.EMAIL

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.mock = Mock(name='backend')

    def get_message(self, model):
        return sentinel.message

    def open(self):
        self.mock.open()

    def _send(self, message):
        self.mock(message)


@override_database()
class SendTest(TestCase):
    def setUp(self):
        self.session = test_session_cls()()
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()
        self.session.close()

    def test_send_email(self):
        uuid = UUID(int=42)
        backend = TestBackend()
        make_email(self.session, uuid=uuid, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   body_template=BODY_TEMPLATE, context={})

        self.assertTrue(_send(TransportMethod.EMAIL, (Email.uuid == uuid), [backend]))

        # Check email was updated
        email = self.session.query(Email).one()
        self.assertEqual(email.status, MessageStatus.SENT)
        self.assertEqual(email.attempts, 1)
        # Check backend was called
        self.assertEqual(backend.mock.mock_calls, [call.open(), call(sentinel.message)])

    def test_send_sms(self):
        uuid = UUID(int=42)
        backend = TestBackend()
        make_sms(self.session, uuid=uuid, recipient='+12345', body_template=BODY_TEMPLATE, context={})

        self.assertTrue(_send(TransportMethod.SMS, (Sms.uuid == uuid), [backend]))

        # Check SMS was updated
        sms = self.session.query(Sms).one()
        self.assertEqual(sms.status, MessageStatus.SENT)
        self.assertEqual(sms.attempts, 1)
        # Check backend was called
        self.assertEqual(backend.mock.mock_calls, [call.open(), call(sentinel.message)])

    def test_send_letter(self):
        uuid = UUID(int=42)
        backend = TestBackend()
        make_letter(self.session, uuid=uuid, file=UUID(int=1024))

        self.assertTrue(_send(TransportMethod.LETTER, (Letter.uuid == uuid), [backend]))

        # Check letter was updated
        letter = self.session.query(Letter).one()
        self.assertEqual(letter.status, MessageStatus.SENT)
        self.assertEqual(letter.attempts, 1)
        # Check backend was called
        self.assertEqual(backend.mock.mock_calls, [call.open(), call(sentinel.message)])

    def test_failed(self):
        uuid = UUID(int=42)
        backend = TestBackend()
        backend.mock.side_effect = RuntimeError('Gazpacho!')
        make_email(self.session, uuid=uuid, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   body_template=BODY_TEMPLATE, context={})

        self.assertTrue(_send(TransportMethod.EMAIL, (Email.uuid == uuid), [backend]))

        # Check email was updated
        email = self.session.query(Email).one()
        self.assertEqual(email.status, MessageStatus.PENDING)
        self.assertEqual(email.attempts, 1)
        # Check backend was called
        self.assertEqual(backend.mock.mock_calls, [call.open(), call(sentinel.message)])

    def test_skipped(self):
        # Test message skipped by filters.
        uuid = UUID(int=42)
        filter = Mock(spec=['filter'])
        filter.filter.return_value = False
        backend = TestBackend(filters=[filter])
        make_email(self.session, uuid=uuid, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   body_template=BODY_TEMPLATE, context={})

        self.assertFalse(_send(TransportMethod.EMAIL, (Email.uuid == uuid), [backend]))

        # Check email was not updated
        email = self.session.query(Email).one()
        self.assertEqual(email.status, MessageStatus.PENDING)
        self.assertEqual(email.attempts, 0)
        # Check backend was not called
        self.assertEqual(backend.mock.mock_calls, [])
        # Check warning
        msg = ("Message Email(uuid=UUID('00000000-0000-0000-0000-00000000002a'), recipient='kryten@example.com') "
               "is skipped, because it didn't match any filters.")
        self.log_handler.check(('messenger.commands.sender', 'WARNING', msg))

    def test_not_found(self):
        # Test message not found.
        backend = TestBackend()

        self.assertFalse(_send(TransportMethod.EMAIL, (Email.uuid == Email.uuid), [backend]))

        # Check backend was not called
        self.assertEqual(backend.mock.mock_calls, [])

    @skipIf(APPLICATION.settings.test_db_connection.startswith('sqlite'), 'Does not work on SQLite database.')
    def test_locked(self):
        # Test message is locked.
        uuid = UUID(int=42)
        backend = TestBackend()
        make_email(self.session, uuid=uuid, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   body_template=BODY_TEMPLATE, context={})

        # Lock email
        lock_session = test_session_cls()()
        lock_session.query(Email).filter_by(uuid=uuid).with_for_update().all()

        self.assertFalse(_send(TransportMethod.EMAIL, (Email.uuid == uuid), [backend]))

        # Release the lock, so the data can be checked.
        lock_session.rollback()

        # Check email was not updated
        email = self.session.query(Email).one()
        self.assertEqual(email.status, MessageStatus.PENDING)
        self.assertEqual(email.attempts, 0)
        # Check backend was not called
        self.assertEqual(backend.mock.mock_calls, [])


@override_database()
@override_settings()
class MainTest(CommandMixin, TestCase):
    command = main

    senders = {'email': {'backend': 'messenger.backends.email.MemoryBackend',
                         'options': {'secretary_url': 'http://secretary.example/api/'}},
               'sms': {'backend': 'messenger.backends.sms.MemorySmsBackend',
                       'options': {'secretary_url': 'http://secretary.example/api/'}},
               'letter': {'backend': 'messenger.backends.letter.MemoryLetterBackend'}}

    def setUp(self):
        patcher = patch('messenger.backends.base.SecretaryClient', autospec=True)
        self.addCleanup(patcher.stop)
        client_mock = patcher.start()
        self.secretary_mock = client_mock.return_value

        self.session = test_session_cls()()
        MemoryBackend.outbox.clear()

        APPLICATION.storage = Storage(Mock(FileClient))
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()
        self.session.close()

    def test_main_no_emails(self):
        self.assertCommandSuccess(settings={'senders': self.senders})

        self.assertEqual(MemoryBackend.outbox, [])

    def test_main_sent(self):
        make_email(self.session, recipient=RECIPIENT, status=MessageStatus.SENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        self.assertCommandSuccess(settings={'senders': self.senders})

        self.assertEqual(MemoryBackend.outbox, [])

    def _test_main_email(self, args: Sequence[str], *, settings: Optional[Dict] = None) -> None:
        settings = settings or {'senders': self.senders}
        self.secretary_mock.render.return_value = ''
        make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})
        make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        self.assertCommandSuccess(args, settings=settings)

        self.assertEqual(MemoryBackend.outbox, [Comparison(EmailMessage), Comparison(EmailMessage)])
        self.assertEqual(MemoryBackend.outbox[0]['To'], RECIPIENT)

        # Check emails were updated
        emails = self.session.query(Email)
        self.assertEqual([e.status for e in emails], [MessageStatus.SENT, MessageStatus.SENT])

    def test_main_email(self):
        self._test_main_email([])

    def test_main_sms(self):
        self.secretary_mock.render.return_value = ''
        recipient = '+123456'
        make_sms(self.session, recipient=recipient, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                 context={})
        make_sms(self.session, recipient=recipient, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                 context={})

        self.assertCommandSuccess(settings={'senders': self.senders})

        self.assertEqual(MemoryBackend.outbox, [Comparison(SmsMessage), Comparison(SmsMessage)])
        self.assertEqual(MemoryBackend.outbox[0].recipient, recipient)

        # Check SMS were updated
        sms = self.session.query(Sms)
        self.assertEqual([m.status for m in sms], [MessageStatus.SENT, MessageStatus.SENT])

    def test_main_letter(self):
        file_uuid = UUID(int=1024)
        make_letter(self.session, file=file_uuid)
        make_letter(self.session, file=file_uuid)

        self.assertCommandSuccess(settings={'senders': self.senders})

        self.assertEqual(MemoryBackend.outbox, [Comparison(LetterMessage), Comparison(LetterMessage)])
        self.assertEqual(MemoryBackend.outbox[0].file.uuid, file_uuid)

        # Check letters were updated
        letters = self.session.query(Letter)
        self.assertEqual([m.status for m in letters], [MessageStatus.SENT, MessageStatus.SENT])

    def _test_main_all(self, args: List) -> None:
        self.secretary_mock.render.return_value = ''
        make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})
        make_sms(self.session, recipient='+123456', body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                 context={})
        make_letter(self.session, file=UUID(int=1024))

        self.assertCommandSuccess(args, settings={'senders': self.senders})

    def test_main_all(self):
        self._test_main_all([])
        self.assertEqual(MemoryBackend.outbox,
                         [Comparison(EmailMessage), Comparison(SmsMessage), Comparison(LetterMessage)])

    def test_main_email_only(self):
        self._test_main_all(['--method', 'email'])
        self.assertEqual(MemoryBackend.outbox, [Comparison(EmailMessage)])

    def test_main_sms_only(self):
        self._test_main_all(['--method', 'sms'])
        self.assertEqual(MemoryBackend.outbox, [Comparison(SmsMessage)])

    def test_main_letter_only(self):
        self._test_main_all(['--method', 'letter'])
        self.assertEqual(MemoryBackend.outbox, [Comparison(LetterMessage)])

    def test_main_custom_sender(self):
        senders = {'custom': {'backend': 'messenger.backends.email.MemoryBackend',
                              'options': {'secretary_url': 'http://secretary.example/api/'}}}
        self._test_main_email(['--sender', 'custom', '--method', 'email'], settings={'senders': senders})

    def test_main_failed(self):
        # Test emails, which failed to be sent, are not processed multiple times in one run.
        self.secretary_mock.render.side_effect = RuntimeError
        make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})
        make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        self.assertCommandSuccess(settings={'senders': self.senders})

        self.assertEqual(MemoryBackend.outbox, [])

        # Check emails were updated
        emails = self.session.query(Email)
        self.assertEqual([e.status for e in emails], [MessageStatus.PENDING, MessageStatus.PENDING])
        self.assertEqual([e.attempts for e in emails], [1, 1])

    @skipIf(APPLICATION.settings.test_db_connection.startswith('sqlite'), 'Does not work on SQLite database.')
    def test_main_lock(self):
        # Test locked emails are skipped.
        self.secretary_mock.render.return_value = ''
        uuid = UUID(int=1)
        locked_uuid = UUID(int=42)
        make_email(self.session, uuid=uuid, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})
        make_email(self.session, uuid=locked_uuid, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        # Lock email
        lock_session = test_session_cls()()
        lock_session.query(Email).filter_by(uuid=locked_uuid).with_for_update().all()

        self.assertCommandSuccess(settings={'senders': self.senders})

        # Release the lock, so the data can be checked.
        lock_session.rollback()

        self.assertEqual(MemoryBackend.outbox, [Comparison(EmailMessage)])

        # Check emails were updated
        emails = self.session.query(Email).order_by(Email.uuid).filter(Email.uuid.in_([uuid, locked_uuid]))
        self.assertEqual([e.status for e in emails], [MessageStatus.SENT, MessageStatus.PENDING])
        self.assertEqual([e.attempts for e in emails], [1, 0])

    # In-memory databases can only be seen from within the thread that created them.
    @skipIf(APPLICATION.settings.test_db_connection.startswith('sqlite'), 'Does not work on SQLite database.')
    def test_main_multisend(self):
        # Regression test for #84, a possibility of sending the same message multiple times.
        # The error reproduction steps:
        # 1. Process 1 starts to send a message.
        # 2. Process 1 updates update_datetime.
        step_2 = threading.Event()
        # 3. Process 2 looks up message UUIDs for the current time.
        step_3 = threading.Event()
        # 4. Process 1 stores message.
        step_4 = threading.Event()
        # 5. Process 2 processes the same message, because update_datetime from step 2 is lower than time from step 3.

        make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        # Use secretary mock to synchronize the threads between step 2 and 3.
        def render_1():
            # Sender 1 - fire the sender 2 and wait for it to reach step 3
            step_2.set()
            step_3.wait()

        renders = [render_1]

        def render(*args, **kwargs):
            if renders:
                renders.pop(0)()
            return ''

        self.secretary_mock.render.side_effect = render

        # Use _send to synchronize the threads between step 3 and 4.
        def send_2(*args, **kwargs):
            # Sender 2 - Wait for the sender 1 to complete before processing the message.
            step_3.set()
            step_4.wait()
            _send(*args, **kwargs)

        sends = [_send, send_2]

        def send(*args, **kwargs):
            return sends.pop(0)(*args, **kwargs)  # type: ignore[operator]

        with override_settings(senders=self.senders):
            with patch('messenger.commands.sender._send', new=send):
                test_app = TestApplication(Settings)
                test_app.settings = APPLICATION.settings

                with patch('messenger.commands.utils.APPLICATION', new=test_app):
                    def sender_1():
                        self.runner.invoke(self.command)
                        step_4.set()  # Finished, let the sender 2 complete.

                    def sender_2():
                        step_2.wait()  # Wait for the step 2 to complete.
                        self.runner.invoke(self.command)

                    thread1 = threading.Thread(target=sender_1)
                    thread2 = threading.Thread(target=sender_2)
                    thread1.start()
                    thread2.start()
                    thread1.join()
                    thread2.join()

        self.assertEqual(MemoryBackend.outbox, [Comparison(EmailMessage)])

        # Check emails were updated
        email = self.session.query(Email).one()
        self.assertEqual(email.attempts, 1)
        self.assertEqual(email.status, MessageStatus.SENT)

    def test_main_retry_failed(self):
        self.secretary_mock.render.return_value = ''
        make_email(self.session, recipient=RECIPIENT, status=MessageStatus.FAILED, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        self.assertCommandSuccess(['--retry-failed'], settings={'senders': self.senders})

        self.assertEqual(MemoryBackend.outbox, [Comparison(EmailMessage)])
        self.assertEqual(MemoryBackend.outbox[0]['To'], RECIPIENT)

        # Check email was updated
        email = self.session.query(Email).one()
        self.assertEqual(email.status, MessageStatus.SENT)

    def test_main_retry_error(self):
        self.secretary_mock.render.return_value = ''
        make_email(self.session, recipient=RECIPIENT, status=MessageStatus.ERROR, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        self.assertCommandSuccess(['--retry-errors'], settings={'senders': self.senders})

        self.assertEqual(MemoryBackend.outbox, [Comparison(EmailMessage)])
        self.assertEqual(MemoryBackend.outbox[0]['To'], RECIPIENT)

        # Check email was updated
        email = self.session.query(Email).one()
        self.assertEqual(email.status, MessageStatus.SENT)

    def test_main_loop(self):
        # Test the loop mode.
        # Hack the time.sleep to trigger custom events.
        self.secretary_mock.render.return_value = ''
        email_maker = partial(
            make_email, self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
            subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
            context={})
        side_effects: List[Union[Callable, BaseException]] = [email_maker, KeyboardInterrupt()]

        def call_effects(*args):
            effect = side_effects.pop(0)
            if isinstance(effect, BaseException):
                raise effect
            return effect()

        with patch('messenger.commands.sender.sleep') as sleep_mock:
            sleep_mock.side_effect = call_effects

            make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                       subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                       context={})

            # Interrupting the command will cause non-zero exit code.
            self.assertCommandFail(['--loop'], stdout=ANY, settings={'senders': self.senders})

        self.assertEqual(MemoryBackend.outbox, [Comparison(EmailMessage), Comparison(EmailMessage)])
        self.assertEqual(MemoryBackend.outbox[0]['To'], RECIPIENT)

        # Check emails were updated
        emails = self.session.query(Email)
        self.assertEqual([e.status for e in emails], [MessageStatus.SENT, MessageStatus.SENT])
        # Check sleep was called
        self.assertEqual(sleep_mock.mock_calls, [call(300), call(300)])

    def test_main_loop_delay(self):
        # Test the loop delay option.
        # Hack the time.sleep to trigger custom events.
        with patch('messenger.commands.sender.sleep') as sleep_mock:
            sleep_mock.side_effect = [None, KeyboardInterrupt]
            # Interrupting the command will cause non-zero exit code.
            self.assertCommandFail(['--loop', '--loop-delay', '1024'], stdout=ANY, settings={'senders': self.senders})

        # Check sleep was called
        self.assertEqual(sleep_mock.mock_calls, [call(1024), call(1024)])

    def test_main_connections(self):
        # Test the connection opening and closing.
        self.secretary_mock.render.return_value = ''
        make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        sender_mock = Mock(BaseBackend)
        with patch('messenger.commands.sender._get_senders') as get_senders_mock:
            get_senders_mock.return_value = [sender_mock]

            self.assertCommandSuccess(['--method', 'email'])

        # Check calls to senders.
        self.assertEqual(sender_mock.mock_calls, [call.filter(ANY), call.open(), call.send(ANY), call.close()])

    def test_main_connections_loop(self):
        # Test the connection opening and closing in loop mode.
        self.secretary_mock.render.return_value = ''
        make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        sender_mock = Mock(BaseBackend)
        with patch('messenger.commands.sender._get_senders') as get_senders_mock:
            # Use the patch to return sender twice and interrupt the process on third run.
            get_senders_mock.side_effect = [[sender_mock], [sender_mock], KeyboardInterrupt]

            # Set delay to 0 to skip sleep between cycles.
            # Interrupting the command will cause non-zero exit code.
            self.assertCommandFail(['--method', 'email', '--loop', '--loop-delay', '0'], stdout=ANY)

        # Check calls to senders.
        calls = [call.filter(ANY), call.open(), call.send(ANY), call.close()] * 2
        self.assertEqual(sender_mock.mock_calls, calls)

    def _test_output(self, verbosity: int, output: str, senders: Dict = senders) -> None:
        self.secretary_mock.render.return_value = ''
        make_email(self.session, recipient=RECIPIENT, subject_template=SUBJECT_TEMPLATE,
                   subject_template_uuid=SUBJECT_UUID, body_template=BODY_TEMPLATE, body_template_uuid=BODY_UUID,
                   context={})

        self.assertCommandSuccess(['--verbosity', str(verbosity), '--method', 'email'], stdout=output,
                                  settings={'senders': senders})

    def test_silent(self):
        self._test_output(0, '')

    def test_info(self):
        self._test_output(1, 'Emails: 1 processed, 0 skipped.\n')

    def test_detail(self):
        self._test_output(2, '.\nEmails: 1 processed, 0 skipped.\n')

    def _test_skipped_output(self, verbosity: int, output: str) -> None:
        senders = {'smtp': {'backend': 'messenger.backends.email.MemoryBackend',
                            'filters': [{'class': 'messenger.tests.utils.StaticFilter', 'result': False}],
                            'options': {'secretary_url': 'http://secretary.example/api/'}}}
        self._test_output(verbosity, output, senders)

    def test_silent_skipped(self):
        self._test_skipped_output(0, '')

    def test_info_skipped(self):
        self._test_skipped_output(1, 'Emails: 0 processed, 1 skipped.\n')

    def test_detail_skipped(self):
        self._test_skipped_output(2, 's\nEmails: 0 processed, 1 skipped.\n')

    def test_main_no_senders(self):
        self.assertCommandFail(stderr="No senders found for emails.", settings={'senders': {}})

    def test_main_settings_error(self):
        self.assertCommandFail(stderr="'memory' has no backend defined.", settings={'senders': {'memory': {}}})

    def test_main_unknown_sender(self):
        self.assertCommandFail(['--sender', 'unknown', '--method', 'email'], stderr="No senders found for emails.",
                               settings={})

    def test_sender_all_types(self):
        self.assertCommandFail(['--sender', 'unknown'], stderr="Sender can only be used with single type.",
                               settings={})

    def test_main_custom_netloc(self):
        test_app = TestApplication(Settings)
        test_app.settings = Settings(fileman_netloc=None)
        with patch('messenger.commands.utils.APPLICATION', new=test_app):
            self.runner.invoke(self.command, ['--fileman-netloc', 'test.example.org:12345'])

            # Just check the settings have been set by the CLI arg.
            self.assertEqual(APPLICATION.settings.fileman_netloc, 'test.example.org:12345')
