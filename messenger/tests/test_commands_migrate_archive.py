from datetime import datetime
from typing import Any, Callable, Dict, Iterable, List, Optional, Set, cast
from unittest import TestCase
from unittest.mock import patch, sentinel
from uuid import UUID

from pytz import UTC
from sqlalchemy import Table
from sqlalchemy.sql import Insert
from sqlalchemy.sql.expression import ClauseElement
from testfixtures import OutputCapture, StringComparison

from messenger.commands.migrate_archive import (
    CHECK_NSSET,
    CONTACT_CHECK,
    CONTACT_CHECK_MESSAGE_MAP,
    CONTACT_HISTORY,
    DOMAIN_CONTACT_MAP_HISTORY,
    DOMAIN_HISTORY,
    ENUM_OBJECT_TYPE,
    ENUM_SEND_STATUS,
    FILES,
    FRED_METADATA,
    HISTORY,
    INVOICE,
    INVOICE_MAILS,
    KEYSET_CONTACT_MAP_HISTORY,
    LETTER_ARCHIVE,
    MAIL_ARCHIVE,
    MAIL_ATTACHMENTS,
    MAIL_HEADER,
    MAIL_TEMPLATE,
    MAIL_TEMPLATE_MIGRATION,
    MAIL_TYPE,
    MESSAGE_ARCHIVE,
    MESSAGE_CONTACT_HISTORY_MAP,
    MESSAGE_TYPE,
    MESSAGE_TYPE_MONITORING,
    NOTIFY_STATE_CHANGE,
    NSSET_CONTACT_MAP_HISTORY,
    NSSET_HISTORY,
    OBJECT_REGISTRY,
    OBJECT_STATE,
    PUBLIC_REQUEST,
    PUBLIC_REQUEST_MESSAGES_MAP,
    PUBLIC_REQUEST_OBJECTS_MAP,
    REGISTRAR,
    REMINDER_CONTACT_MESSAGE_MAP,
    SMS_ARCHIVE,
    TECHCHECK_MAIL_TYPE,
    WARNING_MAIL_TYPE,
    FredEmailStatus,
    FredMessageStatus,
    _append_references,
    _get_email_references,
    _get_message_references,
    _migrate_email,
    _migrate_letter,
    _migrate_sms,
    get_fred_engine,
    main,
    migrate_emails,
    migrate_letters,
    migrate_sms,
)
from messenger.constants import ArchiveMessageStatus, MessageStatus, ReferenceType
from messenger.models import Address, ArchiveEmail, ArchiveLetter, ArchiveSms, Email, Letter, Sms
from messenger.settings import APPLICATION, Settings

from .utils import (
    CommandMixin,
    TestApplication,
    make_archive_email,
    make_archive_letter,
    make_archive_sms,
    make_letter,
    override_database,
    override_settings,
    test_session_cls,
)


class AssertOutputMixin:
    """Mixin with assertions for command outputs."""

    def assertOutputContains(self, output: OutputCapture, text: str) -> None:
        """Check if result output contains the expected text."""
        cast(TestCase, self).assertIn(text, output.output.getvalue())


class BaseMigrateTestCase(TestCase):
    """Base test case for migrate archive tests."""

    def setUp(self):
        self.session = test_session_cls()()
        self.fred_engine = get_fred_engine()
        FRED_METADATA.create_all(self.fred_engine)
        self.fred_connection = self.fred_engine.connect()

    def tearDown(self):
        self.session.close()

    def insert(self, query: Insert) -> int:
        """Run insert query on FRED DB and return the new primary key."""
        with self.fred_connection.begin():
            result = self.fred_connection.execute(query)
            return cast(int, result.inserted_primary_key[0])


@override_database(fred_db_connection='sqlite:///:memory:')
class GetEmailReferencesTest(BaseMigrateTestCase):
    sender = 'holly@example.org'
    reply_to = 'reply@example.org'
    errors_to = 'errors@example.org'
    organization = 'JMC'
    template_name = 'notification_unused'
    template_id = 1024
    template_version = 255
    uuid = UUID(int=42)
    message_id = '<mail.archive@example.org>'
    crdate = datetime(2001, 2, 3, 4)

    object_uuid = UUID(int=27)
    state_from = datetime(2010, 1, 1, 12, 0, 0)

    def setUp(self):
        super().setUp()
        self.insert(MAIL_TYPE.insert().values(id=self.template_id, name=self.template_name))
        header = MAIL_HEADER.insert().values(h_from=self.sender, h_replyto=self.reply_to, h_errorsto=self.errors_to,
                                             h_organization=self.organization)
        header_id = self.insert(header)
        template = MAIL_TEMPLATE.insert().values(mail_header_default_id=header_id, mail_type_id=self.template_id,
                                                 version=self.template_version)
        self.insert(template)
        self.domain_type_id = self.insert(ENUM_OBJECT_TYPE.insert().values(name=ReferenceType.DOMAIN))
        self.contact_type_id = self.insert(ENUM_OBJECT_TYPE.insert().values(name=ReferenceType.CONTACT))
        self.nsset_type_id = self.insert(ENUM_OBJECT_TYPE.insert().values(name=ReferenceType.NSSET))
        self.keyset_type_id = self.insert(ENUM_OBJECT_TYPE.insert().values(name=ReferenceType.KEYSET))

    def create_mail(self, message_params: Optional[Dict] = None) -> int:
        mail = MAIL_ARCHIVE.insert().values(
            uuid=self.uuid, crdate=self.crdate, moddate=self.crdate, status=FredEmailStatus.SENT, attempt=0,
            mail_type_id=self.template_id, mail_template_version=self.template_version,
            message_params=(message_params or {}))
        mail_id = self.insert(mail)
        return mail_id

    def create_state_change(self, object_id: int, mail_id: int, **kwargs: Any) -> None:
        object_state_id = self.insert(OBJECT_STATE.insert().values(object_id=object_id, **kwargs))
        self.insert(NOTIFY_STATE_CHANGE.insert().values(state_id=object_state_id, mail_id=mail_id))

    def test_no_relation(self):
        mail_id = self.create_mail()

        references = _get_email_references(mail_id, self.template_name, sentinel.recipient, {}, self.fred_connection)

        self.assertEqual(references, {})

    def _test_state_change(self, mail_type: str, result: Dict[ReferenceType, Set[str]]) -> None:
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.domain_type_id, uuid=self.object_uuid))
        mail_id = self.create_mail()
        self.create_state_change(object_registry_id, mail_id)

        references = _get_email_references(mail_id, mail_type, sentinel.recipient, {}, self.fred_connection)

        self.assertEqual(references, result)

    def test_state_change(self):
        self._test_state_change(self.template_name, {ReferenceType.DOMAIN: {str(self.object_uuid)}})

    def test_state_change_unknown(self):
        # Test unknown mail type returns no references.
        self._test_state_change('unknown', {})

    def _test_registrant(self, contact_uuid: UUID, mail_type: str, result: Dict[ReferenceType, Set[str]]) -> None:
        contact_registry_id = self.insert(OBJECT_REGISTRY.insert().values(type=self.contact_type_id, uuid=contact_uuid))
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.domain_type_id, uuid=self.object_uuid))
        history_id = self.insert(HISTORY.insert())
        self.insert(
            DOMAIN_HISTORY.insert().values(historyid=history_id, id=object_registry_id, registrant=contact_registry_id))
        mail_id = self.create_mail()
        self.create_state_change(object_registry_id, mail_id, ohid_from=history_id)

        references = _get_email_references(mail_id, mail_type, sentinel.recipient, {}, self.fred_connection)

        self.assertEqual(references, result)

    def test_registrant(self):
        contact_uuid = UUID(int=128)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}, ReferenceType.CONTACT: {str(contact_uuid)}}
        self._test_registrant(contact_uuid, self.template_name, result)

    def test_registrant_warning(self):
        # Test warning mail doesn't return reference to contact.
        contact_uuid = UUID(int=128)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}}
        self._test_registrant(contact_uuid, WARNING_MAIL_TYPE, result)

    def _test_contact_map(self, contact_uuid: UUID, mail_type: str, insert_map: Callable[[int, int, int], None],
                          result: Dict[ReferenceType, Set[str]]) -> None:
        contact_registry_id = self.insert(OBJECT_REGISTRY.insert().values(type=self.contact_type_id, uuid=contact_uuid))
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.domain_type_id, uuid=self.object_uuid))
        history_id = self.insert(HISTORY.insert())
        insert_map(history_id, contact_registry_id, object_registry_id)
        mail_id = self.create_mail()
        self.create_state_change(object_registry_id, mail_id, ohid_from=history_id, valid_from=self.state_from)

        references = _get_email_references(mail_id, mail_type, sentinel.recipient, {}, self.fred_connection)

        self.assertEqual(references, result)

    def insert_admin(self, history_id: int, contact_registry_id: int, object_registry_id: int) -> None:
        query = DOMAIN_CONTACT_MAP_HISTORY.insert().values(historyid=history_id, domainid=object_registry_id,
                                                           contactid=contact_registry_id)
        self.insert(query)

    def test_admins(self):
        admin_uuid = UUID(int=255)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}, ReferenceType.CONTACT: {str(admin_uuid)}}
        self._test_contact_map(admin_uuid, self.template_name, self.insert_admin, result)

    def test_admins_tech_mail(self):
        # Test admins are ignored for non-admin mails.
        admin_uuid = UUID(int=255)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}}
        self._test_contact_map(admin_uuid, 'expiration_dns_tech', self.insert_admin, result)

    def test_admins_warning_mail(self):
        # Test admins are ignored for 'warning' mails.
        admin_uuid = UUID(int=255)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}}
        self._test_contact_map(admin_uuid, WARNING_MAIL_TYPE, self.insert_admin, result)

    def insert_nsset(self, history_id: int, contact_registry_id: int, object_registry_id: int) -> None:
        query = NSSET_CONTACT_MAP_HISTORY.insert().values(historyid=history_id, nssetid=object_registry_id,
                                                          contactid=contact_registry_id)
        self.insert(query)

    def test_tech_nsset(self):
        tech_uuid = UUID(int=1024)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}, ReferenceType.CONTACT: {str(tech_uuid)}}
        self._test_contact_map(tech_uuid, self.template_name, self.insert_nsset, result)

    def test_tech_nsset_other(self):
        # Test techs are ignored for non-tech mails.
        tech_uuid = UUID(int=1024)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}}
        self._test_contact_map(tech_uuid, 'expiration_dns_owner', self.insert_nsset, result)

    def insert_keyset(self, history_id: int, contact_registry_id: int, object_registry_id: int) -> None:
        query = KEYSET_CONTACT_MAP_HISTORY.insert().values(historyid=history_id, keysetid=object_registry_id,
                                                           contactid=contact_registry_id)
        self.insert(query)

    def test_tech_keyset(self):
        tech_uuid = UUID(int=2048)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}, ReferenceType.CONTACT: {str(tech_uuid)}}
        self._test_contact_map(tech_uuid, self.template_name, self.insert_keyset, result)

    def test_tech_keyset_other(self):
        # Test techs are ignored for non-tech mails.
        tech_uuid = UUID(int=2048)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}}
        self._test_contact_map(tech_uuid, 'expiration_dns_owner', self.insert_keyset, result)

    def _test_tech_domain(self, tech_uuid: UUID, mail_type: str, result: Dict[ReferenceType, Set[str]],
                          valid_to: Optional[datetime] = None) -> None:
        nsset_uuid = UUID(int=196)
        nsset_registry_id = self.insert(OBJECT_REGISTRY.insert().values(type=ReferenceType.DOMAIN, uuid=nsset_uuid))
        nsset_history_id = self.insert(HISTORY.insert().values(valid_from=self.state_from, valid_to=valid_to))
        self.insert(NSSET_HISTORY.insert().values(id=nsset_registry_id, historyid=nsset_history_id))

        def insert_domain_nsset(history_id: int, contact_registry_id: int, object_registry_id: int) -> None:
            self.insert(
                DOMAIN_HISTORY.insert().values(historyid=history_id, id=object_registry_id, nsset=nsset_registry_id))
            query = NSSET_CONTACT_MAP_HISTORY.insert().values(historyid=nsset_history_id, nssetid=nsset_registry_id,
                                                              contactid=contact_registry_id)
            self.insert(query)

        self._test_contact_map(tech_uuid, mail_type, insert_domain_nsset, result)

    def test_tech_domain_from(self):
        # Test nsset changed at the same time and haven't finished yet
        tech_uuid = UUID(int=4096)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}, ReferenceType.CONTACT: {str(tech_uuid)}}
        self._test_tech_domain(tech_uuid, self.template_name, result)

    def test_tech_domain_from_to(self):
        # Test nsset changed at the same time and finished later on
        tech_uuid = UUID(int=4096)
        state_to = datetime(2011, 1, 1)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}, ReferenceType.CONTACT: {str(tech_uuid)}}
        self._test_tech_domain(tech_uuid, self.template_name, result, valid_to=state_to)

    def test_tech_domain_other(self):
        # Test techs are ignored for non-tech mails.
        tech_uuid = UUID(int=4096)
        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}}
        self._test_tech_domain(tech_uuid, 'expiration_dns_owner', result)

    def test_public_request_answer(self):
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.domain_type_id, uuid=self.object_uuid))
        mail_id = self.create_mail()
        request_id = self.insert(PUBLIC_REQUEST.insert().values(answer_email_id=mail_id))
        self.insert(PUBLIC_REQUEST_OBJECTS_MAP.insert().values(request_id=request_id, object_id=object_registry_id))

        references = _get_email_references(mail_id, sentinel.mail_type, sentinel.recipient, {}, self.fred_connection)

        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}}
        self.assertEqual(references, result)

    def test_public_request_mail_map(self):
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.domain_type_id, uuid=self.object_uuid))
        mail_id = self.create_mail()
        request_id = self.insert(PUBLIC_REQUEST.insert().values())
        self.insert(PUBLIC_REQUEST_OBJECTS_MAP.insert().values(request_id=request_id, object_id=object_registry_id))
        self.insert(PUBLIC_REQUEST_MESSAGES_MAP.insert().values(public_request_id=request_id, mail_archive_id=mail_id))

        references = _get_email_references(mail_id, sentinel.mail_type, sentinel.recipient, {}, self.fred_connection)

        result = {ReferenceType.DOMAIN: {str(self.object_uuid)}}
        self.assertEqual(references, result)

    def test_public_request_no_mail(self):
        # Test public request with no link to mail
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.domain_type_id, uuid=self.object_uuid))
        mail_id = self.create_mail()
        request_id = self.insert(PUBLIC_REQUEST.insert().values())
        self.insert(PUBLIC_REQUEST_OBJECTS_MAP.insert().values(request_id=request_id, object_id=object_registry_id))

        references = _get_email_references(mail_id, sentinel.mail_type, sentinel.recipient, {}, self.fred_connection)

        self.assertEqual(references, {})

    def test_invoice(self):
        registrar = 'JMC'
        mail_id = self.create_mail()
        registrar_id = self.insert(REGISTRAR.insert().values(handle=registrar))
        invoice_id = self.insert(INVOICE.insert().values(registrar_id=registrar_id))
        self.insert(INVOICE_MAILS.insert().values(invoiceid=invoice_id, mailid=mail_id))

        references = _get_email_references(mail_id, sentinel.mail_type, '', {}, self.fred_connection)

        self.assertEqual(references, {ReferenceType.REGISTRAR: {registrar}})

    def test_invoice_email(self):
        registrar = 'JMC'
        recipient = 'jmc@example.org'
        mail_id = self.create_mail(message_params={'header': {'To': recipient}})
        self.insert(REGISTRAR.insert().values(handle=registrar, email=recipient))
        self.insert(INVOICE_MAILS.insert().values(invoiceid=None, mailid=mail_id))

        references = _get_email_references(mail_id, sentinel.mail_type, recipient, {}, self.fred_connection)

        self.assertEqual(references, {ReferenceType.REGISTRAR: {registrar}})

    def test_invoice_override(self):
        # Regression for #30026 - migrate only invoice link, if available.
        registrar = 'JMC'
        registrar_2 = 'DDI'
        recipient = 'ddi@example.org'
        mail_id = self.create_mail()
        # Create invoice link
        registrar_id = self.insert(REGISTRAR.insert().values(handle=registrar))
        invoice_id = self.insert(INVOICE.insert().values(registrar_id=registrar_id))
        self.insert(INVOICE_MAILS.insert().values(invoiceid=invoice_id, mailid=mail_id))
        # Create a second registrar with the matching email to be ignored.
        self.insert(REGISTRAR.insert().values(handle=registrar_2, email=recipient))

        references = _get_email_references(mail_id, sentinel.mail_type, recipient, {}, self.fred_connection)

        self.assertEqual(references, {ReferenceType.REGISTRAR: {registrar}})

    def test_reminder(self):
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.contact_type_id, uuid=self.object_uuid))
        mail_id = self.create_mail()
        self.insert(REMINDER_CONTACT_MESSAGE_MAP.insert().values(contact_id=object_registry_id, message_id=mail_id))

        references = _get_email_references(mail_id, sentinel.mail_type, sentinel.recipient, {}, self.fred_connection)

        self.assertEqual(references, {ReferenceType.CONTACT: {str(self.object_uuid)}})

    def test_contact_check(self):
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.contact_type_id, uuid=self.object_uuid))
        mail_id = self.create_mail()
        history_id = self.insert(HISTORY.insert())
        self.insert(CONTACT_HISTORY.insert().values(historyid=history_id, id=object_registry_id))
        check_id = self.insert(CONTACT_CHECK.insert().values(contact_history_id=history_id))
        self.insert(CONTACT_CHECK_MESSAGE_MAP.insert().values(contact_check_id=check_id, mail_archive_id=mail_id))

        references = _get_email_references(mail_id, sentinel.mail_type, sentinel.recipient, {}, self.fred_connection)

        self.assertEqual(references, {ReferenceType.CONTACT: {str(self.object_uuid)}})

    def test_nsset_check_non_numeric(self):
        # Regression for #29082 - test nsset references do not fail, if ticket value is not numeric.
        mail_id = self.create_mail()

        def _get_tables(obj: ClauseElement) -> Iterable[Table]:
            # Return all tables from the Select query.
            if isinstance(obj, Table):
                yield obj
                return
            for child in obj.get_children():
                yield from _get_tables(child)

        def _test_append_references(query, *args):
            # Fail if query for CHECK_NSSET is found.
            self.assertNotIn(CHECK_NSSET, tuple(_get_tables(query)))
            _append_references(query, *args)

        check_id = 'Invalid-42'
        with patch('messenger.commands.migrate_archive._append_references', new=_test_append_references):
            references = _get_email_references(mail_id, sentinel.mail_type, sentinel.recipient, {'ticket': check_id},
                                               self.fred_connection)

        self.assertEqual(references, {})

    def _test_nsset_check(self, mail_type: str, result: Dict[ReferenceType, Set[str]]) -> None:
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.nsset_type_id, uuid=self.object_uuid))
        history_id = self.insert(HISTORY.insert())
        self.insert(NSSET_HISTORY.insert().values(historyid=history_id, id=object_registry_id))
        mail_id = self.create_mail()
        check_id = self.insert(CHECK_NSSET.insert().values(nsset_hid=history_id))

        references = _get_email_references(mail_id, mail_type, sentinel.recipient, {'ticket': str(check_id)},
                                           self.fred_connection)

        self.assertEqual(references, result)

    def test_nsset_check(self):
        result = {ReferenceType.NSSET: {str(self.object_uuid)}}
        self._test_nsset_check(TECHCHECK_MAIL_TYPE, result)

    def test_nsset_check_unknown(self):
        # Test no references returned for non-techeck mails.
        self._test_nsset_check('unknown', {})


@override_database(fred_db_connection='sqlite:///:memory:')
class MigrateEmailsTest(AssertOutputMixin, BaseMigrateTestCase):
    sender = 'holly@example.org'
    reply_to = 'reply@example.org'
    errors_to = 'errors@example.org'
    organization = 'JMC'
    recipient = 'kryten@example.org'
    uuid = UUID(int=42)
    message_id = '<mail.archive@example.org>'
    crdate = datetime(2001, 2, 3, 4)
    crdate_aware = datetime(2001, 2, 3, 4, tzinfo=UTC)
    moddate = datetime(2001, 3, 5, 7)
    moddate_aware = datetime(2001, 3, 5, 7, tzinfo=UTC)
    attempt = 12
    template = 'template'
    template_version = 255
    subject_uuid = UUID(int=127)
    subject = 'subject.txt'
    body_uuid = UUID(int=128)
    body = 'body.txt'
    context = {'context_key': 'context_value'}
    attachment_uuid = UUID(int=2048)

    def setUp(self):
        super().setUp()
        self.template_id = self.insert(MAIL_TYPE.insert().values(name=self.template))
        header = MAIL_HEADER.insert().values(h_from=self.sender, h_replyto=self.reply_to, h_errorsto=self.errors_to,
                                             h_organization=self.organization)
        self.header_id = self.insert(header)
        template = MAIL_TEMPLATE.insert().values(mail_header_default_id=self.header_id, mail_type_id=self.template_id,
                                                 version=self.template_version)
        self.insert(template)
        template_migration = MAIL_TEMPLATE_MIGRATION.insert().values(
            mail_type_id=self.template_id, version=self.template_version,
            subject=self.subject, subject_uuid=self.subject_uuid, body=self.body, body_uuid=self.body_uuid)
        self.insert(template_migration)

    def make_mail_archive(
            self, uuid: UUID = uuid, crdate: datetime = crdate, moddate: Optional[datetime] = moddate,
            recipient: str = recipient, status: FredEmailStatus = FredEmailStatus.SENT,
            message_params: Optional[Dict] = None, response_header: Optional[Dict[str, str]] = None) -> int:
        message_params = (message_params
                          or {'header': {'To': recipient, 'Message-ID': self.message_id}, 'body': self.context})
        mail = MAIL_ARCHIVE.insert().values(
            uuid=uuid, crdate=crdate, moddate=moddate, status=status, attempt=self.attempt,
            mail_type_id=self.template_id, mail_template_version=self.template_version, message_params=message_params,
            response_header=response_header)
        return self.insert(mail)

    def test_migrate_no_emails(self):
        # Test no email to migrate
        with OutputCapture() as output:
            migrate_emails(self.fred_connection)

        self.assertOutputContains(output, 'Emails: 0 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)

    def _test_migrate(
            self, recipient: Optional[str] = None, status: ArchiveMessageStatus = ArchiveMessageStatus.SENT,
            send_datetime: Optional[datetime] = moddate_aware, delivery_datetime: Optional[datetime] = None,
            delivery_info: Optional[Dict[str, str]] = None, sender: str = sender,
            extra_headers: Optional[Dict[str, str]] = None, context: Dict = context,
            attachments: Optional[List[UUID]] = None, message_id: Optional[str] = message_id,
            update_datetime: Optional[datetime] = moddate_aware) -> ArchiveEmail:
        recipient = recipient or self.recipient
        extra_headers = (extra_headers
                         or {'Reply-To': self.reply_to, 'Errors-To': self.errors_to, 'Organization': self.organization})

        # Test new email is migrated
        with OutputCapture() as output:
            migrate_emails(self.fred_connection)

        self.assertOutputContains(output, 'Emails: 1 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Email).count(), 0)
        email = cast(ArchiveEmail, self.session.query(ArchiveEmail).one())
        self.assertEqual(email.uuid, self.uuid)
        self.assertEqual(email.message_id, message_id)
        self.assertEqual(email.create_datetime, self.crdate_aware)
        self.assertEqual(email.update_datetime, update_datetime)
        self.assertEqual(email.send_datetime, send_datetime)
        self.assertEqual(email.status, status)
        self.assertEqual(email.attempts, self.attempt)
        self.assertEqual(email.sender, sender)
        self.assertEqual(email.type, self.template)
        self.assertEqual(email.recipient, recipient)
        self.assertEqual(email.extra_headers, extra_headers)
        self.assertEqual(email.subject_template, self.subject)
        self.assertEqual(email.subject_template_uuid, self.subject_uuid)
        self.assertEqual(email.body_template, self.body)
        self.assertEqual(email.body_template_uuid, self.body_uuid)
        self.assertIsNone(email.body_template_html)
        self.assertIsNone(email.body_template_html_uuid)
        self.assertEqual(email.context, context)
        self.assertEqual(email.attachments, attachments)
        self.assertEqual(email.delivery_datetime, delivery_datetime)
        self.assertEqual(email.delivery_info, delivery_info)
        return email

    def test_migrate_sent(self):
        self.make_mail_archive()
        self._test_migrate()

    def test_migrate_sent_multiple_recipients(self):
        self.make_mail_archive(recipient='kryten@example.org, holly@example.org')
        self._test_migrate(recipient='kryten@example.org, holly@example.org')

    def test_migrate_sent_format_recipients(self):
        self.make_mail_archive(recipient='kryten@example.org holly@example.org ')
        self._test_migrate(recipient='kryten@example.org, holly@example.org')

    def test_migrate_undelivered(self):
        self.make_mail_archive(status=FredEmailStatus.UNDELIVERED)
        self._test_migrate(status=ArchiveMessageStatus.UNDELIVERED, send_datetime=self.crdate_aware,
                           delivery_datetime=self.moddate_aware)

    def test_migrate_pending(self):
        # Test pending email is not migrated, unless requested.
        self.make_mail_archive(status=FredEmailStatus.PENDING)
        with OutputCapture() as output:
            migrate_emails(self.fred_connection)

        self.assertOutputContains(output, 'Emails: 0 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)

    def test_migrate_pending_requested(self):
        # Test pending email is migrated if requested.
        self.make_mail_archive(status=FredEmailStatus.PENDING)
        with OutputCapture() as output:
            migrate_emails(self.fred_connection, all_messages=True)

        self.assertOutputContains(output, 'Emails: 1 migrated, 0 skipped, 0 errors')

        email = self.session.query(Email).one()
        self.assertEqual(email.uuid, self.uuid)
        self.assertEqual(email.message_id, self.message_id)
        self.assertEqual(email.create_datetime, self.crdate_aware)
        self.assertEqual(email.update_datetime, self.moddate_aware)
        self.assertIsNone(email.send_datetime)
        self.assertEqual(email.status, MessageStatus.PENDING)
        self.assertEqual(email.attempts, self.attempt)
        self.assertEqual(email.sender, self.sender)
        self.assertEqual(email.type, self.template)
        self.assertEqual(email.recipient, self.recipient)
        self.assertEqual(email.subject_template, self.subject)
        self.assertEqual(email.subject_template_uuid, self.subject_uuid)
        self.assertEqual(email.body_template, self.body)
        self.assertEqual(email.body_template_uuid, self.body_uuid)
        self.assertIsNone(email.body_template_html)
        self.assertIsNone(email.body_template_html_uuid)
        self.assertIsNone(email.delivery_datetime)
        self.assertIsNone(email.delivery_info)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)

    def test_migrate_monitoring(self):
        # Test email to monitoring email is not migrated.
        monitoring_email = 'queeg-500@example.org'
        message_params = {'header': {'To': monitoring_email, 'Message-ID': self.message_id}, 'body': self.context}
        self.make_mail_archive(message_params=message_params)
        with override_settings(monitoring_email=monitoring_email):
            with OutputCapture() as output:
                migrate_emails(self.fred_connection)

        self.assertOutputContains(output, 'Emails: 0 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)

    def test_migrate_monitoring_mismatch(self):
        # Test monitoring email is set, but doesn't match the recipient, the mail is migrated.
        self.make_mail_archive()
        with override_settings(monitoring_email='queeg-500@example.org'):
            with OutputCapture() as output:
                migrate_emails(self.fred_connection)

        self.assertOutputContains(output, 'Emails: 1 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 1)

    def test_migrate_monitoring_no_recipient(self):
        # Test message with no recipient (with monitoring email checks) causes error, but doesn't crash migrations.
        message_params = {'header': {}, 'body': self.context}
        self.make_mail_archive(message_params=message_params)
        with override_settings(monitoring_email='queeg-500@example.org'):
            with OutputCapture() as output:
                migrate_emails(self.fred_connection)

        self.assertOutputContains(output, 'Emails: 0 migrated, 0 skipped, 1 errors')

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)

    def test_migrate_no_moddate(self):
        # Test message with no moddate is also migrated.
        self.make_mail_archive(moddate=None)
        self._test_migrate(send_datetime=None, update_datetime=None)

    def test_migrate_sender(self):
        sender = 'queeg-500@example.org'
        message_params = {
            'header': {'To': self.recipient, 'Message-ID': self.message_id, 'From': sender},
            'body': self.context}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(sender=sender)

    def test_migrate_no_message_id(self):
        message_params = {'header': {'To': self.recipient}, 'body': self.context}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(message_id=None)

    def test_migrate_extra_headers(self):
        message_params = {
            'header': {'To': self.recipient, 'Message-ID': self.message_id, 'X-Extra-Header': 'x-value'},
            'body': self.context}
        self.make_mail_archive(message_params=message_params)
        extra_headers = {'X-Extra-Header': 'x-value', 'Errors-To': self.errors_to, 'Reply-To': self.reply_to,
                         'Organization': self.organization}
        self._test_migrate(extra_headers=extra_headers)

    def test_migrate_extra_headers_default_overrides(self):
        # Test custom headers override default headers.
        reply_to = 'custom-reply@example.org'
        errors_to = 'custom-errors@example.org'
        organization = 'Titan Zoo'
        message_params = {
            'header': {'To': self.recipient, 'Message-ID': self.message_id, 'Errors-To': errors_to,
                       'Reply-To': reply_to, 'Organization': organization},
            'body': self.context}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(extra_headers={'Errors-To': errors_to, 'Reply-To': reply_to, 'Organization': organization})

    def test_migrate_extra_headers_conflict_case(self):
        # Test custom headers override default headers - even with different cases used in header names.
        reply_to = 'custom-reply@example.org'
        errors_to = 'custom-errors@example.org'
        organization = 'Titan Zoo'
        message_params = {
            'header': {'To': self.recipient, 'Message-ID': self.message_id, 'ErrORS-to': errors_to,
                       'RepLY-to': reply_to, 'OrgANizATioN': organization},
            'body': self.context}
        self.make_mail_archive(message_params=message_params)
        # The weird case of original headers is kept intact.
        self._test_migrate(extra_headers={'ErrORS-to': errors_to, 'RepLY-to': reply_to, 'OrgANizATioN': organization})

    def test_migrate_attachments(self):
        files = FILES.insert().values(uuid=self.attachment_uuid)
        file_id = self.insert(files)
        mail_id = self.make_mail_archive()
        attachment = MAIL_ATTACHMENTS.insert().values(mailid=mail_id, attachid=file_id)
        self.insert(attachment)

        self._test_migrate(attachments=[self.attachment_uuid])

    def test_migrate_delivery_info(self):
        response_header = {'Action': 'failed', 'Final-Recipient': 'rfc822; kryten@example.org'}
        self.make_mail_archive(response_header=response_header)
        self._test_migrate(delivery_info=response_header)

    def test_migrate_context_keys(self):
        message_params = {'header': {'To': self.recipient, 'Message-ID': self.message_id},
                          'body': {'keys': ['[flags: 257, protocol: 3, algorithm: 5, key: "Gazpacho"]']}}
        context = {'keys': [{'flags': 257, 'protocol': 3, 'algorithm': 5, 'key': 'Gazpacho'}]}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(context=context)

    def test_migrate_context_keys_empty(self):
        message_params = {'header': {'To': self.recipient, 'Message-ID': self.message_id},
                          'body': {'keys': [{}]}}
        context = {'keys': []}  # type: Dict
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(context=context)

    def test_migrate_context_type(self):
        message_params = {'header': {'To': self.recipient, 'Message-ID': self.message_id},
                          'body': {'type': '42'}}
        context = {'type': 42}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(context=context)

    def test_migrate_context_otype(self):
        message_params = {'header': {'To': self.recipient, 'Message-ID': self.message_id},
                          'body': {'otype': '42'}}
        context = {'otype': 42}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(context=context)

    def test_migrate_context_rtype(self):
        message_params = {'header': {'To': self.recipient, 'Message-ID': self.message_id},
                          'body': {'rtype': '42'}}
        context = {'rtype': 42}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(context=context)

    def test_migrate_context_status(self):
        message_params = {'header': {'To': self.recipient, 'Message-ID': self.message_id},
                          'body': {'status': '42'}}
        context = {'status': 42}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(context=context)

    def test_migrate_context_disclose(self):
        message_params = {'header': {'To': self.recipient, 'Message-ID': self.message_id},
                          'body': {'fresh': {'contact': {'disclose': {'name': '1', 'email': '0', 'junk': '1'}}}}}
        context = {'fresh': {'contact': {'disclose': {'name': 1, 'email': 0, 'junk': 1}}}}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(context=context)

    def test_migrate_context_disclose_changes(self):
        orig_context = {
            'changes': {'contact': {'disclose': {'name': {'new': '1', 'old': '0'}, 'junk': {'new': '0', 'old': '1'}}}}}
        message_params = {'header': {'To': self.recipient, 'Message-ID': self.message_id},
                          'body': orig_context}
        context = {'changes': {'contact': {'disclose': {'name': {'new': 1, 'old': 0}, 'junk': {'new': 0, 'old': 1}}}}}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(context=context)

    def test_migrate_context_missing(self):
        message_params = {'header': {'To': self.recipient, 'Message-ID': self.message_id}}
        self.make_mail_archive(message_params=message_params)
        self._test_migrate(context={})

    def test_migrate_references(self):
        mail_id = self.make_mail_archive()
        # Make a reference
        object_uuid = UUID(int=255)
        contact_type_id = self.insert(ENUM_OBJECT_TYPE.insert().values(name=ReferenceType.CONTACT))
        object_registry_id = self.insert(OBJECT_REGISTRY.insert().values(type=contact_type_id, uuid=object_uuid))
        self.insert(REMINDER_CONTACT_MESSAGE_MAP.insert().values(contact_id=object_registry_id, message_id=mail_id))

        email = self._test_migrate()

        self.assertEqual([(r.type, r.value) for r in email.references], [(ReferenceType.CONTACT, str(object_uuid))])

    def test_migrate_existing(self):
        # Test existing email is not updated
        # Create email with mismatching recipient and check it doesn't change
        self.make_mail_archive()
        recipient = 'rimmer@example.org'
        make_archive_email(self.session, id=2, create_datetime=self.crdate_aware, uuid=self.uuid, recipient=recipient,
                           subject_template=self.subject, subject_template_uuid=self.subject_uuid,
                           body_template=self.body, body_template_uuid=self.body_uuid)

        with OutputCapture() as output:
            migrate_emails(self.fred_connection)

        self.assertOutputContains(output, 'Emails: 0 migrated, 1 skipped, 0 errors')

        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, self.uuid)
        self.assertEqual(email.recipient, recipient)

    def test_migrate_update(self):
        # Test email is updated upon delivery change
        response_header = {'Action': 'failed', 'Final-Recipient': 'rfc822; kryten@example.org'}
        self.make_mail_archive(status=FredEmailStatus.UNDELIVERED, response_header=response_header)
        make_archive_email(
            self.session, id=2, create_datetime=self.crdate_aware, uuid=self.uuid, recipient=self.recipient,
            subject_template=self.subject, subject_template_uuid=self.subject_uuid,
            body_template=self.body, body_template_uuid=self.body_uuid)

        with OutputCapture() as output:
            migrate_emails(self.fred_connection)

        self.assertOutputContains(output, 'Emails: 1 migrated, 0 skipped, 0 errors')

        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, self.uuid)
        self.assertEqual(email.status, ArchiveMessageStatus.UNDELIVERED)
        self.assertEqual(email.update_datetime, self.moddate_aware)
        self.assertEqual(email.delivery_datetime, self.moddate_aware)
        self.assertEqual(email.delivery_info, response_header)

    def test_migrate_limits_from(self):
        # Test from_datetime option
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=127))
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_mail_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=129))

        with OutputCapture():
            migrate_emails(self.fred_connection, from_datetime=datetime(2010, 2, 1, 13, 30))

        self.assertEqual(self.session.query(ArchiveEmail).count(), 2)

    def test_migrate_limits_to(self):
        # Test to_datetime option
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 13), moddate=datetime(2020, 1, 1), uuid=UUID(int=127))
        self.make_mail_archive(crdate=datetime(2020, 1, 1), moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 15), moddate=datetime(2020, 1, 1), uuid=UUID(int=129))

        with OutputCapture():
            migrate_emails(self.fred_connection, to_datetime=datetime(2010, 2, 1, 14, 30))

        self.assertEqual(self.session.query(ArchiveEmail).count(), 2)

    def test_migrate_limits_crdate(self):
        # Test from_datetime and to_datetime options applied to crdate
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=127))
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 15), uuid=UUID(int=129))

        with OutputCapture():
            migrate_emails(self.fred_connection, from_datetime=datetime(2010, 2, 1, 13, 30),
                           to_datetime=datetime(2010, 2, 1, 14, 30))

        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, UUID(int=128))
        self.assertEqual(email.create_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.status, ArchiveMessageStatus.SENT)

    def test_migrate_limits_moddate(self):
        # Test from_datetime and to_datetime options applied to moddate
        self.make_mail_archive(moddate=datetime(2010, 2, 1, 13), uuid=UUID(int=127))
        self.make_mail_archive(moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_mail_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=129))

        with OutputCapture():
            migrate_emails(self.fred_connection, from_datetime=datetime(2010, 2, 1, 13, 30),
                           to_datetime=datetime(2010, 2, 1, 14, 30))

        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, UUID(int=128))
        self.assertEqual(email.create_datetime, self.crdate_aware)
        self.assertEqual(email.send_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.status, ArchiveMessageStatus.SENT)

    def test_migrate_error(self):
        # Test error in migration
        self.make_mail_archive()

        with OutputCapture(separate=True, strip_whitespace=False) as output:
            with patch('messenger.commands.migrate_archive._migrate_email', side_effect=ValueError('Gazpacho!')):
                migrate_emails(self.fred_connection)

        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        self.assertEqual(output.stdout.getvalue(), 'Emails: 0 migrated, 0 skipped, 1 errors\n')
        self.assertEqual(output.stderr.getvalue(),
                         StringComparison(r"Error in migration of {}: ValueError\('Gazpacho!'.*".format(self.uuid)))

    def test_migrate_multiple_versions(self):
        # Regression for #30988 - migration of email with mail template with multiple versions.
        self.make_mail_archive()

        # Create new template version
        template = MAIL_TEMPLATE.insert().values(mail_header_default_id=self.header_id, mail_type_id=self.template_id,
                                                 version=self.template_version + 1)
        self.insert(template)
        template_migration = MAIL_TEMPLATE_MIGRATION.insert().values(
            mail_type_id=self.template_id, version=self.template_version + 1,
            subject=self.subject, subject_uuid=UUID(int=1024), body=self.body, body_uuid=UUID(int=1025))
        self.insert(template_migration)

        # Test new email is migrated
        with OutputCapture() as output:
            migrate_emails(self.fred_connection)

        self.assertOutputContains(output, 'Emails: 1 migrated, 0 skipped, 0 errors')

        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, self.uuid)
        self.assertEqual(email.type, self.template)
        self.assertEqual(email.subject_template, self.subject)
        self.assertEqual(email.subject_template_uuid, self.subject_uuid)
        self.assertEqual(email.body_template, self.body)
        self.assertEqual(email.body_template_uuid, self.body_uuid)
        self.assertIsNone(email.body_template_html)
        self.assertIsNone(email.body_template_html_uuid)


@override_database(fred_db_connection='sqlite:///:memory:')
class GetMessageReferencesTest(BaseMigrateTestCase):
    # SMS and letters use the same database tables for references. Use only SMS for tests.
    recipient = '+1.234'
    uuid = UUID(int=42)
    crdate = datetime(2001, 2, 3, 4)
    moddate = datetime(2001, 3, 5, 7)
    status_id = 5
    type_id = 4

    object_uuid = UUID(int=27)
    state_from = datetime(2010, 1, 1, 12, 0, 0)

    def setUp(self):
        super().setUp()
        self.insert(ENUM_SEND_STATUS.insert().values(id=self.status_id, status_name='status'))
        self.insert(MESSAGE_TYPE.insert().values(id=self.type_id, type='sms'))
        self.domain_type_id = self.insert(ENUM_OBJECT_TYPE.insert().values(name=ReferenceType.DOMAIN))

    def create_sms(self, message_params: Optional[Dict] = None) -> int:
        message = MESSAGE_ARCHIVE.insert().values(uuid=self.uuid, crdate=self.crdate, moddate=self.crdate, status_id=5,
                                                  attempt=0, message_type_id=self.type_id)
        sms_id = self.insert(message)
        self.insert(SMS_ARCHIVE.insert().values(id=sms_id, phone_number=self.recipient, content=''))
        return sms_id

    def test_no_relation(self):
        sms_id = self.create_sms()

        references = _get_message_references(sms_id, self.fred_connection)

        self.assertEqual(references, {})

    def test_reference(self):
        object_registry_id = self.insert(
            OBJECT_REGISTRY.insert().values(type=self.domain_type_id, uuid=self.object_uuid))
        sms_id = self.create_sms()
        query = MESSAGE_CONTACT_HISTORY_MAP.insert().values(contact_object_registry_id=object_registry_id,
                                                            message_archive_id=sms_id)
        self.insert(query)

        references = _get_message_references(sms_id, self.fred_connection)

        self.assertEqual(references, {ReferenceType.DOMAIN: {str(self.object_uuid)}})


@override_database(fred_db_connection='sqlite:///:memory:')
class MigrateSmsTest(AssertOutputMixin, BaseMigrateTestCase):
    message_types = {
        4: 'mojeid_sms_change',
        5: MESSAGE_TYPE_MONITORING,
    }
    recipient = '+1.234'
    uuid = UUID(int=42)
    crdate = datetime(2001, 2, 3, 4)
    crdate_aware = datetime(2001, 2, 3, 4, tzinfo=UTC)
    moddate = datetime(2001, 3, 5, 7)
    moddate_aware = datetime(2001, 3, 5, 7, tzinfo=UTC)
    attempt = 12
    content = 'Confirm by PIN: Quagaars!'
    context = {'pin': 'Quagaars!'}
    body = 'mojeid-sms-change-en.txt'
    body_uuid = UUID('b1d3e4f3-d03d-4755-87c3-ffc86a24587b')

    def setUp(self):
        super().setUp()
        enum = ENUM_SEND_STATUS.insert().values(
            [{'id': status.id, 'status_name': status.value} for status in FredMessageStatus])
        self.insert(enum)
        types = MESSAGE_TYPE.insert().values([{'id': i, 'type': t} for i, t in self.message_types.items()])
        self.insert(types)

    def make_sms_archive(
            self, uuid: UUID = uuid, crdate: datetime = crdate, moddate: Optional[datetime] = moddate,
            status: FredMessageStatus = FredMessageStatus.SENT, message_type: int = 4) -> int:
        message = MESSAGE_ARCHIVE.insert().values(uuid=uuid, crdate=crdate, moddate=moddate, status_id=status.id,
                                                  attempt=self.attempt, message_type_id=message_type)
        pk = self.insert(message)
        sms = SMS_ARCHIVE.insert().values(id=pk, phone_number=self.recipient, content=self.content)
        self.insert(sms)
        return pk

    def test_migrate_no_sms(self):
        # Test no sms to migrate
        with OutputCapture() as output:
            migrate_sms(self.fred_connection)

        self.assertOutputContains(output, 'SMS: 0 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Sms).count(), 0)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)

    def test_migrate_monitoring(self):
        # Test monitoring sms are ignored.
        self.make_sms_archive(message_type=5)

        with OutputCapture() as output:
            migrate_sms(self.fred_connection)

        self.assertOutputContains(output, 'SMS: 0 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Sms).count(), 0)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)

    def _test_migrate(self, status: ArchiveMessageStatus = ArchiveMessageStatus.SENT,
                      send_datetime: Optional[datetime] = moddate_aware, delivery_datetime: Optional[datetime] = None,
                      update_datetime: Optional[datetime] = moddate_aware) -> ArchiveSms:
        # Test new sms is migrated
        with OutputCapture() as output:
            migrate_sms(self.fred_connection)

        self.assertOutputContains(output, 'SMS: 1 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Sms).count(), 0)
        sms = cast(ArchiveSms, self.session.query(ArchiveSms).one())
        self.assertEqual(sms.uuid, self.uuid)
        self.assertEqual(sms.create_datetime, self.crdate_aware)
        self.assertEqual(sms.update_datetime, update_datetime)
        self.assertEqual(sms.send_datetime, send_datetime)
        self.assertEqual(sms.status, status)
        self.assertEqual(sms.type, 'mojeid_sms_change')
        self.assertEqual(sms.attempts, self.attempt)
        self.assertEqual(sms.recipient, self.recipient)
        self.assertEqual(sms.body_template, self.body)
        self.assertEqual(sms.body_template_uuid, self.body_uuid)
        self.assertEqual(sms.context, self.context)
        self.assertEqual(sms.delivery_datetime, delivery_datetime)
        return sms

    def test_migrate_sent(self):
        self.make_sms_archive()
        self._test_migrate()

    def test_migrate_failed(self):
        self.make_sms_archive(status=FredMessageStatus.FAILED)
        self._test_migrate(status=ArchiveMessageStatus.FAILED, send_datetime=None, delivery_datetime=None)

    def test_migrate_canceled(self):
        self.make_sms_archive(status=FredMessageStatus.CANCELED)
        self._test_migrate(status=ArchiveMessageStatus.CANCELED, send_datetime=None, delivery_datetime=None)

    def test_migrate_undelivered(self):
        self.make_sms_archive(status=FredMessageStatus.UNDELIVERED)
        self._test_migrate(status=ArchiveMessageStatus.UNDELIVERED, send_datetime=self.crdate_aware,
                           delivery_datetime=self.moddate_aware)

    def test_migrate_pending(self):
        # Test pending SMS is not migrated, unless requested.
        self.make_sms_archive(status=FredMessageStatus.PENDING)
        with OutputCapture() as output:
            migrate_sms(self.fred_connection)

        self.assertOutputContains(output, 'SMS: 0 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Sms).count(), 0)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)

    def test_migrate_pending_requested(self):
        # Test pending SMS is migrated if requested.
        self.make_sms_archive(status=FredMessageStatus.PENDING)
        with OutputCapture() as output:
            migrate_sms(self.fred_connection, all_messages=True)

        self.assertOutputContains(output, 'SMS: 1 migrated, 0 skipped, 0 errors')

        sms = self.session.query(Sms).one()
        self.assertEqual(sms.uuid, self.uuid)
        self.assertEqual(sms.create_datetime, self.crdate_aware)
        self.assertEqual(sms.update_datetime, self.moddate_aware)
        self.assertIsNone(sms.send_datetime)
        self.assertEqual(sms.status, MessageStatus.PENDING)
        self.assertEqual(sms.type, 'mojeid_sms_change')
        self.assertEqual(sms.attempts, self.attempt)
        self.assertEqual(sms.recipient, self.recipient)
        self.assertEqual(sms.body_template, self.body)
        self.assertEqual(sms.body_template_uuid, self.body_uuid)
        self.assertEqual(sms.context, self.context)
        self.assertIsNone(sms.delivery_datetime)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)

    def test_migrate_no_moddate(self):
        self.make_sms_archive(moddate=None)
        self._test_migrate(send_datetime=None, update_datetime=None)

    def test_migrate_references(self):
        sms_id = self.make_sms_archive()
        # Make a reference
        object_uuid = UUID(int=255)
        contact_type_id = self.insert(ENUM_OBJECT_TYPE.insert().values(name=ReferenceType.CONTACT))
        object_registry_id = self.insert(OBJECT_REGISTRY.insert().values(type=contact_type_id, uuid=object_uuid))
        query = MESSAGE_CONTACT_HISTORY_MAP.insert().values(contact_object_registry_id=object_registry_id,
                                                            message_archive_id=sms_id)
        self.insert(query)

        sms = self._test_migrate()

        self.assertEqual([(r.type, r.value) for r in sms.references], [(ReferenceType.CONTACT, str(object_uuid))])

    def test_migrate_existing(self):
        # Test existing SMS is not updated
        # Create SMS with mismatching recipient and check it doesn't change
        self.make_sms_archive()
        recipient = '+9.8765'
        make_archive_sms(self.session, id=2, create_datetime=self.crdate_aware, uuid=self.uuid, recipient=recipient,
                         body_template=self.body, body_template_uuid=self.body_uuid)

        with OutputCapture() as output:
            migrate_sms(self.fred_connection)

        self.assertOutputContains(output, 'SMS: 0 migrated, 1 skipped, 0 errors')

        sms = self.session.query(ArchiveSms).one()
        self.assertEqual(sms.uuid, self.uuid)
        self.assertEqual(sms.recipient, recipient)

    def test_migrate_update(self):
        # Test SMS is updated upon delivery change
        self.make_sms_archive(status=FredMessageStatus.UNDELIVERED)
        make_archive_sms(
            self.session, id=2, create_datetime=self.crdate_aware, uuid=self.uuid, recipient=self.recipient,
            body_template=self.body, body_template_uuid=self.body_uuid)

        with OutputCapture() as output:
            migrate_sms(self.fred_connection)

        self.assertOutputContains(output, 'SMS: 1 migrated, 0 skipped, 0 errors')

        sms = self.session.query(ArchiveSms).one()
        self.assertEqual(sms.uuid, self.uuid)
        self.assertEqual(sms.status, ArchiveMessageStatus.UNDELIVERED)
        self.assertEqual(sms.update_datetime, self.moddate_aware)
        self.assertEqual(sms.delivery_datetime, self.moddate_aware)

    def test_migrate_limits_from(self):
        # Test from_datetime option
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=127))
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_sms_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=129))

        with OutputCapture():
            migrate_sms(self.fred_connection, from_datetime=datetime(2010, 2, 1, 13, 30))

        self.assertEqual(self.session.query(ArchiveSms).count(), 2)

    def test_migrate_limits_to(self):
        # Test to_datetime option
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 13), moddate=datetime(2020, 1, 1), uuid=UUID(int=127))
        self.make_sms_archive(crdate=datetime(2020, 1, 1), moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 15), moddate=datetime(2020, 1, 1), uuid=UUID(int=129))

        with OutputCapture():
            migrate_sms(self.fred_connection, to_datetime=datetime(2010, 2, 1, 14, 30))

        self.assertEqual(self.session.query(ArchiveSms).count(), 2)

    def test_migrate_limits_crdate(self):
        # Test from_datetime and to_datetime options applied to crdate
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=127))
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 15), uuid=UUID(int=129))

        with OutputCapture():
            migrate_sms(self.fred_connection, from_datetime=datetime(2010, 2, 1, 13, 30),
                        to_datetime=datetime(2010, 2, 1, 14, 30))

        sms = self.session.query(ArchiveSms).one()
        self.assertEqual(sms.uuid, UUID(int=128))
        self.assertEqual(sms.create_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(sms.status, ArchiveMessageStatus.SENT)

    def test_migrate_limits_moddate(self):
        # Test from_datetime and to_datetime options applied to moddate
        self.make_sms_archive(moddate=datetime(2010, 2, 1, 13), uuid=UUID(int=127))
        self.make_sms_archive(moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_sms_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=129))

        with OutputCapture():
            migrate_sms(self.fred_connection, from_datetime=datetime(2010, 2, 1, 13, 30),
                        to_datetime=datetime(2010, 2, 1, 14, 30))

        sms = self.session.query(ArchiveSms).one()
        self.assertEqual(sms.uuid, UUID(int=128))
        self.assertEqual(sms.create_datetime, self.crdate_aware)
        self.assertEqual(sms.send_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(sms.status, ArchiveMessageStatus.SENT)

    def test_migrate_error(self):
        # Test error in migration
        self.make_sms_archive()

        with OutputCapture(separate=True, strip_whitespace=False) as output:
            with patch('messenger.commands.migrate_archive._migrate_sms', side_effect=ValueError('Gazpacho!')):
                migrate_sms(self.fred_connection)

        self.assertEqual(self.session.query(ArchiveSms).count(), 0)
        self.assertEqual(output.stdout.getvalue(), 'SMS: 0 migrated, 0 skipped, 1 errors\n')
        self.assertEqual(output.stderr.getvalue(),
                         StringComparison(r"Error in migration of {}: ValueError\('Gazpacho!'.*".format(self.uuid)))


@override_database(fred_db_connection='sqlite:///:memory:')
class MigrateLettersTest(AssertOutputMixin, BaseMigrateTestCase):
    message_types = {
        1: 'domain_expiration',
    }
    recipient_name = 'Kryten'
    recipient_organization = 'Jupiter Mining Company'
    recipient_street1 = 'Floor 16'
    recipient_street2 = 'Closet'
    recipient_street3 = 'Stasis leak'
    recipient_city = 'Red Dwarf'
    recipient_stateorprovince = 'Deep Universe'
    recipient_postalcode = 'JMC'
    recipient_country = 'JU'
    uuid = UUID(int=42)
    crdate = datetime(2001, 2, 3, 4)
    crdate_aware = datetime(2001, 2, 3, 4, tzinfo=UTC)
    moddate = datetime(2001, 3, 5, 7)
    moddate_aware = datetime(2001, 3, 5, 7, tzinfo=UTC)
    attempt = 12
    file_uuid = UUID(int=256)
    service = 'OUT POST'
    batch_id = 'HUMBLE BUNDLE'

    def setUp(self):
        super().setUp()
        enum = ENUM_SEND_STATUS.insert().values(
            [{'id': status.id, 'status_name': status.value} for status in FredMessageStatus])
        self.insert(enum)
        types = MESSAGE_TYPE.insert().values([{'id': i, 'type': t} for i, t in self.message_types.items()])
        self.insert(types)

    def make_letter_archive(
            self, uuid: UUID = uuid, crdate: datetime = crdate, moddate: Optional[datetime] = moddate,
            status: FredMessageStatus = FredMessageStatus.SENT, message_type: int = 1, file_uuid: UUID = file_uuid,
            ) -> int:
        message = MESSAGE_ARCHIVE.insert().values(uuid=uuid, crdate=crdate, moddate=moddate, status_id=status.id,
                                                  attempt=self.attempt, message_type_id=message_type,
                                                  service_handle=self.service)
        pk = self.insert(message)
        file_id = self.insert(FILES.insert().values(uuid=file_uuid))
        letter = LETTER_ARCHIVE.insert().values(
            id=pk, file_id=file_id, postal_address_name=self.recipient_name,
            postal_address_organization=self.recipient_organization, postal_address_street1=self.recipient_street1,
            postal_address_street2=self.recipient_street2, postal_address_street3=self.recipient_street3,
            postal_address_city=self.recipient_city, postal_address_stateorprovince=self.recipient_stateorprovince,
            postal_address_postalcode=self.recipient_postalcode, postal_address_country=self.recipient_country,
            batch_id=self.batch_id)
        self.insert(letter)
        return pk

    def test_migrate_no_letter(self):
        # Test no letter to migrate
        with OutputCapture() as output:
            migrate_letters(self.fred_connection)

        self.assertOutputContains(output, 'Letters: 0 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Letter).count(), 0)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def _test_migrate(self, status: ArchiveMessageStatus = ArchiveMessageStatus.SENT,
                      send_datetime: Optional[datetime] = moddate_aware, delivery_datetime: Optional[datetime] = None,
                      update_datetime: Optional[datetime] = moddate_aware) -> ArchiveLetter:
        # Test new letter is migrated
        with OutputCapture() as output:
            migrate_letters(self.fred_connection)

        self.assertOutputContains(output, 'Letters: 1 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Letter).count(), 0)
        letter = cast(ArchiveLetter, self.session.query(ArchiveLetter).one())
        self.assertEqual(letter.uuid, self.uuid)
        self.assertEqual(letter.create_datetime, self.crdate_aware)
        self.assertEqual(letter.update_datetime, update_datetime)
        self.assertEqual(letter.send_datetime, send_datetime)
        self.assertEqual(letter.status, status)
        self.assertEqual(letter.type, 'domain_expiration')
        self.assertEqual(letter.attempts, self.attempt)
        address = Address(self.recipient_name, self.recipient_organization,
                          '\n'.join((self.recipient_street1, self.recipient_street2, self.recipient_street3)),
                          self.recipient_city, self.recipient_stateorprovince, self.recipient_postalcode,
                          self.recipient_country)
        self.assertEqual(letter.recipient, address)
        self.assertEqual(letter.file, self.file_uuid)
        self.assertEqual(letter.service, self.service)
        self.assertEqual(letter.batch_id, self.batch_id)
        self.assertEqual(letter.delivery_datetime, delivery_datetime)
        return letter

    def test_migrate_sent(self):
        self.make_letter_archive()
        self._test_migrate()

    def test_migrate_failed(self):
        self.make_letter_archive(status=FredMessageStatus.FAILED)
        self._test_migrate(status=ArchiveMessageStatus.FAILED, send_datetime=None, delivery_datetime=None)

    def test_migrate_canceled(self):
        self.make_letter_archive(status=FredMessageStatus.CANCELED)
        self._test_migrate(status=ArchiveMessageStatus.CANCELED, send_datetime=None, delivery_datetime=None)

    def test_migrate_canceled_existing(self):
        # Test migration of canceled letter is skipped when it already exists in archive with another status.
        self.make_letter_archive(status=FredMessageStatus.CANCELED)
        make_archive_letter(self.session, id=2, create_datetime=self.crdate_aware, uuid=self.uuid,
                            file=self.file_uuid)

        with OutputCapture() as output:
            migrate_letters(self.fred_connection)

        self.assertOutputContains(output, 'Letters: 0 migrated, 1 skipped, 0 errors')

        # Check letter unchanged.
        self.assertEqual(self.session.query(Letter).count(), 0)
        # Check archive unchanged.
        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.id, 2)
        self.assertEqual(letter.uuid, self.uuid)
        self.assertIsNone(letter.update_datetime)
        self.assertIsNone(letter.send_datetime)
        self.assertEqual(letter.status, MessageStatus.SENT)

    def test_migrate_canceled_pending_letter(self):
        # Test migration of canceled letter when it's already present as pending.
        self.make_letter_archive(status=FredMessageStatus.CANCELED)
        make_letter(self.session, create_datetime=self.crdate_aware, uuid=self.uuid, file=self.file_uuid)

        # Test new letter is migrated
        with OutputCapture() as output:
            migrate_letters(self.fred_connection)

        self.assertOutputContains(output, 'Letters: 1 migrated, 0 skipped, 0 errors')

        # Check archive unchanged.
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)
        # Check letter canceled.
        letter = self.session.query(Letter).one()
        self.assertEqual(letter.uuid, self.uuid)
        self.assertEqual(letter.create_datetime, self.crdate_aware)
        self.assertEqual(letter.update_datetime, self.moddate_aware)
        self.assertIsNone(letter.send_datetime)
        self.assertEqual(letter.status, MessageStatus.CANCELED)
        self.assertIsNone(letter.delivery_datetime)

    def test_migrate_canceled_non_pending_letter(self):
        # Test migration of canceled letter is skipped when it's already present as non-pending.
        self.make_letter_archive(status=FredMessageStatus.CANCELED)
        make_letter(self.session, create_datetime=self.crdate_aware, uuid=self.uuid, file=self.file_uuid,
                    status=MessageStatus.SENT)

        with OutputCapture() as output:
            migrate_letters(self.fred_connection)

        self.assertOutputContains(output, 'Letters: 0 migrated, 1 skipped, 0 errors')

        # Check archive unchanged.
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)
        # Check letter unchanged.
        letter = self.session.query(Letter).one()
        self.assertEqual(letter.uuid, self.uuid)
        self.assertIsNone(letter.update_datetime)
        self.assertIsNone(letter.send_datetime)
        self.assertEqual(letter.status, MessageStatus.SENT)

    def test_migrate_undelivered(self):
        self.make_letter_archive(status=FredMessageStatus.UNDELIVERED)
        self._test_migrate(status=ArchiveMessageStatus.UNDELIVERED, send_datetime=self.crdate_aware,
                           delivery_datetime=self.moddate_aware)

    def test_migrate_pending(self):
        # Test pending letter is not migrated, unless requested.
        self.make_letter_archive(status=FredMessageStatus.PENDING)
        with OutputCapture() as output:
            migrate_letters(self.fred_connection)

        self.assertOutputContains(output, 'Letters: 0 migrated, 0 skipped, 0 errors')

        self.assertEqual(self.session.query(Letter).count(), 0)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def test_migrate_pending_requested(self):
        # Test pending letter is migrated if requested.
        self.make_letter_archive(status=FredMessageStatus.PENDING)
        with OutputCapture() as output:
            migrate_letters(self.fred_connection, all_messages=True)

        self.assertOutputContains(output, 'Letters: 1 migrated, 0 skipped, 0 errors')

        letter = self.session.query(Letter).one()
        self.assertEqual(letter.uuid, self.uuid)
        self.assertEqual(letter.create_datetime, self.crdate_aware)
        self.assertEqual(letter.update_datetime, self.moddate_aware)
        self.assertIsNone(letter.send_datetime)
        self.assertEqual(letter.status, MessageStatus.PENDING)
        self.assertEqual(letter.type, 'domain_expiration')
        self.assertEqual(letter.attempts, self.attempt)
        address = Address(self.recipient_name, self.recipient_organization,
                          '\n'.join((self.recipient_street1, self.recipient_street2, self.recipient_street3)),
                          self.recipient_city, self.recipient_stateorprovince, self.recipient_postalcode,
                          self.recipient_country)
        self.assertEqual(letter.recipient, address)
        self.assertEqual(letter.file, self.file_uuid)
        self.assertEqual(letter.service, self.service)
        self.assertEqual(letter.batch_id, self.batch_id)
        self.assertIsNone(letter.delivery_datetime)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def test_migrate_no_moddate(self):
        self.make_letter_archive(moddate=None)
        self._test_migrate(send_datetime=None, update_datetime=None)

    def test_migrate_references(self):
        letter_id = self.make_letter_archive()
        # Make a reference
        object_uuid = UUID(int=255)
        contact_type_id = self.insert(ENUM_OBJECT_TYPE.insert().values(name=ReferenceType.CONTACT))
        object_registry_id = self.insert(OBJECT_REGISTRY.insert().values(type=contact_type_id, uuid=object_uuid))
        query = MESSAGE_CONTACT_HISTORY_MAP.insert().values(contact_object_registry_id=object_registry_id,
                                                            message_archive_id=letter_id)
        self.insert(query)

        letter = self._test_migrate()

        self.assertEqual([(r.type, r.value) for r in letter.references], [(ReferenceType.CONTACT, str(object_uuid))])

    def test_migrate_existing(self):
        # Test existing letter is not updated
        # Create letter with mismatching recipient and check it doesn't change
        self.make_letter_archive()
        recipient_name = 'Arnold Rimmer'
        make_archive_letter(self.session, id=2, create_datetime=self.crdate_aware, uuid=self.uuid,
                            recipient_name=recipient_name, file=self.file_uuid)

        with OutputCapture() as output:
            migrate_letters(self.fred_connection)

        self.assertOutputContains(output, 'Letters: 0 migrated, 1 skipped, 0 errors')

        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.uuid, self.uuid)
        self.assertEqual(letter.recipient_name, recipient_name)

    def test_migrate_update(self):
        # Test letter is updated upon delivery change
        self.make_letter_archive(status=FredMessageStatus.UNDELIVERED)
        make_archive_letter(self.session, id=2, create_datetime=self.crdate_aware, uuid=self.uuid,
                            recipient_name=self.recipient_name, file=self.file_uuid)

        with OutputCapture() as output:
            migrate_letters(self.fred_connection)

        self.assertOutputContains(output, 'Letters: 1 migrated, 0 skipped, 0 errors')

        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.uuid, self.uuid)
        self.assertEqual(letter.status, ArchiveMessageStatus.UNDELIVERED)
        self.assertEqual(letter.update_datetime, self.moddate_aware)
        self.assertEqual(letter.delivery_datetime, self.moddate_aware)

    def test_migrate_limits_from(self):
        # Test from_datetime option
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=127), file_uuid=UUID(int=256))
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=128), file_uuid=UUID(int=257))
        self.make_letter_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=129), file_uuid=UUID(int=258))

        with OutputCapture():
            migrate_letters(self.fred_connection, from_datetime=datetime(2010, 2, 1, 13, 30))

        self.assertEqual(self.session.query(ArchiveLetter).count(), 2)

    def test_migrate_limits_to(self):
        # Test to_datetime option
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 13), moddate=datetime(2020, 1, 1), uuid=UUID(int=127),
                                 file_uuid=UUID(int=256))
        self.make_letter_archive(crdate=datetime(2020, 1, 1), moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=128),
                                 file_uuid=UUID(int=257))
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 15), moddate=datetime(2020, 1, 1), uuid=UUID(int=129),
                                 file_uuid=UUID(int=258))

        with OutputCapture():
            migrate_letters(self.fred_connection, to_datetime=datetime(2010, 2, 1, 14, 30))

        self.assertEqual(self.session.query(ArchiveLetter).count(), 2)

    def test_migrate_limits_crdate(self):
        # Test from_datetime and to_datetime options applied to crdate
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=127), file_uuid=UUID(int=256))
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=128), file_uuid=UUID(int=257))
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 15), uuid=UUID(int=129), file_uuid=UUID(int=258))

        with OutputCapture():
            migrate_letters(self.fred_connection, from_datetime=datetime(2010, 2, 1, 13, 30),
                            to_datetime=datetime(2010, 2, 1, 14, 30))

        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.uuid, UUID(int=128))
        self.assertEqual(letter.create_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(letter.status, ArchiveMessageStatus.SENT)

    def test_migrate_limits_moddate(self):
        # Test from_datetime and to_datetime options applied to moddate
        self.make_letter_archive(moddate=datetime(2010, 2, 1, 13), uuid=UUID(int=127), file_uuid=UUID(int=256))
        self.make_letter_archive(moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=128), file_uuid=UUID(int=257))
        self.make_letter_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=129), file_uuid=UUID(int=258))

        with OutputCapture():
            migrate_letters(self.fred_connection, from_datetime=datetime(2010, 2, 1, 13, 30),
                            to_datetime=datetime(2010, 2, 1, 14, 30))

        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.uuid, UUID(int=128))
        self.assertEqual(letter.create_datetime, self.crdate_aware)
        self.assertEqual(letter.send_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(letter.status, ArchiveMessageStatus.SENT)

    def test_migrate_error(self):
        # Test error in migration
        self.make_letter_archive()

        with OutputCapture(separate=True, strip_whitespace=False) as output:
            with patch('messenger.commands.migrate_archive._migrate_letter', side_effect=ValueError('Gazpacho!')):
                migrate_letters(self.fred_connection)

        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)
        self.assertEqual(output.stdout.getvalue(), 'Letters: 0 migrated, 0 skipped, 1 errors\n')
        self.assertEqual(output.stderr.getvalue(),
                         StringComparison(r"Error in migration of {}: ValueError\('Gazpacho!'.*".format(self.uuid)))


@override_database(fred_db_connection='sqlite:///:memory:')
class MigrateArchiveMainTest(CommandMixin, BaseMigrateTestCase):
    command = main

    # Shared fields
    crdate = datetime(2001, 2, 3, 4)
    crdate_aware = datetime(2001, 2, 3, 4, tzinfo=UTC)
    moddate = datetime(2001, 3, 5, 7)
    moddate_aware = datetime(2001, 3, 5, 7, tzinfo=UTC)
    attempt = 12

    # Email fields
    sender = 'holly@example.org'
    reply_to = 'reply@example.org'
    errors_to = 'errors@example.org'
    organization = 'JMC'
    email_recipient = 'kryten@example.org'
    email_uuid = UUID(int=42)
    message_id = '<mail.archive@example.org>'
    template = 'template'
    template_version = 255
    subject_uuid = UUID(int=127)
    subject = 'subject.txt'
    email_body_uuid = UUID(int=128)
    email_body = 'body.txt'
    email_context = {'context_key': 'context_value'}

    # SMS fields
    sms_recipient = '+1.234'
    sms_uuid = UUID(int=43)
    sms_content = 'Confirm by PIN: Quagaars!'
    sms_context = {'pin': 'Quagaars!'}
    sms_body = 'mojeid-sms-change-en.txt'
    sms_body_uuid = UUID('b1d3e4f3-d03d-4755-87c3-ffc86a24587b')

    # Letter fields
    letter_recipient_name = 'Kryten'
    letter_uuid = UUID(int=44)
    file_uuid = UUID(int=256)

    def setUp(self):
        super().setUp()

        self.template_id = self.insert(MAIL_TYPE.insert().values(name=self.template))
        header = MAIL_HEADER.insert().values(h_from=self.sender, h_replyto=self.reply_to, h_errorsto=self.errors_to,
                                             h_organization=self.organization)
        header_id = self.insert(header)
        template = MAIL_TEMPLATE.insert().values(mail_header_default_id=header_id,
                                                 mail_type_id=self.template_id, version=self.template_version)
        self.insert(template)
        template_migration = MAIL_TEMPLATE_MIGRATION.insert().values(
            mail_type_id=self.template_id, version=self.template_version,
            subject=self.subject, subject_uuid=self.subject_uuid, body=self.email_body, body_uuid=self.email_body_uuid)
        self.insert(template_migration)

        enum = ENUM_SEND_STATUS.insert().values(
            [{'id': status.id, 'status_name': status.value} for status in FredMessageStatus])
        self.insert(enum)
        types = MESSAGE_TYPE.insert().values([{'id': i, 'type': t} for i, t in MigrateSmsTest.message_types.items()])
        self.insert(types)
        types = MESSAGE_TYPE.insert().values(
            [{'id': i, 'type': t} for i, t in MigrateLettersTest.message_types.items()])
        self.insert(types)

    def make_mail_archive(
            self, uuid: UUID = email_uuid, crdate: datetime = crdate, moddate: datetime = moddate,
            status: int = FredEmailStatus.SENT, message_params: Optional[Dict[str, Dict[str, str]]] = None,
            response_header: Optional[Dict[str, str]] = None) -> int:
        message_params = (
            message_params
            or {'header': {'To': self.email_recipient, 'Message-ID': self.message_id}, 'body': self.email_context})
        mail = MAIL_ARCHIVE.insert().values(
            uuid=uuid, crdate=crdate, moddate=moddate, status=status, attempt=self.attempt,
            mail_type_id=self.template_id, mail_template_version=self.template_version, message_params=message_params,
            response_header=response_header)
        return self.insert(mail)

    def make_sms_archive(
            self, uuid: UUID = sms_uuid, crdate: datetime = crdate, moddate: datetime = moddate,
            status: FredMessageStatus = FredMessageStatus.SENT, message_type: int = 4) -> int:
        message = MESSAGE_ARCHIVE.insert().values(uuid=uuid, crdate=crdate, moddate=moddate, status_id=status.id,
                                                  attempt=self.attempt, message_type_id=message_type)
        pk = self.insert(message)
        sms = SMS_ARCHIVE.insert().values(id=pk, phone_number=self.sms_recipient, content=self.sms_content)
        self.insert(sms)
        return pk

    def make_letter_archive(
            self, uuid: UUID = letter_uuid, crdate: datetime = crdate, moddate: datetime = moddate,
            status: FredMessageStatus = FredMessageStatus.SENT, message_type: int = 1, file_uuid: UUID = file_uuid,
            ) -> int:
        message = MESSAGE_ARCHIVE.insert().values(uuid=uuid, crdate=crdate, moddate=moddate,
                                                  status_id=status.id, attempt=self.attempt,
                                                  message_type_id=message_type)
        pk = self.insert(message)
        file_id = self.insert(FILES.insert().values(uuid=file_uuid))
        letter = LETTER_ARCHIVE.insert().values(id=pk, file_id=file_id, postal_address_name=self.letter_recipient_name)
        self.insert(letter)
        return pk

    def test_main(self):
        # Test migrate all messages
        self.make_mail_archive()
        self.make_sms_archive()
        self.make_letter_archive()

        output = ('Emails: 1 migrated, 0 skipped, 0 errors\n'
                  'SMS: 1 migrated, 0 skipped, 0 errors\n'
                  'Letters: 1 migrated, 0 skipped, 0 errors\n')
        self.assertCommandSuccess(stdout=output)

        self.assertEqual(self.session.query(Email).count(), 0)
        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, self.email_uuid)
        self.assertEqual(email.status, ArchiveMessageStatus.SENT)
        self.assertEqual(self.session.query(Sms).count(), 0)
        sms = self.session.query(ArchiveSms).one()
        self.assertEqual(sms.uuid, self.sms_uuid)
        self.assertEqual(sms.status, ArchiveMessageStatus.SENT)
        self.assertEqual(self.session.query(Letter).count(), 0)
        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.uuid, self.letter_uuid)
        self.assertEqual(letter.status, ArchiveMessageStatus.SENT)

    def test_main_email(self):
        # Test email is migrated
        self.make_mail_archive()
        self.make_sms_archive()
        self.make_letter_archive()

        self.assertCommandSuccess(['--method', 'email'], stdout='Emails: 1 migrated, 0 skipped, 0 errors\n')

        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, self.email_uuid)
        self.assertEqual(email.status, ArchiveMessageStatus.SENT)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def test_main_sms(self):
        # Test SMS is migrated
        self.make_mail_archive()
        self.make_sms_archive()
        self.make_letter_archive()

        self.assertCommandSuccess(['--method', 'sms'], stdout='SMS: 1 migrated, 0 skipped, 0 errors\n')

        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        sms = self.session.query(ArchiveSms).one()
        self.assertEqual(sms.uuid, self.sms_uuid)
        self.assertEqual(sms.status, ArchiveMessageStatus.SENT)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def test_main_letter(self):
        # Test letter is migrated
        self.make_mail_archive()
        self.make_sms_archive()
        self.make_letter_archive()

        self.assertCommandSuccess(['--method', 'letter'], stdout='Letters: 1 migrated, 0 skipped, 0 errors\n')

        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)
        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.uuid, self.letter_uuid)
        self.assertEqual(letter.status, ArchiveMessageStatus.SENT)

    def test_main_limits_from(self):
        # Test --from option
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=127))
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_mail_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=129))

        self.make_sms_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=130))
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=131))
        self.make_sms_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=132))

        self.make_letter_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=133), file_uuid=UUID(int=256))
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=134), file_uuid=UUID(int=257))
        self.make_letter_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=135), file_uuid=UUID(int=258))

        self.assertCommandSuccess(['--from', '2010-02-01 13:30:00'])

        self.assertEqual(self.session.query(ArchiveEmail).count(), 2)
        self.assertEqual(self.session.query(ArchiveSms).count(), 2)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 2)

    def test_main_limits_to(self):
        # Test --to option
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 13), moddate=datetime(2020, 1, 1), uuid=UUID(int=127))
        self.make_mail_archive(crdate=datetime(2020, 1, 1), moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 15), moddate=datetime(2020, 1, 1), uuid=UUID(int=129))

        self.make_sms_archive(crdate=datetime(2010, 2, 1, 13), moddate=datetime(2020, 1, 1), uuid=UUID(int=130))
        self.make_sms_archive(crdate=datetime(2020, 1, 1), moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=131))
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 15), moddate=datetime(2020, 1, 1), uuid=UUID(int=132))

        self.make_letter_archive(crdate=datetime(2010, 2, 1, 13), moddate=datetime(2020, 1, 1), uuid=UUID(int=133),
                                 file_uuid=UUID(int=256))
        self.make_letter_archive(crdate=datetime(2020, 1, 1), moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=134),
                                 file_uuid=UUID(int=257))
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 15), moddate=datetime(2020, 1, 1), uuid=UUID(int=135),
                                 file_uuid=UUID(int=258))

        self.assertCommandSuccess(['--to', '2010-02-01 14:30:00'])

        self.assertEqual(self.session.query(ArchiveEmail).count(), 2)
        self.assertEqual(self.session.query(ArchiveSms).count(), 2)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 2)

    def test_main_limits_crdate(self):
        # Test --from and --to options applied to crdate
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=127))
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_mail_archive(crdate=datetime(2010, 2, 1, 15), uuid=UUID(int=129))

        self.make_sms_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=130))
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=131))
        self.make_sms_archive(crdate=datetime(2010, 2, 1, 15), uuid=UUID(int=132))

        self.make_letter_archive(crdate=datetime(2010, 2, 1, 13), uuid=UUID(int=133), file_uuid=UUID(int=256))
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 14), uuid=UUID(int=134), file_uuid=UUID(int=257))
        self.make_letter_archive(crdate=datetime(2010, 2, 1, 15), uuid=UUID(int=135), file_uuid=UUID(int=258))

        self.assertCommandSuccess(['--from', '2010-02-01 13:30:00', '--to', '2010-02-01 14:30:00'])

        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, UUID(int=128))
        self.assertEqual(email.create_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.status, ArchiveMessageStatus.SENT)

        sms = self.session.query(ArchiveSms).one()
        self.assertEqual(sms.uuid, UUID(int=131))
        self.assertEqual(sms.create_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(sms.status, ArchiveMessageStatus.SENT)

        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.uuid, UUID(int=134))
        self.assertEqual(letter.create_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(letter.status, ArchiveMessageStatus.SENT)

    def test_main_limits_moddate(self):
        # Test --from and --to options applied to moddate
        self.make_mail_archive(moddate=datetime(2010, 2, 1, 13), uuid=UUID(int=127))
        self.make_mail_archive(moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=128))
        self.make_mail_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=129))

        self.make_sms_archive(moddate=datetime(2010, 2, 1, 13), uuid=UUID(int=130))
        self.make_sms_archive(moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=131))
        self.make_sms_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=132))

        self.make_letter_archive(moddate=datetime(2010, 2, 1, 13), uuid=UUID(int=133), file_uuid=UUID(int=256))
        self.make_letter_archive(moddate=datetime(2010, 2, 1, 14), uuid=UUID(int=134), file_uuid=UUID(int=257))
        self.make_letter_archive(moddate=datetime(2010, 2, 1, 15), uuid=UUID(int=135), file_uuid=UUID(int=258))

        self.assertCommandSuccess(['--from', '2010-02-01 13:30:00', '--to', '2010-02-01 14:30:00'])

        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, UUID(int=128))
        self.assertEqual(email.create_datetime, self.crdate_aware)
        self.assertEqual(email.send_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(email.status, ArchiveMessageStatus.SENT)

        sms = self.session.query(ArchiveSms).one()
        self.assertEqual(sms.uuid, UUID(int=131))
        self.assertEqual(sms.create_datetime, self.crdate_aware)
        self.assertEqual(sms.send_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(sms.status, ArchiveMessageStatus.SENT)

        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.uuid, UUID(int=134))
        self.assertEqual(letter.create_datetime, self.crdate_aware)
        self.assertEqual(letter.send_datetime, datetime(2010, 2, 1, 14, tzinfo=UTC))
        self.assertEqual(letter.status, ArchiveMessageStatus.SENT)

    def test_main_pending(self):
        # Test pending messages are not migrated, unless requested.
        self.make_mail_archive(status=FredEmailStatus.PENDING)
        self.make_sms_archive(status=FredMessageStatus.PENDING)
        self.make_letter_archive(status=FredMessageStatus.PENDING)

        output = ('Emails: 0 migrated, 0 skipped, 0 errors\n'
                  'SMS: 0 migrated, 0 skipped, 0 errors\n'
                  'Letters: 0 migrated, 0 skipped, 0 errors\n')
        self.assertCommandSuccess(stdout=output)

        self.assertEqual(self.session.query(Email).count(), 0)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 0)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 0)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def test_main_pending_requested(self):
        # Test pending messages are migrated if requested.
        self.make_mail_archive(status=FredEmailStatus.PENDING)
        self.make_sms_archive(status=FredMessageStatus.PENDING)
        self.make_letter_archive(status=FredMessageStatus.PENDING)

        output = ('Emails: 1 migrated, 0 skipped, 0 errors\n'
                  'SMS: 1 migrated, 0 skipped, 0 errors\n'
                  'Letters: 1 migrated, 0 skipped, 0 errors\n')
        self.assertCommandSuccess(['--all-messages'], stdout=output)

        self.assertEqual(self.session.query(Email).count(), 1)
        self.assertEqual(self.session.query(ArchiveEmail).count(), 0)
        self.assertEqual(self.session.query(Sms).count(), 1)
        self.assertEqual(self.session.query(ArchiveSms).count(), 0)
        self.assertEqual(self.session.query(Letter).count(), 1)
        self.assertEqual(self.session.query(ArchiveLetter).count(), 0)

    def test_main_disabled(self):
        self.assertCommandFail(stderr='Fred DB connection not defined.', settings={'fred_db_connection': None})

    def test_main_custom_connection(self):
        with override_settings(fileman_netloc=None):
            test_app = TestApplication(Settings)
            test_app.settings = APPLICATION.settings
            with patch('messenger.commands.utils.APPLICATION', new=test_app):
                self.runner.invoke(self.command, ['--fred-connection', 'sqlite://test@localhost/:memory:'])

                # Just check the settings have changed.
                self.assertEqual(APPLICATION.settings.fred_db_connection, 'sqlite://test@localhost/:memory:')


# Use the same test database for both messenger and fred.
@override_database(db_connection=APPLICATION.settings.test_db_connection,
                   fred_db_connection=APPLICATION.settings.test_db_connection)
class MigrateArchiveMainIsolationTest(CommandMixin, BaseMigrateTestCase):
    command = main

    # Shared fields
    crdate = datetime(2001, 2, 3, 4)
    moddate = datetime(2001, 3, 5, 7)
    attempt = 12

    # Email fields
    sender = 'holly@example.org'
    reply_to = 'reply@example.org'
    errors_to = 'errors@example.org'
    organization = 'JMC'
    email_recipient = 'kryten@example.org'
    email_uuid = UUID(int=42)
    message_id = '<mail.archive@example.org>'
    template = 'template'
    template_version = 255
    subject_uuid = UUID(int=127)
    subject = 'subject.txt'
    email_body_uuid = UUID(int=128)
    email_body = 'body.txt'
    email_context = {'context_key': 'context_value'}

    # SMS fields
    sms_recipient = '+1.234'
    sms_uuid = UUID(int=43)
    sms_content = 'Confirm by PIN: Quagaars!'
    sms_context = {'pin': 'Quagaars!'}
    sms_body = 'mojeid-sms-change-en.txt'
    sms_body_uuid = UUID('b1d3e4f3-d03d-4755-87c3-ffc86a24587b')

    # Letter fields
    letter_recipient_name = 'Kryten'
    letter_uuid = UUID(int=44)
    file_uuid = UUID(int=256)

    def setUp(self):
        super().setUp()

        self.template_id = self.insert(MAIL_TYPE.insert().values(name=self.template))
        header = MAIL_HEADER.insert().values(h_from=self.sender, h_replyto=self.reply_to, h_errorsto=self.errors_to,
                                             h_organization=self.organization)
        header_id = self.insert(header)
        template = MAIL_TEMPLATE.insert().values(mail_header_default_id=header_id,
                                                 mail_type_id=self.template_id, version=self.template_version)
        self.insert(template)
        template_migration = MAIL_TEMPLATE_MIGRATION.insert().values(
            mail_type_id=self.template_id, version=self.template_version,
            subject=self.subject, subject_uuid=self.subject_uuid, body=self.email_body, body_uuid=self.email_body_uuid)
        self.insert(template_migration)

        enum = ENUM_SEND_STATUS.insert().values(
            [{'id': status.id, 'status_name': status.value} for status in FredMessageStatus])
        self.insert(enum)
        types = MESSAGE_TYPE.insert().values([{'id': i, 'type': t} for i, t in MigrateSmsTest.message_types.items()])
        self.insert(types)
        types = MESSAGE_TYPE.insert().values(
            [{'id': i, 'type': t} for i, t in MigrateLettersTest.message_types.items()])
        self.insert(types)

    def make_mail_archive(
            self, uuid: UUID = email_uuid, crdate: datetime = crdate, moddate: datetime = moddate,
            status: int = FredEmailStatus.SENT, response_header: Optional[Dict[str, str]] = None) -> int:
        message_params = {'header': {'To': self.email_recipient, 'Message-ID': self.message_id},
                          'body': self.email_context}
        mail = MAIL_ARCHIVE.insert().values(
            uuid=uuid, crdate=crdate, moddate=moddate, status=status, attempt=self.attempt,
            mail_type_id=self.template_id, mail_template_version=self.template_version, message_params=message_params,
            response_header=response_header)
        return self.insert(mail)

    def make_sms_archive(
            self, uuid: UUID = sms_uuid, crdate: datetime = crdate, moddate: datetime = moddate,
            status: FredMessageStatus = FredMessageStatus.SENT, message_type: int = 4) -> int:
        message = MESSAGE_ARCHIVE.insert().values(uuid=uuid, crdate=crdate, moddate=moddate, status_id=status.id,
                                                  attempt=self.attempt, message_type_id=message_type)
        pk = self.insert(message)
        sms = SMS_ARCHIVE.insert().values(id=pk, phone_number=self.sms_recipient, content=self.sms_content)
        self.insert(sms)
        return pk

    def make_letter_archive(
            self, uuid: UUID = letter_uuid, crdate: datetime = crdate, moddate: datetime = moddate,
            status: FredMessageStatus = FredMessageStatus.SENT, message_type: int = 1) -> int:
        message = MESSAGE_ARCHIVE.insert().values(uuid=uuid, crdate=crdate, moddate=moddate, status_id=status.id,
                                                  attempt=self.attempt, message_type_id=message_type)
        pk = self.insert(message)
        file_id = self.insert(FILES.insert().values(uuid=self.file_uuid))
        letter = LETTER_ARCHIVE.insert().values(id=pk, file_id=file_id, postal_address_name=self.letter_recipient_name)
        self.insert(letter)
        return pk

    def test_main_isolation_email(self):
        # Test migration is run in repeatable read isolation level.
        self.make_mail_archive()

        def migrate_email_test(*args):
            # Update emails in Fred database.
            with self.fred_engine.connect() as connection:
                update = MAIL_ARCHIVE.update().values(status=FredEmailStatus.UNDELIVERED)
                connection.execute(update)
                connection.commit()
            # Run the standard migration
            _migrate_email(*args)

        with patch('messenger.commands.migrate_archive._migrate_email', new=migrate_email_test):
            self.assertCommandSuccess(['--method', 'email'], stdout='Emails: 0 migrated, 1 skipped, 0 errors\n')

        # Check the migration reflects original data.
        email = self.session.query(ArchiveEmail).one()
        self.assertEqual(email.uuid, self.email_uuid)
        self.assertEqual(email.status, ArchiveMessageStatus.SENT)

    def test_main_isolation_sms(self):
        # Test migration is run in repeatable read isolation level.
        self.make_sms_archive()

        def migrate_sms_test(*args):
            # Update sms in Fred database.
            with self.fred_engine.connect() as connection:
                update = MESSAGE_ARCHIVE.update().values(status_id=7)  # undelivered
                connection.execute(update)
                connection.commit()
            # Run the standard migration
            _migrate_sms(*args)

        with patch('messenger.commands.migrate_archive._migrate_sms', new=migrate_sms_test):
            self.assertCommandSuccess(['--method', 'sms'], stdout='SMS: 0 migrated, 1 skipped, 0 errors\n')

        # Check the migration reflects original data.
        sms = self.session.query(ArchiveSms).one()
        self.assertEqual(sms.uuid, self.sms_uuid)
        self.assertEqual(sms.status, ArchiveMessageStatus.SENT)

    def test_main_isolation_letter(self):
        # Test migration is run in repeatable read isolation level.
        self.make_letter_archive()

        def migrate_letter_test(*args):
            # Update letter in Fred database.
            with self.fred_engine.connect() as connection:
                update = MESSAGE_ARCHIVE.update().values(status_id=7)  # undelivered
                connection.execute(update)
                connection.commit()
            # Run the standard migration
            _migrate_letter(*args)

        with patch('messenger.commands.migrate_archive._migrate_letter', new=migrate_letter_test):
            self.assertCommandSuccess(['--method', 'letter'], stdout='Letters: 0 migrated, 1 skipped, 0 errors\n')

        # Check the migration reflects original data.
        letter = self.session.query(ArchiveLetter).one()
        self.assertEqual(letter.uuid, self.letter_uuid)
        self.assertEqual(letter.status, ArchiveMessageStatus.SENT)
