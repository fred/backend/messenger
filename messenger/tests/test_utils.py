import ssl
from unittest import TestCase
from unittest.mock import call, patch, sentinel

from testfixtures import LogCapture

from messenger.settings import ENVIRON_NAME
from messenger.utils import (
    Result,
    create_ssl_context,
    import_path,
    load_instance,
    normalize_email_address,
    normalize_phone_number,
)


class NormalizeEmailAddress(TestCase):
    def test_valid(self):
        data = (
            ('holly@example.org', 'holly@example.org'),
            ('HOLLY@example.org', 'holly@example.org'),
            ('HOLLY@EXAMPLE.ORG', 'holly@example.org'),
            ('Holly <holly@example.org>', 'holly@example.org'),
            ('Holly <HOLLY@EXAMPLE.ORG>', 'holly@example.org'),
        )
        for raw, normalized in data:
            with self.subTest(raw=raw):
                self.assertEqual(normalize_email_address(raw), normalized)

    def test_invalid(self):
        data = (
            '',
            '<>',
        )
        for raw in data:
            with self.subTest(raw=raw):
                self.assertIsNone(normalize_email_address(raw))


class NormalizePhoneNumber(TestCase):
    def test_valid(self):
        data = (
            ('+1234', '+1234'),  # E164 format
            (' +1234 ', '+1234'),
            ('+1.234', '+1234'),  # E164 format used in FRED
        )
        for raw, normalized in data:
            with self.subTest(raw=raw):
                self.assertEqual(normalize_phone_number(raw), normalized)

    def test_invalid(self):
        data = (
            '',
            'invalid',
            '1234',  # Just a number, not a phone number.
        )
        for raw in data:
            with self.subTest(raw=raw):
                self.assertIsNone(normalize_phone_number(raw))


class CreateSslContextTest(TestCase):
    def setUp(self):
        self.log_handler = LogCapture('messenger', propagate=False)

    def tearDown(self):
        self.log_handler.uninstall()

    def test_context(self):
        data = (
            # certfile, keyfile, ssl_calls
            (None, None, [call()]),
            (sentinel.certfile, None, [call(), call().load_cert_chain(sentinel.certfile, None)]),
            (sentinel.certfile, sentinel.keyfile,
             [call(), call().load_cert_chain(sentinel.certfile, sentinel.keyfile)]),
        )
        for certfile, keyfile, ssl_calls in data:
            with self.subTest(certfile=certfile, keyfile=keyfile):
                with patch('ssl.create_default_context', autospec=True) as ssl_mock:
                    ssl_mock.return_value.check_hostname = True
                    ssl_mock.return_value.verify_mode = ssl.CERT_REQUIRED

                    context = create_ssl_context(certfile, keyfile)

                self.assertTrue(context.check_hostname)
                self.assertEqual(context.verify_mode, ssl.CERT_REQUIRED)
                # Check calls first, then the result. To avoid __eq__ call on mocked context.
                self.assertEqual(ssl_mock.mock_calls, ssl_calls)
                self.assertEqual(context, ssl_mock.return_value)

    def test_context_verify_off(self):
        with patch('ssl.create_default_context', autospec=True) as ssl_mock:
            ssl_mock.return_value.check_hostname = True
            ssl_mock.return_value.verify_mode = ssl.CERT_REQUIRED

            context = create_ssl_context(verify=False)

        self.assertFalse(context.check_hostname)
        self.assertEqual(context.verify_mode, ssl.CERT_NONE)
        # Check calls first, then the result. To avoid __eq__ call on mocked context.
        self.assertEqual(ssl_mock.mock_calls, [call()])
        self.assertEqual(context, ssl_mock.return_value)

        self.log_handler.check(('messenger.utils', 'WARNING', "Verification of the peer certificate is disabled."))


class ImportPathTest(TestCase):
    def test_class(self):
        cls = import_path('messenger.tests.test_utils.ImportPathTest')
        self.assertEqual(cls, ImportPathTest)

    def test_attribute(self):
        attribute = import_path('messenger.settings.ENVIRON_NAME')
        self.assertEqual(attribute, ENVIRON_NAME)

    def test_invalid(self):
        data = (
            # path, error_message
            ('', 'is not a valid dotted path'),
            ('.', 'Empty module name'),
            ('.invalid', 'Empty module name'),
            ('invalid', 'is not a valid dotted path'),
            ('invalid.', 'No module named'),
            ('invalid.path', 'No module named'),
            ('invalid..path', 'No module named'),
            ('messenger.tests.invalid.TestCase', 'No module named'),
            ('messenger.tests.test_utils.', 'does not have a class or attribute'),
            ('messenger.tests.test_utils.InvalidTestCase', 'does not have a class or attribute'),
        )
        for path, error in data:
            with self.subTest(path=path):
                with self.assertRaisesRegex(ImportError, error):
                    import_path(path)


class TestClass:
    """Test class for load_instance."""
    def __init__(self, *, keyword=None, another=None):
        self.keyword = keyword
        self.another = another


class LoadInstanceTest(TestCase):
    def test_class_only(self):
        obj = load_instance('messenger.tests.test_utils.TestClass')

        self.assertIsInstance(obj, TestClass)
        self.assertIsNone(obj.keyword)
        self.assertIsNone(obj.another)

    def test_class_kwargs(self):
        obj = load_instance('messenger.tests.test_utils.TestClass', keyword=sentinel.value, another=sentinel.another)

        self.assertIsInstance(obj, TestClass)
        self.assertEqual(obj.keyword, sentinel.value)
        self.assertEqual(obj.another, sentinel.another)

    def test_invalid_path(self):
        data = (
            # path, error_message
            ('', 'is not a valid dotted path'),
            ('messenger.tests.test_utils.InvalidTestCase', 'does not have a class or attribute'),
        )
        for path, error in data:
            with self.subTest(path=path):
                with self.assertRaisesRegex(ImportError, error):
                    load_instance(path)

    def test_invalid_kwargs(self):
        data = (
                {'invalid': sentinel.invalid},
                {'keyword': sentinel.keyword, 'another': sentinel.another, 'invalid': sentinel.invalid},
        )
        for kwargs in data:
            with self.subTest(kwargs=kwargs):
                with self.assertRaises(TypeError):
                    load_instance('messenger.tests.test_utils.TestClass', **kwargs)


class ResultTest(TestCase):
    def test_total(self):
        data = (
            # result, total
            (Result(), 0),
            (Result(any=1), 1),
            (Result(any=1, other=2), 3),
        )
        for result, total in data:
            with self.subTest(result=result):
                self.assertEqual(result.total(), total)

    def test_mark_success(self):
        data = (
            # input, output
            (Result(), Result(success=1)),
            (Result(any=42), Result(success=1, any=42)),
        )
        for input, output in data:
            with self.subTest(input=input):
                input.mark_success()
                self.assertEqual(input, output)

    def test_mark_fail(self):
        data = (
            # input, output
            (Result(), Result(fail=1)),
            (Result(any=42), Result(fail=1, any=42)),
        )
        for input, output in data:
            with self.subTest(input=input):
                input.mark_fail()
                self.assertEqual(input, output)

    def test_mark_error(self):
        data = (
            # input, output
            (Result(), Result(error=1)),
            (Result(any=42), Result(error=1, any=42)),
        )
        for input, output in data:
            with self.subTest(input=input):
                input.mark_error()
                self.assertEqual(input, output)

    def test_mark_skip(self):
        data = (
            # input, output
            (Result(), Result(skip=1)),
            (Result(any=42), Result(skip=1, any=42)),
        )
        for input, output in data:
            with self.subTest(input=input):
                input.mark_skip()
                self.assertEqual(input, output)

    def test_successes(self):
        data = (
            # result, count
            (Result(), 0),
            (Result(success=1), 1),
            (Result(success=42), 42),
            (Result(other=2), 0),
        )
        for result, count in data:
            with self.subTest(result=result):
                self.assertEqual(result.successes, count)

    def test_fails(self):
        data = (
            # result, count
            (Result(), 0),
            (Result(fail=1), 1),
            (Result(fail=42), 42),
            (Result(other=2), 0),
        )
        for result, count in data:
            with self.subTest(result=result):
                self.assertEqual(result.fails, count)

    def test_errors(self):
        data = (
            # result, count
            (Result(), 0),
            (Result(error=1), 1),
            (Result(error=42), 42),
            (Result(other=2), 0),
        )
        for result, count in data:
            with self.subTest(result=result):
                self.assertEqual(result.errors, count)

    def test_skips(self):
        data = (
            # result, count
            (Result(), 0),
            (Result(skip=1), 1),
            (Result(skip=42), 42),
            (Result(other=2), 0),
        )
        for result, count in data:
            with self.subTest(result=result):
                self.assertEqual(result.skips, count)
