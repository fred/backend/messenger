from typing import Iterable, Tuple, Type
from unittest import TestCase
from unittest.mock import sentinel

from messenger.filters import AddressCountryFilter, NegativeFilter, TypeFilter
from messenger.models import Email, Letter, Record, Sms

from .utils import StaticFilter


class NegativeFilterTest(TestCase):
    def test_init_filter(self):
        inner = StaticFilter(True)
        filter = NegativeFilter(inner)
        self.assertIs(filter.inner_filter, inner)

    def test_init_str(self):
        filter = NegativeFilter('messenger.tests.utils.StaticFilter')
        self.assertIsInstance(filter.inner_filter, StaticFilter)

    def test_init_args(self):
        filter = NegativeFilter('messenger.tests.utils.StaticFilter', result=sentinel.result)
        self.assertIsInstance(filter.inner_filter, StaticFilter)
        self.assertEqual(filter.inner_filter.result, sentinel.result)

    def test_filter(self):
        data: Iterable[Tuple[NegativeFilter, Record, bool]] = (
            # filter, model, result
            # False to True
            (NegativeFilter(StaticFilter(True)), Email(), False),
            (NegativeFilter(StaticFilter(True)), Sms(), False),
            (NegativeFilter(StaticFilter(True)), Letter(), False),
            # True to False
            (NegativeFilter(StaticFilter(False)), Email(), True),
            (NegativeFilter(StaticFilter(False)), Sms(), True),
            (NegativeFilter(StaticFilter(False)), Letter(), True),
        )
        for filter, model, result in data:
            with self.subTest(filter=filter, model=model):
                self.assertEqual(filter.filter(model), result)


class TypeFilterTest(TestCase):
    def _test(self, model_cls: Type[Record]) -> None:
        data: Iterable[Tuple[TypeFilter, Record, bool]] = (
            # filter, model, result
            # Catch nothing
            (TypeFilter([]), model_cls(), False),
            (TypeFilter([]), model_cls(type=""), False),
            (TypeFilter([]), model_cls(type="red-dwarf"), False),
            # Match type
            (TypeFilter(["red-dwarf"]), model_cls(type="red-dwarf"), True),
            (TypeFilter(["red-dwarf"]), model_cls(type=""), False),
            (TypeFilter(["red-dwarf"]), model_cls(), False),
            (TypeFilter([""]), model_cls(type="red-dwarf"), False),
            (TypeFilter([""]), model_cls(type=""), True),
            (TypeFilter([""]), model_cls(), False),
            # Match one of types
            (TypeFilter(["red-dwarf", "star-bug"]), model_cls(type="red-dwarf"), True),
            (TypeFilter(["red-dwarf", "star-bug"]), model_cls(type="star-bug"), True),
            (TypeFilter(["red-dwarf", "star-bug"]), model_cls(type="mismatch"), False),
            (TypeFilter(["red-dwarf", "star-bug"]), model_cls(type=""), False),
            (TypeFilter(["red-dwarf", "star-bug"]), model_cls(), False),
            # Match None as a type
            (TypeFilter([None]), model_cls(), True),
            (TypeFilter([None]), model_cls(type=""), False),
            (TypeFilter([None]), model_cls(type="red-dwarf"), False),
            (TypeFilter([None, "star-bug"]), model_cls(), True),
            (TypeFilter([None, "star-bug"]), model_cls(type="red-dwarf"), False),
            # Match glob patterns
            (TypeFilter(["red-*"]), model_cls(type="red-dwarf"), True),
            (TypeFilter(["*"]), model_cls(type="red-dwarf"), True),
            (TypeFilter(["*-dwarf"]), model_cls(type="red-dwarf"), True),
            (TypeFilter(["red-*"]), model_cls(type="star-bug"), False),
            (TypeFilter(["star-*"]), model_cls(type="red-dwarf"), False),
            (TypeFilter(["red-*"]), model_cls(), False),
            (TypeFilter(["*"]), model_cls(), False),
            (TypeFilter(["*-dwarf"]), model_cls(), False),
        )
        for filter, model, result in data:
            with self.subTest(filter=filter, model=model):
                self.assertEqual(filter.filter(model), result)

    def test_email(self):
        self._test(Email)

    def test_letter(self):
        self._test(Letter)

    def test_sms(self):
        self._test(Sms)


class AddressCountryFilterTest(TestCase):
    def test_filter(self):
        data = (
            # filter, model, result
            # Catch nothing
            (AddressCountryFilter([]), Letter(), False),
            (AddressCountryFilter([]), Letter(recipient_country=sentinel.country), False),
            # Match country
            (AddressCountryFilter([sentinel.country]), Letter(recipient_country=sentinel.country), True),
            (AddressCountryFilter([sentinel.country]), Letter(), False),
            # Match one of countries
            (AddressCountryFilter([sentinel.country1, sentinel.country2]),
             Letter(recipient_country=sentinel.country1), True),
            (AddressCountryFilter([sentinel.country1, sentinel.country2]),
             Letter(recipient_country=sentinel.country2), True),
            (AddressCountryFilter([sentinel.country1, sentinel.country2]), Letter(), False),
            # Match None as country
            (AddressCountryFilter([None]), Letter(), True),
        )
        for filter, model, result in data:
            with self.subTest(filter=filter, model=model):
                self.assertEqual(filter.filter(model), result)
