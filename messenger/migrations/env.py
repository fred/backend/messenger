from alembic import context
from sqlalchemy import engine_from_config, pool

from messenger.models import METADATA
from messenger.settings import APPLICATION

# This script is run by alembic command. We need to load settings here.
APPLICATION.load()

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config
# Use databases defined in this order:
# 1. sqlalchemy.url from alembic configuration
# 2. db_connection from messenger configuration (recommended)
config.set_main_option("sqlalchemy.url", APPLICATION.settings.db_connection)

# add your model's MetaData object here
# for 'autogenerate' support
target_metadata = METADATA


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            # Use batch mode for SQLite ALTERs.
            # See https://alembic.sqlalchemy.org/en/latest/batch.html#batch-mode-with-autogenerate
            render_as_batch=True,
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
