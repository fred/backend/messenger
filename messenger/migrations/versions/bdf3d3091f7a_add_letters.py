"""Add letters

Revision ID: bdf3d3091f7a
Revises: e6724d51dde7
Create Date: 2020-10-08 14:24:30.787662+00:00

"""
import sqlalchemy as sa
import sqlalchemy_utc
import sqlalchemy_utils
from alembic import op
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.sql import expression, func

# revision identifiers, used by Alembic.
revision = "bdf3d3091f7a"
down_revision = "e6724d51dde7"
branch_labels = None
depends_on = None


def upgrade():
    archive_enum_values = ("SENT", "FAILED", "DELIVERED", "UNDELIVERED")
    archive_enum_name = "archivemessagestatus"
    archive_status_enum = sa.Enum(*archive_enum_values, name=archive_enum_name, create_constraint=True).with_variant(
        ENUM(*archive_enum_values, name=archive_enum_name, create_type=False), "postgresql"
    )

    status_enum_values = ("PENDING", "SENT", "FAILED", "ERROR", "DELIVERED", "UNDELIVERED")
    status_enum_name = "messagestatus"
    status_enum = sa.Enum(*status_enum_values, name=status_enum_name, create_constraint=True).with_variant(
        ENUM(*status_enum_values, name=status_enum_name, create_type=False), "postgresql"
    )

    reference_enum_values = ("CONTACT", "DOMAIN", "NSSET", "KEYSET", "REGISTRAR")
    reference_enum_name = "referencetype"
    reference_type_enum = sa.Enum(*reference_enum_values, name=reference_enum_name, create_constraint=True)
    reference_type_enum = reference_type_enum.with_variant(
        ENUM(*reference_enum_values, name=reference_enum_name, create_type=False), "postgresql"
    )

    op.create_table(
        "messenger_archiveletter",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("update_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("send_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("attempts", sa.SmallInteger(), server_default=sa.text("0"), nullable=False),
        sa.Column("delivery_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("delivery_info", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.Column("uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column(
            "create_datetime",
            sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True),
            server_default=func.now(),
            nullable=False,
        ),
        sa.Column("status", archive_status_enum, nullable=False),
        sa.Column("recipient_name", sa.Text(), nullable=True),
        sa.Column("recipient_organization", sa.Text(), nullable=True),
        sa.Column("recipient_street", sa.Text(), nullable=True),
        sa.Column("recipient_city", sa.Text(), nullable=True),
        sa.Column("recipient_state", sa.Text(), nullable=True),
        sa.Column("recipient_postal_code", sa.Text(), nullable=True),
        sa.Column("recipient_country", sa.Text(), nullable=True),
        sa.Column("file", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.PrimaryKeyConstraint("id", "create_datetime", name=op.f("messenger_archiveletter_pkey")),
        postgresql_partition_by="RANGE (create_datetime)",
    )
    with op.batch_alter_table("messenger_archiveletter", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("messenger_archiveletter_status_key"), ["status"], unique=False)
        batch_op.create_index(batch_op.f("messenger_archiveletter_uuid_key"), ["uuid"], unique=False)

    op.create_table(
        "messenger_letter",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("update_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("send_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("attempts", sa.SmallInteger(), server_default=sa.text("0"), nullable=False),
        sa.Column("delivery_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("delivery_info", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.Column("uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column(
            "create_datetime",
            sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True),
            server_default=func.now(),
            nullable=False,
        ),
        sa.Column("status", status_enum, nullable=False),
        sa.Column(
            "archive",
            sa.Boolean(name="archive", create_constraint=True),
            server_default=expression.true(),
            nullable=False,
        ),
        sa.Column("recipient_name", sa.Text(), nullable=True),
        sa.Column("recipient_organization", sa.Text(), nullable=True),
        sa.Column("recipient_street", sa.Text(), nullable=True),
        sa.Column("recipient_city", sa.Text(), nullable=True),
        sa.Column("recipient_state", sa.Text(), nullable=True),
        sa.Column("recipient_postal_code", sa.Text(), nullable=True),
        sa.Column("recipient_country", sa.Text(), nullable=True),
        sa.Column("file", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("messenger_letter_pkey")),
        sa.UniqueConstraint("uuid", name=op.f("messenger_letter_uuid_key")),
        sqlite_autoincrement=True,
    )
    with op.batch_alter_table("messenger_letter", schema=None) as batch_op:
        batch_op.create_index(batch_op.f("messenger_letter_status_key"), ["status"], unique=False)

    op.create_table(
        "messenger_archiveletterreference",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("type", reference_type_enum, nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.Column("create_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=False),
        sa.Column("message_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["message_id", "create_datetime"],
            ["messenger_archiveletter.id", "messenger_archiveletter.create_datetime"],
            name=op.f("messenger_archiveletterreference_message_id_fk"),
        ),
        sa.PrimaryKeyConstraint("id", "create_datetime", name=op.f("messenger_archiveletterreference_pkey")),
        sa.UniqueConstraint(
            "value", "type", "message_id", "create_datetime", name=op.f("messenger_archiveletterreference_value_key")
        ),
        postgresql_partition_by="RANGE (create_datetime)",
    )
    op.create_table(
        "messenger_letterreference",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("type", reference_type_enum, nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.Column("message_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["message_id"], ["messenger_letter.id"], name=op.f("messenger_letterreference_message_id_fk")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("messenger_letterreference_pkey")),
        sa.UniqueConstraint("value", "type", "message_id", name=op.f("messenger_letterreference_value_key")),
    )


def downgrade():
    op.drop_table("messenger_letterreference")
    op.drop_table("messenger_archiveletterreference")
    with op.batch_alter_table("messenger_letter", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("messenger_letter_status_key"))

    op.drop_table("messenger_letter")
    with op.batch_alter_table("messenger_archiveletter", schema=None) as batch_op:
        batch_op.drop_index(batch_op.f("messenger_archiveletter_uuid_key"))
        batch_op.drop_index(batch_op.f("messenger_archiveletter_status_key"))

    op.drop_table("messenger_archiveletter")
