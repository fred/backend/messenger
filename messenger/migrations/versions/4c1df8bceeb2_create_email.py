"""Create Email

Revision ID: 4c1df8bceeb2
Revises:
Create Date: 2020-02-04 13:37:21.855670+00:00

"""
import sqlalchemy as sa
import sqlalchemy_utc
import sqlalchemy_utils
from alembic import op
from sqlalchemy.sql import expression, func

# revision identifiers, used by Alembic.
revision = "4c1df8bceeb2"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "messenger_email",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column("message_id", sa.Text(), nullable=False),
        sa.Column(
            "create_datetime",
            sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True),
            server_default=func.now(),
            nullable=False,
        ),
        sa.Column("send_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("delivery_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("attempts", sa.SmallInteger(), server_default=sa.text("0"), nullable=False),
        sa.Column(
            "status",
            sa.Enum("PENDING", "SENT", "FAILED", "ERROR", "DELIVERED", "UNDELIVERED", name="messagestatus",
                    create_constraint=True),
            nullable=False,
        ),
        sa.Column("sender", sa.Text(), nullable=True),
        sa.Column("recipient", sa.Text(), nullable=False),
        sa.Column("extra_headers", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.Column("subject_template", sa.Text(), nullable=False),
        sa.Column("subject_template_uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=True),
        sa.Column("body_template", sa.Text(), nullable=False),
        sa.Column("body_template_uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=True),
        sa.Column("body_template_html", sa.Text(), nullable=True),
        sa.Column(
            "body_template_html_uuid",
            sqlalchemy_utils.types.uuid.UUIDType(binary=False),
            sa.CheckConstraint(
                "body_template_html_uuid IS NULL OR body_template_html IS NOT NULL",
                name=op.f("messenger_email_body_template_html_uuid"),
            ),
            nullable=True,
        ),
        sa.Column("context", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.Column(
            "archive",
            sa.Boolean(name="archive", create_constraint=True),
            server_default=expression.true(),
            nullable=False,
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("messenger_email_pkey")),
        sa.UniqueConstraint("message_id", name=op.f("messenger_email_message_id_key")),
        sa.UniqueConstraint("uuid", name=op.f("messenger_email_uuid_key")),
        sqlite_autoincrement=True,
    )


def downgrade():
    op.drop_table("messenger_email")
