"""Unique reference values

Revision ID: e6724d51dde7
Revises: 7671a2901415
Create Date: 2020-10-05 11:40:25.052539+00:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "e6724d51dde7"
down_revision = "7671a2901415"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("messenger_archiveemailreference", schema=None) as batch_op:
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archiveemailreference_value_key"),
            ["value", "type", "message_id", "create_datetime"],
        )

    with op.batch_alter_table("messenger_archivesmsreference", schema=None) as batch_op:
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archivesmsreference_value_key"), ["value", "type", "message_id", "create_datetime"]
        )

    with op.batch_alter_table("messenger_emailreference", schema=None) as batch_op:
        batch_op.create_unique_constraint(
            batch_op.f("messenger_emailreference_value_key"), ["value", "type", "message_id"]
        )

    with op.batch_alter_table("messenger_smsreference", schema=None) as batch_op:
        batch_op.create_unique_constraint(
            batch_op.f("messenger_smsreference_value_key"), ["value", "type", "message_id"]
        )


def downgrade():
    with op.batch_alter_table("messenger_smsreference", schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f("messenger_smsreference_value_key"), type_="unique")

    with op.batch_alter_table("messenger_emailreference", schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f("messenger_emailreference_value_key"), type_="unique")

    with op.batch_alter_table("messenger_archivesmsreference", schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f("messenger_archivesmsreference_value_key"), type_="unique")

    with op.batch_alter_table("messenger_archiveemailreference", schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f("messenger_archiveemailreference_value_key"), type_="unique")
