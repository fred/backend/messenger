"""Fix letters

Revision ID: 2154d7884407
Revises: 9d86b23ae9fd
Create Date: 2020-10-19 11:57:16.617979+00:00

"""
import sqlalchemy as sa
import sqlalchemy_utils
from alembic import op

# revision identifiers, used by Alembic.
revision = "2154d7884407"
down_revision = "9d86b23ae9fd"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("messenger_archiveletter", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))

    with op.batch_alter_table("messenger_letter", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))
        batch_op.alter_column("file", existing_type=sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False)


def downgrade():
    with op.batch_alter_table("messenger_letter", schema=None) as batch_op:
        batch_op.alter_column("file", existing_type=sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=True)
        batch_op.drop_column("type")

    with op.batch_alter_table("messenger_archiveletter", schema=None) as batch_op:
        batch_op.drop_column("type")
