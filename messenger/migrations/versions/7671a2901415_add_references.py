"""Add references

Revision ID: 7671a2901415
Revises: 5985f341b0da
Create Date: 2020-09-16 15:11:25.878772+00:00

"""
import sqlalchemy as sa
import sqlalchemy_utc
from alembic import op
from sqlalchemy.dialects.postgresql import ENUM

# revision identifiers, used by Alembic.
revision = "7671a2901415"
down_revision = "5985f341b0da"
branch_labels = None
depends_on = None


def upgrade():
    reference_enum_values = ("CONTACT", "DOMAIN", "NSSET", "KEYSET", "REGISTRAR")
    reference_enum_name = "referencetype"
    reference_type_enum = sa.Enum(*reference_enum_values, name=reference_enum_name, create_constraint=True)
    reference_type_enum = reference_type_enum.with_variant(
        ENUM(*reference_enum_values, name=reference_enum_name), "postgresql"
    )

    op.create_table(
        "messenger_archiveemailreference",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("type", reference_type_enum, nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.Column(
            "create_datetime",
            sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True),
            nullable=False,
        ),
        sa.Column("message_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["message_id", "create_datetime"],
            ["messenger_archiveemail.id", "messenger_archiveemail.create_datetime"],
            name=op.f("messenger_archiveemailreference_message_id_fk"),
        ),
        sa.PrimaryKeyConstraint("id", "create_datetime", name=op.f("messenger_archiveemailreference_pkey")),
        postgresql_partition_by="RANGE (create_datetime)",
    )
    op.create_table(
        "messenger_archivesmsreference",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("type", reference_type_enum, nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.Column(
            "create_datetime",
            sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True),
            nullable=False,
        ),
        sa.Column("message_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["message_id", "create_datetime"],
            ["messenger_archivesms.id", "messenger_archivesms.create_datetime"],
            name=op.f("messenger_archivesmsreference_message_id_fk"),
        ),
        sa.PrimaryKeyConstraint("id", "create_datetime", name=op.f("messenger_archivesmsreference_pkey")),
        postgresql_partition_by="RANGE (create_datetime)",
    )
    op.create_table(
        "messenger_emailreference",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("type", reference_type_enum, nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.Column("message_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["message_id"], ["messenger_email.id"], name=op.f("messenger_emailreference_message_id_fk")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("messenger_emailreference_pkey")),
    )
    op.create_table(
        "messenger_smsreference",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("type", reference_type_enum, nullable=False),
        sa.Column("value", sa.Text(), nullable=False),
        sa.Column("message_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["message_id"], ["messenger_sms.id"], name=op.f("messenger_smsreference_message_id_fk")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("messenger_smsreference_pkey")),
    )


def downgrade():
    op.drop_table("messenger_smsreference")
    op.drop_table("messenger_emailreference")
    op.drop_table("messenger_archivesmsreference")
    op.drop_table("messenger_archiveemailreference")
