"""Add letter send data

Revision ID: 64ff4cbdc5cd
Revises: 578bee8f5313
Create Date: 2020-11-25 12:26:26.836899+00:00

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "64ff4cbdc5cd"
down_revision = "578bee8f5313"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("messenger_archiveletter", schema=None) as batch_op:
        batch_op.add_column(sa.Column("batch_id", sa.Text(), nullable=True))
        batch_op.add_column(sa.Column("service", sa.Text(), nullable=True))

    with op.batch_alter_table("messenger_letter", schema=None) as batch_op:
        batch_op.add_column(sa.Column("batch_id", sa.Text(), nullable=True))
        batch_op.add_column(sa.Column("service", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("messenger_letter", schema=None) as batch_op:
        batch_op.drop_column("service")
        batch_op.drop_column("batch_id")

    with op.batch_alter_table("messenger_archiveletter", schema=None) as batch_op:
        batch_op.drop_column("service")
        batch_op.drop_column("batch_id")
