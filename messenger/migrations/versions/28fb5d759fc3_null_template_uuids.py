"""Allow NULL template UUIDs in archive

Revision ID: 28fb5d759fc3
Revises: 578bee8f5313
Create Date: 2020-11-25 13:11:34.289924+00:00

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "28fb5d759fc3"
down_revision = "578bee8f5313"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.alter_column("body_template_uuid", existing_type=sa.CHAR(length=32), nullable=True)
        batch_op.alter_column("subject_template_uuid", existing_type=sa.CHAR(length=32), nullable=True)

    with op.batch_alter_table("messenger_archivesms", schema=None) as batch_op:
        batch_op.alter_column("body_template_uuid", existing_type=sa.CHAR(length=32), nullable=True)


def downgrade():
    with op.batch_alter_table("messenger_archivesms", schema=None) as batch_op:
        batch_op.alter_column("body_template_uuid", existing_type=sa.CHAR(length=32), nullable=False)

    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.alter_column("subject_template_uuid", existing_type=sa.CHAR(length=32), nullable=False)
        batch_op.alter_column("body_template_uuid", existing_type=sa.CHAR(length=32), nullable=False)
