"""Use arrays in postgres

Revision ID: d05b9ccccb10
Revises: 3e9857205ccc
Create Date: 2020-12-22 14:29:54.314029+00:00

"""
from uuid import UUID

import sqlalchemy as sa
from alembic import op
from sqlalchemy_utils import UUIDType

from messenger.models.utils import ScalarListType

# revision identifiers, used by Alembic.
revision = "d05b9ccccb10"
down_revision = "3e9857205ccc"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.alter_column(
            "attachments",
            type_=sa.ARRAY(UUIDType).with_variant(ScalarListType(UUID), "sqlite"),
            postgresql_using="regexp_split_to_array(attachments, ',')::UUID[]",
        )
        batch_op.alter_column(
            "recipient",
            type_=sa.ARRAY(sa.Text).with_variant(ScalarListType(), "sqlite"),
            postgresql_using="regexp_split_to_array(recipient, ', +')",
        )

    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.alter_column(
            "attachments",
            type_=sa.ARRAY(UUIDType).with_variant(ScalarListType(UUID), "sqlite"),
            postgresql_using="regexp_split_to_array(attachments, ',')::UUID[]",
        )
        batch_op.alter_column(
            "recipient",
            type_=sa.ARRAY(sa.Text).with_variant(ScalarListType(), "sqlite"),
            postgresql_using="regexp_split_to_array(recipient, ', +')",
        )


def downgrade():
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.alter_column("attachments", type_=ScalarListType(UUID))
        batch_op.alter_column("recipient", type_=sa.Text())

    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.alter_column("attachments", type_=ScalarListType(UUID))
        batch_op.alter_column("recipient", type_=sa.Text())
