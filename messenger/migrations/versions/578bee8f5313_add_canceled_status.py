"""Add canceled status

Revision ID: 578bee8f5313
Revises: 2154d7884407
Create Date: 2020-11-02 14:56:04.309463+00:00

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "578bee8f5313"
down_revision = "2154d7884407"
branch_labels = None
depends_on = None


def upgrade():
    connection = op.get_bind()
    if connection.dialect.name == "postgresql":
        connection.execute(sa.text("ALTER TYPE messagestatus ADD VALUE 'CANCELED'"))
        connection.execute(sa.text("ALTER TYPE archivemessagestatus ADD VALUE 'CANCELED'"))
    else:
        old_status = sa.Enum("PENDING", "SENT", "FAILED", "ERROR", "DELIVERED", "UNDELIVERED", name="messagestatus")
        status = sa.Enum("PENDING", "SENT", "FAILED", "ERROR", "DELIVERED", "UNDELIVERED", "CANCELED",
                         name="messagestatus", create_constraint=True)
        old_archive_status = sa.Enum("SENT", "FAILED", "DELIVERED", "UNDELIVERED", name="archivemessagestatus")
        archive_status = sa.Enum("SENT", "FAILED", "DELIVERED", "UNDELIVERED", "CANCELED", name="archivemessagestatus",
                                 create_constraint=True)
        with op.batch_alter_table("messenger_email", schema=None) as batch_op:
            batch_op.alter_column("status", type_=status, existing_type=old_status)
        with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
            batch_op.alter_column("status", type_=archive_status, existing_type=old_archive_status)
        with op.batch_alter_table("messenger_letter", schema=None) as batch_op:
            batch_op.alter_column("status", type_=status, existing_type=old_status)
        with op.batch_alter_table("messenger_archiveletter", schema=None) as batch_op:
            batch_op.alter_column("status", type_=archive_status, existing_type=old_archive_status)
        with op.batch_alter_table("messenger_sms", schema=None) as batch_op:
            batch_op.alter_column("status", type_=status, existing_type=old_status)
        with op.batch_alter_table("messenger_archivesms", schema=None) as batch_op:
            batch_op.alter_column("status", type_=archive_status, existing_type=old_archive_status)


def downgrade():
    connection = op.get_bind()
    if connection.dialect.name != "postgresql":
        old_status = sa.Enum("PENDING", "SENT", "FAILED", "ERROR", "DELIVERED", "UNDELIVERED", "CANCELED",
                             name="messagestatus")
        status = sa.Enum("PENDING", "SENT", "FAILED", "ERROR", "DELIVERED", "UNDELIVERED", create_constraint=True)
        old_archive_status = sa.Enum("SENT", "FAILED", "DELIVERED", "UNDELIVERED", "CANCELED",
                                     name="archivemessagestatus")
        archive_status = sa.Enum("SENT", "FAILED", "DELIVERED", "UNDELIVERED", create_constraint=True)
        with op.batch_alter_table("messenger_email", schema=None) as batch_op:
            batch_op.alter_column("status", type_=status, existing_type=old_status)
        with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
            batch_op.alter_column("status", type_=archive_status, existing_type=old_archive_status)
        with op.batch_alter_table("messenger_letter", schema=None) as batch_op:
            batch_op.alter_column("status", type_=status, existing_type=old_status)
        with op.batch_alter_table("messenger_archiveletter", schema=None) as batch_op:
            batch_op.alter_column("status", type_=archive_status, existing_type=old_archive_status)
        with op.batch_alter_table("messenger_sms", schema=None) as batch_op:
            batch_op.alter_column("status", type_=status, existing_type=old_status)
        with op.batch_alter_table("messenger_archivesms", schema=None) as batch_op:
            batch_op.alter_column("status", type_=archive_status, existing_type=old_archive_status)
