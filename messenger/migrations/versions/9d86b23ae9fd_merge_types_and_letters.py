"""Merge types and letters

Revision ID: 9d86b23ae9fd
Revises: 71b8ed984bf5, bdf3d3091f7a
Create Date: 2020-10-19 11:51:44.499474+00:00

"""
# revision identifiers, used by Alembic.
revision = "9d86b23ae9fd"
down_revision = ("71b8ed984bf5", "bdf3d3091f7a")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
