"""Merge archive and letter changes

Revision ID: 3e9857205ccc
Revises: 64ff4cbdc5cd, 28fb5d759fc3
Create Date: 2020-11-30 13:38:53.136729+00:00

"""
# revision identifiers, used by Alembic.
revision = "3e9857205ccc"
down_revision = ("64ff4cbdc5cd", "28fb5d759fc3")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
