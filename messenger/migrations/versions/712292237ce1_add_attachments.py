"""Add attachments

Revision ID: 712292237ce1
Revises: 30b01f650bd6
Create Date: 2020-04-30 08:44:54.848428+00:00

"""
import sqlalchemy as sa
from alembic import op

from messenger.models.utils import ScalarListType

# revision identifiers, used by Alembic.
revision = "712292237ce1"
down_revision = "30b01f650bd6"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "messenger_archiveemail",
        sa.Column("attachments", ScalarListType(), nullable=True),
    )
    op.add_column(
        "messenger_email",
        sa.Column("attachments", ScalarListType(), nullable=True),
    )


def downgrade():
    op.drop_column("messenger_email", "attachments")
    op.drop_column("messenger_archiveemail", "attachments")
