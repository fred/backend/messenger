"""Allow NULL Message-ID in archive

Revision ID: 9568aac90e7f
Revises: 3814e4798c7a
Create Date: 2021-09-06 09:47:50.530404+00:00

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "9568aac90e7f"
down_revision = "3814e4798c7a"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.alter_column("message_id", existing_type=sa.Text(), nullable=True)


def downgrade():
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.alter_column("message_id", existing_type=sa.Text(), nullable=False)
