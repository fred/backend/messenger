"""Create ArchiveEmail

Revision ID: 30b01f650bd6
Revises: 4c1df8bceeb2
Create Date: 2020-03-16 15:10:35.678635+00:00

"""
import sqlalchemy as sa
import sqlalchemy_utc
import sqlalchemy_utils
from alembic import op
from sqlalchemy.sql import func

# revision identifiers, used by Alembic.
revision = "30b01f650bd6"
down_revision = "4c1df8bceeb2"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "messenger_archiveemail",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column("message_id", sa.Text(), nullable=False),
        sa.Column(
            "create_datetime",
            sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True),
            server_default=func.now(),
            nullable=False,
        ),
        sa.Column("send_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("delivery_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("attempts", sa.SmallInteger(), server_default=sa.text("0"), nullable=False),
        sa.Column(
            "status",
            sa.Enum("SENT", "FAILED", "DELIVERED", "UNDELIVERED", name="archivemessagestatus", create_constraint=True),
            nullable=False,
        ),
        sa.Column("sender", sa.Text(), nullable=True),
        sa.Column("recipient", sa.Text(), nullable=False),
        sa.Column("extra_headers", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.Column("subject_template", sa.Text(), nullable=False),
        sa.Column("subject_template_uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column("body_template", sa.Text(), nullable=False),
        sa.Column("body_template_uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column("body_template_html", sa.Text(), nullable=True),
        sa.Column(
            "body_template_html_uuid",
            sqlalchemy_utils.types.uuid.UUIDType(binary=False),
            sa.CheckConstraint(
                "body_template_html_uuid IS NULL OR body_template_html IS NOT NULL",
                name=op.f("messenger_email_body_template_html_uuid"),
            ),
            nullable=True,
        ),
        sa.Column("context", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.PrimaryKeyConstraint("id", "create_datetime", name=op.f("messenger_archiveemail_pkey")),
        postgresql_partition_by="RANGE (create_datetime)",
    )


def downgrade():
    op.drop_table("messenger_archiveemail")
