"""Custom reference type

Revision ID: d7c84f4dbfb7
Revises: 9568aac90e7f
Create Date: 2022-05-17 15:35:20.706649+00:00

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import ENUM

# revision identifiers, used by Alembic.
revision = "d7c84f4dbfb7"
down_revision = "9568aac90e7f"
branch_labels = None
depends_on = None


def upgrade():
    reference_enum_values = ("CONTACT", "DOMAIN", "NSSET", "KEYSET", "REGISTRAR")
    reference_enum_name = "referencetype"
    reference_type_enum = sa.Enum(*reference_enum_values, name=reference_enum_name, create_constraint=True)
    reference_type_enum = reference_type_enum.with_variant(
        ENUM(*reference_enum_values, name=reference_enum_name), "postgresql"
    )

    # Rename the old column from type to old_type.
    # Also rename the related unique constraint.
    with op.batch_alter_table("messenger_archiveemailreference", schema=None) as batch_op:
        batch_op.alter_column("type", new_column_name="old_type", nullable=True, type_=reference_type_enum)
        batch_op.drop_constraint("messenger_archiveemailreference_value_key", type_="unique")

    with op.batch_alter_table("messenger_archiveletterreference", schema=None) as batch_op:
        batch_op.alter_column("type", new_column_name="old_type", nullable=True, type_=reference_type_enum)
        batch_op.drop_constraint("messenger_archiveletterreference_value_key", type_="unique")

    with op.batch_alter_table("messenger_archivesmsreference", schema=None) as batch_op:
        batch_op.alter_column("type", new_column_name="old_type", nullable=True, type_=reference_type_enum)
        batch_op.drop_constraint("messenger_archivesmsreference_value_key", type_="unique")

    with op.batch_alter_table("messenger_emailreference", schema=None) as batch_op:
        batch_op.alter_column("type", new_column_name="old_type", nullable=True, type_=reference_type_enum)
        batch_op.drop_constraint("messenger_emailreference_value_key", type_="unique")

    with op.batch_alter_table("messenger_letterreference", schema=None) as batch_op:
        batch_op.alter_column("type", new_column_name="old_type", nullable=True, type_=reference_type_enum)
        batch_op.drop_constraint("messenger_letterreference_value_key", type_="unique")

    with op.batch_alter_table("messenger_smsreference", schema=None) as batch_op:
        batch_op.alter_column("type", new_column_name="old_type", nullable=True, type_=reference_type_enum)
        batch_op.drop_constraint("messenger_smsreference_value_key", type_="unique")

    # Add the new column type.
    with op.batch_alter_table("messenger_archiveemailreference", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archiveemailreference_old_type_key"),
            ["old_type", "value", "message_id", "create_datetime"],
        )
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archiveemailreference_type_key"),
            ["type", "value", "message_id", "create_datetime"],
        )

    with op.batch_alter_table("messenger_archiveletterreference", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archiveletterreference_old_type_key"),
            ["old_type", "value", "message_id", "create_datetime"],
        )
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archiveletterreference_type_key"),
            ["type", "value", "message_id", "create_datetime"],
        )

    with op.batch_alter_table("messenger_archivesmsreference", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archivesmsreference_old_type_key"),
            ["old_type", "value", "message_id", "create_datetime"],
        )
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archivesmsreference_type_key"),
            ["type", "value", "message_id", "create_datetime"],
        )

    with op.batch_alter_table("messenger_emailreference", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))
        batch_op.create_unique_constraint(
            batch_op.f("messenger_emailreference_old_type_key"), ["old_type", "value", "message_id"]
        )
        batch_op.create_unique_constraint(
            batch_op.f("messenger_emailreference_type_key"),
            ["type", "value", "message_id"],
        )

    with op.batch_alter_table("messenger_letterreference", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))
        batch_op.create_unique_constraint(
            batch_op.f("messenger_letterreference_old_type_key"), ["old_type", "value", "message_id"]
        )
        batch_op.create_unique_constraint(
            batch_op.f("messenger_letterreference_type_key"),
            ["type", "value", "message_id"],
        )

    with op.batch_alter_table("messenger_smsreference", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))
        batch_op.create_unique_constraint(
            batch_op.f("messenger_smsreference_old_type_key"), ["old_type", "value", "message_id"]
        )
        batch_op.create_unique_constraint(
            batch_op.f("messenger_smsreference_type_key"),
            ["type", "value", "message_id"],
        )


def downgrade():
    reference_enum_values = ("CONTACT", "DOMAIN", "NSSET", "KEYSET", "REGISTRAR")
    reference_enum_name = "referencetype"
    reference_type_enum = sa.Enum(*reference_enum_values, name=reference_enum_name, create_constraint=True)
    reference_type_enum = reference_type_enum.with_variant(
        ENUM(*reference_enum_values, name=reference_enum_name), "postgresql"
    )

    # Drop the new column type.
    with op.batch_alter_table("messenger_archiveemailreference", schema=None) as batch_op:
        batch_op.drop_column("type")

    with op.batch_alter_table("messenger_archiveletterreference", schema=None) as batch_op:
        batch_op.drop_column("type")

    with op.batch_alter_table("messenger_archivesmsreference", schema=None) as batch_op:
        batch_op.drop_column("type")

    with op.batch_alter_table("messenger_emailreference", schema=None) as batch_op:
        batch_op.drop_column("type")

    with op.batch_alter_table("messenger_letterreference", schema=None) as batch_op:
        batch_op.drop_column("type")

    with op.batch_alter_table("messenger_smsreference", schema=None) as batch_op:
        batch_op.drop_column("type")

    # Rename the old column from old_type to type.
    # Also rename the related unique constraint.
    with op.batch_alter_table("messenger_archiveemailreference", schema=None) as batch_op:
        batch_op.alter_column("old_type", new_column_name="type", nullable=False, type_=reference_type_enum)
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archiveemailreference_value_key"),
            ["type", "value", "message_id", "create_datetime"],
        )
        batch_op.drop_constraint("messenger_archiveemailreference_old_type_key", type_="unique")

    with op.batch_alter_table("messenger_archiveletterreference", schema=None) as batch_op:
        batch_op.alter_column("old_type", new_column_name="type", nullable=False, type_=reference_type_enum)
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archiveletterreference_value_key"),
            ["type", "value", "message_id", "create_datetime"],
        )
        batch_op.drop_constraint("messenger_archiveletterreference_old_type_key", type_="unique")

    with op.batch_alter_table("messenger_archivesmsreference", schema=None) as batch_op:
        batch_op.alter_column("old_type", new_column_name="type", nullable=False, type_=reference_type_enum)
        batch_op.create_unique_constraint(
            batch_op.f("messenger_archivesmsreference_value_key"),
            ["type", "value", "message_id", "create_datetime"],
        )
        batch_op.drop_constraint("messenger_archivesmsreference_old_type_key", type_="unique")

    with op.batch_alter_table("messenger_emailreference", schema=None) as batch_op:
        batch_op.alter_column("old_type", new_column_name="type", nullable=False, type_=reference_type_enum)
        batch_op.create_unique_constraint(
            batch_op.f("messenger_emailreference_value_key"),
            ["type", "value", "message_id"],
        )
        batch_op.drop_constraint("messenger_emailreference_old_type_key", type_="unique")

    with op.batch_alter_table("messenger_letterreference", schema=None) as batch_op:
        batch_op.alter_column("old_type", new_column_name="type", nullable=False, type_=reference_type_enum)
        batch_op.create_unique_constraint(
            batch_op.f("messenger_letterreference_value_key"),
            ["type", "value", "message_id"],
        )
        batch_op.drop_constraint("messenger_letterreference_old_type_key", type_="unique")

    with op.batch_alter_table("messenger_smsreference", schema=None) as batch_op:
        batch_op.alter_column("old_type", new_column_name="type", nullable=False, type_=reference_type_enum)
        batch_op.create_unique_constraint(
            batch_op.f("messenger_smsreference_value_key"),
            ["type", "value", "message_id"],
        )
        batch_op.drop_constraint("messenger_smsreference_old_type_key", type_="unique")
