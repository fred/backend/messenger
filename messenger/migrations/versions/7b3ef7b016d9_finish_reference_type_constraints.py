"""Finish reference type constraints

Revision ID: 7b3ef7b016d9
Revises: d7c84f4dbfb7
Create Date: 2022-05-23 13:21:35.037740+00:00

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7b3ef7b016d9"
down_revision = "d7c84f4dbfb7"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("messenger_archiveemailreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=False)

    with op.batch_alter_table("messenger_archiveletterreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=False)

    with op.batch_alter_table("messenger_archivesmsreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=False)

    with op.batch_alter_table("messenger_emailreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=False)

    with op.batch_alter_table("messenger_letterreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=False)

    with op.batch_alter_table("messenger_smsreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=False)


def downgrade():
    with op.batch_alter_table("messenger_smsreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=True)

    with op.batch_alter_table("messenger_letterreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=True)

    with op.batch_alter_table("messenger_emailreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=True)

    with op.batch_alter_table("messenger_archivesmsreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=True)

    with op.batch_alter_table("messenger_archiveletterreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=True)

    with op.batch_alter_table("messenger_archiveemailreference", schema=None) as batch_op:
        batch_op.alter_column("type", existing_type=sa.TEXT(), nullable=True)
