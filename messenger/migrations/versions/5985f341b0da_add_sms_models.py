"""Add SMS models

Revision ID: 5985f341b0da
Revises: 1ce2d7ccb45f
Create Date: 2020-08-10 13:37:18.099036+00:00

"""
import sqlalchemy as sa
import sqlalchemy_utc
import sqlalchemy_utils
from alembic import op
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.sql import expression, func

# revision identifiers, used by Alembic.
revision = "5985f341b0da"
down_revision = "1ce2d7ccb45f"
branch_labels = None
depends_on = None


def upgrade():
    archive_enum_values = ("SENT", "FAILED", "DELIVERED", "UNDELIVERED")
    archive_enum_name = "archivemessagestatus"
    archive_status_enum = sa.Enum(*archive_enum_values, name=archive_enum_name, create_constraint=True).with_variant(
        ENUM(*archive_enum_values, name=archive_enum_name, create_type=False), "postgresql"
    )

    status_enum_values = ("PENDING", "SENT", "FAILED", "ERROR", "DELIVERED", "UNDELIVERED")
    status_enum_name = "messagestatus"
    status_enum = sa.Enum(*status_enum_values, name=status_enum_name, create_constraint=True).with_variant(
        ENUM(*status_enum_values, name=status_enum_name, create_type=False), "postgresql"
    )

    op.create_table(
        "messenger_archivesms",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("update_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("send_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("attempts", sa.SmallInteger(), server_default=sa.text("0"), nullable=False),
        sa.Column("body_template", sa.Text(), nullable=False),
        sa.Column("context", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.Column("delivery_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("delivery_info", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.Column("uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column(
            "create_datetime",
            sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True),
            server_default=func.now(),
            nullable=False,
        ),
        sa.Column("status", archive_status_enum, nullable=False),
        sa.Column("body_template_uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column("recipient", sa.Text(), nullable=False),
        sa.PrimaryKeyConstraint("id", "create_datetime", name=op.f("messenger_archivesms_pkey")),
        postgresql_partition_by="RANGE (create_datetime)",
    )
    op.create_index(op.f("messenger_archivesms_status_key"), "messenger_archivesms", ["status"], unique=False)
    op.create_index(op.f("messenger_archivesms_uuid_key"), "messenger_archivesms", ["uuid"], unique=False)
    op.create_table(
        "messenger_sms",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("update_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("send_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("attempts", sa.SmallInteger(), server_default=sa.text("0"), nullable=False),
        sa.Column("body_template", sa.Text(), nullable=False),
        sa.Column("context", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.Column("delivery_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
        sa.Column("delivery_info", sqlalchemy_utils.types.json.JSONType(), nullable=True),
        sa.Column("uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=False),
        sa.Column(
            "create_datetime",
            sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True),
            server_default=func.now(),
            nullable=False,
        ),
        sa.Column("status", status_enum, nullable=False),
        sa.Column("body_template_uuid", sqlalchemy_utils.types.uuid.UUIDType(binary=False), nullable=True),
        sa.Column(
            "archive",
            sa.Boolean(name="archive", create_constraint=True),
            server_default=expression.true(),
            nullable=False,
        ),
        sa.Column("recipient", sa.Text(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("messenger_sms_pkey")),
        sa.UniqueConstraint("uuid", name=op.f("messenger_sms_uuid_key")),
        sqlite_autoincrement=True,
    )
    op.create_index(op.f("messenger_sms_status_key"), "messenger_sms", ["status"], unique=False)


def downgrade():
    op.drop_index(op.f("messenger_sms_status_key"), table_name="messenger_sms")
    op.drop_table("messenger_sms")
    op.drop_index(op.f("messenger_archivesms_uuid_key"), table_name="messenger_archivesms")
    op.drop_index(op.f("messenger_archivesms_status_key"), table_name="messenger_archivesms")
    op.drop_table("messenger_archivesms")
