"""Add blocked addresses

Revision ID: 567ede1fcf01
Revises: d05b9ccccb10
Create Date: 2021-02-25 08:32:29.013487+00:00

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "567ede1fcf01"
down_revision = "d05b9ccccb10"
branch_labels = None
depends_on = None

ADDRESS_PART_CHECK_SQL = (
    "name IS NOT NULL OR organization IS NOT NULL OR street IS NOT NULL OR city IS NOT NULL OR state IS NOT NULL "
    "OR postal_code IS NOT NULL or country IS NOT NULL"
)


def upgrade():
    op.create_table(
        "messenger_blockedaddress",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("name", sa.Text(), nullable=True),
        sa.Column("organization", sa.Text(), nullable=True),
        sa.Column("street", sa.Text(), nullable=True),
        sa.Column("city", sa.Text(), nullable=True),
        sa.Column("state", sa.Text(), nullable=True),
        sa.Column("postal_code", sa.Text(), nullable=True),
        sa.Column("country", sa.Text(), nullable=True),
        sa.CheckConstraint(ADDRESS_PART_CHECK_SQL, name=op.f("messenger_blockedaddress_part_required")),
        sa.PrimaryKeyConstraint("id", name=op.f("messenger_blockedaddress_pkey")),
        sa.UniqueConstraint(
            "name",
            "organization",
            "street",
            "city",
            "state",
            "postal_code",
            "country",
            name=op.f("messenger_blockedaddress_name_key"),
        ),
    )
    op.create_table(
        "messenger_blockedemail",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("email", sa.Text(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("messenger_blockedemail_pkey")),
        sa.UniqueConstraint("email", name=op.f("messenger_blockedemail_email_key")),
    )
    op.create_table(
        "messenger_blockedphone",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("phone", sa.Text(), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("messenger_blockedphone_pkey")),
        sa.UniqueConstraint("phone", name=op.f("messenger_blockedphone_phone_key")),
    )


def downgrade():
    op.drop_table("messenger_blockedphone")
    op.drop_table("messenger_blockedemail")
    op.drop_table("messenger_blockedaddress")
