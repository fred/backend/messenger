"""Add delivery headers

Revision ID: 4c65a602cd94
Revises: 712292237ce1
Create Date: 2020-05-05 11:08:28.746213+00:00

"""
import sqlalchemy as sa
import sqlalchemy_utils
from alembic import op

# revision identifiers, used by Alembic.
revision = "4c65a602cd94"
down_revision = "712292237ce1"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "messenger_archiveemail", sa.Column("delivery_info", sqlalchemy_utils.types.json.JSONType(), nullable=True)
    )
    op.add_column(
        "messenger_email", sa.Column("delivery_info", sqlalchemy_utils.types.json.JSONType(), nullable=True)
    )


def downgrade():
    op.drop_column("messenger_email", "delivery_info")
    op.drop_column("messenger_archiveemail", "delivery_info")
