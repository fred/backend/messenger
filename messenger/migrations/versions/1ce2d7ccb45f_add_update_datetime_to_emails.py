"""Add update datetime

Revision ID: 1ce2d7ccb45f
Revises: 2b2bee2ad223
Create Date: 2020-07-01 10:25:11.293972+00:00

"""
import sqlalchemy as sa
import sqlalchemy_utc
from alembic import op

# revision identifiers, used by Alembic.
revision = "1ce2d7ccb45f"
down_revision = "2b2bee2ad223"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "messenger_archiveemail",
        sa.Column("update_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
    )
    op.add_column(
        "messenger_email",
        sa.Column("update_datetime", sqlalchemy_utc.sqltypes.UtcDateTime(timezone=True), nullable=True),
    )


def downgrade():
    op.drop_column("messenger_email", "update_datetime")
    op.drop_column("messenger_archiveemail", "update_datetime")
