"""Add message types

Revision ID: 71b8ed984bf5
Revises: e6724d51dde7
Create Date: 2020-10-08 11:49:16.464027+00:00

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "71b8ed984bf5"
down_revision = "e6724d51dde7"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))

    with op.batch_alter_table("messenger_archivesms", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))

    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))

    with op.batch_alter_table("messenger_sms", schema=None) as batch_op:
        batch_op.add_column(sa.Column("type", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("messenger_sms", schema=None) as batch_op:
        batch_op.drop_column("type")

    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.drop_column("type")

    with op.batch_alter_table("messenger_archivesms", schema=None) as batch_op:
        batch_op.drop_column("type")

    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.drop_column("type")
