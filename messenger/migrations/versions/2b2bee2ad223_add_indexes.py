"""Add indexes

Revision ID: 2b2bee2ad223
Revises: 4c65a602cd94
Create Date: 2020-05-27 13:53:13.281250+00:00

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "2b2bee2ad223"
down_revision = "4c65a602cd94"
branch_labels = None
depends_on = None


def upgrade():
    op.create_index(
        op.f("messenger_archiveemail_message_id_key"), "messenger_archiveemail", ["message_id"], unique=False
    )
    op.create_index(op.f("messenger_archiveemail_status_key"), "messenger_archiveemail", ["status"], unique=False)
    op.create_index(op.f("messenger_archiveemail_uuid_key"), "messenger_archiveemail", ["uuid"], unique=False)
    op.create_index(op.f("messenger_email_status_key"), "messenger_email", ["status"], unique=False)


def downgrade():
    op.drop_index(op.f("messenger_email_status_key"), table_name="messenger_email")
    op.drop_index(op.f("messenger_archiveemail_uuid_key"), table_name="messenger_archiveemail")
    op.drop_index(op.f("messenger_archiveemail_status_key"), table_name="messenger_archiveemail")
    op.drop_index(op.f("messenger_archiveemail_message_id_key"), table_name="messenger_archiveemail")
