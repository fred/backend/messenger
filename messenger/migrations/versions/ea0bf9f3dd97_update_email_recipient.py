"""Update email recipient

Revision ID: ea0bf9f3dd97
Revises: 7b3ef7b016d9
Create Date: 2024-04-10 12:46:20.572156+00:00

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "ea0bf9f3dd97"
down_revision = "7b3ef7b016d9"
branch_labels = None
depends_on = None


def upgrade():
    # Rename the old column and drop NOT NULL
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.alter_column("recipient", new_column_name="old_recipient", nullable=True)
    # Add new column
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.add_column(sa.Column("recipient", sa.Text(), nullable=True))
        batch_op.create_check_constraint("messenger_archiveemail_recipient",
                                         '(recipient IS NULL) != (old_recipient IS NULL)')

    # Rename the old column and drop NOT NULL
    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.alter_column("recipient", new_column_name="old_recipient", nullable=True)
    # Add new column
    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.add_column(sa.Column("recipient", sa.Text(), nullable=True))
        batch_op.create_check_constraint("messenger_email_recipient",
                                         '(recipient IS NULL) != (old_recipient IS NULL)')


def downgrade():
    # Drop new column
    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.drop_constraint("messenger_email_recipient")
        batch_op.drop_column("recipient")
    # Rename the old column and set NOT NULL
    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.alter_column("old_recipient", new_column_name="recipient", nullable=False)

    # Drop new column
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.drop_constraint("messenger_archiveemail_recipient")
        batch_op.drop_column("recipient")
    # Rename the old column and set NOT NULL
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.alter_column("old_recipient", new_column_name="recipient", nullable=False)
