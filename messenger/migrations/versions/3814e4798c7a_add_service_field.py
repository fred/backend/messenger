"""Add service field

Revision ID: 3814e4798c7a
Revises: d05b9ccccb10
Create Date: 2021-02-24 13:40:01.746726+00:00

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "3814e4798c7a"
down_revision = "567ede1fcf01"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.add_column(sa.Column("service", sa.Text(), nullable=True))

    with op.batch_alter_table("messenger_archivesms", schema=None) as batch_op:
        batch_op.add_column(sa.Column("service", sa.Text(), nullable=True))

    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.add_column(sa.Column("service", sa.Text(), nullable=True))

    with op.batch_alter_table("messenger_sms", schema=None) as batch_op:
        batch_op.add_column(sa.Column("service", sa.Text(), nullable=True))


def downgrade():
    with op.batch_alter_table("messenger_sms", schema=None) as batch_op:
        batch_op.drop_column("service")

    with op.batch_alter_table("messenger_email", schema=None) as batch_op:
        batch_op.drop_column("service")

    with op.batch_alter_table("messenger_archivesms", schema=None) as batch_op:
        batch_op.drop_column("service")

    with op.batch_alter_table("messenger_archiveemail", schema=None) as batch_op:
        batch_op.drop_column("service")
