"""Messenger exceptions."""


class MessengerError(Exception):
    """Base messenger exception."""


class MessageCanceled(MessengerError):
    """A message send is canceled."""


class MessageUndelivered(MessengerError):
    """A message was send, but it can't be delivered."""
