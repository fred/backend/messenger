"""Base backends for letters."""
from dataclasses import dataclass
from typing import Any, Dict, Optional
from uuid import UUID

from filed import File

from messenger.constants import TransportMethod
from messenger.models import Address, Letter

from .base import BaseBackend, FileBackendMixin, MemoryBackendMixin


@dataclass
class LetterMessage:
    """A letter message prepared for send.

    Attributes:
        uuid: The message UUID.
        recipient: The address of the message recipient.
        file: The file with the actual letter.
        sender: The sender address.
    """

    uuid: UUID
    recipient: Address
    file: File
    sender: Optional[Address] = None


class BaseLetterBackend(FileBackendMixin, BaseBackend):
    """Base backend for all letter backends.

    Attributes:
        sender: The sender address.
        user: The user for authentication at the provider.
        password: The password for authentication at the provider.
        test: Whether a test mode should be used.

    Behaviour of `user`, `password` and `test` arguments is implementation specific.
    """

    method = TransportMethod.LETTER

    def __init__(self, *, sender: Optional[Dict[str, str]] = None, user: Optional[str] = None,
                 password: Optional[str] = None, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        if sender is None:
            self.sender = None
        else:
            try:
                self.sender = Address(**sender)
            except TypeError as error:
                raise ValueError("Invalid sender: {}".format(error)) from error
        self.user = user
        self.password = password

    def get_message(self, model: Letter) -> LetterMessage:
        """Return formatted letter message from Letter object."""
        return LetterMessage(uuid=model.uuid, recipient=model.recipient, file=self.get_file(model.file),
                             sender=self.sender)


class MemoryLetterBackend(MemoryBackendMixin, BaseLetterBackend):
    """Backend which stores sent letters in memory.

    This backend is not intended for production use, but only for testing and development.
    """
