"""Base classes for message backends."""
import itertools
import logging
from abc import ABC, abstractmethod
from datetime import datetime
from typing import TYPE_CHECKING, Any, Dict, Generic, List, Optional, Sequence, Tuple, TypeVar, Union
from uuid import UUID

from filed import File
from pytz import UTC
from typist import HTTPTokenAuth, SecretaryClient
from typist.client import TemplateNotFound

from messenger.constants import MessageStatus, TransportMethod
from messenger.exceptions import MessageCanceled, MessageUndelivered
from messenger.filters import Filter
from messenger.settings import APPLICATION
from messenger.utils import load_instance

from ..models import Record

# Prevent circular imports
if TYPE_CHECKING:
    from messenger.processors import Processor

_LOGGER = logging.getLogger(__name__)

ModelType = TypeVar('ModelType', bound=Record)
MessageType = TypeVar('MessageType')


class BaseBackend(Generic[ModelType, MessageType], ABC):
    """Base sender backend.

    Class attributes:
        method: A transport method served by this backend.

    Attributes:
        name: An alias of a service.
        max_attempts: Maximal number of attempts to sent the message, before it's marked as failed.
    """

    method: TransportMethod

    def __init__(self, *, name: Optional[str] = None, max_attempts: int = 10,
                 filters: Optional[Sequence[Filter]] = None, processors: Optional[Sequence[Dict[str, Any]]] = None):
        self.name = name
        self.max_attempts = max_attempts
        self.filters = filters or ()
        self._processors = processors or ()
        self._processors_cache: Optional[Tuple[Processor, ...]] = None

    @property
    def processors(self) -> Tuple['Processor', ...]:
        """Return processors instances."""
        if self._processors_cache is None:
            result = []
            processors = itertools.chain(self._processors, APPLICATION.settings.processors.get(self.method, []))
            for processor_settings in processors:
                processor_settings = processor_settings.copy()
                cls_name = processor_settings.pop('class', None)
                if cls_name is None:
                    raise RuntimeError("Processor has no class defined.")
                try:
                    result.append(load_instance(cls_name, **processor_settings))
                except Exception as error:
                    raise RuntimeError("Error in loading processor: {}".format(error)) from error
            self._processors_cache = tuple(result)
        return self._processors_cache

    def filter(self, model: ModelType) -> bool:
        """Return whether model matches all filters."""
        return all(f.filter(model) for f in self.filters)

    def open(self) -> None:
        """Open a connection if it doesn't already exist.

        Default implementation is a noop.
        """

    def close(self) -> None:
        """Close the connection if it exists.

        Default implementation is a noop.
        """

    @abstractmethod
    def get_message(self, model: ModelType) -> MessageType:
        """Return formatted message created from model.

        Raises:
            ValueError: Message can't be created.
            RuntimeError: Transient error in message creation.
            MessageCanceled: The message is canceled.
        """

    @abstractmethod
    def _send(self, message: MessageType) -> None:
        """Actually send the message.

        Derived class must implement this method.

        Raises:
            ValueError: Message can't be sent.
            RuntimeError: Transient error in message sending.
            MessageCanceled: The message is canceled.
            MessageUndelivered: The message was sent, but can't be delivered.
        """

    def send(self, model: ModelType) -> ModelType:
        """Send message based on its model and update its status."""
        now = datetime.now(UTC)
        model.attempts += 1
        model.update_datetime = now

        try:
            message = self.get_message(model)
            # Apply processors
            for processor in self.processors:
                message = processor.process_message(message)
            self._send(message)
        except MessageCanceled:
            model.status = MessageStatus.CANCELED
            _LOGGER.info("Sending %s %s canceled.", self.method, model)
        except MessageUndelivered:
            model.send_datetime = now
            model.delivery_datetime = now
            model.status = MessageStatus.UNDELIVERED
            model.service = self.name
            _LOGGER.info("Message %s %s sent, but undelivered.", self.method, model)
        except RuntimeError as error:
            # Check if the number of attempts reached limit. Otherwise just log.
            if model.attempts >= self.max_attempts:
                model.status = MessageStatus.FAILED
            _LOGGER.info("Sending %s %s failed with error: %s", self.method, model, error)
        except Exception as error:
            model.status = MessageStatus.ERROR
            _LOGGER.warning("Message %s contains error: %s", model, error)
        else:
            model.send_datetime = now
            model.status = MessageStatus.SENT
            model.service = self.name
        return model


class FileBackendMixin:
    """Mixin which handles files for backends."""

    def get_file(self, uuid: UUID) -> File:
        """Return an attachment file.

        Raises:
            ValueError: Attachment doesn't exist.
            RuntimeError: Transient error in file API.
        """
        if APPLICATION.storage is None:
            raise RuntimeError("Storage not available.")

        try:
            return APPLICATION.storage.open(uuid)
        except FileNotFoundError as error:
            raise ValueError(str(error)) from error
        except ConnectionAbortedError as error:
            raise RuntimeError(str(error)) from error


class TemplateBackendMixin:
    """Mixin which handles templates for backends.

    Attributes:
        secretary_url: URL of the django-secretary API.
        secretary_timeout: Timeout for the django-secretary API.
        secretary_token: An authentication token for the django-secretary API.
    """

    def __init__(self, *, secretary_url: str, secretary_timeout: Union[float, Tuple[float, float]] = 3.05,
                 secretary_token: Optional[str] = None, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        auth = None
        if secretary_token:
            auth = HTTPTokenAuth(secretary_token)
        self.secretary = SecretaryClient(secretary_url, secretary_timeout, auth=auth)

    def get_template_uuid(self, template_name: str) -> UUID:
        """Return UUID of the active template.

        Arguments:
            template_name: Name of an active template.

        Raises:
            ValueError: Template name is invalid.
            RuntimeError: Transient error in template API.
        """
        try:
            info = self.secretary.get_template_info(template_name)
        except TemplateNotFound:
            raise ValueError("Template '{}' not found.".format(template_name)) from None
        except (ValueError, RuntimeError) as error:
            raise RuntimeError("Attempt to obtain template UUID failed with: {!r}".format(error)) from error

        try:
            return UUID(info.uuid)
        except ValueError as error:
            raise RuntimeError("Response contain invalid UUID.") from error


class MemoryBackendMixin:
    """Mixin for memory backends.

    This mixin is not intended for production use, but only for testing and development.
    """

    outbox: List[Any] = []

    def _send(self, message: Any) -> None:
        """Append message to outbox."""
        self.outbox.append(message)
