"""Message backends for messenger."""
import logging
import os
import smtplib
from datetime import datetime
from email.message import EmailMessage
from typing import Any, Dict, Optional, Type, Union, cast

from pytz import UTC

from ..constants import TransportMethod
from ..models import Email
from ..utils import create_ssl_context
from .base import BaseBackend, FileBackendMixin, MemoryBackendMixin, TemplateBackendMixin

_LOGGER = logging.getLogger(__name__)


class BaseEmailBackend(FileBackendMixin, TemplateBackendMixin, BaseBackend):
    """Base backend for all email backends.

    Attributes:
        default_from: Default email address of the `From` header.
        sender: An email address of the `Sender` header if any.
    """

    method = TransportMethod.EMAIL

    def __init__(self, *, default_from: str = 'webmaster@localhost', sender: Optional[str] = None, **kwargs: Any,
                 ) -> None:
        super().__init__(**kwargs)
        self.default_from = default_from
        self.sender = sender

    def get_message(self, email: Email) -> EmailMessage:
        """Return formatted email message from email object.

        Raises:
            ValueError: Message can't be created.
            RuntimeError: Transient error in message creation.
        """
        if email.subject_template_uuid is None:
            email.subject_template_uuid = self.get_template_uuid(email.subject_template)

        if email.body_template_uuid is None:
            email.body_template_uuid = self.get_template_uuid(email.body_template)

        if email.body_template_html and email.body_template_html_uuid is None:
            email.body_template_html_uuid = self.get_template_uuid(email.body_template_html)

        message = EmailMessage()
        message['Auto-Submitted'] = 'auto-generated'
        if self.sender:
            message['Sender'] = self.sender
        message['From'] = email.sender or self.default_from
        message['To'] = email.recipient
        # Handled in service
        assert email.context is not None  # noqa: S101
        subject = self.secretary.render(email.subject_template_uuid, email.context)
        # The rendered subject may contain whitespaces, which may break the message. Strip 'em all!
        message['Subject'] = subject.strip()
        message['Message-ID'] = email.message_id
        for key, value in (email.extra_headers or {}).items():
            message[key] = value
        message.set_content(self.secretary.render(email.body_template_uuid, email.context))
        if email.body_template_html_uuid:
            message.add_alternative(self.secretary.render_html(email.body_template_html_uuid, email.context),
                                    subtype='html')
        for uuid in (email.attachments or ()):
            attachment = self.get_file(uuid)
            message.add_attachment(attachment.read(), *cast(str, attachment.mimetype).split('/', 1),
                                   filename=attachment.name)

        return message


class MemoryBackend(MemoryBackendMixin, BaseEmailBackend):
    """Backend which stores sent emails in memory.

    This backend is not intended for production use, but only for testing and development.
    """


class FileBackend(BaseEmailBackend):
    """Backend which stores sent emails in files.

    This backend is not intended for production use, but only for testing and development.

    Attributes:
        path: Path to a directory, where email files are stored.
    """

    def __init__(self, *, path: str, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.path = path

    def _send(self, message: EmailMessage) -> None:
        """Write email message to a file."""
        filename = '{}-{}.eml'.format(datetime.now(UTC).strftime("%Y%m%d-%H%M%S"), message['Message-ID'])
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        with open(os.path.join(self.path, filename), 'wb') as output:
            output.write(bytes(message))


class SmtpBackend(BaseEmailBackend):
    """Backend which sends emails via SMTP.

    Attributes:
        host: The host to use for the SMTP server.
        port: The port to use for the SMTP server.
        username: Username to use for login to the SMTP server.
        password: Password to use for login to the SMTP server.
        use_ssl: Whether to use implicit TLS connection to the SMTP server.
        use_tls: Whether to use explicit TLS connection to the SMTP server.
        timeout: A timeout for blocking operations on the SMTP server.
        keyfile: Path to a PEM-formatted private key to use for the SSL connection.
        certfile: Path to a PEM-formatted certificate chain file to use for the SSL connection.
        verify: Whether to verify a peer certificate.
    """

    def __init__(self, *, host: str = 'localhost', port: int = 25, username: Optional[str] = None,
                 password: Optional[str] = None, use_ssl: bool = False, use_tls: bool = False,
                 timeout: Optional[int] = None, keyfile: Optional[str] = None, certfile: Optional[str] = None,
                 verify: bool = True, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        if use_ssl and use_tls:
            raise ValueError('Arguments use_ssl and use_tls are mutually exclusive.')
        self.host = host
        self.port = port
        self.username = username
        self.password = password
        self.use_ssl = use_ssl
        self.use_tls = use_tls
        self.timeout = timeout
        self.keyfile = keyfile
        self.certfile = certfile
        self.verify = verify
        self.connection = None  # type: Optional[smtplib.SMTP]

    def open(self) -> None:
        """Open connection to the SMTP server.

        Raises:
            SMTPException: An error occurs in connection.
        """
        if self.connection is not None:
            return

        smtp_cls: Union[Type[smtplib.SMTP], Type[smtplib.SMTP_SSL]] = smtplib.SMTP
        if self.use_ssl:
            smtp_cls = smtplib.SMTP_SSL

        conn_kwargs: Dict[str, Any] = {}
        if self.timeout:
            conn_kwargs['timeout'] = self.timeout
        if self.use_ssl:
            conn_kwargs['context'] = create_ssl_context(self.certfile, self.keyfile, verify=self.verify)

        self.connection = smtp_cls(self.host, self.port, **conn_kwargs)
        if self.use_tls:
            self.connection.starttls(context=create_ssl_context(self.certfile, self.keyfile, verify=self.verify))
        if self.username and self.password:
            self.connection.login(self.username, self.password)

    def close(self) -> None:
        """Close the connection to the SMTP server."""
        if not self.connection:
            # Nothing to do.
            return

        try:
            self.connection.quit()
        except smtplib.SMTPServerDisconnected:
            # Already disconnected.
            pass
        self.connection = None

    def _send(self, message: EmailMessage) -> None:
        """Actually send the email.

        Raises:
            RuntimeError: An error occures while sending email.
        """
        try:
            cast(smtplib.SMTP, self.connection).send_message(message)
        except smtplib.SMTPException as error:
            raise RuntimeError('Sending failed with error: {}'.format(error)) from error
