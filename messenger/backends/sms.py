"""Base backends for SMS."""
from dataclasses import dataclass
from typing import Any, Optional
from uuid import UUID

from messenger.constants import TransportMethod
from messenger.models import Sms
from messenger.utils import normalize_phone_number

from .base import BaseBackend, MemoryBackendMixin, TemplateBackendMixin


@dataclass
class SmsMessage:
    """An SMS message prepared for send.

    Attributes:
        uuid: A message UUID.
        recipient: A message recipient phone number. Value is normalized to E164 format.
        body: A text of the SMS.
        sender: A sender label.
        id: A message identifier. A numeric version of uuid by default.
    """

    uuid: UUID
    recipient: str
    body: str
    sender: Optional[str] = None
    # Ignore the warning about the incompatible default value. It is fixed in post_init.
    id: str = None  # type: ignore[assignment]

    def __post_init__(self, *args: Any, id: Optional[int] = None, **kwargs: Any) -> None:
        # Try to normalize recipient or keep the raw value
        self.recipient = normalize_phone_number(self.recipient) or self.recipient
        # Set default message identifier.
        if self.id is None:
            self.id = str(self.uuid.int)  # type: ignore[unreachable]  # mypy is confused by the `id` type


class BaseSmsBackend(TemplateBackendMixin, BaseBackend):
    """Base backend for all SMS backends.

    Attributes:
        sender: A sender label.
        user: User for authentication at provider.
        password: Password for authentication at provider.
        test: Whether a test mode should be used.

    Behaviour of `user`, `password` and `test` arguments is implementation specific.
    """

    method = TransportMethod.SMS

    def __init__(self, *, sender: Optional[str] = None, user: Optional[str] = None, password: Optional[str] = None,
                 test: bool = False, **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self.sender = sender
        self.user = user
        self.password = password
        self.test = test

    def get_message(self, model: Sms) -> SmsMessage:
        """Return formatted SMS message from SMS object."""
        if model.body_template_uuid is None:
            model.body_template_uuid = self.get_template_uuid(model.body_template)

        # Handled in service
        assert model.context is not None  # noqa: S101
        return SmsMessage(uuid=model.uuid, recipient=model.recipient, sender=self.sender,
                          body=self.secretary.render(model.body_template_uuid, model.context))


class MemorySmsBackend(MemoryBackendMixin, BaseSmsBackend):
    """Backend which stores sent SMS in memory.

    This backend is not intended for production use, but only for testing and development.
    """
