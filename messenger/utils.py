"""Various utility functions."""
import logging
import re
import ssl
from collections import Counter
from email.utils import parseaddr
from importlib import import_module
from os import PathLike
from typing import Any, Optional, Union

import phonenumbers

_LOGGER = logging.getLogger(__name__)
EMAIL_ADDRESS_RE = re.compile(r'\S+@\S+')


def normalize_email_address(value: str) -> Optional[str]:
    """Return normalized email address, without real name and lowercased, or None if input is not an email address."""
    parsed_email = parseaddr(value)
    bare_email = parsed_email[1].lower()
    if EMAIL_ADDRESS_RE.match(bare_email):
        return bare_email
    else:
        return None


def normalize_phone_number(value: str) -> Optional[str]:
    """Return normalized phone number in E164 format or None if input is not a phone number."""
    try:
        parsed = phonenumbers.parse(value)
    except phonenumbers.NumberParseException:
        return None
    return phonenumbers.format_number(parsed, phonenumbers.PhoneNumberFormat.E164)


def create_ssl_context(certfile: Optional[Union[PathLike, str]] = None, keyfile: Optional[Union[PathLike, str]] = None,
                       *, verify: bool = True) -> ssl.SSLContext:
    """Create and return a SSL context.

    Arguments:
        certfile: Path to a file with a client certificate.
        keyfile: Path to a file with a private key.
        verify: Whether to verify a peer certificate.
    """
    context = ssl.create_default_context()
    if certfile:
        context.load_cert_chain(certfile, keyfile)
    if not verify:
        _LOGGER.warning("Verification of the peer certificate is disabled.")
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
    return context


def import_path(path: str) -> Any:
    """Import a dotted path and return the attribute or class defined.

    Raises:
        ImportError: If the import fails.
    """
    try:
        module_name, class_name = path.rsplit('.', 1)
    except ValueError as error:
        raise ImportError("'{}' is not a valid dotted path.".format(path)) from error

    try:
        module = import_module(module_name)
    except ValueError as error:
        raise ImportError(str(error)) from error

    try:
        return getattr(module, class_name)
    except AttributeError as error:
        raise ImportError("'{}' does not have a class or attribute '{}'.".format(module_name, class_name)) from error


# TODO: Make first argument positional only, once Python 3.8 is minimal version.
def load_instance(_path: str, **kwargs: Any) -> Any:
    """Return instance of class specified by a dotted path and keyword arguments."""
    cls = import_path(_path)
    return cls(**kwargs)


class Result(Counter):
    """Collects process statistics.

    Basically a Counter with a defined set of fields.
    """

    # TODO: Remove in favor of Counter.total(), when support for python 3.9 is dropped.
    def total(self) -> int:
        """Compute the sum of the counts."""
        return sum(self.values())

    def mark_success(self) -> None:
        """Mark a success to a result."""
        self.update(success=1)

    def mark_fail(self) -> None:
        """Mark a failure to a result."""
        self.update(fail=1)

    def mark_error(self) -> None:
        """Mark an error to a result."""
        self.update(error=1)

    def mark_skip(self) -> None:
        """Mark a skip to a result."""
        self.update(skip=1)

    @property
    def successes(self) -> int:
        """Return a number of successes."""
        return self['success']

    @property
    def fails(self) -> int:
        """Return a number of failures."""
        return self['fail']

    @property
    def errors(self) -> int:
        """Return a number of errors."""
        return self['error']

    @property
    def skips(self) -> int:
        """Return a number of skips."""
        return self['skip']
