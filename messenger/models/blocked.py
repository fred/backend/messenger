"""Models for blocked addresses."""
from typing import Optional

from sqlalchemy import CheckConstraint, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column

from .base import BaseModel


class BlockedEmail(BaseModel):
    """Represents a blocked email address."""

    email: Mapped[str] = mapped_column(unique=True)

    def __str__(self) -> str:
        return self.email or ''


class BlockedPhone(BaseModel):
    """Represents a blocked phone number."""

    phone: Mapped[str] = mapped_column(unique=True)

    def __str__(self) -> str:
        return self.phone or ''


ADDRESS_PART_CHECK_SQL = ('name IS NOT NULL OR organization IS NOT NULL OR street IS NOT NULL OR city IS NOT NULL OR '
                          'state IS NOT NULL OR postal_code IS NOT NULL or country IS NOT NULL')


class BlockedAddress(BaseModel):
    """Represents a blocked address."""

    __table_args__ = (
        # At least one part of address needs to be defined.
        CheckConstraint(ADDRESS_PART_CHECK_SQL, name='part_required'),
        # Ensure addresses are unique.
        UniqueConstraint('name', 'organization', 'street', 'city', 'state', 'postal_code', 'country'),
    )

    name: Mapped[Optional[str]]
    organization: Mapped[Optional[str]]
    street: Mapped[Optional[str]] = mapped_column(doc="May contain multiple lines")
    city: Mapped[Optional[str]]
    state: Mapped[Optional[str]] = mapped_column(doc="State or province name")
    postal_code: Mapped[Optional[str]]
    country: Mapped[Optional[str]] = mapped_column(doc="Country name, not code")

    def __str__(self) -> str:
        values = (self.name, self.organization, self.street, self.city, self.state, self.postal_code, self.country)
        return ', '.join(v for v in values if v)
