"""Models for letter messages."""
from dataclasses import astuple, dataclass
from typing import List, Optional
from uuid import UUID

from sqlalchemy import ForeignKey, ForeignKeyConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base import BaseModel
from .message import ArchiveMessageMixin, ArchiveReferenceMixin, MessageMixin, ReferenceMixin


@dataclass(frozen=True)
class Address:
    """Represents letter address."""

    name: Optional[str] = None
    organization: Optional[str] = None
    street: Optional[str] = None
    city: Optional[str] = None
    state: Optional[str] = None
    postal_code: Optional[str] = None
    country: Optional[str] = None

    def __str__(self) -> str:
        chunks = (i for i in astuple(self) if i is not None)
        return ', '.join(chunks)


class Letter(MessageMixin, BaseModel):
    """Represents a letter."""

    # Message attributes
    recipient_name: Mapped[Optional[str]]
    recipient_organization: Mapped[Optional[str]]
    recipient_street: Mapped[Optional[str]] = mapped_column(doc="May contain multiple lines")
    recipient_city: Mapped[Optional[str]]
    recipient_state: Mapped[Optional[str]] = mapped_column(doc="State or province name")
    recipient_postal_code: Mapped[Optional[str]]
    recipient_country: Mapped[Optional[str]] = mapped_column(doc="Country name, not code")
    file: Mapped[UUID]

    # Send attributes
    batch_id: Mapped[Optional[str]] = mapped_column(doc="An identifier of a batch that sent the letter.")

    references: Mapped[List['LetterReference']] = relationship('LetterReference')

    @property
    def recipient(self) -> Address:
        """Return address object."""
        return Address(name=self.recipient_name, organization=self.recipient_organization, street=self.recipient_street,
                       city=self.recipient_city, state=self.recipient_state, postal_code=self.recipient_postal_code,
                       country=self.recipient_country)

    @recipient.setter
    def recipient(self, value: Address) -> None:
        self.recipient_name = value.name
        self.recipient_organization = value.organization
        self.recipient_street = value.street
        self.recipient_city = value.city
        self.recipient_state = value.state
        self.recipient_postal_code = value.postal_code
        self.recipient_country = value.country


class LetterReference(ReferenceMixin, BaseModel):
    """Represents a letter reference."""

    message_id: Mapped[int] = mapped_column(ForeignKey(Letter.__table__.columns.id))


class ArchiveLetter(ArchiveMessageMixin, BaseModel):
    """Represents an archived letter."""

    # Message attributes
    recipient_name: Mapped[Optional[str]]
    recipient_organization: Mapped[Optional[str]]
    recipient_street: Mapped[Optional[str]] = mapped_column(doc="May contain multiple lines")
    recipient_city: Mapped[Optional[str]]
    recipient_state: Mapped[Optional[str]] = mapped_column(doc="State or province name")
    recipient_postal_code: Mapped[Optional[str]]
    recipient_country: Mapped[Optional[str]] = mapped_column(doc="Country name, not code")
    file: Mapped[UUID]

    # Send attributes
    batch_id: Mapped[Optional[str]] = mapped_column(doc="An identifier of a batch that sent the letter.")

    references: Mapped[List['ArchiveLetterReference']] = relationship('ArchiveLetterReference')

    @property
    def recipient(self) -> Address:
        """Return address object."""
        return Address(name=self.recipient_name, organization=self.recipient_organization, street=self.recipient_street,
                       city=self.recipient_city, state=self.recipient_state, postal_code=self.recipient_postal_code,
                       country=self.recipient_country)

    @recipient.setter
    def recipient(self, value: Address) -> None:
        self.recipient_name = value.name
        self.recipient_organization = value.organization
        self.recipient_street = value.street
        self.recipient_city = value.city
        self.recipient_state = value.state
        self.recipient_postal_code = value.postal_code
        self.recipient_country = value.country


class ArchiveLetterReference(ArchiveReferenceMixin, BaseModel):
    """Represents an archive letter message reference."""

    __table_args__ = (  # type: ignore[assignment]
        ForeignKeyConstraint(
            ['message_id', 'create_datetime'],
            [ArchiveLetter.__table__.columns.id,
             ArchiveLetter.__table__.columns.create_datetime]),
    ) + ArchiveReferenceMixin.__table_args__

    message_id: Mapped[int]
