"""Base classes for models."""
from datetime import datetime
from uuid import UUID

from sqlalchemy import Column, Integer, MetaData, Table, Text
from sqlalchemy.orm import DeclarativeBase, declared_attr
from sqlalchemy_utc import UtcDateTime
from sqlalchemy_utils import UUIDType, generic_repr

NAMING_CONVENTION = {
    'pk': '%(table_name)s_pkey',
    'ix': '%(table_name)s_%(column_0_name)s_key',
    'fk': '%(table_name)s_%(column_0_name)s_fk',
    'ck': '%(table_name)s_%(constraint_name)s',
    'uq': '%(table_name)s_%(column_0_name)s_key',
}
METADATA = MetaData(naming_convention=NAMING_CONVENTION)


APPLICATION_PREFIX = 'messenger_'


@generic_repr
class BaseModel(DeclarativeBase):
    """Base class for messenger models."""

    metadata = METADATA

    id = Column(Integer, primary_key=True)

    type_annotation_map = {
        str: Text(),
        UUID: UUIDType(binary=False),
        datetime: UtcDateTime(timezone=True),
    }

    @declared_attr.directive
    def __tablename__(cls) -> str:
        return APPLICATION_PREFIX + cls.__name__.lower()


# Create a separate metadata, so the sqlite_sequence is not managed by migrations.
_SQLITE_METADATA = MetaData()
# Map the SQLite internal table 'sqlite_sequence'.
SQLITE_SEQUENCE = Table(
    'sqlite_sequence', _SQLITE_METADATA,
    Column('name', Text),
    Column('seq', Integer),
)
