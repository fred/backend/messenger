"""Base classes for all message types."""
from datetime import datetime
from typing import Any, Dict, NoReturn, Optional, Tuple, cast
from uuid import UUID, uuid4

from sqlalchemy import Boolean, Enum, SmallInteger, UniqueConstraint, null
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.sql import expression, func
from sqlalchemy_utils import JSONType
from typing_extensions import Literal

from ..constants import ArchiveMessageStatus, MessageStatus, ReferenceType


def _not_implemented(*args: Any) -> NoReturn:
    raise NotImplementedError


MAX_REPR_LENGHT = 1024


class BaseMessageMixin:
    """Abstract mixin with basic fields shared by all message types."""

    # Envelope attributes
    uuid: Mapped[UUID] = property(_not_implemented, _not_implemented)  # type: ignore[assignment]
    create_datetime: Mapped[datetime] = property(_not_implemented, _not_implemented)  # type: ignore[assignment]
    update_datetime: Mapped[Optional[datetime]]
    send_datetime: Mapped[Optional[datetime]]
    attempts: Mapped[int] = mapped_column(SmallInteger(), server_default=expression.text('0'))
    status: Any = property(_not_implemented, _not_implemented)
    type: Mapped[Optional[str]]
    references: Any = property(_not_implemented, _not_implemented)

    # Message attributes
    recipient: Any = property(_not_implemented, _not_implemented)

    # Send attributes
    service: Mapped[Optional[str]] = mapped_column(doc="An alias of a service that sent the message.")

    # Delivery attributes
    delivery_datetime: Mapped[Optional[datetime]]
    delivery_info: Mapped[Optional[Dict[str, Any]]] = mapped_column(JSONType(),
                                                                    # Force default to sql NULL.
                                                                    default=null(),
                                                                    doc="Selected information about delivery message.")

    # Archive attribute
    archive: Mapped[bool] = property(_not_implemented, _not_implemented)  # type: ignore[assignment]

    def __repr__(self) -> str:
        field_data = [('uuid', self.uuid), ('recipient', self.recipient)]
        out = '{}({})'.format(self.__class__.__name__, ', '.join(k + '=' + repr(v) for k, v in field_data))
        if len(out) > MAX_REPR_LENGHT:
            return out[:MAX_REPR_LENGHT] + '...)'
        else:
            return out


class BaseTemplateMessageMixin(BaseMessageMixin):
    """A mixin with basic fields for messages with templates."""

    # Message attributes
    body_template: Mapped[str] = mapped_column(doc="Name of the body template.")
    body_template_uuid: Mapped[Optional[UUID]]
    context: Mapped[Optional[Dict[str, Any]]] = mapped_column(JSONType(), doc="Context for all message templates.")


class MessageMixin(BaseMessageMixin):
    """A mixin with basic fields for a regular message object."""

    __table_args__: Dict[str, Any] = {'sqlite_autoincrement': True}

    # Envelope attributes
    uuid: Mapped[UUID] = mapped_column(unique=True, default=uuid4)
    create_datetime: Mapped[datetime] = mapped_column(server_default=func.now())
    # Archive has a different set of statuses
    status: Mapped[MessageStatus] = mapped_column(Enum(MessageStatus, create_constraint=True), index=True)

    # Archive attribute
    archive: Mapped[bool] = mapped_column(Boolean(name='archive', create_constraint=True),
                                          server_default=expression.true())


class TemplateMessageMixin(MessageMixin, BaseTemplateMessageMixin):
    """A mixin with basic fields for a regular message object with templates."""


class ArchiveMessageMixin(BaseMessageMixin):
    """A mixin with basic fields for an archived message object.

    Also defines partitioning on postgres.
    """

    __table_args__ = {'postgresql_partition_by': 'RANGE (create_datetime)'}

    # Envelope attributes
    # Partitioning prevents unique constraints without partition key.
    uuid: Mapped[UUID] = mapped_column(default=uuid4, index=True)
    # Partitioning requires partition key to be a part of primary key.
    create_datetime: Mapped[datetime] = mapped_column(server_default=func.now(), primary_key=True)
    # Archive has a different set of statuses
    status: Mapped[ArchiveMessageStatus] = mapped_column(Enum(ArchiveMessageStatus, create_constraint=True), index=True)

    @property
    def archive(self) -> Literal[True]:  # type: ignore[override]
        """Return True, because the message is already archived."""
        return True


class TemplateArchiveMessageMixin(ArchiveMessageMixin, BaseTemplateMessageMixin):
    """A mixin with basic fields for an archived message object with templates."""


class ReferenceMixin:
    """A mixin with basic fields for a message reference."""

    @declared_attr.directive
    def __table_args__(cls) -> Tuple:
        return (
            UniqueConstraint('type', 'value', 'message_id'),
            UniqueConstraint('old_type', 'value', 'message_id'),
        )

    message_id: Any = property(_not_implemented, _not_implemented)
    _old_type: Mapped[Optional[ReferenceType]] = mapped_column("old_type", Enum(ReferenceType, create_constraint=True))
    _new_type: Mapped[str] = mapped_column("type")
    value: Mapped[str]

    @hybrid_property
    def type(self) -> str:
        """Return reference type."""
        return cast(str, self._new_type or self._old_type)

    @type.setter  # type: ignore[no-redef]  # mypy doesn't understand hybrid_property
    def type(self, value: str) -> None:
        """Set reference type."""
        self._new_type = value

    @type.expression  # type: ignore[no-redef]  # mypy doesn't understand hybrid_property
    def type(cls):
        """Return reference type SQL expression."""
        # In SQL just fallback to the new column.
        return cls._new_type

    @property
    def old_type(self) -> Optional[ReferenceType]:
        """Return old-style reference type."""
        if self._old_type:
            return self._old_type

        try:
            return ReferenceType(self._new_type)
        except ValueError:
            return None


class ArchiveReferenceMixin(ReferenceMixin):
    """A mixin with basic fields for an archived message reference.

    Also defines partitioning on postgres.
    """

    @declared_attr.directive
    def __table_args__(cls) -> Tuple:
        return (
            UniqueConstraint('type', 'value', 'message_id', 'create_datetime'),
            UniqueConstraint('old_type', 'value', 'message_id', 'create_datetime'),
            {'postgresql_partition_by': 'RANGE (create_datetime)'},
        )

    # We want the partitions constructed in the same way as the messages.
    # Partitioning requires partition key to be a part of primary key.
    create_datetime: Mapped[datetime] = mapped_column(primary_key=True)
