"""Models for SMS messages."""
from typing import List

from sqlalchemy import ForeignKey, ForeignKeyConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base import BaseModel
from .message import ArchiveReferenceMixin, ReferenceMixin, TemplateArchiveMessageMixin, TemplateMessageMixin


class Sms(TemplateMessageMixin, BaseModel):
    """Represents an SMS message."""

    # Message attributes
    recipient: Mapped[str]

    references: Mapped[List['SmsReference']] = relationship('SmsReference')


class SmsReference(ReferenceMixin, BaseModel):
    """Represents a SMS message reference."""

    message_id: Mapped[int] = mapped_column(ForeignKey(Sms.__table__.columns.id))


class ArchiveSms(TemplateArchiveMessageMixin, BaseModel):
    """Represents an archived SMS message."""

    # Message attributes
    recipient: Mapped[str]

    references: Mapped[List['ArchiveSmsReference']] = relationship('ArchiveSmsReference')


class ArchiveSmsReference(ArchiveReferenceMixin, BaseModel):
    """Represents an archive SMS message reference."""

    __table_args__ = (  # type: ignore[assignment]
        ForeignKeyConstraint(
            ['message_id', 'create_datetime'],
            [ArchiveSms.__table__.columns.id,
             ArchiveSms.__table__.columns.create_datetime]),
    ) + ArchiveReferenceMixin.__table_args__

    message_id: Mapped[int]
