"""Utilities for models."""
from typing import Optional, Sequence, Type, cast

from sqlalchemy.engine import Dialect
from sqlalchemy_utils import ScalarListType as _ScalarListType


class ScalarListType(_ScalarListType):
    """Fixed version of sqlalchemy_utils.ScalarListType.

    :param inner_type:
    The type of the values. Default is ``str``.
    """

    cache_ok = True

    def __init__(self, inner_type: Type = str, separator: str = ','):
        # Inner type is stored to `coerce_func` attribute.
        if not isinstance(inner_type, type):
            raise ValueError('Inner type {} is not a type.'.format(inner_type))
        super().__init__(inner_type, separator)

    def process_bind_param(self, value: Optional[Sequence], dialect: Dialect) -> Optional[Sequence]:
        """Check if all values has the correct type before serialization."""
        if value is not None and any(not isinstance(i, self.coerce_func) for i in value):
            msg = "Not all items in value {} match the type {}"
            raise ValueError(msg.format(value, self.coerce_func))
        return cast(Optional[Sequence], super().process_bind_param(value, dialect))
