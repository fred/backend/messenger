"""Messenger models."""
from typing import Union

from .base import METADATA
from .blocked import BlockedAddress, BlockedEmail, BlockedPhone
from .email import ArchiveEmail, ArchiveEmailReference, Email, EmailReference
from .letter import Address, ArchiveLetter, ArchiveLetterReference, Letter, LetterReference
from .sms import ArchiveSms, ArchiveSmsReference, Sms, SmsReference

__all__ = [
    'Address',
    'AnyRecord',
    'ArchiveEmail',
    'ArchiveEmailReference',
    'ArchiveLetter',
    'ArchiveLetterReference',
    'ArchiveRecord',
    'ArchiveSms',
    'ArchiveSmsReference',
    'BlockedAddress',
    'BlockedEmail',
    'BlockedPhone',
    'Email',
    'EmailReference',
    'Letter',
    'LetterReference',
    'METADATA',
    'Record',
    'Sms',
    'SmsReference',
]

Record = Union[Email, Sms, Letter]
ArchiveRecord = Union[ArchiveEmail, ArchiveSms, ArchiveLetter]
AnyRecord = Union[Record, ArchiveRecord]
