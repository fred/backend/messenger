"""Models for email messages."""
from email.utils import make_msgid
from typing import Any, Dict, List, Optional, cast
from uuid import UUID

from sqlalchemy import ARRAY, CheckConstraint, ForeignKey, ForeignKeyConstraint, Text
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Mapped, mapped_column, relationship
from sqlalchemy.sql import case, func
from sqlalchemy.sql.compiler import SQLCompiler
from sqlalchemy.sql.expression import ColumnElement, FunctionElement
from sqlalchemy_utils import JSONType, UUIDType

from ..settings import APPLICATION
from .base import BaseModel
from .message import ArchiveReferenceMixin, ReferenceMixin, TemplateArchiveMessageMixin, TemplateMessageMixin
from .utils import ScalarListType


def make_message_id() -> str:
    """Return a new Message-ID."""
    # Separate function to make lazy usage of settings.
    return make_msgid(domain=APPLICATION.settings.msgid_domain)


class _effective_recipient(FunctionElement):
    """Expression to wrap Email.recipient attribute."""

    type = Text()
    inherit_cache = True


@compiles(_effective_recipient)
def _default_effective_recipient(element: _effective_recipient, compiler: SQLCompiler, **kw: Any) -> str:
    old_recipient, recipient = tuple(element.clauses)
    return compiler.process(
        case(
            (old_recipient.is_(None), recipient),
            else_=old_recipient,
        )
    )


@compiles(_effective_recipient, 'postgresql')
def _pg_effective_recipient(element: _effective_recipient, compiler: SQLCompiler, **kw: Any) -> str:
    old_recipient, recipient = tuple(element.clauses)
    return compiler.process(
        case(
            (old_recipient.is_(None), recipient),
            else_=func.array_to_string(old_recipient, ", ")
        )
    )


class Email(TemplateMessageMixin, BaseModel):
    """Represents an email message."""

    # Envelope attributes
    message_id: Mapped[str] = mapped_column(unique=True, default=make_message_id,
                                            doc="RFC 2822-compliant Message-ID header.")

    # Message attributes
    sender: Mapped[Optional[str]]
    _old_recipient: Mapped[Optional[List[str]]] = mapped_column("old_recipient",
                                                                ARRAY(Text).with_variant(ScalarListType(), 'sqlite'))
    _recipient: Mapped[Optional[str]] = mapped_column(
        "recipient",
        # Ensure only one of `old_recipient` and `recipient` is set.
        CheckConstraint('(recipient IS NULL) != (old_recipient IS NULL)', name='recipient'),
    )
    extra_headers: Mapped[Optional[Dict[str, Any]]] = mapped_column(JSONType(), doc="Additional email message headers.")
    subject_template: Mapped[str] = mapped_column(doc="Name of the subject template.")
    subject_template_uuid: Mapped[Optional[UUID]]
    body_template_html: Mapped[Optional[str]] = mapped_column(doc="Name of the HTML body template.")
    body_template_html_uuid: Mapped[Optional[UUID]] = mapped_column(
        CheckConstraint('body_template_html_uuid IS NULL OR body_template_html IS NOT NULL',
                        name='body_template_html_uuid'))
    attachments: Mapped[Optional[List[UUID]]] = mapped_column(
        ARRAY(UUIDType).with_variant(ScalarListType(UUID), 'sqlite'),
        doc="List of attachment UUIDs.")

    references: Mapped[List['EmailReference']] = relationship('EmailReference')

    @hybrid_property
    def recipient(self) -> str:
        """Return recipient."""
        if self._old_recipient is not None:
            return ', '.join(self._old_recipient)
        else:
            return cast(str, self._recipient)

    @recipient.inplace.setter
    def _recipient_setter(self, value: str) -> None:
        """Set recipient."""
        # Set the new column and clear the old one.
        self._recipient = value
        self._old_recipient = None

    @recipient.inplace.expression
    def _recipient_expression(cls) -> ColumnElement[str]:
        # In SQL fallback to the recipient column.
        return _effective_recipient(cls._old_recipient, cls._recipient)


class EmailReference(ReferenceMixin, BaseModel):
    """Represents an email message reference."""

    message_id: Mapped[int] = mapped_column(ForeignKey(Email.__table__.columns.id))


class ArchiveEmail(TemplateArchiveMessageMixin, BaseModel):
    """Represents an archived email message."""

    # Envelope attributes
    # Message-ID may be NULL in old emails.
    message_id: Mapped[Optional[str]] = mapped_column(default=make_message_id, index=True,
                                                      doc="RFC 2822-compliant Message-ID header.")

    # Message attributes
    sender: Mapped[Optional[str]]
    _old_recipient: Mapped[Optional[List[str]]] = mapped_column("old_recipient",
                                                                ARRAY(Text).with_variant(ScalarListType(), 'sqlite'))
    _recipient: Mapped[Optional[str]] = mapped_column(
        "recipient",
        # Ensure only one of `old_recipient` and `recipient` is set.
        CheckConstraint('(recipient IS NULL) != (old_recipient IS NULL)', name='recipient'),
    )
    extra_headers: Mapped[Optional[Dict[str, Any]]] = mapped_column(JSONType(), doc="Additional email message headers.")
    subject_template: Mapped[str] = mapped_column(doc="Name of the subject template.")
    subject_template_uuid: Mapped[Optional[UUID]]
    body_template_html: Mapped[Optional[str]] = mapped_column(doc="Name of the HTML body template.")
    body_template_html_uuid: Mapped[Optional[UUID]] = mapped_column(
        CheckConstraint('body_template_html_uuid IS NULL OR body_template_html IS NOT NULL',
                        name='body_template_html_uuid'))
    attachments: Mapped[Optional[List[UUID]]] = mapped_column(
        ARRAY(UUIDType).with_variant(ScalarListType(UUID), 'sqlite'),
        doc="List of attachment UUIDs.")

    references: Mapped[List['ArchiveEmailReference']] = relationship('ArchiveEmailReference')

    @hybrid_property
    def recipient(self) -> str:
        """Return recipient."""
        if self._old_recipient is not None:
            return ', '.join(self._old_recipient)
        else:
            return cast(str, self._recipient)

    @recipient.inplace.setter
    def _recipient_setter(self, value: str) -> None:
        """Set recipient."""
        # Set the new column and clear the old one.
        self._recipient = value
        self._old_recipient = None

    @recipient.inplace.expression
    def _recipient_expression(cls) -> ColumnElement[str]:
        # In SQL fallback to the recipient column.
        return _effective_recipient(cls._old_recipient, cls._recipient)


class ArchiveEmailReference(ArchiveReferenceMixin, BaseModel):
    """Represents an archive email message reference."""

    __table_args__ = (  # type: ignore[assignment]
        ForeignKeyConstraint(
            ['message_id', 'create_datetime'],
            [ArchiveEmail.__table__.columns.id,
             ArchiveEmail.__table__.columns.create_datetime]),
    ) + ArchiveReferenceMixin.__table_args__

    message_id: Mapped[int]
