#!/usr/bin/python3
"""Run messenger gRPC server."""
from concurrent.futures import ThreadPoolExecutor
from time import sleep
from typing import Iterable, Sequence

import click
import grpc
from fred_api.messenger import service_email_grpc_pb2, service_letter_grpc_pb2, service_sms_grpc_pb2
from fred_api.messenger.service_email_grpc_pb2_grpc import add_EmailMessengerServicer_to_server
from fred_api.messenger.service_letter_grpc_pb2_grpc import add_LetterMessengerServicer_to_server
from fred_api.messenger.service_sms_grpc_pb2_grpc import add_SmsMessengerServicer_to_server

from messenger.commands.utils import messenger_command, method_option
from messenger.constants import TransportMethod
from messenger.service import EmailServicer, LetterServicer, SmsServicer
from messenger.settings import APPLICATION

try:
    from grpc_reflection.v1alpha import reflection
except ImportError:
    reflection = None


def _add_services(methods: Sequence[TransportMethod], server: grpc.Server) -> None:
    """Add services to a server."""
    service_names = []
    if TransportMethod.EMAIL in methods:
        add_EmailMessengerServicer_to_server(EmailServicer(), server)
        service_names.append(service_email_grpc_pb2.DESCRIPTOR.services_by_name['EmailMessenger'].full_name)
    if TransportMethod.SMS in methods:
        add_SmsMessengerServicer_to_server(SmsServicer(), server)
        service_names.append(service_sms_grpc_pb2.DESCRIPTOR.services_by_name['SmsMessenger'].full_name)
    if TransportMethod.LETTER in methods:
        add_LetterMessengerServicer_to_server(LetterServicer(), server)
        service_names.append(service_letter_grpc_pb2.DESCRIPTOR.services_by_name['LetterMessenger'].full_name)
    if reflection is not None:
        reflection.enable_server_reflection(service_names + [reflection.SERVICE_NAME], server)


@messenger_command()
@method_option
@click.option('-p', '--port', type=int, help="Set custom port.")
@click.option('--grpc-options', multiple=True,
              help="Set custom options to the gRPC server, can be used multiple times.")
@click.option('--max-workers', type=int, help="Set custom maximum number of workers.")
@click.option('--max-concurrency', type=int, help="Set custom maximum number of concurrent RPCs.")
def main(methods: Sequence[TransportMethod], port: int, grpc_options: Iterable[str], max_workers: int,
         max_concurrency: int) -> None:
    """Start the server."""
    if port:
        APPLICATION.settings.grpc_port = port
    if grpc_options:
        options = [p.split('=', 1) for p in grpc_options]
        APPLICATION.settings.grpc_options = options
    if max_workers:
        APPLICATION.settings.grpc_max_workers = max_workers
    if max_concurrency:
        APPLICATION.settings.grpc_maximum_concurrent_rpcs = max_concurrency

    server = grpc.server(ThreadPoolExecutor(max_workers or APPLICATION.settings.grpc_max_workers),
                         options=APPLICATION.settings.grpc_options,
                         maximum_concurrent_rpcs=APPLICATION.settings.grpc_maximum_concurrent_rpcs)
    _add_services(methods, server)
    server.add_insecure_port('[::]:{port}'.format(port=APPLICATION.settings.grpc_port))
    server.start()
    # Sadly this is the recommended way to keep the server running.
    try:
        while True:
            sleep(86400)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    main()
