#!/usr/bin/python3
"""Archive messages."""
import logging
from datetime import datetime
from functools import lru_cache
from typing import Optional, Sequence, Tuple, cast

from setapp.constants import Verbosity
from sqlalchemy import Column
from sqlalchemy.orm import Session, class_mapper
from sqlalchemy_utils import get_type

from messenger.commands.utils import echo, limits_options, messenger_command, method_option
from messenger.constants import ArchiveMessageStatus, TransportMethod
from messenger.database import atomic
from messenger.models import Record

_LOGGER = logging.getLogger(__name__)


@lru_cache
def _get_archived_columns(method: TransportMethod) -> Tuple[str, ...]:
    """Return columns copied to archive model."""
    shared_columns = set(class_mapper(method.model_cls).columns.keys()).intersection(
        class_mapper(method.archive_cls).columns.keys())
    return tuple(c for c in shared_columns if c != 'references')


def _archive_model(method: TransportMethod, session: Session, model: Record) -> None:
    """Archive single message."""
    # mypy gets confused by lru_cache, see https://github.com/python/mypy/issues/5858
    archive_cls = method.archive_cls
    data = {a: getattr(model, a) for a in _get_archived_columns(method)}
    archived = archive_cls(**data)
    reference_cls = get_type(archive_cls.references)
    for reference in model.references:
        arch_ref = reference_cls(id=reference.id, create_datetime=archived.create_datetime,
                                 message_id=reference.message_id, type=reference.type, value=reference.value)
        archived.references.append(arch_ref)
    session.add(archived)
    for reference in model.references:
        session.delete(reference)
    session.delete(model)


def _archive(method: TransportMethod, from_datetime: Optional[datetime], to_datetime: Optional[datetime]) -> None:
    """Archive messages of single method."""
    archived = 0
    while True:
        with atomic() as session:
            # mypy gets confused by lru_cache, see https://github.com/python/mypy/issues/5858
            model_cls = cast(Record, method.model_cls)
            # Filter by status & archive
            query = session.query(model_cls)  # type: ignore[arg-type]
            query = query.filter_by(archive=True).filter(cast(Column, model_cls.status).in_(ArchiveMessageStatus))
            if from_datetime:
                query = query.filter(cast(Column, model_cls.create_datetime) >= from_datetime)
            if to_datetime:
                query = query.filter(cast(Column, model_cls.create_datetime) < to_datetime)
            # Fetch one message at a time.
            # Skip any message locked in other transaction and lock the selected message for updates.
            model = query.with_for_update(skip_locked=True).first()
            if model is None:
                # No more messages. Quit.
                break
            _archive_model(method, session, model)
            archived += 1
            echo('.', Verbosity.DETAIL, nl=False)
    # Print newline between progress and summary.
    echo('', Verbosity.DETAIL)
    echo('Archived {} {}s.'.format(archived, method.value.lower()), Verbosity.INFO, log=_LOGGER.info)


@messenger_command()
@limits_options
@method_option
def main(from_datetime: Optional[datetime], to_datetime: Optional[datetime], methods: Sequence[TransportMethod],
         ) -> None:
    """Archive messages."""
    for method in methods:
        _archive(method, from_datetime, to_datetime)


if __name__ == '__main__':
    main()
