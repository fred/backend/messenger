#!/usr/bin/python3
"""Manage table partitions."""
import sys
from datetime import date
from typing import Iterable, Sequence

import click
from dateutil.relativedelta import relativedelta
from setapp.constants import Verbosity
from sqlalchemy import inspect
from sqlalchemy.sql import text

from messenger.commands.utils import echo, messenger_command
from messenger.database import atomic, get_engine
from messenger.models import (
    ArchiveEmail,
    ArchiveEmailReference,
    ArchiveLetter,
    ArchiveLetterReference,
    ArchiveSms,
    ArchiveSmsReference,
)

# Year and month are replaced first. Use zero-filled two chars for month.
PARTITION_NAME = '{{table}}_{year}_{month:02}'
DEFAULT_PARTITION_NAME = '{table}_default'
PARTITION_ATTACHED_SQL = text("SELECT relid FROM pg_partition_tree(:table) WHERE relid = cast(:partition AS regclass)")
CREATE_PARTITION_SQL = "CREATE TABLE {partition} PARTITION OF {table} {values}"
DETACH_PARTITION_SQL = "ALTER TABLE {table} DETACH PARTITION {partition}"
DROP_PARTITION_SQL = "DROP TABLE {partition}"
MODELS = (ArchiveEmail, ArchiveEmailReference, ArchiveLetter, ArchiveLetterReference, ArchiveSms, ArchiveSmsReference)


def _get_create_sqls(partition_name: str, values: str) -> Iterable[str]:
    """Return SQLs for partition creations."""
    for model in MODELS:
        table = model.__tablename__
        partition = partition_name.format(table=table)
        inspector = inspect(get_engine())
        if inspector.has_table(partition):
            echo("-- Partition {} already exists".format(partition), Verbosity.DETAIL)
            continue
        yield CREATE_PARTITION_SQL.format(table=table, partition=partition, values=values)


def _get_drop_sqls(partition_name: str) -> Iterable[str]:
    """Return SQLs for partition deletions."""
    for model in reversed(MODELS):
        table = model.__tablename__
        partition = partition_name.format(table=table)
        inspector = inspect(get_engine())
        if not inspector.has_table(partition):
            # Partition no longer exists
            continue

        with get_engine().connect() as connection:
            is_attached = connection.execute(PARTITION_ATTACHED_SQL, {'table': table, 'partition': partition}).scalar()

        if is_attached:
            yield DETACH_PARTITION_SQL.format(table=table, partition=partition)
        yield DROP_PARTITION_SQL.format(table=table, partition=partition)


def _run_sqls(sqls: Iterable[str], dry_run: bool) -> None:
    """Execute or print SQLs."""
    with atomic() as session:
        for sql in sqls:
            if dry_run:
                sys.stdout.write(sql + '\n')
            else:
                session.execute(text(sql))


@messenger_command()
@click.argument('operation', type=click.Choice(['create', 'drop']), metavar='(create|drop)')
@click.argument('args', nargs=-1, metavar='(DEFAULT|<year> <month>)')
@click.option('-n', '--dry-run', default=False, is_flag=True, help="Just output SQL.")
def main(operation: str, args: Sequence[str], dry_run: bool) -> None:
    """Manage table partitions."""
    if get_engine().dialect.name != 'postgresql':
        raise click.UsageError("Partition only supported for PostgreSQL databases.")

    if len(args) == 1 and args[0] == 'DEFAULT':
        partition_name = DEFAULT_PARTITION_NAME
        values = 'DEFAULT'
    elif len(args) == 2:
        try:
            since = date(int(args[0]), int(args[1]), 1)
        except ValueError as error:
            raise click.BadArgumentUsage('Provided arguments are not valid year and month.') from error
        partition_name = PARTITION_NAME.format(year=since.year, month=since.month)
        to = since + relativedelta(months=1)
        values = "FOR VALUES FROM ('{since}') TO ('{to}')".format(since=since, to=to)
    else:
        raise click.BadArgumentUsage('Invalid number of arguments.')

    if operation == 'drop':
        _run_sqls(_get_drop_sqls(partition_name), dry_run)
    else:
        assert operation == 'create'  # noqa: S101
        _run_sqls(_get_create_sqls(partition_name, values), dry_run)


if __name__ == '__main__':
    main()
