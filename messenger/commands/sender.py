#!/usr/bin/python3
"""Send all pending messages."""
import logging
from datetime import datetime
from time import sleep
from typing import Collection, Dict, Iterable, Optional, Sequence, cast

import click
from pytz import UTC
from setapp.constants import Verbosity
from sqlalchemy import Column, and_, or_
from sqlalchemy.sql import ColumnElement, func, select

from messenger.backends.base import BaseBackend
from messenger.commands.utils import echo, messenger_command, method_option
from messenger.constants import MessageStatus, TransportMethod
from messenger.database import atomic, get_engine
from messenger.settings import APPLICATION
from messenger.utils import load_instance

_LOGGER = logging.getLogger(__name__)


class SenderManager:
    """Loads and manages senders."""

    def __init__(self) -> None:
        self.senders: Dict[TransportMethod, Dict[str, BaseBackend]] = {}
        self.load()

    def load(self) -> None:
        """Load senders from settings."""
        for name, sender_settings in APPLICATION.settings.senders.items():
            if 'backend' not in sender_settings:
                raise ValueError("'{}' has no backend defined.".format(name))

            filters = []
            for filter_setting in sender_settings.get('filters', ()):
                filter_setting = filter_setting.copy()
                filter_cls = filter_setting.pop('class', None)
                if filter_cls is None:
                    raise ValueError("Filter has no class defined.")
                try:
                    filters.append(load_instance(filter_cls, **filter_setting))
                except Exception as error:
                    raise ValueError("Error in loading filter: {}".format(error)) from error
            try:
                sender = load_instance(sender_settings['backend'], name=name, **sender_settings.get('options', {}),
                                       filters=filters)
            except ImportError as error:
                raise ValueError('Error in loading sender {}: {}'.format(name, error)) from error

            method_senders = self.senders.setdefault(sender.method, {})
            method_senders[name] = sender

    def __getitem__(self, method: TransportMethod) -> Collection[BaseBackend]:
        """Return all backends for a transport method."""
        return self.senders.get(method, {}).values()

    def get(self, method: TransportMethod, name: Optional[str] = None) -> BaseBackend:
        """Return sender based on transport method and name.

        Return the first one found, if name is not provided.

        Raises:
            KeyError: If sender is not found.
        """
        if self.senders.get(method) is None:
            raise KeyError('No senders found for {}s.'.format(method.value))
        method_senders = self.senders[method]
        # If the sender is not defined, take the first one.
        if name is None:
            name = next(iter(method_senders))
        if name not in method_senders:
            raise KeyError("'{}' sender not found.".format(name))
        return method_senders[name]


def _send(method: TransportMethod, where: ColumnElement, senders: Collection[BaseBackend]) -> bool:
    """Send a single message specified by where condition using one of the senders.

    Args:
        method: The transport method.
        where: An expression to filter the message.
        senders: A collection of possible senders.

    Returns:
        True if the send was attempted, False if message was not found or skipped because of filters.
    """
    with atomic() as session:
        model = session.query(method.model_cls).filter(where).with_for_update(skip_locked=True).one_or_none()
        if model is None:
            # Message not found.
            return False
        for sender in senders:
            if sender.filter(model):
                sender.open()
                sender.send(model)
                return True
        _LOGGER.warning("Message %s is skipped, because it didn't match any filters.", str(model))
        return False


def _send_batch(method: TransportMethod, senders: Collection[BaseBackend], statuses: Iterable[MessageStatus]) -> None:
    """Try to send all unsent messages.

    Args:
        method: The transport method.
        senders: A collection of possible senders.
        statuses: An iterable of processed message statuses.
    """
    sent = 0
    skipped = 0
    # mypy gets confused by lru_cache, see https://github.com/python/mypy/issues/5858
    model_cls = method.model_cls
    where_statuses = cast(Column, model_cls.status).in_(statuses)

    # Prepare basic filter for the _send function.
    # Filter by statuses to skip message which was being sent by another process while the UUID query runs.
    # See #84 for details.
    # Filter by update datetime to prevent message, which fails to be send, to be processed multiple times
    # in parallel runs.
    now = datetime.now(UTC)
    where = and_(where_statuses, or_(cast(Column, model_cls.update_datetime) <= now,
                                     cast(Column, model_cls.update_datetime).is_(None)))

    # Query all message UUIDs only by status.
    # This is a pre-selection, so no transaction.
    uuid_query = select(cast(Column, model_cls.uuid)).where(where_statuses)
    # Round-robin messages by type.
    uuid_query = uuid_query.order_by(func.row_number().over(partition_by=model_cls.type))
    with get_engine().begin() as connection:
        for result in connection.execute(uuid_query):
            uuid = result[0]
            processed = _send(
                method,
                and_(model_cls.uuid == uuid, where),
                senders=senders,
            )
            if processed:
                sent += 1
                echo('.', Verbosity.DETAIL, nl=False)
            else:
                skipped += 1
                echo('s', Verbosity.DETAIL, nl=False)
    # Print newline between progress and summary.
    echo('', Verbosity.DETAIL)
    echo('{}s: {} processed, {} skipped.'.format(method.value.capitalize(), sent, skipped), Verbosity.INFO,
         log=_LOGGER.info)
    for sender in senders:
        sender.close()


def _get_senders(method: TransportMethod, manager: SenderManager, sender: Optional[str]) -> Collection[BaseBackend]:
    """Return possible senders.

    Arguments:
        method: The transport method.
        manager: The sender manager.
        sender: The --sender value.
    """
    if sender:
        try:
            return (manager.get(method, sender), )
        except Exception as error:
            # Print nicer error message using click.
            raise click.ClickException(str(error)) from error
    else:
        senders = manager[method]
        if not senders:
            raise click.ClickException("No senders found for {}s.".format(method.value))
        return senders


@messenger_command()
@method_option
@click.option('--retry-failed', default=False, is_flag=True, help="Retry to send the messages with failed status.")
@click.option('--retry-errors', default=False, is_flag=True, help="Retry to send the messages with error status.")
@click.option('--loop', default=False, is_flag=True, help="Run in infinite loop.")
@click.option('--loop-delay', default=300, show_default=True, help="Set delay in seconds between batches in loop mode.")
@click.option('--sender', help="Set custom sender to be used. Single message type must be selected.")
@click.option('--fileman-netloc', help="Set custom host and port of a fileman service.")
def main(methods: Sequence[TransportMethod], retry_failed: bool, retry_errors: bool, loop: bool, loop_delay: int,
         sender: Optional[str], fileman_netloc: str) -> None:
    """Send all pending messages."""
    if fileman_netloc:
        APPLICATION.settings.fileman_netloc = fileman_netloc

    if sender is not None and len(methods) > 1:
        raise click.UsageError("Sender can only be used with single type.")

    try:
        manager = SenderManager()
    except Exception as error:
        # Print nicer error message using click.
        raise click.ClickException(str(error)) from error

    statuses = [MessageStatus.PENDING]
    if retry_failed:
        statuses.append(MessageStatus.FAILED)
    if retry_errors:
        statuses.append(MessageStatus.ERROR)

    while True:
        for method in methods:
            senders = _get_senders(method, manager, sender)
            _send_batch(method, senders, statuses)
        if not loop:
            break
        sleep(loop_delay)


if __name__ == '__main__':
    main()
