#!/usr/bin/python3
"""Delete old messages that are not to be archived."""
import logging
from datetime import datetime, timedelta
from typing import Sequence, cast

import click
from pytz import UTC
from setapp.constants import Verbosity
from sqlalchemy import Column
from sqlalchemy_utils import get_type

from messenger.commands.utils import echo, messenger_command, method_option
from messenger.constants import MessageStatus, TransportMethod
from messenger.database import atomic
from messenger.models import Record
from messenger.settings import APPLICATION

_LOGGER = logging.getLogger(__name__)


@messenger_command()
@method_option
@click.option('--days', type=int, help="Set custom number of days before message deletion.")
def main(methods: Sequence[TransportMethod], days: int) -> None:
    """Delete old messages that are not to be archived."""
    if days is not None:
        APPLICATION.settings.clean_days = days
    for method in methods:
        with atomic() as session:
            # mypy gets confused by lru_cache, see https://github.com/python/mypy/issues/5858
            model_cls = cast(Record, method.model_cls)
            reference_cls = get_type(model_cls.references)
            cutoff = datetime.now(UTC) - timedelta(days=APPLICATION.settings.clean_days)
            query = session.query(method.model_cls).filter(cast(Column, model_cls.create_datetime) <= cutoff)
            # Delete error messages
            error_query = query.filter_by(status=MessageStatus.ERROR)
            session.query(reference_cls).filter(
                reference_cls.message_id.in_(error_query.with_entities(model_cls.id))
            ).delete(synchronize_session=False)
            deleted = error_query.delete()
            # Delete messages that are not to be archived
            unarchived_query = query.filter_by(archive=False).filter(
                cast(Column, model_cls.status) != MessageStatus.PENDING)
            session.query(reference_cls).filter(
                reference_cls.message_id.in_(unarchived_query.with_entities(model_cls.id))
            ).delete(synchronize_session=False)
            deleted += unarchived_query.delete()
        echo('Deleted {} {}s.'.format(deleted, method), Verbosity.INFO, log=_LOGGER.info)


if __name__ == '__main__':
    main()
