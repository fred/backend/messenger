"""Command utilities."""
from datetime import datetime
from functools import wraps
from typing import Any, Callable, Optional, Sequence, Tuple, TypeVar, Union, cast

import click
from click import BadOptionUsage, echo as _echo
from click.core import ParameterSource
from pytz import UTC
from setapp.constants import Verbosity
from typing_extensions import Literal

import messenger
from messenger.constants import TransportMethod
from messenger.settings import APPLICATION


def echo(message: str, verbosity: Verbosity, *, nl: bool = True, err: bool = False,
         log: Optional[Callable[[str], None]] = None) -> None:
    """Print message with respect to verbosity level.

    Arguments:
        message: The message to print.
        verbosity: Verbosity of the message.
        nl: Whether to print newline after message.
        err: Whether to print the message to error output instead.
        log: Callable to log the message as well.
    """
    if verbosity <= APPLICATION.verbosity:
        _echo(message, nl=nl, err=err)
    if log is not None:
        log(message)


F = TypeVar('F', bound=Callable[..., Any])


def messenger_command(command: Callable = click.command) -> Callable:
    """Turn function into a messenger command.

    Arguments:
        command: A click `command` callable to attach command to a group.
    """
    def decorator(func: F) -> F:
        # This decorator turns the function into a click command. To keep a reasonable order of options, that is
        # end usage with --connection, --config, --verbosity, --version and --help in this order,
        # we need to keep this order of click decorators.
        @click.version_option(version=messenger.__version__, message="fred-messenger %(version)s")
        @click.option('-v', '--verbosity', type=int, default=Verbosity.INFO, show_default=True,
                      help="Set verbosity level.")
        @click.option('--config', type=click.Path(exists=True, dir_okay=False), help="Set custom config file.")
        @click.option('--connection', help="Set custom database connection string.")
        @command()
        @wraps(func)
        def wrapped(*, config: str, connection: str, verbosity: int, **kwargs: Any) -> Any:
            APPLICATION.load(config)
            APPLICATION.verbosity = cast(Verbosity, verbosity)
            if connection:
                APPLICATION.settings.db_connection = connection
            return func(**kwargs)

        return cast(F, wrapped)
    return decorator


_MethodsType = Sequence[Union[Literal['all'], TransportMethod]]


def _parse_methods(ctx: click.Context, param: click.Parameter, value: _MethodsType) -> Tuple[TransportMethod, ...]:
    """Return selected collection of transport methods."""
    if 'all' in value:
        return tuple(TransportMethod)
    else:
        # Only TransportMethod values and 'all' are allowed choices.
        assert all(isinstance(v, TransportMethod) for v in value)  # noqa: S101
        return tuple(cast(Sequence[TransportMethod], value))


WrappedType = TypeVar('WrappedType', bound=Callable)


def _merge_methods(func: WrappedType) -> WrappedType:
    @click.pass_context
    @wraps(func)
    def wrapper(ctx: click.Context, *args: Any, types: Optional[_MethodsType], methods: _MethodsType, **kwargs: Any,
                ) -> Any:
        if ctx.get_parameter_source("types") == ParameterSource.COMMANDLINE:
            if ctx.get_parameter_source("methods") == ParameterSource.COMMANDLINE:
                raise BadOptionUsage("types", "Options --type and --method can't be both set.", ctx)
            else:
                echo("Option --type is deprecated, use --method instead.", verbosity=Verbosity.SILENT, err=True)
        return func(*args, methods=types or methods, **kwargs)
    return cast(WrappedType, wrapper)


def method_option(func: WrappedType) -> WrappedType:
    """Add --type option to a command."""
    # Use `methods` as the name of the variable.
    option = click.option(
        '--method', 'methods', type=click.Choice(('all', ) + tuple(TransportMethod)), multiple=True, default=('all', ),
        show_default=True, callback=_parse_methods,
        help="Run command only on messages with selected transport method. May be set multiple times.")
    type_option = click.option(
        '--type', 'types', type=click.Choice(('all', ) + tuple(TransportMethod)), multiple=True, default=None,
        callback=_parse_methods, help="Alias for --method. Deprecated.")
    return option(type_option(_merge_methods(func)))


def _make_aware_callback(ctx: click.Context, param: click.Parameter, value: Optional[datetime]) -> Optional[datetime]:
    """Make the datetime aware."""
    if value is None:
        return value
    return UTC.localize(value)


def limits_options(func: Callable) -> Callable:
    """Add --from and --to options to a command."""
    from_option = click.option(
        '--from', 'from_datetime', type=click.DateTime(), callback=_make_aware_callback,
        help="Set minimal date and time limit for messages in UTC timezone.")
    to_option = click.option(
        '--to', 'to_datetime', type=click.DateTime(), callback=_make_aware_callback,
        help="Set maximal date and time limit for messages in UTC timezone.")
    return from_option(to_option(func))
