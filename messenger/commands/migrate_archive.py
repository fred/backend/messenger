#!/usr/bin/python3
"""Migrate archive messages."""
import json
import logging
import re
import sys
from datetime import datetime
from email.message import EmailMessage
from enum import Enum, unique
from functools import lru_cache
from typing import Any, Dict, Optional, Sequence as SequenceType, Set, Type, Union, cast
from uuid import UUID

import click
from backports.strenum import StrEnum
from pytz import UTC
from setapp.constants import Verbosity
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    ForeignKeyConstraint,
    Integer,
    MetaData,
    Sequence,
    Table,
    Text,
    and_,
    create_engine,
    null,
    or_,
)
from sqlalchemy.engine import Connection, Engine, Row
from sqlalchemy.orm import Session
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import LABEL_STYLE_TABLENAME_PLUS_COL, Select, func, literal, select
from sqlalchemy.sql.expression import ValuesBase
from sqlalchemy_utils import JSONType, UUIDType, get_type

from messenger.commands.utils import echo, limits_options, messenger_command, method_option
from messenger.constants import ArchiveMessageStatus, MessageStatus, ReferenceType, TransportMethod
from messenger.database import atomic
from messenger.models import (
    ArchiveEmail,
    ArchiveLetter,
    ArchiveSms,
    Email,
    EmailReference,
    Letter,
    LetterReference,
    Sms,
    SmsReference,
)
from messenger.models.base import SQLITE_SEQUENCE, BaseModel
from messenger.settings import APPLICATION

_LOGGER = logging.getLogger(__name__)
FRED_METADATA = MetaData()
MAIL_TYPE = Table(
    'mail_type', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('name', Text),
)
MAIL_HEADER = Table(
    'mail_header_default', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('h_from', Text),
    Column('h_replyto', Text),
    Column('h_errorsto', Text),
    Column('h_organization', Text),
)
MAIL_TEMPLATE = Table(
    'mail_template', FRED_METADATA,
    Column('mail_type_id', ForeignKey('mail_type.id'), primary_key=True),
    Column('version', Integer, primary_key=True),
    Column('mail_header_default_id', ForeignKey('mail_header_default.id')),
)
MAIL_TEMPLATE_MIGRATION = Table(
    'mail_template_messenger_migration', FRED_METADATA,
    Column('mail_type_id', ForeignKey('mail_type.id'), primary_key=True),
    Column('version', Integer, primary_key=True),
    Column('subject', Text(), nullable=False),
    Column('subject_uuid', UUIDType(binary=False), nullable=False),
    Column('body', Text(), nullable=False),
    Column('body_uuid', UUIDType(binary=False), nullable=False),
    ForeignKeyConstraint(['mail_type_id', 'version'],
                         ['mail_template.mail_type_id', 'mail_template.version']),
)
MAIL_ARCHIVE = Table(
    'mail_archive', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('uuid', UUIDType(binary=False), nullable=False, unique=True),
    Column('crdate', DateTime(timezone=False)),
    Column('moddate', DateTime(timezone=False)),
    Column('status', Integer),
    Column('attempt', Integer, nullable=False),
    Column('mail_type_id'),
    Column('mail_template_version'),
    Column('message_params', JSONType(), nullable=False),
    Column('response_header', JSONType()),
    ForeignKeyConstraint(['mail_type_id', 'mail_template_version'],
                         ['mail_template.mail_type_id', 'mail_template.version']),
)
MAIL_ATTACHMENTS = Table(
    'mail_attachments', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('mailid', ForeignKey('mail_archive.id')),
    Column('attachid', ForeignKey('files.id')),
)
ENUM_SEND_STATUS = Table(
    'enum_send_status', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('status_name', Text),
)
MESSAGE_TYPE = Table(
    'message_type', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('type', Text),
)
MESSAGE_ARCHIVE = Table(
    'message_archive', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('uuid', UUIDType(binary=False), nullable=False, unique=True),
    Column('crdate', DateTime(timezone=False)),
    Column('moddate', DateTime(timezone=False)),
    Column('status_id', ForeignKey('enum_send_status.id')),
    Column('attempt', Integer, nullable=False),
    Column('message_type_id', ForeignKey('message_type.id')),
    Column('service_handle', Text),
)
SMS_ARCHIVE = Table(
    'sms_archive', FRED_METADATA,
    Column('id', ForeignKey('message_archive.id'), primary_key=True),
    Column('phone_number', Text),
    Column('content', Text),
)
LETTER_ARCHIVE = Table(
    'letter_archive', FRED_METADATA,
    Column('id', ForeignKey('message_archive.id'), primary_key=True),
    Column('file_id', ForeignKey('files.id')),
    Column('postal_address_name', Text),
    Column('postal_address_organization', Text),
    Column('postal_address_street1', Text),
    Column('postal_address_street2', Text),
    Column('postal_address_street3', Text),
    Column('postal_address_city', Text),
    Column('postal_address_stateorprovince', Text),
    Column('postal_address_postalcode', Text),
    Column('postal_address_country', Text),
    Column('batch_id', Text),
)
FILES = Table(
    'files', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('uuid', UUIDType(binary=False), nullable=False, unique=True),
)
EMAIL_RECIPIENT_DELIMITER = re.compile(r', *| +')


@unique
class FredEmailStatus(int, Enum):
    """Enum with email status in FRED database."""

    SENT = (0, MessageStatus.SENT)
    PENDING = (1, MessageStatus.PENDING)
    UNDELIVERED = (4, MessageStatus.UNDELIVERED)

    status: MessageStatus

    def __new__(cls, value: int, status: MessageStatus) -> 'FredEmailStatus':
        """Construct item with status."""
        obj = super().__new__(cls, value)
        obj._value_ = value

        obj.status = status
        return obj


@unique
class FredMessageStatus(StrEnum):
    """Enum with message status in FRED database."""

    PENDING = ('ready', 1, MessageStatus.PENDING)
    FAILED = ('send_failed', 2, MessageStatus.FAILED)
    CANCELED = ('no_processing', 3, MessageStatus.CANCELED)
    SENT = ('sent', 5, MessageStatus.SENT)
    UNDELIVERED = ('undelivered', 7, MessageStatus.UNDELIVERED)

    id: int
    status: MessageStatus

    def __new__(cls, value: str, id: int, status: MessageStatus) -> 'FredMessageStatus':
        """Construct item with status."""
        obj = str.__new__(cls, value)
        obj._value_ = value

        obj.id = id
        obj.status = status
        return obj


MESSAGE_TYPE_MONITORING = 'monitoring'
# A heuristics to match SMS content to templates.
# All templates have two versions, which differ either by language or age.
# A regex is used to match which version should be used (second key in SMS template map).
SMS_TEMPLATE_REGEX_MAP = {
    'mojeid_pin2': re.compile('zaslan e-mailem'),
    'mojeid_sms_change': re.compile('^Confirm'),
    'contact_verification_pin2': re.compile('Vam byl'),
    'mojeid_sms_password_reset': re.compile('^Confirm'),
    'mojeid_sms_two_factor_reset': re.compile('PIN1 and PIN2'),
}  # type: Dict[str, Any] # re.Pattern is available since 3.7
SMS_TEMPLATE_MAP = {
    # (message type, regex match): (template name, template uuid)
    ('mojeid_pin2', False): ('mojeid-pin2-cs.txt', UUID('0efb1bb2-8a15-492c-8dfa-42f528198b76')),
    ('mojeid_pin2', True): ('mojeid-pin2-cs.txt', UUID('44b8fa55-dd11-4658-9cd3-bf80f0cb7fde')),
    ('mojeid_sms_change', True): ('mojeid-sms-change-en.txt', UUID('b1d3e4f3-d03d-4755-87c3-ffc86a24587b')),
    ('mojeid_sms_change', False): ('mojeid-sms-change-cs.txt', UUID('9a0ea845-e607-441a-b114-d829a41dca07')),
    ('contact_verification_pin2', False): ('contact-verification-pin2-cs.txt',
                                           UUID('99511759-64fd-4df3-b95a-25841713828f')),
    ('contact_verification_pin2', True): ('contact-verification-pin2-cs.txt',
                                          UUID('739a4d8e-bee3-4f0d-a5be-14c4fe7a2cd8')),
    ('mojeid_sms_password_reset', True): ('mojeid-sms-password-reset-en.txt',
                                          UUID('1219771a-20c0-4d16-b068-2892f16d00ab')),
    ('mojeid_sms_password_reset', False): ('mojeid-sms-password-reset-cs.txt',
                                           UUID('36e22b1f-9747-4e76-9a1b-89d3baebe2a0')),
    ('mojeid_sms_two_factor_reset', True): ('mojeid-sms-two-factor-reset-en.txt',
                                            UUID('43b02a6d-7327-46b5-b5ac-c06ffdeae996')),
    ('mojeid_sms_two_factor_reset', False): ('mojeid-sms-two-factor-reset-cs.txt',
                                             UUID('1de06f5c-877d-4bdc-bcfe-1f7a02a7eec8')),
}
# And a regex for match PIN from SMS content to context.
SMS_MESSAGE_REGEX = re.compile(r':\s+(?P<pin>[^\s]+)')


# Reference tables
ENUM_OBJECT_TYPE = Table(
    'enum_object_type', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('name', Text, nullable=False),
)
# We skip the 'object' and 'object_history' table, since it's not important for migrations.
OBJECT_REGISTRY = Table(
    'object_registry', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('type', ForeignKey('enum_object_type.id'), nullable=False),
    Column('uuid', UUIDType(binary=False), nullable=False, unique=True),
)
HISTORY = Table(
    'history', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('valid_from', DateTime(timezone=False), nullable=False, server_default=func.now()),
    Column('valid_to', DateTime(timezone=False)),
)
CONTACT_HISTORY = Table(
    'contact_history', FRED_METADATA,
    Column('historyid', ForeignKey('history.id'), primary_key=True),
    Column('id', ForeignKey('object_registry.id')),
)
DOMAIN_HISTORY = Table(
    'domain_history', FRED_METADATA,
    Column('historyid', ForeignKey('history.id'), primary_key=True),
    Column('id', ForeignKey('object_registry.id')),
    Column('registrant', ForeignKey('object_registry.id')),
    Column('nsset', ForeignKey('object_registry.id')),
)
DOMAIN_CONTACT_MAP_HISTORY = Table(
    'domain_contact_map_history', FRED_METADATA,
    Column('historyid', ForeignKey('history.id'), primary_key=True),
    Column('domainid', ForeignKey('object_registry.id'), primary_key=True),
    Column('contactid', ForeignKey('object_registry.id'), primary_key=True),
)
NSSET_HISTORY = Table(
    'nsset_history', FRED_METADATA,
    Column('historyid', ForeignKey('history.id'), primary_key=True),
    Column('id', ForeignKey('object_registry.id')),
)
NSSET_CONTACT_MAP_HISTORY = Table(
    'nsset_contact_map_history', FRED_METADATA,
    Column('historyid', ForeignKey('history.id'), primary_key=True),
    Column('nssetid', ForeignKey('object_registry.id'), primary_key=True),
    Column('contactid', ForeignKey('object_registry.id'), primary_key=True),
)
KEYSET_CONTACT_MAP_HISTORY = Table(
    'keyset_contact_map_history', FRED_METADATA,
    Column('historyid', ForeignKey('history.id'), primary_key=True),
    Column('keysetid', ForeignKey('object_registry.id'), primary_key=True),
    Column('contactid', ForeignKey('object_registry.id'), primary_key=True),
)
OBJECT_STATE = Table(
    'object_state', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('object_id', ForeignKey('object_registry.id'), nullable=False),
    Column('ohid_from', ForeignKey('history.id')),
    # Real database doesn't have default, but we don't created it from this structure and it helps with tests.
    Column('valid_from', DateTime(timezone=False), nullable=False, server_default=func.now()),
)
NOTIFY_STATE_CHANGE = Table(
    'notify_statechange', FRED_METADATA,
    Column('state_id', ForeignKey('object_state.id'), primary_key=True),
    Column('mail_id', ForeignKey('mail_archive.id')),
)
WARNING_MAIL_TYPE = 'expiration_dns_warning_owner'
TECHNICAL_MAIL_TYPES = (
    'expiration_dns_tech',
    'expiration_register_tech',
    'notification_unused',
)
ADMIN_MAIL_TYPES = (
    'expiration_dns_owner',
    'expiration_notify',
    'expiration_register_owner',
    'expiration_validation',
    'expiration_validation_before',
    'notification_unused',
)
STATE_CHANGE_MAIL_TYPES = tuple(set(ADMIN_MAIL_TYPES + TECHNICAL_MAIL_TYPES + (WARNING_MAIL_TYPE, )))
TECHCHECK_MAIL_TYPE = 'techcheck'
PUBLIC_REQUEST = Table(
    'public_request', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('answer_email_id', ForeignKey('mail_archive.id')),
)
PUBLIC_REQUEST_OBJECTS_MAP = Table(
    'public_request_objects_map', FRED_METADATA,
    Column('request_id', ForeignKey('public_request.id'), primary_key=True),
    Column('object_id', ForeignKey('object_registry.id')),
)
PUBLIC_REQUEST_MESSAGES_MAP = Table(
    'public_request_messages_map', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('public_request_id', ForeignKey('public_request.id')),
    Column('mail_archive_id', ForeignKey('mail_archive.id')),
)
REGISTRAR = Table(
    'registrar', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('handle', Text, nullable=False),
    Column('email', Text),
)
INVOICE = Table(
    'invoice', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('registrar_id', ForeignKey('registrar.id'), nullable=False),
)
INVOICE_MAILS = Table(
    'invoice_mails', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('invoiceid', ForeignKey('invoice.id')),
    Column('mailid', ForeignKey('mail_archive.id'), nullable=False),
)
REMINDER_CONTACT_MESSAGE_MAP = Table(
    'reminder_contact_message_map', FRED_METADATA,
    Column('contact_id', ForeignKey('object_registry.id'), primary_key=True),
    Column('message_id', ForeignKey('mail_archive.id'), primary_key=True, nullable=True),
)
CONTACT_CHECK = Table(
    'contact_check', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('contact_history_id', ForeignKey('contact_history.historyid'), nullable=False),
)
CONTACT_CHECK_MESSAGE_MAP = Table(
    'contact_check_message_map', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('contact_check_id', ForeignKey('contact_check.id'), nullable=False),
    Column('mail_archive_id', ForeignKey('mail_archive.id')),
)
CHECK_NSSET = Table(
    'check_nsset', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('nsset_hid', ForeignKey('nsset_history.historyid')),
)
MESSAGE_CONTACT_HISTORY_MAP = Table(
    'message_contact_history_map', FRED_METADATA,
    Column('id', Integer, primary_key=True),
    Column('contact_object_registry_id', ForeignKey('object_registry.id')),
    Column('message_archive_id', ForeignKey('message_archive.id')),
)


@lru_cache
def get_fred_engine() -> Engine:
    """Return FRED engine as defined by settings."""
    assert APPLICATION.settings.fred_db_connection is not None  # noqa: S101
    engine = create_engine(APPLICATION.settings.fred_db_connection, echo=APPLICATION.settings.db_echo)
    if engine.dialect.name != 'sqlite':
        engine.update_execution_options(isolation_level='REPEATABLE READ')
    return engine


ID_SEQUENCES = {
    Email: Sequence('messenger_email_id_seq'),
    EmailReference: Sequence('messenger_emailreference_id_seq'),
    Sms: Sequence('messenger_sms_id_seq'),
    SmsReference: Sequence('messenger_smsreference_id_seq'),
    Letter: Sequence('messenger_letter_id_seq'),
    LetterReference: Sequence('messenger_letterreference_id_seq'),
}  # type: Dict[Type[BaseModel], Sequence]


def get_next_id(session: Session, model: Type[BaseModel]) -> int:
    """Return a next id for an archive message from the ID sequence."""
    if cast(Engine, session.bind).dialect.name == 'sqlite':
        # SQLite doesn't have a sequence object, so we need to get next ID manually.
        # Get the last ID
        curval_query = select(SQLITE_SEQUENCE.columns.seq).where(SQLITE_SEQUENCE.columns.name == model.__tablename__)
        result = cast(Optional[int], session.execute(curval_query).scalar())
        if result is None:
            result = 1
            # Insert the sequence
            query: ValuesBase = SQLITE_SEQUENCE.insert()
            session.execute(query.values(seq=result, name=model.__tablename__))
        else:
            # Increase by 1
            result = result + 1
            # Update the sequence
            query = SQLITE_SEQUENCE.update().where(SQLITE_SEQUENCE.columns.name == model.__tablename__)
            session.execute(query.values(seq=result))
        return result
    # Expect sequence object will work otherwise.
    return cast(int, ID_SEQUENCES[model].next_value())


def _apply_limits(
        query: Select, table: Table, from_datetime: Optional[datetime], to_datetime: Optional[datetime]) -> Select:
    """Apply datetime limits to query."""
    crdate_filter = None
    moddate_filter = None
    if from_datetime:
        crdate_filter = table.columns.crdate >= from_datetime
        moddate_filter = table.columns.moddate >= from_datetime
    if to_datetime:
        if crdate_filter is not None:
            assert moddate_filter is not None  # noqa: S101
            crdate_filter = and_(crdate_filter, table.columns.crdate < to_datetime)
            moddate_filter = and_(moddate_filter, table.columns.moddate < to_datetime)
        else:
            crdate_filter = table.columns.crdate < to_datetime
            moddate_filter = table.columns.moddate < to_datetime
    if crdate_filter is not None:
        assert moddate_filter is not None  # noqa: S101
        query = query.where(or_(crdate_filter, moddate_filter))
    return query


def _append_references(query: Select, connection: Connection, references: Dict[ReferenceType, Set[str]]) -> None:
    """Run query and append results to references."""
    for row in connection.execute(query):
        values = references.setdefault(ReferenceType(row[0]), set())
        values.add(str(row[1]))


def _append_state_change_email_references(mail_id: int, mail_type: str, connection: Connection,
                                          references: Dict[ReferenceType, Set[str]]) -> None:
    """Collect state change related emails and append to references."""
    # Common select for queries using 'notify_state_change'
    select_state_change = select(ENUM_OBJECT_TYPE.columns.name, OBJECT_REGISTRY.columns.uuid)
    select_state_change = select_state_change.where(NOTIFY_STATE_CHANGE.columns.mail_id == mail_id)

    # Subject which changed state
    state_query_joins = NOTIFY_STATE_CHANGE.join(OBJECT_STATE).join(OBJECT_REGISTRY).join(ENUM_OBJECT_TYPE)
    state_query = select_state_change.select_from(state_query_joins)
    _append_references(state_query, connection, references)

    if mail_type == WARNING_MAIL_TYPE:
        return

    notify_joins = NOTIFY_STATE_CHANGE.join(OBJECT_STATE).join(HISTORY)

    # Registrant of a subject which changed state
    registrant_joins = notify_joins.join(DOMAIN_HISTORY).join(
        OBJECT_REGISTRY, DOMAIN_HISTORY.columns.registrant == OBJECT_REGISTRY.columns.id)
    registrant_query = select_state_change.select_from(registrant_joins.join(ENUM_OBJECT_TYPE))
    _append_references(registrant_query, connection, references)

    if mail_type in ADMIN_MAIL_TYPES:
        # Admin of a subject which changed state
        admin_joins = notify_joins.join(DOMAIN_CONTACT_MAP_HISTORY).join(
            OBJECT_REGISTRY, DOMAIN_CONTACT_MAP_HISTORY.columns.contactid == OBJECT_REGISTRY.columns.id)
        admin_query = select_state_change.select_from(admin_joins.join(ENUM_OBJECT_TYPE))
        _append_references(admin_query, connection, references)

    if mail_type in TECHNICAL_MAIL_TYPES:
        # Technical contact of a nsset which changed state
        tech_nsset_joins = notify_joins.join(NSSET_CONTACT_MAP_HISTORY).join(
            OBJECT_REGISTRY, NSSET_CONTACT_MAP_HISTORY.columns.contactid == OBJECT_REGISTRY.columns.id)
        tech_nsset_query = select_state_change.select_from(tech_nsset_joins.join(ENUM_OBJECT_TYPE))
        _append_references(tech_nsset_query, connection, references)

        # Technical contact of a keyset which changed state
        tech_keyset_joins = notify_joins.join(KEYSET_CONTACT_MAP_HISTORY).join(
            OBJECT_REGISTRY, KEYSET_CONTACT_MAP_HISTORY.columns.contactid == OBJECT_REGISTRY.columns.id)
        tech_keyset_query = select_state_change.select_from(tech_keyset_joins.join(ENUM_OBJECT_TYPE))
        _append_references(tech_keyset_query, connection, references)

        # Technical contact of a domain which changed state
        n_history = HISTORY.alias('n_history')
        nsset_history_on = and_(
            NSSET_HISTORY.columns.historyid == n_history.columns.id,
            n_history.columns.valid_from <= OBJECT_STATE.columns.valid_from,
            or_(n_history.columns.valid_to > OBJECT_STATE.columns.valid_from, n_history.columns.valid_to.is_(None)))
        tech_domain_joins = notify_joins.join(DOMAIN_HISTORY).join(
            NSSET_HISTORY, DOMAIN_HISTORY.columns.nsset == NSSET_HISTORY.columns.id)
        tech_domain_joins = tech_domain_joins.join(n_history, nsset_history_on).join(NSSET_CONTACT_MAP_HISTORY).join(
            OBJECT_REGISTRY, NSSET_CONTACT_MAP_HISTORY.columns.contactid == OBJECT_REGISTRY.columns.id)
        tech_domain_query = select_state_change.select_from(tech_domain_joins.join(ENUM_OBJECT_TYPE))
        _append_references(tech_domain_query, connection, references)


def _get_email_references(mail_id: int, mail_type: str, recipient: str, context: Dict,
                          connection: Connection) -> Dict[ReferenceType, Set[str]]:
    """Return all known references to an email."""
    references = {}  # type: Dict[ReferenceType, Set[str]]

    if mail_type in STATE_CHANGE_MAIL_TYPES:
        _append_state_change_email_references(mail_id, mail_type, connection, references)

    select_object = select(ENUM_OBJECT_TYPE.columns.name, OBJECT_REGISTRY.columns.uuid)
    public_joins = PUBLIC_REQUEST.join(PUBLIC_REQUEST_OBJECTS_MAP).join(OBJECT_REGISTRY).join(ENUM_OBJECT_TYPE)

    public_answer_query = select_object.select_from(public_joins).where(
        PUBLIC_REQUEST.columns.answer_email_id == mail_id)
    _append_references(public_answer_query, connection, references)

    public_message_query = select_object.select_from(public_joins.join(PUBLIC_REQUEST_MESSAGES_MAP)).where(
        PUBLIC_REQUEST_MESSAGES_MAP.columns.mail_archive_id == mail_id)
    _append_references(public_message_query, connection, references)

    select_registrar = select(literal(ReferenceType.REGISTRAR), REGISTRAR.columns.handle)
    invoice_joins = INVOICE_MAILS.join(INVOICE).join(REGISTRAR)
    invoice_query = select_registrar.select_from(invoice_joins).where(INVOICE_MAILS.columns.mailid == mail_id)
    _append_references(invoice_query, connection, references)

    # Only use email matching if direct link through invoice isn't available.
    no_invoice_links = select(INVOICE_MAILS.columns.id).where(INVOICE_MAILS.columns.mailid == mail_id).where(
        INVOICE_MAILS.columns.invoiceid.is_(None))
    if connection.execute(no_invoice_links).first():
        registrar_query = select_registrar.where(REGISTRAR.columns.email == recipient)
        _append_references(registrar_query, connection, references)

    reminder_joins = REMINDER_CONTACT_MESSAGE_MAP.join(OBJECT_REGISTRY).join(ENUM_OBJECT_TYPE)
    reminder_query = select_object.select_from(reminder_joins).where(
        REMINDER_CONTACT_MESSAGE_MAP.columns.message_id == mail_id)
    _append_references(reminder_query, connection, references)

    check_joins = CONTACT_CHECK_MESSAGE_MAP.join(CONTACT_CHECK).join(CONTACT_HISTORY).join(OBJECT_REGISTRY)
    check_query = select_object.select_from(check_joins.join(ENUM_OBJECT_TYPE)).where(
        CONTACT_CHECK_MESSAGE_MAP.columns.mail_archive_id == mail_id)
    _append_references(check_query, connection, references)

    check_id = context.get('ticket')
    if mail_type == TECHCHECK_MAIL_TYPE and check_id and check_id.isdigit():
        techcheck_joins = CHECK_NSSET.join(NSSET_HISTORY).join(OBJECT_REGISTRY).join(ENUM_OBJECT_TYPE)
        techcheck_query = select_object.select_from(techcheck_joins).where(CHECK_NSSET.columns.id == check_id)
        _append_references(techcheck_query, connection, references)

    return references


def _migrate_context(context: Optional[Dict]) -> Dict:
    if context is None:
        # Context is missing. May occur for old emails.
        return {}

    if 'keys' in context:
        keys = []
        for key in context['keys']:
            if not key:
                continue
            # Put keys in quotes and change brackets.
            key_json = re.sub('([a-z]+):', '"\\1":', key).replace('[', '{').replace(']', '}')
            keys.append(json.loads(key_json))
        context['keys'] = keys
    for key in ('type', 'otype', 'rtype', 'status'):
        if key in context:
            context[key] = int(context[key])
    disclose = context.get('fresh', {}).get('contact', {}).get('disclose')
    if disclose is not None:
        context['fresh']['contact']['disclose'] = {k: int(v) for k, v in disclose.items()}
    disclose_changes = context.get('changes', {}).get('contact', {}).get('disclose')
    if disclose_changes is not None:
        context['changes']['contact']['disclose'] = {k: {s: int(f) for s, f in v.items()}
                                                     for k, v in disclose_changes.items()}
    return context


def _migrate_extra_headers(headers: Dict[str, Any], row: Row) -> Dict[str, Any]:
    """Extract and migrate extra headers."""
    # Use EmailMessage as a handler for headers.
    message = EmailMessage()
    for key, value in headers.items():
        if key not in ('From', 'To', 'Message-ID'):
            message[key] = value
    if 'Reply-To' not in message:
        message['Reply-To'] = row.mail_header_default_h_replyto
    if 'Errors-To' not in message:
        message['Errors-To'] = row.mail_header_default_h_errorsto
    if 'Organization' not in message:
        message['Organization'] = row.mail_header_default_h_organization
    return dict(message.items())


def _localize_moddate(moddate: Optional[datetime]) -> Optional[datetime]:
    if not moddate:
        return None
    else:
        return UTC.localize(moddate)


def _migrate_email(row: Row, connection: Connection) -> bool:
    """Migrate single email, return whether email was actually migrated."""
    uuid = row.mail_archive_uuid
    status = FredEmailStatus(row.mail_archive_status).status  # type: ignore[call-arg]
    if status == MessageStatus.PENDING:
        model_cls: Union[Type[Email], Type[ArchiveEmail]] = Email
    else:
        model_cls = ArchiveEmail

    attach_query = select(FILES.columns.uuid).select_from(FILES.join(MAIL_ATTACHMENTS))
    attach_query = attach_query.where(MAIL_ATTACHMENTS.columns.mailid == row.mail_archive_id)
    attachments = [a for r in connection.execute(attach_query).fetchall() for a in r]
    with atomic() as session:
        try:
            email = cast(Union[Email, ArchiveEmail],
                         session.query(model_cls).with_for_update().filter_by(uuid=uuid).one())
        except NoResultFound:
            # Create a new record.
            message_params = row.mail_archive_message_params
            headers = message_params['header']
            raw_recipient = headers['To']
            recipient = ', '.join(EMAIL_RECIPIENT_DELIMITER.split(raw_recipient.strip()))
            context = _migrate_context(message_params.get('body'))
            moddate = _localize_moddate(row.mail_archive_moddate)
            if status == MessageStatus.SENT:
                send_datetime: Optional[datetime] = moddate
                delivery_datetime = None
            elif status == MessageStatus.UNDELIVERED:
                # We don't know the sent datetime, use create instead.
                send_datetime = UTC.localize(row.mail_archive_crdate)
                delivery_datetime = moddate
            else:
                assert status == MessageStatus.PENDING  # noqa: S101
                send_datetime = None
                delivery_datetime = None
            extra_headers = _migrate_extra_headers(headers, row)
            references = _get_email_references(row.mail_archive_id, row.mail_type_name, raw_recipient,
                                               context, connection)

            email = model_cls(
                id=get_next_id(session, Email),
                uuid=uuid,
                message_id=headers.get('Message-ID', null()),
                create_datetime=UTC.localize(row.mail_archive_crdate),
                update_datetime=moddate,
                send_datetime=send_datetime,
                status=status,
                type=row.mail_type_name,
                attempts=row.mail_archive_attempt,
                sender=headers.get('From', row.mail_header_default_h_from),
                recipient=recipient,
                extra_headers=extra_headers,
                subject_template=row.mail_template_messenger_migration_subject,
                subject_template_uuid=row.mail_template_messenger_migration_subject_uuid,
                body_template=row.mail_template_messenger_migration_body,
                body_template_uuid=row.mail_template_messenger_migration_body_uuid,
                context=context,
                delivery_datetime=delivery_datetime,
                delivery_info=row.mail_archive_response_header,
            )
            if attachments:
                email.attachments = attachments
            reference_cls = get_type(model_cls.references)
            for ref_type, values in references.items():
                for value in values:
                    email.references.append(
                        reference_cls(id=get_next_id(session, EmailReference), type=ref_type, value=value))
            session.add(email)
            return True
        else:
            if status == MessageStatus.UNDELIVERED and email.status == MessageStatus.SENT:
                # Email was updated to undelivered.
                email.status = status
                email.update_datetime = UTC.localize(row.mail_archive_moddate)
                email.delivery_datetime = UTC.localize(row.mail_archive_moddate)
                email.delivery_info = row.mail_archive_response_header
                return True
    return False


def migrate_emails(connection: Connection, from_datetime: Optional[datetime] = None,
                   to_datetime: Optional[datetime] = None, *, all_messages: bool = False) -> None:
    """Migrate emails."""
    if all_messages:
        statuses = list(FredEmailStatus)
    else:
        statuses = [s for s in FredEmailStatus if s.status in tuple(ArchiveMessageStatus)]
    joins = MAIL_ARCHIVE.join(MAIL_TEMPLATE).join(MAIL_TYPE).join(MAIL_HEADER)
    joins = joins.join(MAIL_TEMPLATE_MIGRATION,
                       and_(MAIL_TEMPLATE_MIGRATION.columns.version == MAIL_TEMPLATE.columns.version,
                            MAIL_TEMPLATE_MIGRATION.columns.mail_type_id == MAIL_TEMPLATE.columns.mail_type_id))
    query = select(MAIL_ARCHIVE, MAIL_TEMPLATE, MAIL_TEMPLATE_MIGRATION, MAIL_HEADER, MAIL_TYPE)
    query = query.set_label_style(LABEL_STYLE_TABLENAME_PLUS_COL)
    query = query.select_from(joins)
    query = query.where(MAIL_ARCHIVE.columns.status.in_(statuses))
    query = _apply_limits(query, MAIL_ARCHIVE, from_datetime, to_datetime)
    migrated = 0
    skipped = 0
    errors = 0
    for row in connection.execute(query):
        try:
            if APPLICATION.settings.monitoring_email:
                recipient = row.mail_archive_message_params['header']['To']
                if recipient == APPLICATION.settings.monitoring_email:
                    # Silently skip monitoring emails
                    continue

            result = _migrate_email(row, connection)
        except Exception as error:
            sys.stderr.write('Error in migration of {}: {!r}\n'.format(row.mail_archive_uuid, error))
            errors += 1
            continue
        if result:
            migrated += 1
        else:
            skipped += 1
    echo("Emails: {} migrated, {} skipped, {} errors".format(migrated, skipped, errors), Verbosity.INFO,
         log=_LOGGER.info)


def _get_message_references(message_id: int, connection: Connection) -> Dict[ReferenceType, Set[str]]:
    """Return all known references to a SMS message or a letter."""
    references = {}  # type: Dict[ReferenceType, Set[str]]
    select_object = select(ENUM_OBJECT_TYPE.columns.name, OBJECT_REGISTRY.columns.uuid)
    joins = MESSAGE_CONTACT_HISTORY_MAP.join(OBJECT_REGISTRY).join(ENUM_OBJECT_TYPE)
    query = select_object.select_from(joins).where(MESSAGE_CONTACT_HISTORY_MAP.columns.message_archive_id == message_id)
    _append_references(query, connection, references)
    return references


def _migrate_sms(row: Row, connection: Connection) -> bool:
    """Migrate single SMS message, return whether message was actually migrated."""
    message_type = row.message_type_type
    uuid = row.message_archive_uuid
    status = FredMessageStatus(row.enum_send_status_status_name).status  # type: ignore[call-arg]
    if status == MessageStatus.PENDING:
        model_cls: Union[Type[Sms], Type[ArchiveSms]] = Sms
    else:
        model_cls = ArchiveSms

    with atomic() as session:
        try:
            sms = cast(Union[Sms, ArchiveSms], session.query(model_cls).with_for_update().filter_by(uuid=uuid).one())
        except NoResultFound:
            # Create a new record.
            moddate = _localize_moddate(row.message_archive_moddate)
            if status == MessageStatus.SENT:
                send_datetime: Optional[datetime] = moddate
                delivery_datetime = None
            elif status == MessageStatus.UNDELIVERED:
                # We don't know the sent datetime, use create instead.
                send_datetime = UTC.localize(row.message_archive_crdate)
                delivery_datetime = moddate
            else:
                assert status in (MessageStatus.PENDING, MessageStatus.FAILED, MessageStatus.CANCELED)  # noqa: S101
                send_datetime = None
                delivery_datetime = None
            # Detect template, its version and context.
            content = row.sms_archive_content
            content_version = SMS_TEMPLATE_REGEX_MAP[message_type].search(content) is not None
            body, body_uuid = SMS_TEMPLATE_MAP[(message_type, content_version)]
            # Ignore instead of a cast, because re.Match is available since python 3.7
            context = SMS_MESSAGE_REGEX.search(content).groupdict()  # type: ignore[union-attr]
            references = _get_message_references(row.message_archive_id, connection)

            # Save the SMS
            sms = model_cls(
                id=get_next_id(session, Sms),
                uuid=uuid,
                create_datetime=UTC.localize(row.message_archive_crdate),
                update_datetime=moddate,
                send_datetime=send_datetime,
                status=status,
                type=row.message_type_type,
                attempts=row.message_archive_attempt,
                recipient=row.sms_archive_phone_number,
                body_template=body,
                body_template_uuid=body_uuid,
                context=context,
                delivery_datetime=delivery_datetime,
            )
            reference_cls = get_type(model_cls.references)
            for ref_type, values in references.items():
                for value in values:
                    sms.references.append(
                        reference_cls(id=get_next_id(session, SmsReference), type=ref_type, value=value))
            session.add(sms)
            return True
        else:
            if status == MessageStatus.UNDELIVERED and sms.status == MessageStatus.SENT:
                # Sms was updated to undelivered.
                sms.status = status
                sms.update_datetime = UTC.localize(row.message_archive_moddate)
                sms.delivery_datetime = UTC.localize(row.message_archive_moddate)
                return True
    return False


def migrate_sms(connection: Connection, from_datetime: Optional[datetime] = None,
                to_datetime: Optional[datetime] = None, *, all_messages: bool = False) -> None:
    """Migrate SMS messages."""
    if all_messages:
        statuses = list(FredMessageStatus)
    else:
        statuses = [s for s in FredMessageStatus if s.status in tuple(ArchiveMessageStatus)]
    query = select(SMS_ARCHIVE, MESSAGE_ARCHIVE, MESSAGE_TYPE, ENUM_SEND_STATUS)
    query = query.set_label_style(LABEL_STYLE_TABLENAME_PLUS_COL)
    query = query.select_from(SMS_ARCHIVE.join(MESSAGE_ARCHIVE).join(MESSAGE_TYPE).join(ENUM_SEND_STATUS))
    query = query.where(ENUM_SEND_STATUS.columns.status_name.in_(statuses))
    query = query.where(MESSAGE_TYPE.columns.type != MESSAGE_TYPE_MONITORING)
    query = _apply_limits(query, MESSAGE_ARCHIVE, from_datetime, to_datetime)
    migrated = 0
    skipped = 0
    errors = 0
    for row in connection.execute(query):
        try:
            result = _migrate_sms(row, connection)
        except Exception as error:
            sys.stderr.write('Error in migration of {}: {!r}\n'.format(row.message_archive_uuid, error))
            errors += 1
            continue
        if result:
            migrated += 1
        else:
            skipped += 1
    echo("SMS: {} migrated, {} skipped, {} errors".format(migrated, skipped, errors), Verbosity.INFO, log=_LOGGER.info)


def _handle_canceled_letter(session: Session, uuid: UUID, status: MessageStatus, moddate: Optional[datetime],
                            ) -> Optional[bool]:
    """Handle special case of canceled letters.

    Returns: Whether letter was migrated (True), skipped (False) or untouched (None).
    """
    if status != MessageStatus.CANCELED:
        # Letter isn't canceled, continue as usual.
        return None

    try:
        # Check if the letter isn't already in the queue.
        letter = session.query(Letter).with_for_update().filter_by(uuid=uuid).one()
    except NoResultFound:
        # Letter not in queue, continue as usual.
        return None
    if letter.status == MessageStatus.PENDING:
        # Letter was canceled, sync its status.
        letter.status = MessageStatus.CANCELED
        letter.update_datetime = moddate
        return True
    else:
        # Letter was already taken care of (either canceled or sent). Don't touch it!
        return False


def _migrate_letter(row: Row, connection: Connection) -> bool:
    """Migrate single letter, return whether letter was actually migrated."""
    uuid = row.message_archive_uuid
    status = FredMessageStatus(row.enum_send_status_status_name).status  # type: ignore[call-arg]
    if status == MessageStatus.PENDING:
        model_cls: Union[Type[Letter], Type[ArchiveLetter]] = Letter
    else:
        model_cls = ArchiveLetter

    with atomic() as session:
        try:
            letter = cast(Union[Letter, ArchiveLetter],
                          session.query(model_cls).with_for_update().filter_by(uuid=uuid).one())
        except NoResultFound:
            moddate = _localize_moddate(row.message_archive_moddate)
            result = _handle_canceled_letter(session, uuid, status, moddate)
            # If the canceled case was handled, just return the result. Otherwise continue as usual.
            if result is not None:
                return result
            # Create a new record.
            if status == MessageStatus.SENT:
                send_datetime: Optional[datetime] = moddate
                delivery_datetime = None
            elif status == MessageStatus.UNDELIVERED:
                # We don't know the sent datetime, use create instead.
                send_datetime = UTC.localize(row.message_archive_crdate)
                delivery_datetime = moddate
            else:
                assert status in (MessageStatus.PENDING, MessageStatus.FAILED, MessageStatus.CANCELED)  # noqa: S101
                send_datetime = None
                delivery_datetime = None
            street_chunks = (row.letter_archive_postal_address_street1,
                             row.letter_archive_postal_address_street2,
                             row.letter_archive_postal_address_street3)
            street = '\n'.join(i for i in street_chunks if i)
            references = _get_message_references(row.message_archive_id, connection)

            # Save the message
            letter = model_cls(
                id=get_next_id(session, Letter),
                uuid=uuid,
                create_datetime=UTC.localize(row.message_archive_crdate),
                update_datetime=moddate,
                send_datetime=send_datetime,
                status=status,
                type=row.message_type_type,
                attempts=row.message_archive_attempt,
                recipient_name=row.letter_archive_postal_address_name,
                recipient_organization=row.letter_archive_postal_address_organization,
                recipient_street=street,
                recipient_city=row.letter_archive_postal_address_city,
                recipient_state=row.letter_archive_postal_address_stateorprovince,
                recipient_postal_code=row.letter_archive_postal_address_postalcode,
                recipient_country=row.letter_archive_postal_address_country,
                file=row.files_uuid,
                service=row.message_archive_service_handle,
                batch_id=row.letter_archive_batch_id,
                delivery_datetime=delivery_datetime,
            )
            reference_cls = get_type(model_cls.references)
            for ref_type, values in references.items():
                for value in values:
                    letter.references.append(
                        reference_cls(id=get_next_id(session, LetterReference), type=ref_type, value=value))
            session.add(letter)
            return True
        else:
            if status == MessageStatus.UNDELIVERED and letter.status == MessageStatus.SENT:
                # Letter was updated to undelivered.
                letter.status = status
                letter.update_datetime = UTC.localize(row.message_archive_moddate)
                letter.delivery_datetime = UTC.localize(row.message_archive_moddate)
                return True
    return False


def migrate_letters(connection: Connection, from_datetime: Optional[datetime] = None,
                    to_datetime: Optional[datetime] = None, *, all_messages: bool = False) -> None:
    """Migrate letters."""
    if all_messages:
        statuses = list(FredMessageStatus)
    else:
        statuses = [s for s in FredMessageStatus if s.status in tuple(ArchiveMessageStatus)]
    query = select(LETTER_ARCHIVE, MESSAGE_ARCHIVE, MESSAGE_TYPE, ENUM_SEND_STATUS, FILES)
    query = query.set_label_style(LABEL_STYLE_TABLENAME_PLUS_COL)
    query = query.select_from(
        LETTER_ARCHIVE.join(MESSAGE_ARCHIVE).join(MESSAGE_TYPE).join(ENUM_SEND_STATUS).join(FILES))
    query = query.where(ENUM_SEND_STATUS.columns.status_name.in_(statuses))
    query = _apply_limits(query, MESSAGE_ARCHIVE, from_datetime, to_datetime)
    migrated = 0
    skipped = 0
    errors = 0
    for row in connection.execute(query):
        try:
            result = _migrate_letter(row, connection)
        except Exception as error:
            sys.stderr.write('Error in migration of {}: {!r}\n'.format(row.message_archive_uuid, error))
            errors += 1
            continue
        if result:
            migrated += 1
        else:
            skipped += 1
    echo("Letters: {} migrated, {} skipped, {} errors".format(migrated, skipped, errors), Verbosity.INFO,
         log=_LOGGER.info)


@messenger_command()
@limits_options
@click.option('--all-messages/--archive-only', default=False,
              help="Migrate all messages or only archived ones (default).")
@method_option
@click.option('--fred-connection', help="Set custom database connection string for FRED database.")
def main(from_datetime: Optional[datetime], to_datetime: Optional[datetime], all_messages: bool,
         methods: SequenceType[TransportMethod], fred_connection: str) -> None:
    """Migrate archive messages."""
    if fred_connection:
        APPLICATION.settings.fred_db_connection = fred_connection
    if not APPLICATION.settings.fred_db_connection:
        raise click.UsageError('Fred DB connection not defined.')

    engine = get_fred_engine()
    connection = engine.connect()

    if TransportMethod.EMAIL in methods:
        # Run the migration in single transaction.
        with connection.begin():
            migrate_emails(connection, from_datetime, to_datetime, all_messages=all_messages)
    if TransportMethod.SMS in methods:
        # Run the migration in single transaction.
        with connection.begin():
            migrate_sms(connection, from_datetime, to_datetime, all_messages=all_messages)
    if TransportMethod.LETTER in methods:
        # Run the migration in single transaction.
        with connection.begin():
            migrate_letters(connection, from_datetime, to_datetime, all_messages=all_messages)


if __name__ == '__main__':
    main()
