#!/usr/bin/python3
"""Manage blocked addresses."""
import json
import logging
from enum import unique
from typing import Dict, Iterable, Optional

import click
from backports.strenum import StrEnum
from setapp.constants import Verbosity
from sqlalchemy.orm import class_mapper
from sqlalchemy.sql import func
from sqlalchemy.sql.expression import ColumnElement

from messenger.commands.utils import echo, messenger_command
from messenger.database import atomic
from messenger.models import BlockedAddress, BlockedEmail, BlockedPhone
from messenger.models.base import BaseModel
from messenger.utils import normalize_email_address, normalize_phone_number

_LOGGER = logging.getLogger(__name__)
_ADDRESS_ATTRIBUTES = tuple(k for k in class_mapper(BlockedAddress).columns.keys() if k != 'id')


@unique
class BlockedModel(StrEnum):
    """Enum with blocked model names and classes."""

    def __new__(cls, value: str, model: BaseModel) -> 'BlockedModel':
        """Construct item with message."""
        obj = str.__new__(cls, value)
        obj._value_ = value

        obj.model = model
        return obj

    model: BaseModel

    EMAIL = ('email', BlockedEmail)
    PHONE = ('phone', BlockedPhone)
    ADDRESS = ('address', BlockedAddress)


@click.group()
def main() -> None:
    """Manage blocked addresses."""


def _get_address_filter(data: Dict[str, Optional[str]]) -> Iterable[ColumnElement]:
    """Turn address data to filter expression."""
    for attr in _ADDRESS_ATTRIBUTES:
        value = data.get(attr)
        if value is None:
            yield func.lower(getattr(BlockedAddress, attr)) == value
        else:
            yield func.lower(getattr(BlockedAddress, attr)) == value.lower()


@messenger_command(main.command)
@click.argument('model', type=click.Choice(tuple(BlockedModel)))
@click.argument('value')
def add(model: BlockedModel, value: str) -> None:
    """Add a blocked address.

    To add address use address as JSON encoded VALUE.
    """
    if model == BlockedModel.EMAIL:
        email = normalize_email_address(value)
        if email is None:
            raise click.BadParameter("'{}' is not a valid email address.".format(value), param_hint='value')
        obj: BaseModel = BlockedEmail(email=email)
        filter_args: Iterable[ColumnElement] = [func.lower(BlockedEmail.email) == email]
    elif model == BlockedModel.PHONE:
        phone = normalize_phone_number(value)
        if phone is None:
            raise click.BadParameter("'{}' is not a valid phone number.".format(value), param_hint='value')
        obj = BlockedPhone(phone=phone)
        filter_args = [func.lower(BlockedPhone.phone) == phone]
    else:
        assert model == BlockedModel.ADDRESS  # noqa: S101
        try:
            data = json.loads(value)
        except json.JSONDecodeError as error:
            raise click.BadParameter("'{}' is not a valid address.".format(value), param_hint='value') from error
        if not data or set(data.keys()).difference(set(_ADDRESS_ATTRIBUTES)):
            raise click.BadParameter("'{}' is not a valid address.".format(value), param_hint='value')
        obj = BlockedAddress(**data)
        filter_args = _get_address_filter(data)

    with atomic() as session:
        query = session.query(model.model).filter(*filter_args)  # type: ignore[arg-type]
        if query.one_or_none():
            echo('{} {} is already blocked.'.format(model.capitalize(), value), Verbosity.INFO)
        else:
            session.add(obj)
            echo('{} {} was blocked.'.format(model.capitalize(), obj), Verbosity.INFO, log=_LOGGER.info)


@messenger_command(main.command)
@click.argument('model', type=click.Choice(tuple(BlockedModel)))
@click.argument('value')
def remove(model: BlockedModel, value: str) -> None:
    """Remove a blocked address.

    To remove address use address as JSON encoded VALUE.
    """
    if model == BlockedModel.EMAIL:
        email = normalize_email_address(value)
        if email is None:
            raise click.BadParameter("'{}' is not a valid email address.".format(value), param_hint='value')
        filter_args: Iterable[ColumnElement] = [func.lower(BlockedEmail.email) == email]
    elif model == BlockedModel.PHONE:
        phone = normalize_phone_number(value)
        if phone is None:
            raise click.BadParameter("'{}' is not a valid phone number.".format(value), param_hint='value')
        filter_args = [func.lower(BlockedPhone.phone) == phone]
    else:
        assert model == BlockedModel.ADDRESS  # noqa: S101
        try:
            data = json.loads(value)
        except json.JSONDecodeError as error:
            raise click.BadParameter("'{}' is not a valid address.".format(value), param_hint='value') from error
        if not data or any(not hasattr(BlockedAddress, k) for k in data):
            raise click.BadParameter("'{}' is not a valid address.".format(value), param_hint='value')
        filter_args = _get_address_filter(data)

    with atomic() as session:
        query = session.query(model.model).filter(*filter_args)  # type: ignore[arg-type]
        obj = query.one_or_none()
        if obj is None:
            echo('{} {} is not blocked.'.format(model.capitalize(), value), Verbosity.INFO)
        else:
            session.delete(obj)
            echo('{} {} block was removed.'.format(model.capitalize(), obj), Verbosity.INFO, log=_LOGGER.info)


@messenger_command(main.command)
@click.argument('model', type=click.Choice(tuple(BlockedModel)))
def list(model: BlockedModel) -> None:
    """List all blocked addresses."""
    with atomic() as session:
        query = session.query(model.model)  # type: ignore[arg-type]
        for obj in query.all():
            print(obj)


if __name__ == '__main__':
    main()
