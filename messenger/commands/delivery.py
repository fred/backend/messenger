#!/usr/bin/python3
"""Check message delivery status."""
import logging
from typing import Iterable, Tuple, cast

import click
from setapp.constants import Verbosity

from messenger.commands.utils import echo, messenger_command
from messenger.settings import APPLICATION
from messenger.utils import load_instance

_LOGGER = logging.getLogger(__name__)


@messenger_command()
@click.option('--limit', type=int, help="Set limit for delivery checker.")
@click.option('--delivery-backend', help="Set custom backend.")
@click.option('--delivery-param', multiple=True,
              help="Set custom parameters for the backend, can be used multiple times.")
def main(limit: int, delivery_backend: str, delivery_param: Iterable[str]) -> None:
    """Check message delivery status."""
    if delivery_backend:
        APPLICATION.settings.delivery_backend = delivery_backend
    if delivery_param:
        options = dict(cast(Tuple[str, str], p.split('=', 1)) for p in delivery_param)
        APPLICATION.settings.delivery_options = options
    try:
        backend = load_instance(APPLICATION.settings.delivery_backend, **APPLICATION.settings.delivery_options)
    except Exception as error:
        raise click.BadParameter("Error in loading delivery backend: {}.".format(error)) from error
    echo('Checking email delivery (may take long time)...', Verbosity.INFO)
    result = backend.check_delivery(limit=limit)
    msg = 'Processed {total} messages: {result.successes} OK, {result.errors} errors, {result.skips} skipped.'
    echo(msg.format(result=result, total=result.total()), Verbosity.INFO, log=_LOGGER.info)


if __name__ == '__main__':
    main()
