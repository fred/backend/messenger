#!/usr/bin/python3
"""Print messenger statistics over defined period."""
import json
from datetime import datetime, timedelta
from enum import unique
from typing import Any, Callable, Dict, Sequence, cast

import click
import yaml
from backports.strenum import StrEnum
from pytz import UTC
from sqlalchemy import Column

from messenger.commands.utils import messenger_command, method_option
from messenger.constants import MessageStatus, TransportMethod
from messenger.database import atomic

TEXT_OUTPUT = ('{method.value}s per last {period} seconds:\n'
               'Created: {created}\n'
               'Sent: {sent}\n'
               'Failed: {failed}\n'
               'Errors: {errors}\n'
               'Pending total: {pending}')


def _print_json(stats: Dict[str, Any]) -> None:
    click.echo(json.dumps(stats), nl=False)


def _print_text(stats: Dict[str, Any]) -> None:
    for method in TransportMethod:
        if method in stats:
            click.echo(TEXT_OUTPUT.format(method=method, period=stats['period'], **stats[method]))


def _print_yaml(stats: Dict[str, Any]) -> None:
    click.echo(yaml.dump(stats), nl=False)


@unique
class Format(StrEnum):
    """Enum with available output formats."""

    def __new__(cls, value: str, formatter: Callable[[Dict[str, Any]], None]) -> 'Format':
        """Construct item with function."""
        obj = str.__new__(cls, value)
        obj._value_ = value

        obj.formatter = formatter
        return obj

    formatter: Callable[[Dict[str, Any]], None]

    JSON = ('json', _print_json)
    TEXT = ('text', _print_text)
    YAML = ('yaml', _print_yaml)


@messenger_command()
@method_option
@click.option('--period', type=int, default=3600, show_default=True,
              help="Set length of a monitored period in seconds.")
@click.option('--format', type=click.Choice(tuple(Format)), default=Format.TEXT.value, show_default=True,
              help="Set output format.")
def main(methods: Sequence[TransportMethod], period: int, format: Format) -> None:
    """Print messenger statistics over defined period."""
    cutoff = datetime.now(UTC) - timedelta(seconds=period)
    stats: Dict[str, Any] = {'period': period}
    for method in methods:
        with atomic() as session:
            # mypy gets confused by lru_cache, see https://github.com/python/mypy/issues/5858
            model_cls = method.model_cls
            query = session.query(model_cls)
            stats[method.value] = {
                'created': query.filter(cast(Column, model_cls.create_datetime) >= cutoff).count(),
                'pending': query.filter_by(status=MessageStatus.PENDING).count(),
                'sent': query.filter(cast(Column, model_cls.send_datetime) >= cutoff).filter_by(
                    status=MessageStatus.SENT).count(),
                'failed': query.filter(cast(Column, model_cls.update_datetime) >= cutoff).filter_by(
                    status=MessageStatus.FAILED).count(),
                'errors': query.filter(cast(Column, model_cls.update_datetime) >= cutoff).filter_by(
                    status=MessageStatus.ERROR).count(),
            }
    format.formatter(stats)


if __name__ == '__main__':
    main()
